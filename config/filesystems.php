<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),
            'use_path_style_endpoint' => env('AWS_USE_PATH_STYLE_ENDPOINT', false),
        ],
        // 'upcloud' => [
        //     'driver' => 's3',
        //     'key' => 'UCOBFHXU31IAQ7AETO69',
        //     'secret' => '2qjXYNKKdjH3A0ZrX79pMAc0r1jfsaUkC5HTvKus',
        //     'endpoint' => 'https://we-are-the-blues-internal.sg-sin1.upcloudobjects.com',
        //     'region' => 'sg-sin1',
        //     'bucket' => 'the-color-is-blue',
        //     'correctClockSkew' => true
        // ],
//diubah disini setelah migrate ke object storage 2.0
        'upcloud' => [
            'driver' => 's3',
            'key' => 'AKIA635C6BECE1CFBAB2',
            'secret' => 'qitG60sMGWxWhZfu4Wwx/bLB6PpEYS00d8EFSXYr',
            'endpoint' => 'https://35qee.upcloudobjects.com',
            'region' => 'APAC-1',
            'bucket' => 'investorich',
            'correctClockSkew' => true
        ],

        'upcloudinvestorich' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
            // 'driver' => 's3',
            // 'key' => 'UCOBFHXU31IAQ7AETO69',
            // 'secret' => '2qjXYNKKdjH3A0ZrX79pMAc0r1jfsaUkC5HTvKus',
            // 'endpoint' => 'https://we-are-the-blues-internal.sg-sin1.upcloudobjects.com',
            // 'region' => 'sg-sin1',
            // 'bucket' => 'investorich',
            // 'correctClockSkew' => true
        ],

        // 'upcloudinvestorich' => [
        //   'driver' => 's3',
        //   'key' => 'UCOBG6MNXC0UKKUXFDMU',
        //   'secret' => 'zCkHtOfUP1DvO4tH+b6HSFwxWw1n8ONaY7Vl32w6',
        //   'endpoint' => 'https://static-storage-1928-internal.sg-sin1.upcloudobjects.com',
        //   'region' => 'sg-sin1',
        //   'bucket' => 'warisan',
        //   'correctClockSkew' => true
        // ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Symbolic Links
    |--------------------------------------------------------------------------
    |
    | Here you may configure the symbolic links that will be created when the
    | `storage:link` Artisan command is executed. The array keys should be
    | the locations of the links and the values should be their targets.
    |
    */

    'links' => [
        public_path('storage') => storage_path('app/public'),
    ],

];
