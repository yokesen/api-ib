<?php

use App\Http\Controllers\mt5createaccountController;
use App\Http\Controllers\mt5creategroupController; 
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LabController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\mt5UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/lab/order', [LabController::class,'order']);
// Route::get('/lab/deal', [LabController::class,'deal']);
// Route::get('/lab/position', [LabController::class,'position']);
// Route::get('/lab/time-correction', [LabController::class,'timeCorrection']);
// Route::get('/lab/check/{login}', [LabController::class,'checkPosition']);
// Route::get('/lab/check/login/{login}', [LabController::class,'checkAcc']);
// Route::get('/lab/report', [LabController::class,'dailyReport']);
// Route::get('/lab/test-report/{id}', [LabController::class,'testReport']);
// Route::get('/lab/group', [LabController::class,'testgroup']);
// Route::get('/lab/data-api/{uuid}', [LabController::class,'challengeDariFinley']);
// Route::get('/lab/check-api', [LabController::class,'checkapi']);
// Route::get('/lab/login', [mt5UserController::class,'login']);

Route::get('/lab/asyncTest', [LabController::class,'asyncTest']);


// Route::get('/lab/create-group-competition/{g}', [mt5creategroupController::class,'createGroupCompetition']);
// Route::get('/lab/suntik-balance/{accountId}', [mt5createaccountController::class,'suntikManual']);

// Route::get('/lab/pindahgrup', [LabController::class,'pindahgroupMT5']);
// Route::get('/lab/pindahgrup/ffx', [LabController::class,'pindahgroupMT5ffx']);


  Route::any('optimize-cache', function() {
        \Illuminate\Support\Facades\Artisan::call('optimize');
    });

// Route::get('mail-send', [MailController::class, 'index']);
