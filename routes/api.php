<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsernameController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UploadMedia;
use App\Http\Controllers\BankController;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\WithdrawalController;
use App\Http\Controllers\ReferalController;
use App\Http\Controllers\SignalController;
use App\Http\Controllers\mt5UserController;
use App\Http\Controllers\Calendarcontroller;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\mt5createaccountController;
use App\Http\Controllers\TickerController;
use App\Http\Controllers\mt5creategroupController; 
use App\Http\Controllers\DepoWdController;
use App\Http\Controllers\getEquityController;
use App\Http\Controllers\LeadController;
use App\Http\Controllers\BalanceStatementController;
use App\Http\Controllers\MarginController;
use App\Http\Controllers\ReconsileController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\MarketingController;
use App\Http\Controllers\TradingStatementController;
use App\Http\Controllers\StatisticController;

use App\Http\Controllers\TwilioController;
use App\Http\Controllers\LabController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->group(function () {
  Route::post('login-with-google', [UserController::class, 'authenticateWithGoogle']);
  Route::post('secure-pptp-connection', [UserController::class, 'authenticateWithUUID']);
  Route::get('/lab/acc/{id}', [mt5createaccountController::class, 'checkAcc']);
  Route::post('login', [UserController::class, 'authenticate']);
  Route::post('register', [UserController::class, 'register']);
  Route::post('create-ib', [UserController::class, 'createib']);
  Route::get('calendar', [Calendarcontroller::class, 'calendar']);
  Route::get('calendar/{symbol}', [Calendarcontroller::class, 'calendarPerSymbol']);
  Route::get('bearish-bullish/{symbol}', [Calendarcontroller::class, 'bearishBullish']);
  Route::group(['middleware' => ['jwt.login']], function () {
    Route::get('logout', [UserController::class, 'logout']);
    Route::get('get_user', [UserController::class, 'get_user']);
    Route::post('uploadprofileimage', [UserController::class, 'UploadImage']);
    Route::get('my-account', [UserController::class, 'my_account']);
    Route::post('account-detail', [AccountController::class, 'data_account']);
    Route::get('account-type-list', [UserController::class, 'account_type_list']);
    Route::post('create-account', [UserController::class, 'create_account']);
    Route::post('update-active-account', [UserController::class, 'updateActiveAccount']);
    Route::post('update-password', [UserController::class, 'update_password']);
    Route::post('update-profile', [UserController::class, 'update_profile']);
    Route::post('update-profile-oboarding', [UserController::class, 'update_profile_onboarding']);
    Route::get('check-username-changed', [UsernameController::class, 'check_username_changed']);
    Route::post('search-username', [UsernameController::class, 'search_username']);
    Route::post('change-username', [UsernameController::class, 'change_username']);
    Route::post('uploadKTP', [UploadMedia::class, 'uploadKTP']);
    Route::post('uploadBukuTabungan', [UploadMedia::class, 'uploadBukuTabungan']);
    Route::post('uploadAgreement', [UploadMedia::class, 'uploadAgreement']);
    Route::get('custody-bank-list', [BankController::class, 'custody_bank_list']);
    Route::get('get-bank-list', [BankController::class, 'getBankList']);
    Route::get('mt4-detail/{accunt?}', [BankController::class, 'mt4_detail']);
    Route::get('my-deposit', [BankController::class, 'my_deposit']);
    Route::get('my-bank', [BankController::class, 'my_bank']);
    Route::post('update-bank', [BankController::class, 'update_bank']);
    Route::post('request-deposit', [BankController::class, 'request_deposit']);
    Route::get('my-internal-transfer', [WithdrawalController::class, 'my_internal_transfer']);
    Route::get('my-withdrawal', [WithdrawalController::class, 'my_withdrawal']);
    Route::post('request-internal-transfer', [WithdrawalController::class, 'request_internal_transfer']);
    Route::post('request-withdrawal', [WithdrawalController::class, 'request_withdrawal']);
    Route::get('detail-deposit/{id?}', [WithdrawalController::class, 'detail_deposit']);
    Route::get('detail-internal-transfer/{id?}', [WithdrawalController::class, 'detail_internal_transfer']);
    Route::get('detail-withdrawal/{id?}', [WithdrawalController::class, 'detail_withdrawal']);
    Route::get('video-list', [VideoController::class, 'video_list']);
    Route::get('video-detail/{video_id?}', [VideoController::class, 'video_detail']);
    Route::get('question/{video_id?}', [VideoController::class, 'question']);
    Route::post('submit-quiz/{video_id?}', [VideoController::class, 'submit_quiz']);
    Route::post('register-referral-program', [ReferalController::class, 'register_referral_program']);
    Route::get('my-client-list', [ReferalController::class, 'my_client_list']);
    Route::get('this-month-volume', [ReferalController::class, 'this_month_volume']);
    Route::get('signal-release/{signalname_id?}', [SignalController::class, 'signal_release']);
    Route::get('signal-list/{category_id?}', [SignalController::class, 'signal_list']);
    Route::get('signal-category', [SignalController::class, 'signal_category']);
    Route::get('signal-roadmap', [SignalController::class, 'petaHarga']);
    Route::get('academy-score', [VideoController::class, 'academyScore']);

    Route::get('get-notification', [NotificationController::class, 'getNotif']);
    Route::post('read-notification', [NotificationController::class, 'readNotif']);
    Route::get('sidebar-notification/{uuid}', [NotificationController::class, 'sidebarNotif']);

    /*MT5API*/
    Route::post('check-mt5', [mt5UserController::class, 'dataAccount']);
    Route::post('instant-order', [mt5UserController::class, 'openOrder']);
    Route::post('pending-order', [mt5UserController::class, 'openPendingOrder']);
    Route::post('update-order', [mt5UserController::class, 'updateOrder']);
    Route::post('cancel-order', [mt5UserController::class, 'cancelOrder']);
    Route::post('data-trading', [mt5UserController::class, 'dataTrading']);

    Route::post('self-insight', [mt5UserController::class, 'selfinsight']);
    Route::post('list-fav-pair', [mt5UserController::class, 'favPair']);
    Route::get('insight-account-data/{uuid}', [mt5UserController::class, 'dataEquityDepositReturn']);
    Route::get('insight-account-data-mt4id/{login}', [mt5UserController::class, 'dataEquityDepositReturnMtAccount']);
    Route::get('return-profit-grafik/{login}', [mt5UserController::class, 'returnProfitGrafik']);

    Route::post('check-order', [mt5UserController::class, 'getTrades']);
    Route::post('check-daily', [mt5UserController::class, 'checkDaily']);
    Route::post('login-account-trading', [mt5UserController::class, 'tradeLoginAccount']);

    Route::get('/setting/setting-limit-master-ib', [SettingController::class, 'limitMasterIb']);  
    Route::get('/setting/setting-limit-ib', [SettingController::class, 'limitIb']);
    //Route::post('bypass/1129-1891/data/account', [mt5UserController::class, 'bypassAccountData']);

    Route::get('/group/account-type', [AccountController::class, 'accountType']);
    Route::get('/my/account-type', [AccountController::class, 'myAccountType']);
    Route::post('/my/edit-comm', [AccountController::class, 'postCommissionIb']);

    Route::post('/register-master-ib', [UserController::class, 'create_master_ib']);
    Route::post('/register-ib', [UserController::class, 'create_ib']);

    Route::get('/list/master-ib', [MarketingController::class, 'master_ib']);
    Route::get('/list/ib', [MarketingController::class, 'ib']);
    Route::get('/list/senior-see-ib', [MarketingController::class, 'seniorIbSeeIb']);
    Route::get('/list/master-see-client', [MarketingController::class, 'masterSeeClient']);

    /**
     **New Route By Andika
     */

    /** Leads*/
    Route::get('/leads', [LeadController::class, 'viewLead'])->name('viewLead');
    Route::post('/leads', [LeadController::class, 'processAddLead'])->name('processAddLead');
    Route::get('/lead/{uuid}', [LeadController::class, 'getLeadDetail'])->name('getLeadDetail');
    Route::get('/account/{mt5}/{uuid}', [LeadController::class, 'mt5detail'])->name('mt5detail');
    Route::post('/search/lead', [LeadController::class, 'searchLeads'])->name('searchLeads');

    /** Users*/
    Route::post('/user/update-level/{id}', [UserController::class, 'editLevel'])->name('updateUserLevel');
    Route::post('/user/update/{id}', [UserController::class, 'updateUser'])->name('updateUser');
    Route::post('/user/detail', [UserController::class, 'userDetail'])->name('userDetail');

    /** Create metatrader account for client*/
    Route::post('/metatrader-4/create/by-admin', [mt5createaccountController::class, 'createAccountMt4'])->name('createAccountMt4byAdmin');
    Route::post('/metatrader/trading-competition/give-account', [mt5createaccountController::class, 'giveTradingCompetitionAccount'])->name('giveTradingCompetitionAccount');

    /**
     *Deposits
     **/

    /** Create Bank Account*/
    Route::post('/user/update-bank', [UserController::class, 'editBank'])->name('updateUserBank');
    Route::post('/deposit/by-admin', [DepoWdController::class, 'directDeposit'])->name('submitDepositbyAdmin');
    Route::post('/deposit-confirmation', [DepoWdController::class, 'depositConfirmation'])->name('depositConfirmation');
    Route::post('/withdraw/by-admin', [DepoWdController::class, 'directWithdrawal'])->name('submitWithdrawbyAdmin');
    Route::post('/withdraw-confirmation', [DepoWdController::class, 'withdrawConfirmation'])->name('withdrawConfirmation');
    Route::get('/deposits', [DepoWdController::class, 'getDeposit'])->name('getDepositList');
    Route::get('/deposit/{id}', [DepoWdController::class, 'getDepositDetail'])->name('getDepositDetail');
    Route::get('/withdraws', [DepoWdController::class, 'getWithdraws'])->name('getWithdrawList');
    Route::get('/withdraw/{id}', [DepoWdController::class, 'getWithdrawDetail'])->name('getWithdrawDetail');
    Route::get('/detail-pending-receivable/{uuid}', [DepoWdController::class, 'detailPendingReceivable'])->name('detailPendingReceivable');
    Route::get('/detail-pending-payable/{uuid}', [DepoWdController::class, 'detailPendingPayable'])->name('detailPendingPayable');

    /** Balanced statements*/
    Route::get('/balance-statement', [BalanceStatementController::class, 'weekly'])->name('viewBalanceStatement');
    Route::get('/weekly-settlement', [BalanceStatementController::class, 'weeklySettlement'])->name('weeklySettlement');
    Route::get('/weekly-settlement-detail', [BalanceStatementController::class, 'weeklySettlementDetail'])->name('weeklySettlementDetail');
    Route::post('/weekly-settlement-payment-slip', [BalanceStatementController::class, 'weeklySettlementPaymentSlip']);
    Route::post('/weekly-settlement-payment-verification', [BalanceStatementController::class, 'weeklySettlementPaymentVerification']);
    /** Commision*/
    Route::get('/commisions', [BalanceStatementController::class, 'commission'])->name('viewCommisionStatement');
    Route::get('/commision/{id}', [BalanceStatementController::class, 'commissionDetail'])->name('viewCommisionStatementDetail');

    /** Trading Statement*/
    Route::get('/trading-statements', [BalanceStatementController::class, 'tradingStatement'])->name('getTradingStatements');
    Route::get('/trading-statement-per-client', [BalanceStatementController::class, 'showTradingStatementClient'])->name('viewTradingStatementClient');
    Route::get('/trading-statement-per-account', [BalanceStatementController::class, 'showTradingStatementAccount']);
    Route::get('/trading-statement-current', [TradingStatementController::class, 'currentTrade']);

    /** Margin*/
    Route::get('/net-margin', [MarginController::class, 'viewNetMargin'])->name('margin.viewNetMargin');
    Route::get('/margin-in', [MarginController::class, 'viewMarginIn'])->name('margin.viewMarginIn');
    Route::get('/margin-out', [MarginController::class, 'viewMarginOut'])->name('margin.viewMarginOut');
    Route::get('/margin-internal', [MarginController::class, 'viewInternalTransfer'])->name('viewInternalTransfer');

    /** Weekly Reconsile*/
    Route::get('/weekly-reconsile', [MarginController::class, 'weeklyReconsile'])->name('viewWeeklyReconsile');
    Route::get('/weekly-reconsile-detail/{id}', [MarginController::class, 'weeklyReconsileDetail'])->name('viewWeeklyReconsileDetail');

    /** Document uploads*/
    Route::post('upload-ktp', [UserController::class, 'uploadKtp']);
    Route::post('upload-buku-tabungan', [UserController::class, 'uploadBukuTabungan']);

    /** Home data*/
    Route::get('home', [UserController::class, 'homeData']);
    Route::get('amount-account/{uuid}', [DepoWdController::class, 'getAccountAmount']);

    /** Banks*/
    Route::get('mybanks', [BankController::class, 'mybanks']);
    Route::get('mybankswd', [BankController::class, 'mybankswd']);
    Route::get('bank-segre', [BankController::class, 'custody_bank_list']);
    Route::post('banks', [BankController::class, 'storeBank']);
    Route::post('bank/add-segre', [BankController::class, 'storeSegreBank']);
    Route::post('bank/add-wd', [BankController::class, 'storeWdBank']); 
    Route::post('bank/add-segre/children', [BankController::class, 'storeSegreBankChildren']);
    Route::post('bank/delete-segre/children', [BankController::class, 'deleteSegreBankChildren']);
    Route::post('bank/update-segre/children', [BankController::class, 'updateSegreBankChildren']);
    Route::post('bank/change-status', [BankController::class, 'changeBankStatus']);
    Route::post('get-bank', [BankController::class, 'getBank']);
    Route::get('get-bank-children/{uuid}', [BankController::class, 'getBankChildren']);
    Route::get('my-active-banks', [BankController::class, 'myActiveBanks']);

    Route::get('list-warrant-bank', [BankController::class, 'listWarantCapital']);
    Route::get('detail-warrant-bank/{id}', [BankController::class, 'getWarantDetail']);
    Route::get('top-up-warrant-bank', [BankController::class, 'topUpWarrantBank']); 
    Route::post('top-up-capital', [BankController::class, 'topUpCapital']);
    Route::post('top-up-capital-new', [BankController::class, 'topUpCapitalNew']);
    Route::post('warant-confirmation', [BankController::class, 'warrantConfirm']);

    Route::get('referral-link/{uuid}', [UserController::class, 'getReferralLink']);

    Route::group(['prefix' => 'reconsile'], function () {
      Route::get('get-week',[ReconsileController::class,'getWeekReconsile'])->name('getWeekReconsile');
      Route::group(['prefix' => 'sib'], function () {
        Route::get('weekly', [ReconsileController::class, 'sibReconsileWeekly'])->name('sibReconsileWeekly');
        Route::get('weeklyBackdate/{week}', [ReconsileController::class, 'sibReconsileWeeklyBackDate'])->name('sibReconsileWeeklyBackDate');
        Route::get('weekly-detail/{trx}', [ReconsileController::class, 'sibReconsileWeeklyDetail'])->name('sibReconsileWeeklyDetail');
      });
      Route::group(['prefix' => 'mib'], function () {
        Route::get('weekly-detail/{trx}', [ReconsileController::class, 'mibReconsileWeeklyDetail'])->name('mibReconsileWeeklyDetail');
      });
      Route::group(['prefix' => 'ib'], function () {
        Route::get('weekly-detail/{trx}', [ReconsileController::class, 'ibReconsileWeeklyDetail'])->name('ibReconsileWeeklyDetail'); 
        Route::get('weekly-detail-download/{trx}', [ReconsileController::class, 'ibDownloadReconsile'])->name('ibDownloadReconsile');
      });
      Route::group(['prefix' => 'account'], function () {
        Route::get('weekly-detail/{trx}', [ReconsileController::class, 'accountReconsileWeeklyDetail'])->name('accountReconsileWeeklyDetail');
      });
    });

  });
  /** Forgot password token*/
  Route::post('forgot-password-token', [UserController::class, 'createTokenForgotPassword']);
  Route::post('reset-password', [UserController::class, 'resetPassword']);
  Route::get('check-onboarding/{uuid}', [UserController::class, 'checkOnboarding']); 


  Route::post('/go/create-group', [mt5creategroupController::class, 'createGroup']);  

  Route::post('/go/create-akun-x', [mt5createaccountController::class, 'createUser']);
  Route::post('/go/create-akun', [mt5createaccountController::class, 'createMt5']);
  Route::post('/go/edit-akun', [mt5createaccountController::class, 'updateGroup']);

  Route::post('/go/check-account', [mt5createaccountController::class, 'checkAvailableAccount']);

  Route::post('/go/deposit', [DepoWdController::class, 'deposit']);
  Route::post('/go/withdrawal', [DepoWdController::class, 'withdrawal']);
  Route::get('/list-warrant/{uuid}', [DepoWdController::class, 'warrantList']);

  Route::get('/tickers/list-pair', [TickerController::class, 'listTicker']);
  Route::get('/tickers/get-price', [TickerController::class, 'getTicker']);

  Route::post('/go/intrade/instant', [mt5UserController::class, 'inTradeInstant']);
  Route::post('/go/intrade/pending', [mt5UserController::class, 'inTradePending']);

  Route::post('/check-account', [mt5UserController::class, 'dataAccountMT5']);

  Route::post('/check-balance', [mt5UserController::class, 'checkBalance']);

  Route::post('/check/equity', [getEquityController::class, 'checkEquity']);

  Route::get('/price/{symbol}', [TickerController::class, 'price']);
  Route::get('/volume/{symbol}', [TickerController::class, 'volume']);
  Route::get('/statistic/insight/{login}/{symbol}', [StatisticController::class, 'selfInsightperSymbol']);
  Route::get('/statistic/trader-total/{symbol}', [StatisticController::class, 'traderTradeTotalperSymbol']);
  Route::get('/statistic/trader-closed/{symbol}', [StatisticController::class, 'traderTradeClosedperSymbol']);
  Route::get('/statistic/trader-floating/{symbol}', [StatisticController::class, 'traderTradeFloatingperSymbol']);

  //Dimas//
  Route::get('/get-account-type/{uuid}', [mt5createaccountController::class, 'listAccountType']);


  Route::post('/statistic/inmarket-closed', [StatisticController::class, 'inMarketClosedData']);
  Route::post('/statistic/inmarket-floating', [StatisticController::class, 'inmarketFloatingData']);
  Route::get('/price-tam5/{symbol}', [TickerController::class, 'pricetam5']);
  Route::get('/price-tam15/{symbol}', [TickerController::class, 'pricetam15']);
  Route::get('/price-tam30/{symbol}', [TickerController::class, 'pricetam30']);
  Route::get('/price-tah1/{symbol}', [TickerController::class, 'pricetah1']);
  Route::get('/price-tah4/{symbol}', [TickerController::class, 'pricetah4']);
  Route::get('/price-tad1/{symbol}', [TickerController::class, 'pricetad1']);
  Route::get('/price-taw1/{symbol}', [TickerController::class, 'pricetaw1']);
  Route::get('/price-tamn1/{symbol}', [TickerController::class, 'pricetamn1']);
  // Twilio
  Route::post('/send-otp', [TwilioController::class, 'sendSms']);
  Route::post('/twilio-callback', [TwilioController::class, 'twilioCallback']);
  Route::post('/verify-otp', [TwilioController::class, 'verifyOtpCode']);

  Route::get('/tam5/{symbol}', [mt5UserController::class, 'tam5']);
  Route::get('/tam15/{symbol}', [mt5UserController::class, 'tam15']);
  Route::get('/tam30/{symbol}', [mt5UserController::class, 'tam30']);
  Route::get('/tah1/{symbol}', [mt5UserController::class, 'tah1']);
  Route::get('/tah4/{symbol}', [mt5UserController::class, 'tah4']);
  Route::get('/tad1/{symbol}', [mt5UserController::class, 'tad1']);
  Route::get('/taw1/{symbol}', [mt5UserController::class, 'taw1']);
  Route::get('/tamn1/{symbol}', [mt5UserController::class, 'tamn1']);

  // lab
  Route::post('/lab/check-account', [LabController::class, 'dataAccountMt5']);
  Route::post('/lab/insight-total-graph', [LabController::class, 'selfInsightTotalGraph']);
  Route::post('/lab/self-insight', [LabController::class, 'selfinsight']);
  Route::get('/lab/typemt4-choose-level', [LabController::class, 'filltypemt4accountschooselevel']);
  Route::get('/lab/top-trader-week', [LabController::class, 'TopTraderWeek']);
  Route::get('/lab/top-all-trader-week', [LabController::class, 'TopAllTraderWeek']);
  Route::get('/lab/volume-price', [LabController::class, 'VolumePrice']);
  Route::get('/lab/return-profit-grafik/{login}', [LabController::class, 'returnProfitGrafik']);

});