<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;
    protected $table = 'users_ib';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'name',
        'uuid',
        'email',
        'password',
        'phone',
        'whatsapp',
        'disclaimer',
        'providerId',
        'providerOrigin',
        'parent',
        'first_landing',
        'utm_campaign',
        'utm_source',
        'utm_medium',
        'utm_content',
        'utm_term',
        'ipaddress',
        'device',
        'screen',
        'platform',
        'platformVersion',
        'browser',
        'browserVersion',
        'language',
        'robot',
        'id_cms_privileges',
        'takingPositionPercentage',
        'seniorib',
        'senioribDefaultPercent',
        'masterib',
        'masteribDefaultPercent',
        'ib',
        'ibDefaultPercent'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}
