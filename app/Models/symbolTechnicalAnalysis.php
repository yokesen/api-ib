<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;
use App\Models\symbolTechnicalAnalysis;
class symbolTechnicalAnalysis extends Model
{
  use HasFactory;
  protected $table = 'symbol_technical_analysis';
  protected $appends = ['total_volume','price'];
  protected $hidden = ['time_price','volume_price','id','timeframe','open_price', 'high_price', 'low_price', 'close_price', 'spread_price', 'real_volume', 'adl_indicator', 'atr_indicator', 'bb_indicator', 'cmf_indicator', 'co_indicator', 'chm_indicator',
  'cci_indicator', 'dpo_indicator', 'dmi_indicator', 'dema_indicator', 'eom_indicator', 'enve_indicator', 'fira_indicator', 'fore_indicator', 'imc_indicator', 'imi_indicator', 'klor_indicator', 'lri_indicator', 'lrs_indicator',
  'mfi_indicator', 'mi_indicator', 'mp_indicator', 'mom_indicator', 'mage_indicator', 'macd_indicator', 'nvi_indicator', 'obv_indicator', 'psar_indicator', 'pfc_indicator', 'pvi_indicator', 'pavt_indicator', 'pc_indicator', 'po_indicator',
  'proc_indicator', 'pb_indicator', 'pjo_indicator', 'qsk_indicator', 'ri_indicator', 'rmi_indicator', 'rsi_indicator', 'rvi_indicator', 'sdev_indicator', 'smi_indicator', 'so_indicator', 'si_indicator', 'rsf_indicator', 'tema_indicator', 'tp_indicator',
  'uo_indicator', 'vhf_indicator', 'vc_indicator', 'vo_indicator', 'vroc_indicator', 'wc_indicator', 'ws_indicator', 'wad_indicator', 'wr_indicator', 'adl_signal', 'atr_signal', 'bb_signal', 'cmf_signal', 'co_signal', 'chm_signal', 'cci_signal', 'dpo_signal',
  'dmi_signal', 'dema_signal', 'eom_signal', 'enve_signal', 'fira_signal', 'fore_signal', 'imc_signal', 'imi_signal', 'klor_signal', 'lri_signal', 'lrs_signal', 'mfi_signal', 'mi_signal', 'mp_signal', 'mom_signal', 'mage_signal', 'macd_signal', 'nvi_signal',
  'obv_signal', 'psar_signal', 'pfc_signal', 'pvi_signal', 'pavt_signal', 'pc_signal', 'po_signal', 'proc_signal', 'pb_signal', 'pjo_signal', 'qsk_signal', 'ri_signal', 'rmi_signal', 'rsi_signal', 'rvi_signal', 'sdev_signal', 'smi_signal', 'so_signal',
  'si_signal', 'rsf_signal', 'tema_signal', 'tp_signal', 'uo_signal', 'vhf_signal', 'vc_signal', 'vo_signal', 'vroc_signal', 'wc_signal', 'ws_signal', 'wad_signal', 'wr_signal', 'datadetail', 'bullish', 'bearish', 'neutral', 'bullishpct', 'bearishpct',
  'neutralpct', 'created_at', 'updated_at', 'flag_pct'];


  // public function gettotalVolumeAttribute()
  // {
  //   $totalVolume = symbolTechnicalAnalysis::where('timeframe', 'M5')
  //   ->whereBetween('created_at', [Carbon::now()->timezone('Asia/Dubai')->subDay(), Carbon::now('UTC')->timezone('Asia/Dubai')])
  //   ->where('symbol', $this->attributes['symbol'])
  //   ->sum('volume_price');

  //   return $totalVolume;
  // }

  public function openMarket()
  {
    $openMarket = Carbon::now('UTC')->timezone('Asia/Dubai');
    $openMarket = Carbon::parse($openMarket)->format('Y-m-d');
    $openMarket = $openMarket . ' ' . '02:00:00';
    $openMarket = Carbon::createFromFormat('Y-m-d H:i:s', $openMarket);

    return $openMarket;
  }

  public function getpriceAttribute()
  {
    // $price = 0;
    $price = symbolTechnicalAnalysisModel::select('open_price')->where('timeframe','M5')->where('symbol',$this->attributes['symbol'])->orderBy('id','DESC')->first();
    return $price->open_price;
  }

  public function gettotalVolumeAttribute()
  {
    return $this->attributes['volume_price'];
  }

  public function getpercentageAttribute()
  {
    $percentageOpenMarket = symbolTechnicalAnalysis::select('open_price')->where('timeframe','M5')->where('symbol',$this->attributes['symbol'])->where('created_at','>=' ,$this->openMarket())->first();

    $price = symbolTechnicalAnalysis::select('open_price')->where('timeframe','M5')->where('symbol',$this->attributes['symbol'])->orderBy('id','DESC')->first();

    $hasil = ($price->open_price - $percentageOpenMarket->open_price)/$percentageOpenMarket->open_price;
    $percentage = $hasil*100;


    // $percentage = 0;

    return $percentage;
  }

}
