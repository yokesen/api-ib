<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class symbolTechnicalAnalysisModel extends Model
{
    use HasFactory;
    protected $table = 'symbol_technical_analysis';

}
