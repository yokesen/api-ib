<?php

namespace App\Http\Controllers;
use JWTAuth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use Hash;
use Storage;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Carbon\Carbon;
use GuzzleHttp\Client;

class MarketingController extends Controller
{
    public function master_ib(){
      $masters = DB::table('users_ib')->where('users_ib.seniorib', auth()->user()->id)->where('users_ib.id_cms_privileges', '60')->paginate(20);
      $warrant = DB::table('partner_deposits')->where('uuid',auth()->user()->uuid)->where('status','approved')->count();
      return response()->json([
        'success' => true,
        'data' => $masters,
        'warrant' => $warrant
      ]);
    }

    public function ib(){
      $masters = DB::table('users_ib')->where('users_ib.masterib', auth()->user()->id)->where('users_ib.id_cms_privileges', '70')->paginate(20);
      $warrant = DB::table('partner_deposits')->where('uuid',auth()->user()->uuid)->where('status','approved')->count();
      return response()->json([
        'success' => true,
        'data' => $masters,
        'warrant' => $warrant
      ]);
    }

    public function seniorIbSeeIb(){
      $ibs = DB::table('users_ib')->where('users_ib.seniorib', auth()->user()->id)->where('users_ib.masterib','!=', 0)->where('users_ib.id_cms_privileges', '70')->paginate(15);

      return response()->json([
        'success'=>true,
        'ibs'=>$ibs
      ]);
    }

    public function masterSeeClient(){
      $cekIb = DB::table('users_ib')->where('masterib', auth()->user()->id)->pluck('id');
      $clients = DB::table('users_cabinet')->whereIn('parent', $cekIb)->where('users_cabinet.status', 'winning')->paginate(15);

      return response()->json([
        'success'=>true,
        'clients'=>$clients
      ]);
    }
}
