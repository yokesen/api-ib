<?php

namespace App\Http\Controllers;
use JWTAuth;
use Auth;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Http;

class NotificationController extends Controller
{
    public function notification(){

      $singleNotif = DB::table('notifications')->orderby('created_at','asc')->where('is_read','0')->limit(500)->get();

      foreach ($singleNotif as $key => $value) {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $fcmNotification = [
          'to' => $value->fcm_token,
          'notification' => $value->title,
          'data' => $value->text
        ];

        $headers = [
          'Authorization: key=AAAAvSfrM3g:APA91bGjtnEuFPg2duQCdJMWypmK0tlmcVpFSvBr2_63CCVKUxMxHmzrNvli_7tSGzSw3_p3_tGqk1V4Pc6SAo3ktApwJGgYoK56EJLyfQk4HBavY7-tZs1F3eKKxdoBWhXnA5hGtyAT',
          'Content-Type: application/json'
        ];

        $response = Http::withHeaders($headers)->post($fcmUrl,$fcmNotification);
      }
    }

    public function getNotif(Request $request){
      $token = $request->header('Authorization');
      try {
        if ($user = JWTAuth::authenticate($token)) {
          $uuid =   $user['uuid'];
          $notifs = DB::table('notifications')->where('uuid',$uuid)->orderby('id','desc')->get();
          return response()->json([
            'success' => true,
            'data' => $notifs
          ]);
        }
      } catch (JWTException $e) {
        return response()->json([
          'success' => false,
          'message' => 'Invalid Token.',
        ], 401);
      }

    }

    public function readNotif(Request $request){
      $rules = [
        'id' => 'required'
      ];

      $validator = Validator::make($request->all(), $rules);

      if($validator->fails()){
        return response()->json([
          'status' => false,
          'errors' => [
            $validator->errors()
          ]
        ]);
      }

      $token = $request->header('Authorization');
      try {
        if ($user = JWTAuth::authenticate($token)) {
          $uuid =   $user['uuid'];
          $notifs = DB::table('notifications')->where('id',$request->id)->update([
            'is_read' => 1
          ]);
          return response()->json([
            'success' => true,
          ]);
        }
      } catch (JWTException $e) {
        return response()->json([
          'success' => false,
          'message' => 'Invalid Token.',
        ], 401);
      }

    }

    public function sidebarNotif($uuid){
      $ib = DB::table('users_ib')->select('id')->where('uuid', $uuid)->first();
      $deposit = DB::table('deposits')->where('parent', $ib->id)->where('status', 'pending')->count();
      $withdraw = DB::table('withdrawals')->where('parent', $ib->id)->where('status', 'pending')->count();
      $leads = DB::table('users_cabinet')->where('parent', $ib->id)->where('status', 'lead')->count();

      return response()->json([
        'success'=>true,
        'deposit'=>$deposit,
        'withdraw'=>$withdraw,
        'lead'=>$leads
      ]);
    }
}
