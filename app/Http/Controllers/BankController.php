<?php

namespace App\Http\Controllers;

use JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use Storage;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class BankController extends Controller
{
  public function my_bank(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid = $user['uuid'];
      $count = DB::table('banks')->where('uuid', $uuid)->count();
      if ($count < 1) {
        $insert = DB::table('banks')->insert([
          'uuid' => $uuid
        ]);
        $bank = DB::table('banks')->where('uuid', $uuid)->orderby('id', 'desc')->first();
        return response()->json([
          'success' => true,
          'Data' => $bank
        ]);
      } else {
        $bank = DB::table('banks')->where('uuid', $uuid)->orderby('id', 'desc')->first();
        return response()->json([
          'success' => true,
          'Data' => $bank
        ]);
      }

    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }
  public function custody_bank_list(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid = $user['uuid'];
      $custody_banks = DB::table('bank_segre')->get();
      if ($custody_banks) {
        return response()->json([
          'success' => true,
          'Data' => $custody_banks
        ]);
      } else {
        return response()->json([
          'success' => true,
          'Data' => 'Data not Found'
        ]);
      }
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function topUpWarrantBank(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $parent = $user['parent'];
      $priv = $user['id_cms_privileges'];
      $uuid = $user['uuid'];
      if ($priv == '50') {
        $custody_banks = DB::table('bank_segre')->where('statusRekening', 'active')->where('parent', '0')->get();
      } elseif ($priv == '60') {
        $custody_banks = DB::table('bank_segre')->where('statusRekening', 'active')->where('parent', $parent)->get();
      } elseif ($priv == '70') {
        $custody_banks = DB::table('bank_segre')->where('statusRekening', 'active')->where('parent', $parent)->get();
      }

      if ($custody_banks) {
        return response()->json([
          'success' => true,
          'Data' => $custody_banks
        ]);
      } else {
        return response()->json([
          'success' => true,
          'Data' => 'Data not Found'
        ]);
      }
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function topUpCapital(Request $request)
  {
    $time = date('Y-m-d H:i:s');
    // dd($request->all(), $time);
    // dd($request->all());
    $rules = [
      'bank_name' => 'required',
      'account_number' => 'required',
      'account_name' => 'required',
      'amount' => "required",
      'credit' => "required",
      'transferTo' => 'required',
      'photo' => 'required'
    ];
    $validator = \Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ], 422);
    }

    $amount = preg_replace("/[^0-9]/", "", $request->amount);
    $credit = preg_replace("/[^0-9]/", "", $request->credit);
    $approved_amount = $amount;

    $priv = auth()->user()->id_cms_privileges;

    if ($priv == '50') {
      $bankDetail = DB::table('bank_segre')->where('parent', '0')->where('namaBank', $request->transferTo)->first();
    } else {
      $parent = auth()->user()->parent;
      $bankDetail = DB::table('bank_segre')->where('parent', $parent)->where('namaBank', $request->transferTo)->first();
    }

    /** if request has photo then do upload photo*/
    $path = NULL;
    if ($request->file('photo')) {

      return response()->json([
        "success" => false,
        "message" => "image bukan base64"
      ], 422);

    } else if ($request->photo) {
      $imageName = "depo-" . \Str::random(2) . time();
      $this->uploadGambar($request->photo, $imageName);
      $path = $imageName . ".png";
    } else {
      $path = '/images/upload-default.jpg';
    }
    /** end upload photo*/

    $udid = strtotime($time) . md5($amount . $request->bank_name . $request->account_name . auth()->user()->uuid);
    // dd($time, $udid);

    $insert = DB::table('partner_deposits')->insert([
      'udid' => $udid,
      'uuid' => auth()->user()->uuid,
      'parent' => auth()->user()->parent,
      'amount' => $amount,
      'tipe_deposit' => 'fixed',
      'currency' => 'IDR',
      'approved_amount' => $approved_amount,
      'percentage' => auth()->user()->takingPositionPercentage,
      'currency_rate' => 1,
      'credit' => $credit,
      'photo' => $path,
      'status' => 'pending',
      'process_by' => '',
      'created_at' => $time,
      'updated_at' => $time,
      'reason' => "Deposit by " . auth()->user()->name,
      'from_bank' => $request->bank_name,
      'from_name' => $request->account_name,
      'from_rekening' => $request->account_number,
      'to_bank' => $bankDetail->namaBank,
      'to_name' => $bankDetail->namaRekening,
      'to_rekening' => $bankDetail->nomorRekening
    ]);
    return response()->json([
      'success' => true,
      'message' => 'berhasil melakukan request deposit sejumlah ' . $request->amount,
      'status' => 'pending'
    ]);

  }
  public function mt4_detail(Request $request, $account = null)
  {
    $token = $request->header('Authorization');
    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid = $user['uuid'];
      if (isset($account)) {
        $myAccount = DB::table('typeMT4AvailabeAccount')->where('mt4_id', $account)->first();
        if ($myAccount) {
          return response()->json([
            'success' => true,
            'Data' => $myAccount
          ]);
        } else {
          return response()->json([
            'success' => true,
            'Data' => 'Data not Found'
          ]);
        }
      } else {
        return response()->json([
          'success' => true,
          'Data' => 'Please Provide Account'
        ]);
      }


    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }
  public function my_deposit(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid = $user['uuid'];
      $deposits = DB::table('deposits')->where('uuid', $uuid)->orderby('id', 'desc')->get();
      if ($deposits) {
        return response()->json([
          'success' => true,
          'Data' => $deposits
        ]);
      } else {
        return response()->json([
          'success' => true,
          'Data' => 'Data not Found'
        ]);
      }
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }
  public function update_bank(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid = $user['uuid'];
      $update = DB::table('banks')->where('uuid', $uuid)->update([
        'bank_name' => $request->bank_name,
        'account_name' => $request->account_name,
        'account_number' => $request->account_number
      ]);
      if ($update) {
        return response()->json([
          'success' => true,
          'Data' => 'Bank Details Updated Successfully'
        ]);
      } else {
        return response()->json([
          'success' => true,
          'Data' => 'Data not Updted or Same Data'
        ]);
      }
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }


  public function request_deposit(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid = $user['uuid'];

      if ($request->photo) {
        $photo = $request->photo;
      } else {
        $photo = '/images/upload-default.jpg';
      }

      if ($request->tipeDeposit < 10) {
        $checkTipe = DB::table('typeMT4AvailabeAccount')->where('mt4_id', $request->accountmt4)->first();
        $type = $checkTipe->tipe_deposit;
        if ($type == 0) {
          return response()->json([
            'success' => false,
            'Data' => 'Please choose Tipe'
          ]);
        }
      } else {
        $type = $request->tipeDeposit;
        $checkTipe = DB::table('typeMT4AvailabeAccount')->where('mt4_id', $request->accountmt4)->first();
        $newtype = $checkTipe->tipe_deposit;
        if ($newtype == 0) {
          $update = DB::table('typeMT4AvailabeAccount')->where('mt4_id', $request->accountmt5)->update([
            'tipe_deposit' => $request->tipeDeposit // IF TIPE DEPOSIT WAS NOT SET, PLEASE UPDATE THE TIPE DEPOSIT HERE
          ]);
        } else {
          $type = $request->tipeDeposit;
          $checkTipe = DB::table('typeMT4AvailabeAccount')->where('mt4_id', $request->accountmt4)->first();
          $newtype = $checkTipe->tipe_deposit;
          if ($newtype == 0) {
            $update = DB::table('typeMT4AvailabeAccount')->where('mt4_id', $request->accountmt5)->update([
              'tipe_deposit' => $request->tipeDeposit // IF TIPE DEPOSIT WAS NOT SET, PLEASE UPDATE THE TIPE DEPOSIT HERE
            ]);
          }
        }
        $amount = preg_replace("/[^0-9]/", "", $request->amount);
        $insert = DB::table('deposits')->insert([
          'uuid' => $uuid,
          //STRING
          'amount' => $amount,
          //FLOAT
          'tipe_deposit' => $type,
          //INT
          'currency' => 'IDR',
          'approved_amount' => 1,
          'currency_rate' => 1,
          'photo' => $photo,
          //STRING
          'status' => 'pending',
          'from_bank' => $request->bank_name,
          'from_name' => $request->account_name,
          'from_rekening' => $request->account_number,
          'to_bank' => $request->namaBank,
          'to_name' => $request->namaRekening,
          'to_rekening' => $request->nomorRekening,
          'metatrader' => $request->accountmt4 //FLOAT
        ]);
        if ($insert) {
          return response()->json([
            'success' => true,
            'response' => 'Deposit requested please wait and check the status regulary'
          ]);
        } else {
          return response()->json([
            'success' => false,
            'response' => 'Error Please Check Parm'
          ]);
        }
      }
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

  }

  public function storeBank(Request $request)
  {

    $rules = [
      'bank_name' => 'required',
      'account_name' => 'required',
      'account_number' => 'required|numeric|min:1'
    ];
    $credentials = \Validator::make($request->all(), $rules);
    if ($credentials->fails()) {
      return response()->json([
        'status' => false,
        'errors' => $credentials->errors()
      ], 422);
    }

    DB::table('banks')->insert([
      'uuid' => auth()->user()->uuid,
      'bank_name' => $request->bank_name,
      'account_number' => $request->account_number,
      'account_name' => $request->account_name,
      'created_at' => now(),
    ]);

    return response()->json([
      'success' => true,
      'message' => 'berhasil menambahkan bank'
    ]);

  }

  public function mybanks()
  {
    $banks = DB::table('bank_segre')->where('parent', auth()->user()->id)->get();

    return response()->json([
      'success' => true,
      'data' => $banks
    ]);
  }
  public function mybankswd()
  {
    $banks = DB::table('bank_wd')->where('parent', auth()->user()->id)->get();

    return response()->json([
      'success' => true,
      'data' => $banks
    ]);
  }

  public function storeSegreBank(Request $request)
  {
    $rules = [
      'bank_name' => 'required',
      'account_name' => 'required',
      'account_number' => 'required|numeric|min:1'
    ];
    $credentials = \Validator::make($request->all(), $rules);
    if ($credentials->fails()) {
      return response()->json([
        'status' => false,
        'errors' => $credentials->errors()
      ], 422);
    }

    DB::table('bank_segre')->insert([
      'parent' => auth()->user()->id,
      'namaBank' => $request->bank_name,
      'nomorRekening' => $request->account_number,
      'namaRekening' => $request->account_name
    ]);

    return response()->json([
      'success' => true,
      'message' => 'berhasil menambahkan bank'
    ]);
  }
  public function storeWdBank(Request $request)
  {
    $rules = [
      'bank_name' => 'required',
      'account_name' => 'required',
      'account_number' => 'required|numeric|min:1'
    ];
    $credentials = \Validator::make($request->all(), $rules);
    if ($credentials->fails()) {
      return response()->json([
        'status' => false,
        'errors' => $credentials->errors()
      ], 422);
    }

    DB::table('bank_wd')->insert([
      'parent' => auth()->user()->id,
      'namaBank' => $request->bank_name,
      'nomorRekening' => $request->account_number,
      'namaRekening' => $request->account_name
    ]);

    return response()->json([
      'success' => true,
      'message' => 'berhasil menambahkan bank'
    ]);
  }
  public function uploadGambar($image, $image_name)
  {
    $client = new Client();
    $res = $client->post("https://static.8377-2842.xyz/api/v1/upload/image/ib", [
      "form_params" => [
        "secret" => "anantonthemovedoesmorethanadozingox",
        "gambar" => $image,
        "image_name" => $image_name
      ]
    ]);
  }

  public function listWarantCapital()
  {
    $warrant = DB::table('partner_deposits')->where('parent', auth()->user()->id)->orderby('id', 'desc')->paginate();
    return response()->json([
      'success' => true,
      'data' => $warrant
    ]);
  }

  public function getWarantDetail($id)
  {

    $data = DB::table('partner_deposits')
      ->where('partner_deposits.parent', auth()->user()->id)
      ->where('partner_deposits.id', $id)
      ->orderByDesc('partner_deposits.id')
      ->join('users_ib', 'users_ib.uuid', 'partner_deposits.uuid')
      ->select('partner_deposits.*', 'users_ib.name as deposit_for')
      ->first();
    $bank = DB::table('bank_segre')->where('parent', auth()->user()->id)->first();
    $data->deposit_by = auth()->user()->name;
    $data->bank_partner = $bank;
    return response()->json([
      'success' => true,
      'data' => $data
    ]);

  }

  public function warrantConfirm(Request $request)
  {
    /** Validation*/
    $rules = [
      'status' => 'required|in:approved,rejected',
      'deposit_id' => 'required|exists:partner_deposits,id'
    ];
    $credentials = \Validator::make($request->all(), $rules);
    if ($credentials->fails()) {
      return response()->json([
        'status' => false,
        'errors' => $credentials->errors()
      ], 422);
    }
    /** End validation*/

    /** Check if deposit does exist*/
    $deposit = DB::table('partner_deposits')->where('id', $request->deposit_id)->first();
    if (!$deposit) {
      return response()->json([
        'success' => false,
        'errors' => array(
          'message' => ['deposit tidak ditemukan']
        )
      ], 422);
    }

    /** Check if deposit does not belong to parent that login*/
    if ($deposit->parent != auth()->user()->id) {
      return response()->json([
        'success' => false,
        'errors' => array(
          'message' => ['deposit tidak ditemukan']
        )
      ], 422);
    }
    /** */

    /** Check if deposit status is not pending*/
    if ($deposit->status != 'pending') {
      return response()->json([
        'success' => false,
        'errors' => array(
          'message' => ['tidak dapat merubah data']
        )
      ], 422);
    }
    /***/

    /** Status rejected*/
    if ($request->status == 'rejected') {

      DB::table('partner_deposits')->where('id', $request->deposit_id)->update([
        'status' => 'rejected',
        'updated_at' => now()
      ]);

      return response()->json([
        'status' => true,
        'message' => 'Warrant REJECTED'
      ]);

    }
    /** */
    if ($request->status == 'approved') {
      $uuid = auth()->user()->uuid;
      $myWallet = DB::table('wallets')->where('uuid', auth()->user()->uuid)->first();
      // dd($deposit, $myWallet);

      if ($deposit->credit <= $myWallet->amount) {


        //PULL TRX
        $trxDepo = DB::table('partner_deposits')->where('id', $request->deposit_id)->first();
        //CHECK HASH
        $time = $trxDepo->created_at;
        $udid = strtotime($trxDepo->created_at) . md5($trxDepo->amount . $trxDepo->from_bank . $trxDepo->from_name . $trxDepo->uuid);
        //CHECK WALLET COUNT
        $check_wallet = DB::table('wallets')->where('uuid', $trxDepo->uuid)->count();
        //FETCH USER
        $siUser = DB::table('users_ib')->where('uuid', $trxDepo->uuid)->first();
        //CHECK IF WALLET EXIST
        if ($check_wallet < 1) {
          $create_wallet = DB::table('wallets')->insert([
            'uuid' => $trxDepo->uuid,
            'name' => $siUser->username,
            'amount' => 0
          ]);
        }
        //FETCH WALLET
        $wallet = DB::table('wallets')->where('uuid', $trxDepo->uuid)->first();
        //COMPARE HASH
        // return response()->json([
        //   'status' => true,
        //   'udid' => $udid,
        //   'trxDepo->udid' => $trxDepo->udid
        // ]);
        // if ($udid == $trxDepo->udid) {
        if (1 == 1) {
          //APPROVE THE TRX
          DB::table('partner_deposits')->where('id', $request->deposit_id)->update([
            'approved_amount' => $trxDepo->amount,
            'currency_rate' => 1,
            'status' => 'approved',
            'updated_at' => date('Y-m-d H:i:s'),
            'process_by' => auth()->user()->id
          ]);
          //RECORD ON BOOK WALLET
          $insert_depo_to_book = DB::table('wallet_book')->insert([
            'wallet' => $wallet->uuid,
            'amount' => $trxDepo->credit,
            'type' => "deposit warrant",
            'tx_id' => $udid,
            'tx_hash' => $trxDepo->udid,
            'saldo_awal' => $wallet->amount,
            'saldo_akhir' => $wallet->amount + $trxDepo->credit,
          ]);
          //UPDATE THE WALLET
          $update_balance = DB::table('wallets')->where('uuid', $trxDepo->uuid)->update([
            'signatures' => $udid,
            'amount' => $wallet->amount + $trxDepo->credit
          ]);
          //KURANGIN CREDIT PEMILIK
          $insert_depo_to_book = DB::table('wallet_book')->insert([
            'wallet' => $myWallet->uuid,
            'amount' => $trxDepo->credit * -1,
            'type' => "credit to " . $siUser->username,
            'tx_id' => $udid,
            'tx_hash' => $trxDepo->udid,
            'saldo_awal' => $myWallet->amount,
            'saldo_akhir' => $myWallet->amount - $trxDepo->credit,
          ]);
          //UPDATE MY WALLET
          $update_balance = DB::table('wallets')->where('uuid', auth()->user()->uuid)->update([
            'signatures' => $udid,
            'amount' => $myWallet->amount - $trxDepo->credit
          ]);



          //  $lotdate = date('z',strtotime($deposit->created_at));
          //   $lotweek = date('W',strtotime($deposit->created_at));
          //   $lotmonth = date('m',strtotime($deposit->created_at));
          //   $lotyear = date('Y',strtotime($deposit->created_at));
          //   $humanTime = date('Y-m-d H:i:s');

          // $user = DB::table('users_ib')->where('uuid',$deposit->uuid)->first();
          // $balance = DB::table('statement_balance_sheet')->where('parent',$user->id)->orderby('id','desc')->first();

          // if ($balance) {
          //   $saldo = $balance->saldo;
          // }else{
          //   $saldo = 0;
          // }

          // $insert_balance_statement = DB::table('statement_balance_sheet')->insert([
          //   'parent' => $user->id,
          //   'tanggal' => $humanTime,
          //   'sumber' => "BR-".$deposit->id,
          //   'tipe_transaksi' => "Warrant Capital",
          //   'keterangan' => $user->username,
          //   'mutasi' => $deposit->amount,
          //   'amount_partner' => $deposit->amount,
          //   'tipe_mutasi' => "Top Up Warrant Capital",
          //   'saldo' => $saldo+$deposit->amount,
          //   'saldo_global' => 0,
          //   'status_payment' => '1',
          //   'dayoty' => $lotdate,
          //   'weekoty' => $lotweek,
          //   'monthoty' => $lotmonth,
          //   'year' => $lotyear
          // ]);


          return response()->json([
            'success' => true,
            'message' => 'WARRANT APPROVED ' . $trxDepo->amount
          ]);
        } else {
          // Invalid hash scenario
          return response()->json([
            'success' => false,
            'message' => 'Invalid transaction hash'
          ]);
        }

      } else {
        // Insufficient funds scenario
        return response()->json([
          'success' => false,
          'message' => 'WARRANT NOT APPROVED: Insufficient credit!'
        ]);
      }





    }
  }

  public function changeBankStatus(Request $request)
  {
    $cekBank = DB::table('bank_segre')->select('statusRekening')->where('id', $request->id)->first();
    // dd($cekBank);
    if ($cekBank->statusRekening == 'active') {
      $status = 'inactive';
      $active = 0;
    } else {
      $status = 'active';
      $active = 1;
    }
    $updateBank = DB::table('bank_segre')->where('id', $request->id)->update([
      'statusRekening' => $status
    ]);

    return response()->json([
      'success' => true,
      'msg' => "account $status",
      'isActive' => $active
    ]);
  }

  public function getBank(Request $request)
  {
    $bank = DB::table('bank_segre')->where('id', $request->id)->first();
    return response()->json([
      'success' => true,
      'bank' => $bank
    ]);
  }

  public function myActiveBanks()
  {
    $banks = DB::table('bank_segre')->where('parent', auth()->user()->id)->where('statusRekening', 'active')->get();

    return response()->json([
      'success' => true,
      'data' => $banks
    ]);
  }

  public function getBankList()
  {
    $data = DB::table('bank_new_lists')->select('name_bank', 'code_bank')->where('status', 1)->get();
    return response()->json([
      'success' => true,
      'data' => $data
    ]);
  }

  public function getBankChildren($uuid)
  {
    $usersIb = DB::table('users_ib')->where('uuid', $uuid)->first();
    $banks = DB::table('bank_segre')->where('parent', $usersIb->id)->where('statusRekening', 'active')->get();

    return response()->json([
      'success' => true,
      'banks' => $banks
    ]);
  }

  public function storeSegreBankChildren(Request $request)
  {
    $rules = [
      'bank_name' => 'required',
      'account_name' => 'required',
      'account_number' => 'required|numeric|min:1',
      'uuid' => 'required',
    ];
    $credentials = \Validator::make($request->all(), $rules);
    if ($credentials->fails()) {
      return response()->json([
        'status' => false,
        'errors' => $credentials->errors()
      ], 422);
    }

    $usersIb = DB::table('users_ib')->where('uuid', $request->uuid)->first();
    DB::table('bank_segre')->insert([
      'parent' => $usersIb->id,
      'namaBank' => $request->bank_name,
      'nomorRekening' => $request->account_number,
      'namaRekening' => $request->account_name
    ]);

    return response()->json([
      'success' => true,
      'message' => 'berhasil menambahkan bank'
    ]);
  }

  public function deleteSegreBankChildren(Request $request)
  {
    $delete = DB::table('bank_segre')->where('id', $request->data)->delete();

    return response()->json([
      'success' => true,
      'msg' => 'success delete bank'
    ]);
  }

  public function updateSegreBankChildren(Request $request)
  {
    $usersIb = DB::table('users_ib')->where('uuid', $request->uuid)->first();
    $delete = DB::table('bank_segre')->where('id', $request->data)->update([
      'parent' => $usersIb->id,
      'namaBank' => $request->bank_name,
      'nomorRekening' => $request->account_number,
      'namaRekening' => $request->account_name
    ]);

    return response()->json([
      'success' => true,
      'msg' => 'success update bank'
    ]);
  }


  public function topUpCapitalNew(Request $request)
  {
    $rules = [
      'bank_name' => 'required',
      'account_number' => 'required',
      'account_name' => 'required',
      'amount' => "required|numeric|min:10000",
      'transferTo' => 'required',
      // 'photo' => 'required'
    ];
    $validator = \Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ], 422);
    }

    $amount = preg_replace("/[^0-9]/", "", $request->amount);
    $approved_amount = $amount / 10000;




    if ($request->transferTo == 0) {
      $bankSegreNama = "CHECK REK NOMINI SAAT INI";
      $bankSegreRekening = "CHECK REK NOMINI SAAT INI";
      $bankSegreNomor = "CHECK REK NOMINI SAAT INI";
    } else {
      $bankDetail = DB::table('bank_segre')->where('id', $request->transferTo)->first();
      $bankSegreNama = $bankDetail->namaBank;
      $bankSegreRekening = $bankDetail->namaRekening;
      $bankSegreNomor = $bankDetail->nomorRekening;
    }

    $time = date('Y-m-d H:i:s');
    $udid = strtotime($time) . md5($amount . $request->bank_name . $request->account_name . auth()->user()->uuid);

    /** if request has photo then do upload photo*/
    $path = NULL;
    if ($request->file('photo')) {

      return response()->json([
        "success" => false,
        "message" => "image bukan base64"
      ], 422);

    } else if ($request->photo) {
      $imageName = "depo-" . \Str::random(2) . time();
      $this->uploadGambar($request->photo, $imageName);
      $path = $imageName . ".png";
    } else {
      $path = '/images/upload-default.jpg';
    }
    /** end upload photo*/

    $insert = DB::table('partner_deposits')->insert([
      'udid' => $udid,
      'uuid' => auth()->user()->uuid,
      'parent' => auth()->user()->parent,
      'amount' => $amount,
      'tipe_deposit' => 'fixed',
      'currency' => 'IDR',
      'approved_amount' => $approved_amount,
      'percentage' => auth()->user()->takingPositionPercentage,
      'credit' => auth()->user()->creditGiven,
      'currency_rate' => 10000,
      'photo' => $path,
      'status' => 'pending',
      'process_by' => '',
      'reason' => "Deposit by " . auth()->user()->name,
      'from_bank' => $request->bank_name,
      'from_name' => $request->account_name,
      'from_rekening' => $request->account_number,
      'to_bank' => $bankSegreNama,
      'to_name' => $bankSegreRekening,
      'to_rekening' => $bankSegreNomor
    ]);

    return response()->json([
      'success' => true,
      'message' => 'berhasil melakukan request deposit sejumlah ' . $request->amount,
      'status' => 'pending'
    ]);

  }

}