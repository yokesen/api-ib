<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ReconsileController extends Controller
{


  public function ibDownloadReconsile($trx)
  {
    $ib_id = auth()->user()->id;
    // dd($ib_id);

    //ambil itung2an dulu utk tau bagi2 kue dari calculate_client_weekly pakai hashCalc
    $hitungan = DB::table('calculate_client_weekly')->where('ib', auth()->user()->id)->where('hashCalc', $trx)->first();
    // dd($trx, $hitungan);

    //baru dilist detail client2 nya dari calculate_account_weekly pakai uuid, weekoty, year
    $getData = DB::table('calculate_client_weekly')->join('users_cabinet', 'calculate_client_weekly.uuid', 'users_cabinet.uuid')->where('calculate_client_weekly.hashCalc', $trx)->first();
    // return $getData;
    if ($getData) {
      $getAccount = DB::table('calculate_account_weekly')
        ->join('users_cabinet', 'users_cabinet.uuid', '=', 'calculate_account_weekly.uuid')
        ->join('typeMT4AvailabeAccount', 'typeMT4AvailabeAccount.mt4_id', '=', 'calculate_account_weekly.accountId')
        ->where('calculate_account_weekly.uuid', $getData->uuid)
        ->where('calculate_account_weekly.year', $getData->year)
        ->where('calculate_account_weekly.weekoty', $getData->weekoty)
        ->select('calculate_account_weekly.*', 'users_cabinet.name', 'typeMT4AvailabeAccount.typeAccount')
        ->get();
    } else {
      $getAccount = "there might be wrong parameter you've entered";
    }

    return response()->json([
      'success' => true,
      'data' => $getAccount,
      'client' => $getData
    ]);



  }
  public function getWeekReconsile(Request $request)
  {
    if (auth()->user()->id_cms_privileges == '50') {

      $weeks = DB::table('calculate_mib_weekly')->where('sib', auth()->user()->id)->orderby('id', 'desc')->distinct()->select('weekoty', 'dateStart', 'dateEnd')->get();

    } else if (auth()->user()->id_cms_privileges == '60') {

      $weeks = DB::table('calculate_ib_weekly')->where('mib', auth()->user()->id)->orderby('id', 'desc')->distinct()->select('weekoty', 'dateStart', 'dateEnd')->get();

    } else if (auth()->user()->id_cms_privileges == '70') {


      $weeks = DB::table('calculate_client_weekly')->where('ib', auth()->user()->id)->orderby('id', 'desc')->distinct()->select('weekoty', 'dateStart', 'dateEnd')->get();

    } else {
      $weeks = '';
    }

    return response()->json([
      'success' => true,
      'weeks' => $weeks
    ]);
  }

  public function sibReconsileWeekly(Request $request)
  {
    // dd("sini");
    // dd($request->all());
    if (auth()->user()->id_cms_privileges == '50') {
      $max = DB::table('calculate_mib_weekly')->where('sib', auth()->user()->id)->max('weekoty');
      // $reconsile = DB::table('calculate_mib_weekly')->join('users_ib', 'users_ib.id', 'calculate_mib_weekly.mib')->where('calculate_mib_weekly.sib', auth()->user()->id)->orderby('calculate_mib_weekly.id', 'desc')->where('calculate_mib_weekly.weekoty', $max)->get();
      $reconsile = DB::table('calculate_mib_weekly')
        ->join('users_ib as child', 'child.id', '=', 'calculate_mib_weekly.mib')
        ->leftJoin('users_ib as parent', 'child.parent', '=', 'parent.id')
        ->where('calculate_mib_weekly.sib', auth()->user()->id)
        ->where('calculate_mib_weekly.weekoty', $max)
        ->orderby('calculate_mib_weekly.id', 'desc')
        ->select(
          'calculate_mib_weekly.*',
          'child.*',
          'parent.name as parent_name',
          'parent.username as parent_username'
        )
        ->get();

      $weeks = DB::table('calculate_mib_weekly')->where('sib', auth()->user()->id)->orderby('id', 'desc')->distinct()->select('weekoty', 'dateStart', 'dateEnd')->get();

    } else if (auth()->user()->id_cms_privileges == '60') {
      $max = DB::table('calculate_ib_weekly')->where('mib', auth()->user()->id)->max('weekoty');
      // $reconsile = DB::table('calculate_ib_weekly')->join('users_ib', 'users_ib.id', 'calculate_ib_weekly.ib')->where('calculate_ib_weekly.mib', auth()->user()->id)->orderby('calculate_ib_weekly.id', 'desc')->where('calculate_ib_weekly.weekoty', $max)->get();
      $reconsile = DB::table('calculate_ib_weekly')
        ->join('users_ib as child', 'child.id', '=', 'calculate_ib_weekly.ib')
        ->leftJoin('users_ib as parent', 'child.parent', '=', 'parent.id')
        ->where('calculate_ib_weekly.mib', auth()->user()->id)
        ->where('calculate_ib_weekly.weekoty', $max)
        ->orderby('calculate_ib_weekly.id', 'desc')
        ->select(
          'calculate_ib_weekly.*',
          'child.*',
          'parent.name as parent_name',
          'parent.username as parent_username'
        )
        ->get();

      $weeks = DB::table('calculate_ib_weekly')->where('mib', auth()->user()->id)->orderby('id', 'desc')->distinct()->select('weekoty', 'dateStart', 'dateEnd')->get();

    } else if (auth()->user()->id_cms_privileges == '70') {

      $max = DB::table('calculate_account_weekly')->where('ib', auth()->user()->id)->max('weekoty');
      // $reconsile = DB::table('calculate_client_weekly')->join('users_cabinet', 'users_cabinet.uuid', 'calculate_client_weekly.uuid')->where('calculate_client_weekly.ib', auth()->user()->id)->orderby('calculate_client_weekly.id', 'desc')->where('calculate_client_weekly.weekoty', $max)->get();
      $reconsile = DB::table('calculate_client_weekly')
        ->join('users_cabinet', 'users_cabinet.uuid', '=', 'calculate_client_weekly.uuid')
        ->leftJoin('users_ib', 'users_cabinet.parent', '=', 'users_ib.id')
        ->where('calculate_client_weekly.ib', auth()->user()->id)
        ->where('calculate_client_weekly.weekoty', $max)
        ->orderby('calculate_client_weekly.id', 'desc')
        ->select(
          'calculate_client_weekly.*',
          'users_cabinet.*',
          'users_ib.name as parent_name',
          'users_ib.username as parent_username'
        )
        ->get();

      $weeks = DB::table('calculate_client_weekly')->where('ib', auth()->user()->id)->orderby('id', 'desc')->distinct()->select('weekoty', 'dateStart', 'dateEnd')->get();

    } else {
      $reconsile = '';
    }

    return response()->json([
      'success' => true,
      'data' => $reconsile,
      'weeks' => $weeks
    ]);
  }

  public function sibReconsileWeeklyBackDate($week)
  {


    if (auth()->user()->id_cms_privileges == '50') {
      // $reconsile = DB::table('calculate_mib_weekly')->join('users_ib', 'users_ib.id', 'calculate_mib_weekly.mib')->where('calculate_mib_weekly.sib', auth()->user()->id)->orderby('calculate_mib_weekly.id', 'desc')->where('calculate_mib_weekly.weekoty', $week)->get();
      $reconsile = DB::table('calculate_mib_weekly')
        ->join('users_ib as child', 'child.id', '=', 'calculate_mib_weekly.mib')
        ->leftJoin('users_ib as parent', 'child.parent', '=', 'parent.id')
        ->where('calculate_mib_weekly.sib', auth()->user()->id)
        ->where('calculate_mib_weekly.weekoty', $week)
        ->orderby('calculate_mib_weekly.id', 'desc')
        ->select(
          'calculate_mib_weekly.*',
          'child.*',
          'parent.name as parent_name',
          'parent.username as parent_username'
        )
        ->get();

      $weeks = DB::table('calculate_mib_weekly')->where('sib', auth()->user()->id)->orderby('id', 'desc')->distinct()->select('weekoty', 'dateStart', 'dateEnd')->get();

    } else if (auth()->user()->id_cms_privileges == '60') {
      // $reconsile = DB::table('calculate_ib_weekly')->join('users_ib', 'users_ib.id', 'calculate_ib_weekly.ib')->where('calculate_ib_weekly.mib', auth()->user()->id)->orderby('calculate_ib_weekly.id', 'desc')->where('calculate_ib_weekly.weekoty', $week)->get();
      $reconsile = DB::table('calculate_ib_weekly')
        ->join('users_ib as child', 'child.id', '=', 'calculate_ib_weekly.ib')
        ->leftJoin('users_ib as parent', 'child.parent', '=', 'parent.id')
        ->where('calculate_ib_weekly.mib', auth()->user()->id)
        ->where('calculate_ib_weekly.weekoty', $week)
        ->orderby('calculate_ib_weekly.id', 'desc')
        ->select(
          'calculate_ib_weekly.*',
          'child.*',
          'parent.name as parent_name',
          'parent.username as parent_username'
        )
        ->get();

      $weeks = DB::table('calculate_ib_weekly')->where('mib', auth()->user()->id)->orderby('id', 'desc')->distinct()->select('weekoty', 'dateStart', 'dateEnd')->get();

    } else if (auth()->user()->id_cms_privileges == '70') {
      // $reconsile = DB::table('calculate_client_weekly')->join('users_cabinet', 'users_cabinet.uuid', 'calculate_client_weekly.uuid')->where('calculate_client_weekly.ib', auth()->user()->id)->orderby('calculate_client_weekly.id', 'desc')->where('calculate_client_weekly.weekoty', $week)->get();
      $reconsile = DB::table('calculate_client_weekly')
        ->join('users_cabinet', 'users_cabinet.uuid', '=', 'calculate_client_weekly.uuid')
        ->leftJoin('users_ib', 'users_cabinet.parent', '=', 'users_ib.id')
        ->where('calculate_client_weekly.ib', auth()->user()->id)
        ->where('calculate_client_weekly.weekoty', $week)
        ->orderby('calculate_client_weekly.id', 'desc')
        ->select(
          'calculate_client_weekly.*',
          'users_cabinet.*',
          'users_ib.name as parent_name',
          'users_ib.username as parent_username'
        )
        ->get();

      $weeks = DB::table('calculate_client_weekly')->where('ib', auth()->user()->id)->orderby('id', 'desc')->distinct()->select('weekoty', 'dateStart', 'dateEnd')->get();

    } else {
      $reconsile = '';
    }

    return response()->json([
      'success' => true,
      'data' => $reconsile,
      'weeks' => $weeks
    ]);
  }

  public function sibReconsileWeeklyDetail($trx)
  {

    $getData = DB::table('calculate_mib_weekly')->join('users_ib', 'users_ib.id', 'calculate_mib_weekly.mib')->where('calculate_mib_weekly.hashCalc', $trx)->first();

    if ($getData) {
      // $getIb = DB::table('calculate_ib_weekly')->join('users_ib', 'users_ib.id', 'calculate_ib_weekly.ib')->where('calculate_ib_weekly.mib', $getData->mib)->where('calculate_ib_weekly.year', $getData->year)->where('calculate_ib_weekly.weekoty', $getData->weekoty)->get();
      $getIb = DB::table('calculate_ib_weekly')
        ->join('users_ib as child', 'child.id', '=', 'calculate_ib_weekly.ib')
        ->leftJoin('users_ib as parent', 'child.parent', '=', 'parent.id')
        ->where('calculate_ib_weekly.mib', $getData->mib)
        ->where('calculate_ib_weekly.year', $getData->year)
        ->where('calculate_ib_weekly.weekoty', $getData->weekoty)
        ->select(
          'calculate_ib_weekly.*',
          'child.*',
          'parent.name as parent_name',
          'parent.username as parent_username'
        )
        ->get();


    } else {
      $getIb = "there might be wrong parameter you've entered";
    }

    return response()->json([
      'success' => true,
      'data' => $getIb,
      'mib' => $getData
    ]);
  }

  public function mibReconsileWeeklyDetail($trx)
  {

    $getData = DB::table('calculate_ib_weekly')->join('users_ib', 'calculate_ib_weekly.ib', 'users_ib.id')->where('calculate_ib_weekly.hashCalc', $trx)->first();
// dd($getData);
    if ($getData) {
      // dd($getData->id);
      // $getClient = DB::table('calculate_client_weekly')->join('users_cabinet', 'users_cabinet.uuid', 'calculate_client_weekly.uuid')->where('calculate_client_weekly.ib', $getData->id)->where('calculate_client_weekly.year', $getData->year)->where('calculate_client_weekly.weekoty', $getData->weekoty)->get();
      $getClient = DB::table('calculate_client_weekly')
        ->join('users_cabinet', 'users_cabinet.uuid', '=', 'calculate_client_weekly.uuid')
        ->leftJoin('users_ib', 'users_cabinet.parent', '=', 'users_ib.id')
        ->where('calculate_client_weekly.ib', $getData->id) 
        ->where('calculate_client_weekly.year', $getData->year)
        ->where('calculate_client_weekly.weekoty', $getData->weekoty)
        ->select(
          'calculate_client_weekly.*',
          'users_cabinet.*',
          'users_ib.name as parent_name',
          'users_ib.username as parent_username'
        )
        ->get();

    } else {
      $getClient = "there might be wrong parameter you've entered";
    }

    return response()->json([
      'success' => true,
      'data' => $getClient,
      'ib' => $getData
    ]);
  }

  public function ibReconsileWeeklyDetail($trx)
  {
    // Fetch client data 
    $getData = DB::table('calculate_client_weekly')
      ->join('users_cabinet', 'calculate_client_weekly.uuid', '=', 'users_cabinet.uuid')
      ->where('calculate_client_weekly.hashCalc', $trx)
      ->first();

    if ($getData) {
      // Fetch account data with typeAccount included
      $getAccount = DB::table('calculate_account_weekly')
        ->join('users_cabinet', 'users_cabinet.uuid', '=', 'calculate_account_weekly.uuid')
        ->join('typeMT4AvailabeAccount', 'typeMT4AvailabeAccount.mt4_id', '=', 'calculate_account_weekly.accountId')
        ->where('calculate_account_weekly.uuid', $getData->uuid)
        ->where('calculate_account_weekly.year', $getData->year)
        ->where('calculate_account_weekly.weekoty', $getData->weekoty)
        ->select('calculate_account_weekly.*', 'users_cabinet.name', 
        'typeMT4AvailabeAccount.typeAccount',
        'typeMT4AvailabeAccount.mt4_id',
        'typeMT4AvailabeAccount.kodeGroup',

        )
        ->get();

    } else {
      $getAccount = "there might be wrong parameter you've entered";
    }

    return response()->json([
      'success' => true,
      'data' => $getAccount,
      'client' => $getData
    ]);
  }


  public function accountReconsileWeeklyDetail($trx)
  {

    $getData = DB::table('calculate_account_weekly')->where('calculate_account_weekly.hashCalc', $trx)->first();

    if ($getData) {
      $getAccount = DB::table('raw_data_deal')->where('raw_data_deal.Login', $getData->accountId)->where('Action', '<', '2')->where('VolumeClosed', '>', '0')->where('raw_data_deal.lotyear', $getData->year)->where('raw_data_deal.lotweek', $getData->weekoty)->orderby('Time', 'asc')->get();
      $getAdjust = DB::table('raw_data_deal')->where('raw_data_deal.Login', $getData->accountId)->where('Action', '=', '2')->where('Comment', 'LIKE', '%adj%')->where('raw_data_deal.lotyear', $getData->year)->where('raw_data_deal.lotweek', $getData->weekoty)->orderby('Time', 'asc')->get();
      $getFloating = DB::table('raw_data_position')->where('raw_data_position.Login', $getData->accountId)->where('raw_data_position.lotyear', $getData->year)->where('raw_data_position.lotdate', $getData->dayoty)->orderby('TimeCreate', 'asc')->get();
      // dd($getFloating);
      $getCommission = DB::table('raw_data_deal')->where('raw_data_deal.Login', $getData->accountId)->where('Commission', '<', '0')->where('raw_data_deal.lotyear', $getData->year)->where('raw_data_deal.lotweek', $getData->weekoty)->orderby('Time', 'asc')->get();
    } else {
      $getAccount = "there might be wrong parameter you've entered";
      $getAdjust = 0;
      $getFloating = 0;
      $getCommission = 0;
    }

    return response()->json([
      'success' => true,
      'data' => $getAccount,
      'account' => $getData,
      'adjust' => $getAdjust,
      'floating' => $getFloating,
      'commission' => $getCommission
    ]);
  }
}
