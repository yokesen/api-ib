<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Auth;
use Tarikhagustia\LaravelMt5\LaravelMt5;
use Tarikhagustia\LaravelMt5\Entities\User;
use Tarikhagustia\LaravelMt5\Entities\Trade;
use Tarikh\PhpMeta\MetaTraderClient;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Arr;
use Jenssegers\Agent\Agent;
use Hash;
use GuzzleHttp\Client;
use Tarikh\PhpMeta\Entities\Trade as Trades;
use App\Models\User as Users;
use App\Models\symbolTechnicalAnalysisModel;
use App\Models\symbolTechnicalAnalysis;
use Storage;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class LabController extends Controller
{

  public function asyncTest(){
    dd("HERE");
  }

  public function checkapi()
  {
    $api = new LaravelMt5();
    $user = $api->getTradingAccounts(200001);
    dd($user);
  }

  public function challengeDariFinley($uuid)
  {
    $api = new LaravelMt5();

    $getAccount = DB::table('typeMT4AvailabeAccount')->where('uuid', $uuid)->get();
    // dd($getAccount);
    $totalDeposit = 0;
    $totalProfit = 0;
    $totalEquity = 0;
    foreach ($getAccount as $account) {
      $getReturn = DB::table('raw_data_deal')->where('Login', $account->mt4_id)->where('Entry', '1')->where('Action', '<', '2')->sum('Profit');
      $getDeposit = DB::table('raw_data_deal')->where('Login', $account->mt4_id)->where('Action', '2')->where('Profit', '>', 0)->sum('Profit');
      $user = $api->getTradingAccounts($account->mt4_id);
      // dd($user);
      $equity = $user->Equity;
      $totalDeposit += $getDeposit;
      $totalProfit += $getReturn;
      $totalEquity += $equity;
      // echo '==================<br>account : '.$account->mt4_id.'<br>';
      // echo 'total profit : '.$getReturn."<br>";
      // echo 'total deposit : '.$getDeposit."<br>";
      // echo 'last equity : '.$equity."<br>";
    }

    echo 'total profit : ' . $totalProfit . "<br>";
    echo 'total Deposit : ' . $totalDeposit . "<br>";
    echo 'Equity : ' . $totalEquity . "<br>";

    $return = ($totalProfit / $totalDeposit * 100) + 100;

    echo 'RETURN : ' . $return;
  }

  public function group()
  {
    $api = new LaravelMt5();

    $result = $api->httpGet('/api/group/total', []);
    $total = json_decode($result);
    for ($i = 0; $i < $total->answer->total; $i++) {
      $getGroup = $api->httpGet('/api/group/next?index=' . $i, []);
      $data = json_decode($getGroup);
      $group = $data->answer->Group;
      echo $group . "<br>";
    }
  }

  public function order()
  {
    $senin = strtotime('last monday');
    $jumat = strtotime('last saturday');
    $api = new LaravelMt5();

    $result = $api->httpGet('/api/group/total', []);
    $total = json_decode($result);

    $groups = '';
    for ($i = 0; $i < $total->answer->total; $i++) {
      $getGroup = $api->httpGet('/api/group/next?index=' . $i, []);
      $data = json_decode($getGroup);
      $group = $data->answer->Group;

      $check = str_contains($group, "real");
      echo $group . " " . $check . "<br>";
      if ($check == 1 && $group != 'real\real') {
        if ($groups == '') {
          $groups = $group;
        } else {
          $groups .= ',' . $group;
        }
      }
    }
    $egroup = urlencode($groups);


    $vResult = $api->httpPost('/api/history/get_batch?group=' . $egroup . '&from=' . $senin . '&to=' . $jumat, []);
    $vTotal = json_decode($vResult);
    $data = $vTotal->answer;
    foreach ($data as $v) {
      $catch = DB::table('raw_data_order')->where('OrderId', $v->Order)->count();
      if ($catch < 1) {
        $insert = DB::table('raw_data_order')->insert([
          'OrderId' => $v->Order,
          'ExternalID' => $v->ExternalID,
          'Login' => $v->Login,
          'Dealer' => $v->Dealer,
          'Symbol' => $v->Symbol,
          'Digits' => $v->Digits,
          'ContractSize' => $v->ContractSize,
          'State' => $v->State,
          'Reason' => $v->Reason,
          'TimeSetup' => $v->TimeSetup,
          'TimeExpiration' => $v->TimeExpiration,
          'TimeDone' => $v->TimeDone,
          'TimeSetupMsc' => $v->TimeSetupMsc,
          'TimeDoneMsc' => $v->TimeDoneMsc,
          'ModifyFlags' => $v->ModifyFlags,
          'Type' => $v->Type,
          'TypeFill' => $v->TypeFill,
          'TypeTime' => $v->TypeTime,
          'PriceOrder' => $v->PriceOrder,
          'PriceTrigger' => $v->PriceTrigger,
          'PriceCurrent' => $v->PriceCurrent,
          'PriceSL' => $v->PriceSL,
          'PriceTP' => $v->PriceTP,
          'VolumeInitial' => $v->VolumeInitial,
          'VolumeInitialExt' => $v->VolumeInitialExt,
          'VolumeCurrent' => $v->VolumeCurrent,
          'VolumeCurrentExt' => $v->VolumeCurrentExt,
          'ExpertID' => $v->ExpertID,
          'PositionID' => $v->PositionID,
          'PositionByID' => $v->PositionByID,
          'Comment' => $v->Comment,
          'RateMargin' => $v->RateMargin,
          'ActivationMode' => $v->ActivationMode,
          'ActivationTime' => $v->ActivationTime,
          'ActivationPrice' => $v->ActivationPrice,
          'ActivationFlags' => $v->ActivationFlags,
          'ApiData' => serialize($v->ApiData)
        ]);
      } else {
        // $update = DB::table('raw_data_order')->where('OrderId',$v->Order)->update([
        //   'OrderId' => $v->Order,
        //   'ExternalID' => $v->ExternalID,
        //   'Login' => $v->Login,
        //   'Dealer' => $v->Dealer,
        //   'Symbol' => $v->Symbol,
        //   'Digits' => $v->Digits,
        //   'ContractSize' => $v->ContractSize,
        //   'State' => $v->State,
        //   'Reason' => $v->Reason,
        //   'TimeSetup' => $v->TimeSetup,
        //   'TimeExpiration' => $v->TimeExpiration,
        //   'TimeDone' => $v->TimeDone,
        //   'TimeSetupMsc' => $v->TimeSetupMsc,
        //   'TimeDoneMsc' => $v->TimeDoneMsc,
        //   'ModifyFlags' => $v->ModifyFlags,
        //   'Type' => $v->Type,
        //   'TypeFill' => $v->TypeFill,
        //   'TypeTime' => $v->TypeTime,
        //   'PriceOrder' => $v->PriceOrder,
        //   'PriceTrigger' => $v->PriceTrigger,
        //   'PriceCurrent' => $v->PriceCurrent,
        //   'PriceSL' => $v->PriceSL,
        //   'PriceTP' => $v->PriceTP,
        //   'VolumeInitial' => $v->VolumeInitial,
        //   'VolumeInitialExt' => $v->VolumeInitialExt,
        //   'VolumeCurrent' => $v->VolumeCurrent,
        //   'VolumeCurrentExt' => $v->VolumeCurrentExt,
        //   'ExpertID' => $v->ExpertID,
        //   'PositionID' => $v->PositionID,
        //   'PositionByID' => $v->PositionByID,
        //   'Comment' => $v->Comment,
        //   'RateMargin' => $v->RateMargin,
        //   'ActivationMode' => $v->ActivationMode,
        //   'ActivationTime' => $v->ActivationTime,
        //   'ActivationPrice' => $v->ActivationPrice,
        //   'ActivationFlags' => $v->ActivationFlags,
        //   'ApiData' => serialize($v->ApiData)
        // ]);
      }
    }

  }

  public function position()
  {
    $lotdate = date('z');
    $lotweek = date('W');
    $lotmonth = date('m');
    $lotyear = date('Y');
    $humanTime = date('Y-m-d H:i:s');
    $api = new LaravelMt5();


    $logins = DB::table('typeMT4AvailabeAccount')->distinct()->select('mt4_id')->where('status', 'approved')->get();
    //dd($logins);
    foreach ($logins as $key => $value) {
      echo $value->mt4_id . "<br>";
      $vResult = $api->httpGet('/api/position/get_page?login=' . $value->mt4_id, []);
      $vTotal = json_decode($vResult);
      $data = $vTotal->answer;
      foreach ($data as $v) {
        $catch = DB::table('raw_data_position')->where('Position', $v->Position)->where('lotdate', $lotdate)->where('lotyear', $lotyear)->count();
        if ($catch < 1) {
          $insert = DB::table('raw_data_position')->insert([
            'Position' => $v->Position,
            'ExternalID' => $v->ExternalID,
            'Login' => $v->Login,
            'Dealer' => $v->Dealer,
            'Symbol' => $v->Symbol,
            'Action' => $v->Action,
            'Digits' => $v->Digits,
            'DigitsCurrency' => $v->DigitsCurrency,
            'Reason' => $v->Reason,
            'ContractSize' => $v->ContractSize,
            'TimeCreate' => $v->TimeCreate,
            'TimeUpdate' => $v->TimeUpdate,
            'TimeCreateMsc' => $v->TimeCreateMsc,
            'TimeUpdateMsc' => $v->TimeUpdateMsc,
            'ModifyFlags' => $v->ModifyFlags,
            'PriceOpen' => $v->PriceOpen,
            'PriceCurrent' => $v->PriceCurrent,
            'PriceSL' => $v->PriceSL,
            'PriceTP' => $v->PriceTP,
            'Volume' => $v->Volume,
            'VolumeExt' => $v->VolumeExt,
            'Profit' => $v->Profit,
            'Storage' => $v->Storage,
            'RateProfit' => $v->RateProfit,
            'RateMargin' => $v->RateMargin,
            'ExpertID' => $v->ExpertID,
            'ExpertPositionID' => $v->ExpertPositionID,
            'Comment' => $v->Comment,
            'ActivationMode' => $v->ActivationMode,
            'ActivationTime' => $v->ActivationTime,
            'ActivationPrice' => $v->ActivationPrice,
            'ActivationFlags' => $v->ActivationFlags,
            'ApiData' => serialize($v->ApiData),
            'lotdate' => $lotdate,
            'lotweek' => $lotweek,
            'lotmonth' => $lotmonth,
            'lotyear' => $lotyear,
            'humanTime' => $humanTime
          ]);
          echo "INPUT " . $v->Position . "<br>";
        } else {
          $update = DB::table('raw_data_position')->where('Position', $v->Position)->where('lotdate', $lotdate)->where('lotyear', $lotyear)->update([
            'ExternalID' => $v->ExternalID,
            'Login' => $v->Login,
            'Dealer' => $v->Dealer,
            'Symbol' => $v->Symbol,
            'Action' => $v->Action,
            'Digits' => $v->Digits,
            'DigitsCurrency' => $v->DigitsCurrency,
            'Reason' => $v->Reason,
            'ContractSize' => $v->ContractSize,
            'TimeCreate' => $v->TimeCreate,
            'TimeUpdate' => $v->TimeUpdate,
            'TimeCreateMsc' => $v->TimeCreateMsc,
            'TimeUpdateMsc' => $v->TimeUpdateMsc,
            'ModifyFlags' => $v->ModifyFlags,
            'PriceOpen' => $v->PriceOpen,
            'PriceCurrent' => $v->PriceCurrent,
            'PriceSL' => $v->PriceSL,
            'PriceTP' => $v->PriceTP,
            'Volume' => $v->Volume,
            'VolumeExt' => $v->VolumeExt,
            'Profit' => $v->Profit,
            'Storage' => $v->Storage,
            'RateProfit' => $v->RateProfit,
            'RateMargin' => $v->RateMargin,
            'ExpertID' => $v->ExpertID,
            'ExpertPositionID' => $v->ExpertPositionID,
            'Comment' => $v->Comment,
            'ActivationMode' => $v->ActivationMode,
            'ActivationTime' => $v->ActivationTime,
            'ActivationPrice' => $v->ActivationPrice,
            'ActivationFlags' => $v->ActivationFlags,
            'ApiData' => serialize($v->ApiData),
            'lotdate' => $lotdate,
            'lotweek' => $lotweek,
            'lotmonth' => $lotmonth,
            'lotyear' => $lotyear,
            'humanTime' => $humanTime
          ]);
          echo "update " . $v->Position . "<br>";
        }
      }
    }
  }

  public function xposition()
  {


    $lotdate = date('z');
    $lotweek = date('W');
    $lotmonth = date('m');
    $lotyear = date('Y');
    $humanTime = date('Y-m-d H:i:s');
    $api = new LaravelMt5();

    $result = $api->httpGet('/api/group/total', []);
    $total = json_decode($result);

    $groups = '';
    for ($i = 0; $i < $total->answer->total; $i++) {
      $getGroup = $api->httpGet('/api/group/next?index=' . $i, []);
      $data = json_decode($getGroup);
      $group = $data->answer->Group;

      $check = str_contains($group, "real");
      echo $group . " " . $check . "<br>";
      if ($check == 1 && $group != 'real\real') {
        if ($groups == '') {
          $groups = $group;
        } else {
          $groups .= ',' . $group;
        }
      }
    }
    $egroup = urlencode($groups);

    $vResult = $api->httpGet('/api/position/get_batch?group=' . $egroup, []);
    $vTotal = json_decode($vResult);
    $data = $vTotal->answer;
    foreach ($data as $v) {
      $catch = DB::table('raw_data_position')->where('Position', $v->Position)->where('lotdate', $lotdate)->where('lotyear', $lotyear)->count();
      if ($catch < 1) {
        $insert = DB::table('raw_data_position')->insert([
          'Position' => $v->Position,
          'ExternalID' => $v->ExternalID,
          'Login' => $v->Login,
          'Dealer' => $v->Dealer,
          'Symbol' => $v->Symbol,
          'Action' => $v->Action,
          'Digits' => $v->Digits,
          'DigitsCurrency' => $v->DigitsCurrency,
          'Reason' => $v->Reason,
          'ContractSize' => $v->ContractSize,
          'TimeCreate' => $v->TimeCreate,
          'TimeUpdate' => $v->TimeUpdate,
          'TimeCreateMsc' => $v->TimeCreateMsc,
          'TimeUpdateMsc' => $v->TimeUpdateMsc,
          'ModifyFlags' => $v->ModifyFlags,
          'PriceOpen' => $v->PriceOpen,
          'PriceCurrent' => $v->PriceCurrent,
          'PriceSL' => $v->PriceSL,
          'PriceTP' => $v->PriceTP,
          'Volume' => $v->Volume,
          'VolumeExt' => $v->VolumeExt,
          'Profit' => $v->Profit,
          'Storage' => $v->Storage,
          'RateProfit' => $v->RateProfit,
          'RateMargin' => $v->RateMargin,
          'ExpertID' => $v->ExpertID,
          'ExpertPositionID' => $v->ExpertPositionID,
          'Comment' => $v->Comment,
          'ActivationMode' => $v->ActivationMode,
          'ActivationTime' => $v->ActivationTime,
          'ActivationPrice' => $v->ActivationPrice,
          'ActivationFlags' => $v->ActivationFlags,
          'ApiData' => serialize($v->ApiData),
          'lotdate' => $lotdate,
          'lotweek' => $lotweek,
          'lotmonth' => $lotmonth,
          'lotyear' => $lotyear,
          'humanTime' => $humanTime
        ]);
      } else {
        $update = DB::table('raw_data_position')->where('Position', $v->Position)->where('lotdate', $lotdate)->where('lotyear', $lotyear)->update([
          'ExternalID' => $v->ExternalID,
          'Login' => $v->Login,
          'Dealer' => $v->Dealer,
          'Symbol' => $v->Symbol,
          'Action' => $v->Action,
          'Digits' => $v->Digits,
          'DigitsCurrency' => $v->DigitsCurrency,
          'Reason' => $v->Reason,
          'ContractSize' => $v->ContractSize,
          'TimeCreate' => $v->TimeCreate,
          'TimeUpdate' => $v->TimeUpdate,
          'TimeCreateMsc' => $v->TimeCreateMsc,
          'TimeUpdateMsc' => $v->TimeUpdateMsc,
          'ModifyFlags' => $v->ModifyFlags,
          'PriceOpen' => $v->PriceOpen,
          'PriceCurrent' => $v->PriceCurrent,
          'PriceSL' => $v->PriceSL,
          'PriceTP' => $v->PriceTP,
          'Volume' => $v->Volume,
          'VolumeExt' => $v->VolumeExt,
          'Profit' => $v->Profit,
          'Storage' => $v->Storage,
          'RateProfit' => $v->RateProfit,
          'RateMargin' => $v->RateMargin,
          'ExpertID' => $v->ExpertID,
          'ExpertPositionID' => $v->ExpertPositionID,
          'Comment' => $v->Comment,
          'ActivationMode' => $v->ActivationMode,
          'ActivationTime' => $v->ActivationTime,
          'ActivationPrice' => $v->ActivationPrice,
          'ActivationFlags' => $v->ActivationFlags,
          'ApiData' => serialize($v->ApiData),
          'lotdate' => $lotdate,
          'lotweek' => $lotweek,
          'lotmonth' => $lotmonth,
          'lotyear' => $lotyear,
          'humanTime' => $humanTime
        ]);
      }
    }
  }


  public function deal()
  {
    $senin = strtotime('last monday');
    $jumat = strtotime('this saturday');


    $api = new LaravelMt5();
    $logins = DB::table('typeMT4AvailabeAccount')->distinct()->select('mt4_id')->where('status', 'approved')->get();

    foreach ($logins as $key => $value) {
      echo $value->mt4_id . "<br>";

      $vResult = $api->httpGet('/api/deal/get_page?login=' . $value->mt4_id . '&from=' . $senin . '&to=' . $jumat, []);

      $vTotal = json_decode($vResult);

      $data = $vTotal->answer;
      foreach ($data as $v) {
        $catch = DB::table('raw_data_deal')->where('DealId', $v->Deal)->count();
        if ($catch < 1) {
          $insert = DB::table('raw_data_deal')->insert([
            'DealId' => $v->Deal,
            'ExternalID' => $v->ExternalID,
            'Login' => $v->Login,
            'Dealer' => $v->Dealer,
            'OrderId' => $v->Order,
            'Action' => $v->Action,
            'Entry' => $v->Entry,
            'Reason' => $v->Reason,
            'Digits' => $v->Digits,
            'DigitsCurrency' => $v->DigitsCurrency,
            'ContractSize' => $v->ContractSize,
            'Time' => $v->Time,
            'TimeMsc' => $v->TimeMsc,
            'Symbol' => $v->Symbol,
            'Price' => $v->Price,
            'Volume' => $v->Volume,
            'VolumeExt' => $v->VolumeExt,
            'Profit' => $v->Profit,
            'Storage' => $v->Storage,
            'Commission' => $v->Commission,
            'Fee' => $v->Fee,
            'RateProfit' => $v->RateProfit,
            'RateMargin' => $v->RateMargin,
            'ExpertID' => $v->ExpertID,
            'PositionID' => $v->PositionID,
            'Comment' => $v->Comment,
            'ProfitRaw' => $v->ProfitRaw,
            'PricePosition' => $v->PricePosition,
            'PriceSL' => $v->PriceSL,
            'PriceTP' => $v->PriceTP,
            'VolumeClosed' => $v->VolumeClosed,
            'VolumeClosedExt' => $v->VolumeClosedExt,
            'TickValue' => $v->TickValue,
            'TickSize' => $v->TickSize,
            'Flags' => $v->Flags,
            'Gateway' => $v->Gateway,
            'PriceGateway' => $v->PriceGateway,
            'ModifyFlags' => $v->ModifyFlags,
            'Value' => $v->Value,
            'ApiData' => serialize($v->ApiData),
            'MarketBid' => $v->MarketBid,
            'MarketAsk' => $v->MarketAsk,
            'MarketLast' => $v->MarketLast
          ]);
          echo "INPUT deal no " . $v->Deal . "<br>";
        } else {
          echo "double data, ignored <br>";
          // $update = DB::table('raw_data_deal')->where('DealId',$v->Deal)->update([
          //   'ExternalID' => $v->ExternalID,
          //   'Login' => $v->Login,
          //   'Dealer' => $v->Dealer,
          //   'OrderId' => $v->Order,
          //   'Action' => $v->Action,
          //   'Entry' => $v->Entry,
          //   'Reason' => $v->Reason,
          //   'Digits' => $v->Digits,
          //   'DigitsCurrency' => $v->DigitsCurrency,
          //   'ContractSize' => $v->ContractSize,
          //   'Time' => $v->Time,
          //   'TimeMsc' => $v->TimeMsc,
          //   'Symbol' => $v->Symbol,
          //   'Price' => $v->Price,
          //   'Volume' => $v->Volume,
          //   'VolumeExt' => $v->VolumeExt,
          //   'Profit' => $v->Profit,
          //   'Storage' => $v->Storage,
          //   'Commission' => $v->Commission,
          //   'Fee' => $v->Fee,
          //   'RateProfit' => $v->RateProfit,
          //   'RateMargin' => $v->RateMargin,
          //   'ExpertID' => $v->ExpertID,
          //   'PositionID' => $v->PositionID,
          //   'Comment' => $v->Comment,
          //   'ProfitRaw' => $v->ProfitRaw,
          //   'PricePosition' => $v->PricePosition,
          //   'PriceSL' => $v->PriceSL,
          //   'PriceTP' => $v->PriceTP,
          //   'VolumeClosed' => $v->VolumeClosed,
          //   'VolumeClosedExt' => $v->VolumeClosedExt,
          //   'TickValue' => $v->TickValue,
          //   'TickSize' => $v->TickSize,
          //   'Flags' => $v->Flags,
          //   'Gateway' => $v->Gateway,
          //   'PriceGateway' => $v->PriceGateway,
          //   'ModifyFlags' => $v->ModifyFlags,
          //   'Value' => $v->Value,
          //   'ApiData' => serialize($v->ApiData),
          //   'MarketBid' => $v->MarketBid,
          //   'MarketAsk' => $v->MarketAsk,
          //   'MarketLast' => $v->MarketLast
          // ]);
        }
        echo $v->Time . "<br>";
      }
    }

  }

  public function ydeal()
  {
    $senin = strtotime('last monday');
    $jumat = strtotime('last saturday');
    //dd(date('d-m-Y',$senin),date('d-m-Y',$jumat));

    $api = new LaravelMt5();
    // $egroup = 'PL05\PL05-10K-reg';
    // $vResult = $api->httpGet('/api/deal/get_batch?group='.$egroup.'&from='.$senin.'&to='.$jumat,[]);
    // $vTotal = json_decode($vResult);
    // $data = $vTotal->answer;
    // dd($data);
    $group = DB::table('typeMT4AvailabeAccount')->distinct()->select('kodeGroup')->get();
    foreach ($group as $key => $value) {
      $folder = explode('-', $value->kodeGroup);
      $egroup = $folder[0] . "\\" . $value->kodeGroup;
      echo $egroup . "<br>";
      $vResult = $api->httpGet('/api/deal/get_batch?group=' . $egroup . '&from=' . $senin . '&to=' . $jumat, []);
      $vTotal = json_decode($vResult);
      $data = $vTotal->answer;
      foreach ($data as $v) {
        $catch = DB::table('raw_data_deal')->where('DealId', $v->Deal)->count();
        if ($catch < 1) {
          $insert = DB::table('raw_data_deal')->insert([
            'DealId' => $v->Deal,
            'ExternalID' => $v->ExternalID,
            'Login' => $v->Login,
            'Dealer' => $v->Dealer,
            'OrderId' => $v->Order,
            'Action' => $v->Action,
            'Entry' => $v->Entry,
            'Reason' => $v->Reason,
            'Digits' => $v->Digits,
            'DigitsCurrency' => $v->DigitsCurrency,
            'ContractSize' => $v->ContractSize,
            'Time' => $v->Time,
            'TimeMsc' => $v->TimeMsc,
            'Symbol' => $v->Symbol,
            'Price' => $v->Price,
            'Volume' => $v->Volume,
            'VolumeExt' => $v->VolumeExt,
            'Profit' => $v->Profit,
            'Storage' => $v->Storage,
            'Commission' => $v->Commission,
            'Fee' => $v->Fee,
            'RateProfit' => $v->RateProfit,
            'RateMargin' => $v->RateMargin,
            'ExpertID' => $v->ExpertID,
            'PositionID' => $v->PositionID,
            'Comment' => $v->Comment,
            'ProfitRaw' => $v->ProfitRaw,
            'PricePosition' => $v->PricePosition,
            'PriceSL' => $v->PriceSL,
            'PriceTP' => $v->PriceTP,
            'VolumeClosed' => $v->VolumeClosed,
            'VolumeClosedExt' => $v->VolumeClosedExt,
            'TickValue' => $v->TickValue,
            'TickSize' => $v->TickSize,
            'Flags' => $v->Flags,
            'Gateway' => $v->Gateway,
            'PriceGateway' => $v->PriceGateway,
            'ModifyFlags' => $v->ModifyFlags,
            'Value' => $v->Value,
            'ApiData' => serialize($v->ApiData),
            'MarketBid' => $v->MarketBid,
            'MarketAsk' => $v->MarketAsk,
            'MarketLast' => $v->MarketLast
          ]);
        } else {
          // $update = DB::table('raw_data_deal')->where('DealId',$v->Deal)->update([
          //   'ExternalID' => $v->ExternalID,
          //   'Login' => $v->Login,
          //   'Dealer' => $v->Dealer,
          //   'OrderId' => $v->Order,
          //   'Action' => $v->Action,
          //   'Entry' => $v->Entry,
          //   'Reason' => $v->Reason,
          //   'Digits' => $v->Digits,
          //   'DigitsCurrency' => $v->DigitsCurrency,
          //   'ContractSize' => $v->ContractSize,
          //   'Time' => $v->Time,
          //   'TimeMsc' => $v->TimeMsc,
          //   'Symbol' => $v->Symbol,
          //   'Price' => $v->Price,
          //   'Volume' => $v->Volume,
          //   'VolumeExt' => $v->VolumeExt,
          //   'Profit' => $v->Profit,
          //   'Storage' => $v->Storage,
          //   'Commission' => $v->Commission,
          //   'Fee' => $v->Fee,
          //   'RateProfit' => $v->RateProfit,
          //   'RateMargin' => $v->RateMargin,
          //   'ExpertID' => $v->ExpertID,
          //   'PositionID' => $v->PositionID,
          //   'Comment' => $v->Comment,
          //   'ProfitRaw' => $v->ProfitRaw,
          //   'PricePosition' => $v->PricePosition,
          //   'PriceSL' => $v->PriceSL,
          //   'PriceTP' => $v->PriceTP,
          //   'VolumeClosed' => $v->VolumeClosed,
          //   'VolumeClosedExt' => $v->VolumeClosedExt,
          //   'TickValue' => $v->TickValue,
          //   'TickSize' => $v->TickSize,
          //   'Flags' => $v->Flags,
          //   'Gateway' => $v->Gateway,
          //   'PriceGateway' => $v->PriceGateway,
          //   'ModifyFlags' => $v->ModifyFlags,
          //   'Value' => $v->Value,
          //   'ApiData' => serialize($v->ApiData),
          //   'MarketBid' => $v->MarketBid,
          //   'MarketAsk' => $v->MarketAsk,
          //   'MarketLast' => $v->MarketLast
          // ]);
        }
        echo $v->Time . "<br>";
      }
    }


  }

  public function xdeal()
  {
    $senin = strtotime('yesterday');
    $jumat = strtotime('tomorrow');
    $api = new LaravelMt5();

    $result = $api->httpGet('/api/group/total', []);
    $total = json_decode($result);
    //dd($total);
    $groups = '';
    for ($i = 0; $i < $total->answer->total; $i++) {
      $getGroup = $api->httpGet('/api/group/next?index=' . $i, []);
      $data = json_decode($getGroup);
      $group = $data->answer->Group;

      $check = str_contains($group, "real");
      echo $group . " " . $check . "<br>";
      if ($check == 1 && $group != 'real\real') {
        if ($groups == '') {
          $groups = $group;
        } else {
          $groups .= ',' . $group;
        }
      }
    }
    dd($groups);
    $egroup = urlencode($groups);

    $vResult = $api->httpGet('/api/deal/get_batch?group=' . $egroup . '&from=' . $senin . '&to=' . $jumat, []);
    $vTotal = json_decode($vResult);
    $data = $vTotal->answer;
    foreach ($data as $v) {
      $catch = DB::table('raw_data_deal')->where('DealId', $v->Deal)->count();
      if ($catch < 1) {
        $insert = DB::table('raw_data_deal')->insert([
          'DealId' => $v->Deal,
          'ExternalID' => $v->ExternalID,
          'Login' => $v->Login,
          'Dealer' => $v->Dealer,
          'OrderId' => $v->Order,
          'Action' => $v->Action,
          'Entry' => $v->Entry,
          'Reason' => $v->Reason,
          'Digits' => $v->Digits,
          'DigitsCurrency' => $v->DigitsCurrency,
          'ContractSize' => $v->ContractSize,
          'Time' => $v->Time,
          'TimeMsc' => $v->TimeMsc,
          'Symbol' => $v->Symbol,
          'Price' => $v->Price,
          'Volume' => $v->Volume,
          'VolumeExt' => $v->VolumeExt,
          'Profit' => $v->Profit,
          'Storage' => $v->Storage,
          'Commission' => $v->Commission,
          'Fee' => $v->Fee,
          'RateProfit' => $v->RateProfit,
          'RateMargin' => $v->RateMargin,
          'ExpertID' => $v->ExpertID,
          'PositionID' => $v->PositionID,
          'Comment' => $v->Comment,
          'ProfitRaw' => $v->ProfitRaw,
          'PricePosition' => $v->PricePosition,
          'PriceSL' => $v->PriceSL,
          'PriceTP' => $v->PriceTP,
          'VolumeClosed' => $v->VolumeClosed,
          'VolumeClosedExt' => $v->VolumeClosedExt,
          'TickValue' => $v->TickValue,
          'TickSize' => $v->TickSize,
          'Flags' => $v->Flags,
          'Gateway' => $v->Gateway,
          'PriceGateway' => $v->PriceGateway,
          'ModifyFlags' => $v->ModifyFlags,
          'Value' => $v->Value,
          'ApiData' => serialize($v->ApiData),
          'MarketBid' => $v->MarketBid,
          'MarketAsk' => $v->MarketAsk,
          'MarketLast' => $v->MarketLast
        ]);
      } else {
        // $update = DB::table('raw_data_deal')->where('DealId',$v->Deal)->update([
        //   'ExternalID' => $v->ExternalID,
        //   'Login' => $v->Login,
        //   'Dealer' => $v->Dealer,
        //   'OrderId' => $v->Order,
        //   'Action' => $v->Action,
        //   'Entry' => $v->Entry,
        //   'Reason' => $v->Reason,
        //   'Digits' => $v->Digits,
        //   'DigitsCurrency' => $v->DigitsCurrency,
        //   'ContractSize' => $v->ContractSize,
        //   'Time' => $v->Time,
        //   'TimeMsc' => $v->TimeMsc,
        //   'Symbol' => $v->Symbol,
        //   'Price' => $v->Price,
        //   'Volume' => $v->Volume,
        //   'VolumeExt' => $v->VolumeExt,
        //   'Profit' => $v->Profit,
        //   'Storage' => $v->Storage,
        //   'Commission' => $v->Commission,
        //   'Fee' => $v->Fee,
        //   'RateProfit' => $v->RateProfit,
        //   'RateMargin' => $v->RateMargin,
        //   'ExpertID' => $v->ExpertID,
        //   'PositionID' => $v->PositionID,
        //   'Comment' => $v->Comment,
        //   'ProfitRaw' => $v->ProfitRaw,
        //   'PricePosition' => $v->PricePosition,
        //   'PriceSL' => $v->PriceSL,
        //   'PriceTP' => $v->PriceTP,
        //   'VolumeClosed' => $v->VolumeClosed,
        //   'VolumeClosedExt' => $v->VolumeClosedExt,
        //   'TickValue' => $v->TickValue,
        //   'TickSize' => $v->TickSize,
        //   'Flags' => $v->Flags,
        //   'Gateway' => $v->Gateway,
        //   'PriceGateway' => $v->PriceGateway,
        //   'ModifyFlags' => $v->ModifyFlags,
        //   'Value' => $v->Value,
        //   'ApiData' => serialize($v->ApiData),
        //   'MarketBid' => $v->MarketBid,
        //   'MarketAsk' => $v->MarketAsk,
        //   'MarketLast' => $v->MarketLast
        // ]);
      }
      echo $v->Time . "<br>";
    }
  }

  public function checkPosition($login)
  {
    $api = new LaravelMt5();
    $vResult = $api->httpGet('/api/position/check?login=' . $login, []);
    $vTotal = json_decode($vResult);
    // $senin = strtotime('last monday');
    // $jumat = strtotime('last saturday');
    // $vResult = $api->httpGet('/api/daily/get_light?from='.$senin.'&to='.$jumat.'&login='.$login,[]);
    $vTotal = json_decode($vResult);

    dd($vTotal);
  }

  public function dailyReport()
  {
    $api = new LaravelMt5();

    $fetchStart = $api->httpGet('/api/report/total', []);
    $vTotal = json_decode($fetchStart);
    $total = $vTotal->answer->total;
    for ($i = 0; $i < $total; $i++) {
      echo "[ " . $i . " ]<br>";
      $fetchBegin = $api->httpGet('/api/report/next?index=' . $i, []);
      print_r($fetchBegin);
      echo "<br>============================================================================================<br>";
    }

    // dd($fetchBegin);
  }

  public function testReport($id)
  {
    $api = new LaravelMt5();

    $name = "Daily%20Reports";
    $from = strtotime("2023-01-01");
    $to = strtotime("2023-11-05");
    $fetchStart = $api->httpGet('/api/daily/get?from=' . $from . '&to=' . $to . '&login=26017', []);
    $vTotal = json_decode($fetchStart);

    dd($vTotal, $name);
  }

  public function testgroup()
  {
    $api = new LaravelMt5();

    $vResult = $api->httpGet('/api/report/next?index=18', []);
    $vTotal = json_decode($vResult);


    dd($vTotal);
  }

  public function timeCorrection()
  {
    $getData = DB::table('lot_raw_data')->get();
    foreach ($getData as $v) {
      $time = $v->dataTime;
      $lotdate = date('z', $time);
      $lotweek = date('W', $time);
      $lotmonth = date('m', $time);
      $lotyear = date('Y', $time);
      $update = DB::table('lot_raw_data')->where('id', $v->id)->update([
        'lotdate' => $lotdate,
        'lotweek' => $lotweek,
        'lotmonth' => $lotmonth,
        'lotyear' => $lotyear
      ]);
    }
  }

  public function titip()
  {
    $ntm5 = 0;
    $bullm5 = 0;
    $bearm5 = 0;

    if ($tam5->adl_signal > 0) {
      $bearm5++;
    } elseif ($tam5->adl_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->atr_signal > 0) {
      $bearm5++;
    } elseif ($tam5->atr_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->bb_signal > 0) {
      $bearm5++;
    } elseif ($tam5->bb_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->cmf_signal > 0) {
      $bearm5++;
    } elseif ($tam5->cmf_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->co_signal > 0) {
      $bearm5++;
    } elseif ($tam5->co_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->chm_signal > 0) {
      $bearm5++;
    } elseif ($tam5->chm_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->cci_signal > 0) {
      $bearm5++;
    } elseif ($tam5->cci_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->dpo_signal > 0) {
      $bearm5++;
    } elseif ($tam5->dpo_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->dmi_signal > 0) {
      $bearm5++;
    } elseif ($tam5->dmi_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->dema_signal > 0) {
      $bearm5++;
    } elseif ($tam5->dema_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->eom_signal > 0) {
      $bearm5++;
    } elseif ($tam5->eom_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->enve_signal > 0) {
      $bearm5++;
    } elseif ($tam5->enve_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->fira_signal > 0) {
      $bearm5++;
    } elseif ($tam5->fira_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->fore_signal > 0) {
      $bearm5++;
    } elseif ($tam5->fore_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->imc_signal > 0) {
      $bearm5++;
    } elseif ($tam5->imc_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->imi_signal > 0) {
      $bearm5++;
    } elseif ($tam5->imi_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->klor_signal > 0) {
      $bearm5++;
    } elseif ($tam5->klor_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->lri_signal > 0) {
      $bearm5++;
    } elseif ($tam5->lri_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->lrs_signal > 0) {
      $bearm5++;
    } elseif ($tam5->lrs_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->mfi_signal > 0) {
      $bearm5++;
    } elseif ($tam5->mfi_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->mi_signal > 0) {
      $bearm5++;
    } elseif ($tam5->mi_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->mp_signal > 0) {
      $bearm5++;
    } elseif ($tam5->mp_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->mom_signal > 0) {
      $bearm5++;
    } elseif ($tam5->mom_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->mage_signal > 0) {
      $bearm5++;
    } elseif ($tam5->mage_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->macd_signal > 0) {
      $bearm5++;
    } elseif ($tam5->macd_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->nvi_signal > 0) {
      $bearm5++;
    } elseif ($tam5->nvi_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->obv_signal > 0) {
      $bearm5++;
    } elseif ($tam5->obv_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->psar_signal > 0) {
      $bearm5++;
    } elseif ($tam5->psar_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->pfc_signal > 0) {
      $bearm5++;
    } elseif ($tam5->pfc_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->pvi_signal > 0) {
      $bearm5++;
    } elseif ($tam5->pvi_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->pavt_signal > 0) {
      $bearm5++;
    } elseif ($tam5->pavt_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->pc_signal > 0) {
      $bearm5++;
    } elseif ($tam5->pc_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->po_signal > 0) {
      $bearm5++;
    } elseif ($tam5->po_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->proc_signal > 0) {
      $bearm5++;
    } elseif ($tam5->proc_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->pb_signal > 0) {
      $bearm5++;
    } elseif ($tam5->pb_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->pjo_signal > 0) {
      $bearm5++;
    } elseif ($tam5->pjo_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->qsk_signal > 0) {
      $bearm5++;
    } elseif ($tam5->qsk_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->ri_signal > 0) {
      $bearm5++;
    } elseif ($tam5->ri_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->rmi_signal > 0) {
      $bearm5++;
    } elseif ($tam5->rmi_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->rsi_signal > 0) {
      $bearm5++;
    } elseif ($tam5->rsi_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->rvi_signal > 0) {
      $bearm5++;
    } elseif ($tam5->rvi_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->sdev_signal > 0) {
      $bearm5++;
    } elseif ($tam5->sdev_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->smi_signal > 0) {
      $bearm5++;
    } elseif ($tam5->smi_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->so_signal > 0) {
      $bearm5++;
    } elseif ($tam5->so_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->si_signal > 0) {
      $bearm5++;
    } elseif ($tam5->si_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->rsf_signal > 0) {
      $bearm5++;
    } elseif ($tam5->rsf_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->tema_signal > 0) {
      $bearm5++;
    } elseif ($tam5->tema_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->tp_signal > 0) {
      $bearm5++;
    } elseif ($tam5->tp_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->uo_signal > 0) {
      $bearm5++;
    } elseif ($tam5->uo_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->vhf_signal > 0) {
      $bearm5++;
    } elseif ($tam5->vhf_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->vc_signal > 0) {
      $bearm5++;
    } elseif ($tam5->vc_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->vo_signal > 0) {
      $bearm5++;
    } elseif ($tam5->vo_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->vroc_signal > 0) {
      $bearm5++;
    } elseif ($tam5->vroc_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->wc_signal > 0) {
      $bearm5++;
    } elseif ($tam5->wc_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->ws_signal > 0) {
      $bearm5++;
    } elseif ($tam5->ws_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->wad_signal > 0) {
      $bearm5++;
    } elseif ($tam5->wad_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }
    if ($tam5->wr_signal > 0) {
      $bearm5++;
    } elseif ($tam5->wr_signal < 0) {
      $bullm5++;
    } else {
      $ntm5++;
    }

    $ntm5pct = 100 * $ntm5 / 57;
    $bullm5pct = 100 * $bullm5 / 57;
    $bearm5pct = 100 * $bearm5 / 57;

    $ntm15 = 0;
    $bullm15 = 0;
    $bearm15 = 0;
    if ($tam15->adl_signal > 0) {
      $bearm15++;
    } elseif ($tam15->adl_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->atr_signal > 0) {
      $bearm15++;
    } elseif ($tam15->atr_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->bb_signal > 0) {
      $bearm15++;
    } elseif ($tam15->bb_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->cmf_signal > 0) {
      $bearm15++;
    } elseif ($tam15->cmf_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->co_signal > 0) {
      $bearm15++;
    } elseif ($tam15->co_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->chm_signal > 0) {
      $bearm15++;
    } elseif ($tam15->chm_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->cci_signal > 0) {
      $bearm15++;
    } elseif ($tam15->cci_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->dpo_signal > 0) {
      $bearm15++;
    } elseif ($tam15->dpo_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->dmi_signal > 0) {
      $bearm15++;
    } elseif ($tam15->dmi_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->dema_signal > 0) {
      $bearm15++;
    } elseif ($tam15->dema_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->eom_signal > 0) {
      $bearm15++;
    } elseif ($tam15->eom_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->enve_signal > 0) {
      $bearm15++;
    } elseif ($tam15->enve_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->fira_signal > 0) {
      $bearm15++;
    } elseif ($tam15->fira_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->fore_signal > 0) {
      $bearm15++;
    } elseif ($tam15->fore_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->imc_signal > 0) {
      $bearm15++;
    } elseif ($tam15->imc_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->imi_signal > 0) {
      $bearm15++;
    } elseif ($tam15->imi_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->klor_signal > 0) {
      $bearm15++;
    } elseif ($tam15->klor_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->lri_signal > 0) {
      $bearm15++;
    } elseif ($tam15->lri_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->lrs_signal > 0) {
      $bearm15++;
    } elseif ($tam15->lrs_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->mfi_signal > 0) {
      $bearm15++;
    } elseif ($tam15->mfi_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->mi_signal > 0) {
      $bearm15++;
    } elseif ($tam15->mi_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->mp_signal > 0) {
      $bearm15++;
    } elseif ($tam15->mp_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->mom_signal > 0) {
      $bearm15++;
    } elseif ($tam15->mom_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->mage_signal > 0) {
      $bearm15++;
    } elseif ($tam15->mage_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->macd_signal > 0) {
      $bearm15++;
    } elseif ($tam15->macd_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->nvi_signal > 0) {
      $bearm15++;
    } elseif ($tam15->nvi_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->obv_signal > 0) {
      $bearm15++;
    } elseif ($tam15->obv_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->psar_signal > 0) {
      $bearm15++;
    } elseif ($tam15->psar_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->pfc_signal > 0) {
      $bearm15++;
    } elseif ($tam15->pfc_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->pvi_signal > 0) {
      $bearm15++;
    } elseif ($tam15->pvi_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->pavt_signal > 0) {
      $bearm15++;
    } elseif ($tam15->pavt_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->pc_signal > 0) {
      $bearm15++;
    } elseif ($tam15->pc_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->po_signal > 0) {
      $bearm15++;
    } elseif ($tam15->po_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->proc_signal > 0) {
      $bearm15++;
    } elseif ($tam15->proc_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->pb_signal > 0) {
      $bearm15++;
    } elseif ($tam15->pb_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->pjo_signal > 0) {
      $bearm15++;
    } elseif ($tam15->pjo_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->qsk_signal > 0) {
      $bearm15++;
    } elseif ($tam15->qsk_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->ri_signal > 0) {
      $bearm15++;
    } elseif ($tam15->ri_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->rmi_signal > 0) {
      $bearm15++;
    } elseif ($tam15->rmi_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->rsi_signal > 0) {
      $bearm15++;
    } elseif ($tam15->rsi_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->rvi_signal > 0) {
      $bearm15++;
    } elseif ($tam15->rvi_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->sdev_signal > 0) {
      $bearm15++;
    } elseif ($tam15->sdev_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->smi_signal > 0) {
      $bearm15++;
    } elseif ($tam15->smi_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->so_signal > 0) {
      $bearm15++;
    } elseif ($tam15->so_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->si_signal > 0) {
      $bearm15++;
    } elseif ($tam15->si_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->rsf_signal > 0) {
      $bearm15++;
    } elseif ($tam15->rsf_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->tema_signal > 0) {
      $bearm15++;
    } elseif ($tam15->tema_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->tp_signal > 0) {
      $bearm15++;
    } elseif ($tam15->tp_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->uo_signal > 0) {
      $bearm15++;
    } elseif ($tam15->uo_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->vhf_signal > 0) {
      $bearm15++;
    } elseif ($tam15->vhf_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->vc_signal > 0) {
      $bearm15++;
    } elseif ($tam15->vc_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->vo_signal > 0) {
      $bearm15++;
    } elseif ($tam15->vo_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->vroc_signal > 0) {
      $bearm15++;
    } elseif ($tam15->vroc_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->wc_signal > 0) {
      $bearm15++;
    } elseif ($tam15->wc_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->ws_signal > 0) {
      $bearm15++;
    } elseif ($tam15->ws_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->wad_signal > 0) {
      $bearm15++;
    } elseif ($tam15->wad_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }
    if ($tam15->wr_signal > 0) {
      $bearm15++;
    } elseif ($tam15->wr_signal < 0) {
      $bullm15++;
    } else {
      $ntm15++;
    }

    $ntm15pct = 100 * $ntm15 / 57;
    $bullm15pct = 100 * $bullm15 / 57;
    $bearm15pct = 100 * $bearm15 / 57;

    $ntm30 = 0;
    $bullm30 = 0;
    $bearm30 = 0;

    if ($tam30->adl_signal > 0) {
      $bearm30++;
    } elseif ($tam30->adl_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->atr_signal > 0) {
      $bearm30++;
    } elseif ($tam30->atr_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->bb_signal > 0) {
      $bearm30++;
    } elseif ($tam30->bb_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->cmf_signal > 0) {
      $bearm30++;
    } elseif ($tam30->cmf_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->co_signal > 0) {
      $bearm30++;
    } elseif ($tam30->co_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->chm_signal > 0) {
      $bearm30++;
    } elseif ($tam30->chm_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->cci_signal > 0) {
      $bearm30++;
    } elseif ($tam30->cci_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->dpo_signal > 0) {
      $bearm30++;
    } elseif ($tam30->dpo_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->dmi_signal > 0) {
      $bearm30++;
    } elseif ($tam30->dmi_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->dema_signal > 0) {
      $bearm30++;
    } elseif ($tam30->dema_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->eom_signal > 0) {
      $bearm30++;
    } elseif ($tam30->eom_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->enve_signal > 0) {
      $bearm30++;
    } elseif ($tam30->enve_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->fira_signal > 0) {
      $bearm30++;
    } elseif ($tam30->fira_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->fore_signal > 0) {
      $bearm30++;
    } elseif ($tam30->fore_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->imc_signal > 0) {
      $bearm30++;
    } elseif ($tam30->imc_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->imi_signal > 0) {
      $bearm30++;
    } elseif ($tam30->imi_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->klor_signal > 0) {
      $bearm30++;
    } elseif ($tam30->klor_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->lri_signal > 0) {
      $bearm30++;
    } elseif ($tam30->lri_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->lrs_signal > 0) {
      $bearm30++;
    } elseif ($tam30->lrs_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->mfi_signal > 0) {
      $bearm30++;
    } elseif ($tam30->mfi_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->mi_signal > 0) {
      $bearm30++;
    } elseif ($tam30->mi_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->mp_signal > 0) {
      $bearm30++;
    } elseif ($tam30->mp_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->mom_signal > 0) {
      $bearm30++;
    } elseif ($tam30->mom_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->mage_signal > 0) {
      $bearm30++;
    } elseif ($tam30->mage_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->macd_signal > 0) {
      $bearm30++;
    } elseif ($tam30->macd_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->nvi_signal > 0) {
      $bearm30++;
    } elseif ($tam30->nvi_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->obv_signal > 0) {
      $bearm30++;
    } elseif ($tam30->obv_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->psar_signal > 0) {
      $bearm30++;
    } elseif ($tam30->psar_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->pfc_signal > 0) {
      $bearm30++;
    } elseif ($tam30->pfc_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->pvi_signal > 0) {
      $bearm30++;
    } elseif ($tam30->pvi_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->pavt_signal > 0) {
      $bearm30++;
    } elseif ($tam30->pavt_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->pc_signal > 0) {
      $bearm30++;
    } elseif ($tam30->pc_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->po_signal > 0) {
      $bearm30++;
    } elseif ($tam30->po_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->proc_signal > 0) {
      $bearm30++;
    } elseif ($tam30->proc_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->pb_signal > 0) {
      $bearm30++;
    } elseif ($tam30->pb_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->pjo_signal > 0) {
      $bearm30++;
    } elseif ($tam30->pjo_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->qsk_signal > 0) {
      $bearm30++;
    } elseif ($tam30->qsk_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->ri_signal > 0) {
      $bearm30++;
    } elseif ($tam30->ri_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->rmi_signal > 0) {
      $bearm30++;
    } elseif ($tam30->rmi_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->rsi_signal > 0) {
      $bearm30++;
    } elseif ($tam30->rsi_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->rvi_signal > 0) {
      $bearm30++;
    } elseif ($tam30->rvi_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->sdev_signal > 0) {
      $bearm30++;
    } elseif ($tam30->sdev_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->smi_signal > 0) {
      $bearm30++;
    } elseif ($tam30->smi_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->so_signal > 0) {
      $bearm30++;
    } elseif ($tam30->so_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->si_signal > 0) {
      $bearm30++;
    } elseif ($tam30->si_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->rsf_signal > 0) {
      $bearm30++;
    } elseif ($tam30->rsf_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->tema_signal > 0) {
      $bearm30++;
    } elseif ($tam30->tema_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->tp_signal > 0) {
      $bearm30++;
    } elseif ($tam30->tp_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->uo_signal > 0) {
      $bearm30++;
    } elseif ($tam30->uo_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->vhf_signal > 0) {
      $bearm30++;
    } elseif ($tam30->vhf_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->vc_signal > 0) {
      $bearm30++;
    } elseif ($tam30->vc_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->vo_signal > 0) {
      $bearm30++;
    } elseif ($tam30->vo_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->vroc_signal > 0) {
      $bearm30++;
    } elseif ($tam30->vroc_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->wc_signal > 0) {
      $bearm30++;
    } elseif ($tam30->wc_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->ws_signal > 0) {
      $bearm30++;
    } elseif ($tam30->ws_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->wad_signal > 0) {
      $bearm30++;
    } elseif ($tam30->wad_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }
    if ($tam30->wr_signal > 0) {
      $bearm30++;
    } elseif ($tam30->wr_signal < 0) {
      $bullm30++;
    } else {
      $ntm30++;
    }

    $ntm30pct = 100 * $ntm30 / 57;
    $bullm30pct = 100 * $bullm30 / 57;
    $bearm30pct = 100 * $bearm30 / 57;

    $nth1 = 0;
    $bullh1 = 0;
    $bearh1 = 0;

    if ($tah1->adl_signal > 0) {
      $bearh1++;
    } elseif ($tah1->adl_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->atr_signal > 0) {
      $bearh1++;
    } elseif ($tah1->atr_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->bb_signal > 0) {
      $bearh1++;
    } elseif ($tah1->bb_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->cmf_signal > 0) {
      $bearh1++;
    } elseif ($tah1->cmf_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->co_signal > 0) {
      $bearh1++;
    } elseif ($tah1->co_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->chm_signal > 0) {
      $bearh1++;
    } elseif ($tah1->chm_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->cci_signal > 0) {
      $bearh1++;
    } elseif ($tah1->cci_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->dpo_signal > 0) {
      $bearh1++;
    } elseif ($tah1->dpo_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->dmi_signal > 0) {
      $bearh1++;
    } elseif ($tah1->dmi_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->dema_signal > 0) {
      $bearh1++;
    } elseif ($tah1->dema_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->eom_signal > 0) {
      $bearh1++;
    } elseif ($tah1->eom_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->enve_signal > 0) {
      $bearh1++;
    } elseif ($tah1->enve_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->fira_signal > 0) {
      $bearh1++;
    } elseif ($tah1->fira_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->fore_signal > 0) {
      $bearh1++;
    } elseif ($tah1->fore_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->imc_signal > 0) {
      $bearh1++;
    } elseif ($tah1->imc_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->imi_signal > 0) {
      $bearh1++;
    } elseif ($tah1->imi_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->klor_signal > 0) {
      $bearh1++;
    } elseif ($tah1->klor_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->lri_signal > 0) {
      $bearh1++;
    } elseif ($tah1->lri_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->lrs_signal > 0) {
      $bearh1++;
    } elseif ($tah1->lrs_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->mfi_signal > 0) {
      $bearh1++;
    } elseif ($tah1->mfi_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->mi_signal > 0) {
      $bearh1++;
    } elseif ($tah1->mi_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->mp_signal > 0) {
      $bearh1++;
    } elseif ($tah1->mp_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->mom_signal > 0) {
      $bearh1++;
    } elseif ($tah1->mom_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->mage_signal > 0) {
      $bearh1++;
    } elseif ($tah1->mage_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->macd_signal > 0) {
      $bearh1++;
    } elseif ($tah1->macd_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->nvi_signal > 0) {
      $bearh1++;
    } elseif ($tah1->nvi_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->obv_signal > 0) {
      $bearh1++;
    } elseif ($tah1->obv_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->psar_signal > 0) {
      $bearh1++;
    } elseif ($tah1->psar_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->pfc_signal > 0) {
      $bearh1++;
    } elseif ($tah1->pfc_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->pvi_signal > 0) {
      $bearh1++;
    } elseif ($tah1->pvi_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->pavt_signal > 0) {
      $bearh1++;
    } elseif ($tah1->pavt_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->pc_signal > 0) {
      $bearh1++;
    } elseif ($tah1->pc_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->po_signal > 0) {
      $bearh1++;
    } elseif ($tah1->po_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->proc_signal > 0) {
      $bearh1++;
    } elseif ($tah1->proc_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->pb_signal > 0) {
      $bearh1++;
    } elseif ($tah1->pb_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->pjo_signal > 0) {
      $bearh1++;
    } elseif ($tah1->pjo_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->qsk_signal > 0) {
      $bearh1++;
    } elseif ($tah1->qsk_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->ri_signal > 0) {
      $bearh1++;
    } elseif ($tah1->ri_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->rmi_signal > 0) {
      $bearh1++;
    } elseif ($tah1->rmi_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->rsi_signal > 0) {
      $bearh1++;
    } elseif ($tah1->rsi_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->rvi_signal > 0) {
      $bearh1++;
    } elseif ($tah1->rvi_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->sdev_signal > 0) {
      $bearh1++;
    } elseif ($tah1->sdev_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->smi_signal > 0) {
      $bearh1++;
    } elseif ($tah1->smi_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->so_signal > 0) {
      $bearh1++;
    } elseif ($tah1->so_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->si_signal > 0) {
      $bearh1++;
    } elseif ($tah1->si_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->rsf_signal > 0) {
      $bearh1++;
    } elseif ($tah1->rsf_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->tema_signal > 0) {
      $bearh1++;
    } elseif ($tah1->tema_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->tp_signal > 0) {
      $bearh1++;
    } elseif ($tah1->tp_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->uo_signal > 0) {
      $bearh1++;
    } elseif ($tah1->uo_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->vhf_signal > 0) {
      $bearh1++;
    } elseif ($tah1->vhf_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->vc_signal > 0) {
      $bearh1++;
    } elseif ($tah1->vc_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->vo_signal > 0) {
      $bearh1++;
    } elseif ($tah1->vo_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->vroc_signal > 0) {
      $bearh1++;
    } elseif ($tah1->vroc_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->wc_signal > 0) {
      $bearh1++;
    } elseif ($tah1->wc_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->ws_signal > 0) {
      $bearh1++;
    } elseif ($tah1->ws_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->wad_signal > 0) {
      $bearh1++;
    } elseif ($tah1->wad_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }
    if ($tah1->wr_signal > 0) {
      $bearh1++;
    } elseif ($tah1->wr_signal < 0) {
      $bullh1++;
    } else {
      $nth1++;
    }

    $nth1pct = 100 * $nth1 / 57;
    $bullh1pct = 100 * $bullh1 / 57;
    $bearh1pct = 100 * $bearh1 / 57;

    $nth4 = 0;
    $bullh4 = 0;
    $bearh4 = 0;

    if ($tah4->adl_signal > 0) {
      $bearh4++;
    } elseif ($tah4->adl_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->atr_signal > 0) {
      $bearh4++;
    } elseif ($tah4->atr_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->bb_signal > 0) {
      $bearh4++;
    } elseif ($tah4->bb_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->cmf_signal > 0) {
      $bearh4++;
    } elseif ($tah4->cmf_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->co_signal > 0) {
      $bearh4++;
    } elseif ($tah4->co_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->chm_signal > 0) {
      $bearh4++;
    } elseif ($tah4->chm_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->cci_signal > 0) {
      $bearh4++;
    } elseif ($tah4->cci_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->dpo_signal > 0) {
      $bearh4++;
    } elseif ($tah4->dpo_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->dmi_signal > 0) {
      $bearh4++;
    } elseif ($tah4->dmi_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->dema_signal > 0) {
      $bearh4++;
    } elseif ($tah4->dema_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->eom_signal > 0) {
      $bearh4++;
    } elseif ($tah4->eom_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->enve_signal > 0) {
      $bearh4++;
    } elseif ($tah4->enve_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->fira_signal > 0) {
      $bearh4++;
    } elseif ($tah4->fira_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->fore_signal > 0) {
      $bearh4++;
    } elseif ($tah4->fore_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->imc_signal > 0) {
      $bearh4++;
    } elseif ($tah4->imc_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->imi_signal > 0) {
      $bearh4++;
    } elseif ($tah4->imi_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->klor_signal > 0) {
      $bearh4++;
    } elseif ($tah4->klor_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->lri_signal > 0) {
      $bearh4++;
    } elseif ($tah4->lri_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->lrs_signal > 0) {
      $bearh4++;
    } elseif ($tah4->lrs_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->mfi_signal > 0) {
      $bearh4++;
    } elseif ($tah4->mfi_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->mi_signal > 0) {
      $bearh4++;
    } elseif ($tah4->mi_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->mp_signal > 0) {
      $bearh4++;
    } elseif ($tah4->mp_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->mom_signal > 0) {
      $bearh4++;
    } elseif ($tah4->mom_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->mage_signal > 0) {
      $bearh4++;
    } elseif ($tah4->mage_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->macd_signal > 0) {
      $bearh4++;
    } elseif ($tah4->macd_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->nvi_signal > 0) {
      $bearh4++;
    } elseif ($tah4->nvi_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->obv_signal > 0) {
      $bearh4++;
    } elseif ($tah4->obv_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->psar_signal > 0) {
      $bearh4++;
    } elseif ($tah4->psar_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->pfc_signal > 0) {
      $bearh4++;
    } elseif ($tah4->pfc_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->pvi_signal > 0) {
      $bearh4++;
    } elseif ($tah4->pvi_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->pavt_signal > 0) {
      $bearh4++;
    } elseif ($tah4->pavt_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->pc_signal > 0) {
      $bearh4++;
    } elseif ($tah4->pc_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->po_signal > 0) {
      $bearh4++;
    } elseif ($tah4->po_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->proc_signal > 0) {
      $bearh4++;
    } elseif ($tah4->proc_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->pb_signal > 0) {
      $bearh4++;
    } elseif ($tah4->pb_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->pjo_signal > 0) {
      $bearh4++;
    } elseif ($tah4->pjo_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->qsk_signal > 0) {
      $bearh4++;
    } elseif ($tah4->qsk_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->ri_signal > 0) {
      $bearh4++;
    } elseif ($tah4->ri_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->rmi_signal > 0) {
      $bearh4++;
    } elseif ($tah4->rmi_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->rsi_signal > 0) {
      $bearh4++;
    } elseif ($tah4->rsi_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->rvi_signal > 0) {
      $bearh4++;
    } elseif ($tah4->rvi_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->sdev_signal > 0) {
      $bearh4++;
    } elseif ($tah4->sdev_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->smi_signal > 0) {
      $bearh4++;
    } elseif ($tah4->smi_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->so_signal > 0) {
      $bearh4++;
    } elseif ($tah4->so_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->si_signal > 0) {
      $bearh4++;
    } elseif ($tah4->si_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->rsf_signal > 0) {
      $bearh4++;
    } elseif ($tah4->rsf_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->tema_signal > 0) {
      $bearh4++;
    } elseif ($tah4->tema_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->tp_signal > 0) {
      $bearh4++;
    } elseif ($tah4->tp_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->uo_signal > 0) {
      $bearh4++;
    } elseif ($tah4->uo_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->vhf_signal > 0) {
      $bearh4++;
    } elseif ($tah4->vhf_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->vc_signal > 0) {
      $bearh4++;
    } elseif ($tah4->vc_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->vo_signal > 0) {
      $bearh4++;
    } elseif ($tah4->vo_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->vroc_signal > 0) {
      $bearh4++;
    } elseif ($tah4->vroc_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->wc_signal > 0) {
      $bearh4++;
    } elseif ($tah4->wc_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->ws_signal > 0) {
      $bearh4++;
    } elseif ($tah4->ws_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->wad_signal > 0) {
      $bearh4++;
    } elseif ($tah4->wad_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }
    if ($tah4->wr_signal > 0) {
      $bearh4++;
    } elseif ($tah4->wr_signal < 0) {
      $bullh4++;
    } else {
      $nth4++;
    }

    $nth4pct = 100 * $nth4 / 57;
    $bullh4pct = 100 * $bullh4 / 57;
    $bearh4pct = 100 * $bearh4 / 57;

    $ntd1 = 0;
    $bulld1 = 0;
    $beard1 = 0;

    if ($tad1->adl_signal > 0) {
      $beard1++;
    } elseif ($tad1->adl_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->atr_signal > 0) {
      $beard1++;
    } elseif ($tad1->atr_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->bb_signal > 0) {
      $beard1++;
    } elseif ($tad1->bb_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->cmf_signal > 0) {
      $beard1++;
    } elseif ($tad1->cmf_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->co_signal > 0) {
      $beard1++;
    } elseif ($tad1->co_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->chm_signal > 0) {
      $beard1++;
    } elseif ($tad1->chm_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->cci_signal > 0) {
      $beard1++;
    } elseif ($tad1->cci_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->dpo_signal > 0) {
      $beard1++;
    } elseif ($tad1->dpo_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->dmi_signal > 0) {
      $beard1++;
    } elseif ($tad1->dmi_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->dema_signal > 0) {
      $beard1++;
    } elseif ($tad1->dema_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->eom_signal > 0) {
      $beard1++;
    } elseif ($tad1->eom_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->enve_signal > 0) {
      $beard1++;
    } elseif ($tad1->enve_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->fira_signal > 0) {
      $beard1++;
    } elseif ($tad1->fira_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->fore_signal > 0) {
      $beard1++;
    } elseif ($tad1->fore_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->imc_signal > 0) {
      $beard1++;
    } elseif ($tad1->imc_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->imi_signal > 0) {
      $beard1++;
    } elseif ($tad1->imi_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->klor_signal > 0) {
      $beard1++;
    } elseif ($tad1->klor_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->lri_signal > 0) {
      $beard1++;
    } elseif ($tad1->lri_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->lrs_signal > 0) {
      $beard1++;
    } elseif ($tad1->lrs_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->mfi_signal > 0) {
      $beard1++;
    } elseif ($tad1->mfi_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->mi_signal > 0) {
      $beard1++;
    } elseif ($tad1->mi_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->mp_signal > 0) {
      $beard1++;
    } elseif ($tad1->mp_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->mom_signal > 0) {
      $beard1++;
    } elseif ($tad1->mom_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->mage_signal > 0) {
      $beard1++;
    } elseif ($tad1->mage_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->macd_signal > 0) {
      $beard1++;
    } elseif ($tad1->macd_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->nvi_signal > 0) {
      $beard1++;
    } elseif ($tad1->nvi_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->obv_signal > 0) {
      $beard1++;
    } elseif ($tad1->obv_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->psar_signal > 0) {
      $beard1++;
    } elseif ($tad1->psar_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->pfc_signal > 0) {
      $beard1++;
    } elseif ($tad1->pfc_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->pvi_signal > 0) {
      $beard1++;
    } elseif ($tad1->pvi_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->pavt_signal > 0) {
      $beard1++;
    } elseif ($tad1->pavt_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->pc_signal > 0) {
      $beard1++;
    } elseif ($tad1->pc_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->po_signal > 0) {
      $beard1++;
    } elseif ($tad1->po_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->proc_signal > 0) {
      $beard1++;
    } elseif ($tad1->proc_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->pb_signal > 0) {
      $beard1++;
    } elseif ($tad1->pb_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->pjo_signal > 0) {
      $beard1++;
    } elseif ($tad1->pjo_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->qsk_signal > 0) {
      $beard1++;
    } elseif ($tad1->qsk_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->ri_signal > 0) {
      $beard1++;
    } elseif ($tad1->ri_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->rmi_signal > 0) {
      $beard1++;
    } elseif ($tad1->rmi_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->rsi_signal > 0) {
      $beard1++;
    } elseif ($tad1->rsi_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->rvi_signal > 0) {
      $beard1++;
    } elseif ($tad1->rvi_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->sdev_signal > 0) {
      $beard1++;
    } elseif ($tad1->sdev_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->smi_signal > 0) {
      $beard1++;
    } elseif ($tad1->smi_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->so_signal > 0) {
      $beard1++;
    } elseif ($tad1->so_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->si_signal > 0) {
      $beard1++;
    } elseif ($tad1->si_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->rsf_signal > 0) {
      $beard1++;
    } elseif ($tad1->rsf_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->tema_signal > 0) {
      $beard1++;
    } elseif ($tad1->tema_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->tp_signal > 0) {
      $beard1++;
    } elseif ($tad1->tp_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->uo_signal > 0) {
      $beard1++;
    } elseif ($tad1->uo_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->vhf_signal > 0) {
      $beard1++;
    } elseif ($tad1->vhf_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->vc_signal > 0) {
      $beard1++;
    } elseif ($tad1->vc_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->vo_signal > 0) {
      $beard1++;
    } elseif ($tad1->vo_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->vroc_signal > 0) {
      $beard1++;
    } elseif ($tad1->vroc_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->wc_signal > 0) {
      $beard1++;
    } elseif ($tad1->wc_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->ws_signal > 0) {
      $beard1++;
    } elseif ($tad1->ws_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->wad_signal > 0) {
      $beard1++;
    } elseif ($tad1->wad_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }
    if ($tad1->wr_signal > 0) {
      $beard1++;
    } elseif ($tad1->wr_signal < 0) {
      $bulld1++;
    } else {
      $ntd1++;
    }

    $ntd1pct = 100 * $ntd1 / 57;
    $bulld1pct = 100 * $bulld1 / 57;
    $beard1pct = 100 * $beard1 / 57;

    $ntw1 = 0;
    $bullw1 = 0;
    $bearw1 = 0;

    if ($taw1->adl_signal > 0) {
      $bearw1++;
    } elseif ($taw1->adl_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->atr_signal > 0) {
      $bearw1++;
    } elseif ($taw1->atr_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->bb_signal > 0) {
      $bearw1++;
    } elseif ($taw1->bb_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->cmf_signal > 0) {
      $bearw1++;
    } elseif ($taw1->cmf_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->co_signal > 0) {
      $bearw1++;
    } elseif ($taw1->co_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->chm_signal > 0) {
      $bearw1++;
    } elseif ($taw1->chm_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->cci_signal > 0) {
      $bearw1++;
    } elseif ($taw1->cci_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->dpo_signal > 0) {
      $bearw1++;
    } elseif ($taw1->dpo_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->dmi_signal > 0) {
      $bearw1++;
    } elseif ($taw1->dmi_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->dema_signal > 0) {
      $bearw1++;
    } elseif ($taw1->dema_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->eom_signal > 0) {
      $bearw1++;
    } elseif ($taw1->eom_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->enve_signal > 0) {
      $bearw1++;
    } elseif ($taw1->enve_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->fira_signal > 0) {
      $bearw1++;
    } elseif ($taw1->fira_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->fore_signal > 0) {
      $bearw1++;
    } elseif ($taw1->fore_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->imc_signal > 0) {
      $bearw1++;
    } elseif ($taw1->imc_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->imi_signal > 0) {
      $bearw1++;
    } elseif ($taw1->imi_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->klor_signal > 0) {
      $bearw1++;
    } elseif ($taw1->klor_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->lri_signal > 0) {
      $bearw1++;
    } elseif ($taw1->lri_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->lrs_signal > 0) {
      $bearw1++;
    } elseif ($taw1->lrs_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->mfi_signal > 0) {
      $bearw1++;
    } elseif ($taw1->mfi_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->mi_signal > 0) {
      $bearw1++;
    } elseif ($taw1->mi_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->mp_signal > 0) {
      $bearw1++;
    } elseif ($taw1->mp_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->mom_signal > 0) {
      $bearw1++;
    } elseif ($taw1->mom_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->mage_signal > 0) {
      $bearw1++;
    } elseif ($taw1->mage_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->macd_signal > 0) {
      $bearw1++;
    } elseif ($taw1->macd_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->nvi_signal > 0) {
      $bearw1++;
    } elseif ($taw1->nvi_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->obv_signal > 0) {
      $bearw1++;
    } elseif ($taw1->obv_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->psar_signal > 0) {
      $bearw1++;
    } elseif ($taw1->psar_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->pfc_signal > 0) {
      $bearw1++;
    } elseif ($taw1->pfc_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->pvi_signal > 0) {
      $bearw1++;
    } elseif ($taw1->pvi_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->pavt_signal > 0) {
      $bearw1++;
    } elseif ($taw1->pavt_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->pc_signal > 0) {
      $bearw1++;
    } elseif ($taw1->pc_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->po_signal > 0) {
      $bearw1++;
    } elseif ($taw1->po_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->proc_signal > 0) {
      $bearw1++;
    } elseif ($taw1->proc_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->pb_signal > 0) {
      $bearw1++;
    } elseif ($taw1->pb_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->pjo_signal > 0) {
      $bearw1++;
    } elseif ($taw1->pjo_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->qsk_signal > 0) {
      $bearw1++;
    } elseif ($taw1->qsk_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->ri_signal > 0) {
      $bearw1++;
    } elseif ($taw1->ri_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->rmi_signal > 0) {
      $bearw1++;
    } elseif ($taw1->rmi_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->rsi_signal > 0) {
      $bearw1++;
    } elseif ($taw1->rsi_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->rvi_signal > 0) {
      $bearw1++;
    } elseif ($taw1->rvi_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->sdev_signal > 0) {
      $bearw1++;
    } elseif ($taw1->sdev_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->smi_signal > 0) {
      $bearw1++;
    } elseif ($taw1->smi_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->so_signal > 0) {
      $bearw1++;
    } elseif ($taw1->so_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->si_signal > 0) {
      $bearw1++;
    } elseif ($taw1->si_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->rsf_signal > 0) {
      $bearw1++;
    } elseif ($taw1->rsf_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->tema_signal > 0) {
      $bearw1++;
    } elseif ($taw1->tema_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->tp_signal > 0) {
      $bearw1++;
    } elseif ($taw1->tp_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->uo_signal > 0) {
      $bearw1++;
    } elseif ($taw1->uo_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->vhf_signal > 0) {
      $bearw1++;
    } elseif ($taw1->vhf_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->vc_signal > 0) {
      $bearw1++;
    } elseif ($taw1->vc_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->vo_signal > 0) {
      $bearw1++;
    } elseif ($taw1->vo_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->vroc_signal > 0) {
      $bearw1++;
    } elseif ($taw1->vroc_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->wc_signal > 0) {
      $bearw1++;
    } elseif ($taw1->wc_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->ws_signal > 0) {
      $bearw1++;
    } elseif ($taw1->ws_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->wad_signal > 0) {
      $bearw1++;
    } elseif ($taw1->wad_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }
    if ($taw1->wr_signal > 0) {
      $bearw1++;
    } elseif ($taw1->wr_signal < 0) {
      $bullw1++;
    } else {
      $ntw1++;
    }

    $ntw1pct = 100 * $ntw1 / 57;
    $bullw1pct = 100 * $bullw1 / 57;
    $bearw1pct = 100 * $bearw1 / 57;

    $ntmn1 = 0;
    $bullmn1 = 0;
    $bearmn1 = 0;

    if ($tamn1->adl_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->adl_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->atr_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->atr_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->bb_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->bb_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->cmf_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->cmf_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->co_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->co_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->chm_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->chm_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->cci_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->cci_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->dpo_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->dpo_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->dmi_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->dmi_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->dema_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->dema_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->eom_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->eom_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->enve_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->enve_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->fira_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->fira_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->fore_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->fore_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->imc_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->imc_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->imi_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->imi_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->klor_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->klor_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->lri_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->lri_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->lrs_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->lrs_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->mfi_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->mfi_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->mi_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->mi_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->mp_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->mp_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->mom_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->mom_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->mage_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->mage_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->macd_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->macd_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->nvi_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->nvi_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->obv_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->obv_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->psar_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->psar_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->pfc_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->pfc_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->pvi_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->pvi_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->pavt_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->pavt_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->pc_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->pc_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->po_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->po_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->proc_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->proc_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->pb_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->pb_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->pjo_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->pjo_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->qsk_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->qsk_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->ri_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->ri_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->rmi_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->rmi_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->rsi_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->rsi_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->rvi_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->rvi_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->sdev_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->sdev_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->smi_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->smi_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->so_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->so_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->si_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->si_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->rsf_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->rsf_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->tema_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->tema_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->tp_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->tp_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->uo_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->uo_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->vhf_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->vhf_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->vc_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->vc_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->vo_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->vo_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->vroc_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->vroc_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->wc_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->wc_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->ws_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->ws_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->wad_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->wad_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }
    if ($tamn1->wr_signal > 0) {
      $bearmn1++;
    } elseif ($tamn1->wr_signal < 0) {
      $bullmn1++;
    } else {
      $ntmn1++;
    }

    $ntmn1pct = 100 * $ntmn1 / 57;
    $bullmn1pct = 100 * $bullmn1 / 57;
    $bearmn1pct = 100 * $bearmn1 / 57;
  }




  public function createMt5($uuid)
  {

    $user = DB::table('users_cabinet')->where('uuid', $uuid)->first();
    /*CREATE MT5*/
    $api = new LaravelMt5();
    $group = "demo\\forex.hedged";
    $mainPassword = "Demo12354";
    $investorPassword = "Demo12352";
    $name = str_replace(' ', '%20', $user->name);
    $email = $user->email;
    $phone = $user->whatsapp;
    $address = "from%20register";
    $city = "register";
    $create = $api->httpPost('/api/user/add?login=0&pass_main=' . $mainPassword . '&pass_investor=' . $investorPassword . '&leverage=100&group=' . $group . '&name=' . $name . '&email=' . $email . '&phone=' . $phone . '&address=' . $address . '&city=' . $city, []);
    $data = json_decode($create);
    $trade = new Trades();
    $trade->setLogin($data->answer->Login);
    $trade->setAmount(10000);
    $trade->setComment("Deposit Demo 10000");
    $trade->setType(Trades::DEAL_BALANCE);
    // dd($trade);
    $result = $api->trade($trade);

    $insert = DB::table('typeMT4AvailabeAccount')->insertGetId([
      'typeAccount' => 'demo',
      'parent' => $user->parent,
      'uuid' => $uuid,
      'mt4_id' => $data->answer->Login,
      'tipe_deposit' => '10',
      'status' => 'approved',
      'password' => $mainPassword,
      'kodeGroup' => $group,
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s')
    ]);

    return $data->answer->Login;
  }

  public function dataAccountMt5(Request $request)
  {
    $api = new LaravelMt5();
    $this_week = date('W');
    $today = date('z');
    $last30day = $today - 30;
    $this_year = date('Y');
    $resultBatch = $api->httpGet('/api/user/get?login=' . $request->login, []);
    $dataBatch = json_decode($resultBatch);

    // dd($dataBatch);
    if ($dataBatch->retcode == "0 Done") {
      $grup = $dataBatch->answer->Group;

      $user = $api->getTradingAccounts($request->login);

      $balance = $user->Balance;
      $equity = $user->Equity;
      $freeMargin = $user->MarginFree;

      $mt5Account = DB::table('typeMT4AvailabeAccount')->where('mt4_id', $request->login)->first();
      $typemt5 = DB::table('typeMT4MasterTemplate')->where('masterName', $mt5Account->typeAccount)->first();
      if ($mt5Account->typeAccount == 'demo') {
        return response()->json([
          'status' => 'failed',
          'data' => "Demo Account Cannot do Trade"
        ], 400);
      }

      $pair = $request->symbol;
      $baseSymbol = explode(".", $pair);

      // dd($mt5Account, $typemt5);
      $pair = $baseSymbol[0] . $typemt5->sufix;

      $symbolGroup = $api->httpGet('/api/symbol/get_group?symbol=' . $pair . '&group=' . $grup, []);
      $sGroup = json_decode($symbolGroup);

      $leverage = $dataBatch->answer->Leverage;
      $MarginInitial = $sGroup->answer->MarginInitial;
      $VolumeMin = $sGroup->answer->VolumeMin / 10000;
      $VolumeMax = $sGroup->answer->VolumeMax / 10000;
      $VolumeStep = $sGroup->answer->VolumeStep / 10000;
      $Digit = $sGroup->answer->Digits;
      $margin_requirement = $MarginInitial / $leverage;
      $maxVolumeFreeMargin = $freeMargin / $margin_requirement;

      switch ($sGroup->answer->VolumeStep) {
        case '100':
          $fraction = 2;
          break;
        case '1000':
          $fraction = 1;
          break;
        case '10000':
          $fraction = 0;
          break;
        default:
          $fraction = 0;
          break;
      }

      if ($maxVolumeFreeMargin > $VolumeMax) {
        $maxVolumeFreeMargin = $VolumeMax;
      }
    }

    return response()->json([
      'success' => true,
      'data' => [
        'digit' => $Digit,
        'today' => $today,
        'last30day' => $last30day,
        'pair' => $pair,
        'grup' => $grup,
        'login' => $user->Login,
        'equity' => number_format($equity, 2, '.', ''),
        'freeMargin' => number_format($freeMargin, 2, '.', ''),
        'leverage' => $leverage,
        'marginRequirement' => number_format($margin_requirement, 0, '.', ''),
        'VolumeMin' => number_format($VolumeMin, 2, '.', ''),
        'VolumeMax' => number_format($VolumeMax, 2, '.', ''),
        'VolumeStep' => number_format($VolumeStep, 2, '.', ''),
        'MaxVolumeFromFreeMargin' => number_format($maxVolumeFreeMargin, 2, '.', ''),
      ]
    ]);
  }

  public function selfinsight(Request $request)
  {

    $year = date('Y');

    $countSelfInsight = DB::table('account_statistic')->where('account', $request->login)->count();
    $accountData = DB::table('typeMT4AvailabeAccount')->select('created_at', 'uuid')->where('mt4_id', $request->login)->first();
    if ($countSelfInsight > 0) {
      $perSymbol = DB::table('account_statistic')->select(DB::raw("`buy_numb` + `sell_numb` as total_numb"), 'sell_numb', 'buy_numb', 'nickname', 'symbol', 'buy_profit', 'sell_profit')->where('account', $request->login)->orderBy(DB::raw("`buy_numb` + `sell_numb`"), 'desc')->get();
      // dd($perSymbol);;
      $favPair = (object) [
        'pair' => '',
        'qty' => 0,
      ];

      // dd($favPair->qty);
      foreach ($perSymbol as $ps) {
        $totalTrade = $ps->total_numb;
        $totalProfit = $ps->buy_profit + $ps->sell_profit;
        $percentage_profit = $totalProfit * 100 / $totalTrade;
        $ps->percentageProfit = $percentage_profit;
        $nickname = $ps->nickname;
        if (($ps->buy_numb + $ps->sell_numb) > $favPair->qty) {
          $favPair->qty = $ps->buy_numb + $ps->sell_numb;
          $pair = explode("_", $ps->symbol);
          $favPair->pair = $pair[0];
        }
      }
    } else {
      $nickname = '';
      $favPair = (object) [
        'pair' => null,
        'qty' => 0,
      ];
      $perSymbol = [];
    }

    $checkTipe = DB::table('typeMT4AvailabeAccount')->where('mt4_id', $request->login)->first();

    $account_statistics = DB::table('account_statistic')->where('account', $request->login)->get();
    $buy_profit = 0;
    $sell_profit = 0;
    $buy_loss = 0;
    $sell_loss = 0;
    $buy_numb = 0;
    $sell_numb = 0;

    foreach ($account_statistics as $account_statistic) {
      $buy_profit += $account_statistic->buy_profit;
      $sell_profit += $account_statistic->sell_profit;
      $buy_loss += $account_statistic->buy_loss;
      $sell_loss += $account_statistic->sell_loss;
      $buy_numb += $account_statistic->buy_numb;
      $sell_numb += $account_statistic->sell_numb;
    }

    if ($buy_profit == 0 && $sell_profit == 0 && $buy_loss == 0 && $sell_numb == 0 && $sell_loss == 0 && $buy_numb == 0) {
      $hasil = 0;
    } else {
      $hasil = (($buy_profit + $sell_profit - $buy_loss - $sell_loss) / ($buy_numb + $sell_numb)) * 100;
    }


    $deposit = DB::table('raw_data_deal')->where('Volume', 0)->where('Login', $request->login)->sum('Profit');
    $avg_pl = DB::table('raw_data_deal')->where('Entry', 1)->where('Login', $request->login)->avg('Profit');

    if ($deposit == 0 or $avg_pl == 0) {
      $risk = 0;
    } else {
      $risk = abs($avg_pl / $deposit);
    }

    if ($risk < 0.05) {
      $behave = 'conservative';
    } elseif ($risk > 0.05 && $risk < 0.25) {
      $behave = 'moderate';
    } elseif ($risk > 0.25) {
      $behave = 'aggresive';
    }

    return response()->json([
      'success' => true,
      'data' => [
        'nickname' => $nickname,
        'favPair' => $favPair,
        'created_at' => $accountData->created_at,
        'listFavPair' => $perSymbol,
        'return' => $hasil,
        'behave' => $behave,
        'uuid' => $accountData->uuid
      ]
    ]);
  }

  public function selfInsightTotalGraph(Request $request)
  {
    $countSelfInsight = DB::table('account_statistic')->where('account', $request->login)->count();
    // dd($countSelfInsight);
    if ($countSelfInsight > 0) {
      $data = DB::table('account_statistic')->where('account', $request->login)->get();
      $totalBuy = 0;
      $totalSell = 0;
      $profitBuy = 0;
      $profitSell = 0;
      $lossBuy = 0;
      $lossSell = 0;
      foreach ($data as $datum) {
        $totalBuy += $datum->buy_numb;
        $totalSell += $datum->sell_numb;
        $profitBuy += $datum->buy_profit;
        $lossBuy += $datum->buy_loss;
        $profitSell += $datum->sell_profit;
        $lossSell += $datum->sell_loss;
      }
      if ($totalBuy != 0 || $totalLoss != 0) {
        $percentageBuy = $totalBuy / ($totalBuy + $totalSell) * 100;
        $percentageSell = $totalSell / ($totalBuy + $totalSell) * 100;
        $percentageProfitBuy = $profitBuy / $totalBuy * 100;
        $percentageLossBuy = $lossBuy / $totalBuy * 100;
        $percentageProfitSell = $profitSell / $totalSell * 100;
        $percentageLossSell = $lossSell / $totalSell * 100;
      } else {
        $percentageBuy = 0;
        $percentageSell = 0;
        $percentageLossSell = 0;
        $percentageProfitSell = 0;
        $percentageLossBuy = 0;
        $percentageProfitBuy = 0;
      }

      $data = [number_format((float) $percentageBuy, 2, '.', ''), number_format((float) $percentageProfitSell, 2, '.', ''), number_format((float) $percentageLossSell, 2, '.', ''), number_format((float) $percentageSell, 2, '.', ''), number_format((float) $percentageLossBuy, 2, '.', ''), number_format((float) $percentageProfitBuy, 2, '.', '')];
    } else {
      $data = [0, 0, 0, 0, 0, 0];
    }


    return response()->json([
      'success' => true,
      'data' => $data,
      'totalTrading' => $totalBuy + $totalSell,
      'totalProfit' => $profitBuy + $profitSell,
      'totalLoss' => $lossBuy + $lossSell
    ]);
  }

  public function filltypemt4accountschooselevel()
  {
    dd($senin = strtotime('last monday'), $jumat = strtotime('this saturday'));
    // // $users = DB::table('users_ib')->where('seniorib', 0)->orWhere('masterIb', 0)->get();
    // $users = DB::table('users_ib')->where('seniorib', 0)->orWhere('masterIb', 0)->get();
    // // return response()->json($users);
    // foreach($users as $user){
    //   $types = DB::table('typeMT4MasterTemplate')->get();
    //
    //   foreach($types as $type){
    //     $dbUsername = DB::table('cities')->where('used','0')->first();
    //     $timesufix = substr(strval(time()),-2);
    //     $name = $user->name;
    //     $prefixs = explode(' ',$name); //okay
    //     $prefix = ''; //okay
    //     foreach ($prefixs as $pre) { //okay
    //       $pref = substr($pre,0,1); //okay
    //       $prefix .= strtoupper($pref); //okay
    //     }
    //     $group = $prefix.$timesufix; //okay
    //     // /dd($username,$group);
    //     $updateDbUsername =  DB::table('cities')->where('id',$dbUsername->id)->update([
    //       'used' => '1'
    //     ]); //okay
    //     $group = $group."-".$type->masterRate."-".$type->masterGroup;
    //     $inputData = DB::table('typeMT4_accounts_choose_level')->insert([
    //           'categoryAccount' => $user->id,
    //           'namaAccount' => $type->masterName,
    //           'accountGroup' => $group,
    //           'bestChoice' => "yes",
    //           'status' => "active",
    //     ]);
    //   }
    //
    // }
    //
    // return response()->json([
    //   'status'=>'success'
    // ]);
  }

  public function TopTraderWeek(Request $request)
  {
    $this_week = date('W');
    $this_year = date('Y');

    $data = DB::table('raw_data_deal')
      ->where('OrderId', '!=', 0)
      ->select(DB::raw("SUM(`Profit`) AS `Profit`"), 'Login')
      ->where('lotweek', $this_week)
      ->where('lotyear', $this_year)
      ->where('Entry', '1')
      ->where('Action', '<', '2')
      ->groupBy('Login')
      ->groupBy('lotweek')
      ->groupBy('lotyear')
      ->orderBy('Profit', 'DESC')
      ->take(10)
      ->get();

    $row = [];
    if ($data->isNotEmpty()) {

      foreach ($data as $key => $value) {
        $accountData = DB::table('typeMT4AvailabeAccount')->where('mt4_id', $value->Login)->first();
        if ($accountData) {
          $perSymbol = DB::table('account_statistic')->select(DB::raw("`buy_numb` + `sell_numb` as total_numb"), 'sell_numb', 'buy_numb', 'nickname', 'symbol', 'buy_profit', 'sell_profit')->where('account', $value->Login)->orderBy(DB::raw("`buy_numb` + `sell_numb`"), 'desc')->get();
          $detail[$key] = (object) [
            'mt4_id' => '',
            'nickname' => '',
            'pair' => '',
            'qty' => 0,
            'behave' => null,
            'return' => 0
          ];
          foreach ($perSymbol as $ps) {
            $totalTrade = $ps->total_numb;
            $totalProfit = $ps->buy_profit + $ps->sell_profit;
            $percentage_profit = $totalProfit * 100 / $totalTrade;
            $ps->percentageProfit = $percentage_profit;
            $nickname = $ps->nickname;
            $detail[$key]->nickname = $nickname;
            if (($ps->buy_numb + $ps->sell_numb) > $detail[$key]->qty) {
              $detail[$key]->qty = $ps->buy_numb + $ps->sell_numb;
              $pair = explode("_", $ps->symbol);
              $detail[$key]->pair = $pair[0];
            }
          }

          $deposit = DB::table('raw_data_deal')->where('Volume', 0)->where('Login', $value->Login)->sum('Profit');
          $avg_pl = DB::table('raw_data_deal')->where('Entry', 1)->where('Login', $value->Login)->avg('Profit');

          if ($deposit == 0 or $avg_pl == 0) {
            $risk = 0;
          } else {
            $risk = abs($avg_pl / $deposit);
          }

          if ($risk < 0.05) {
            $behave = 'conservative';
          } elseif ($risk > 0.05 && $risk < 0.25) {
            $behave = 'moderate';
          } elseif ($risk > 0.25) {
            $behave = 'aggresive';
          }

          $detail[$key]->mt4_id = $value->Login;

          $detail[$key]->behave = $behave;

          $getReturnAfter = 0;
          $getReturnBefore = 0;

          $getReturnAfter = DB::table('raw_data_deal')->where('Login', $value->Login)->where('Entry', '1')->where('lotweek', $this_week)->where('lotyear', $this_year)->where('Action', '<', '2')->sum('Profit');
          $getReturnBefore = DB::table('raw_data_deal')->where('Login', $value->Login)->where('Entry', '1')->where('lotweek', $this_week - 1)->where('lotyear', $this_year)->where('Action', '<', '2')->sum('Profit');

          if ($getReturnAfter == 0 or $getReturnBefore == 0) {
            $return = 0;
          } else {
            $return = ($getReturnAfter / $getReturnBefore * 100) + 100;
          }

          $detail[$key]->return = $return;

        } else {

          $detail[$key] = (object) [
            'mt4_id' => '',
            'nickname' => '',
            'pair' => null,
            'qty' => 0,
            'behave' => null,
            'return' => 0
          ];
        }
      }

      // dd(collect($detail));

      return response()->json([
        'success' => true,
        'data' => [
          'data' => $detail,
        ]
      ]);

    } else {

      $this_week_1 = date('W') - 1;
      $this_year_1 = date('Y');

      // $datas = DB::table('raw_data_deal')->where('OrderId','!=',0)->where('lotweek',$this_week_1)->where('lotyear',$this_year_1)->orderBy('Profit','DESC')->take(10)->distinct()->get('Login');

      $datas = DB::table('raw_data_deal')
        ->where('OrderId', '!=', 0)
        ->select(DB::raw('SUM(Profit) AS Profit'), 'Login', 'lotweek', 'lotyear')
        ->where('lotweek', $this_week_1)
        ->where('lotyear', $this_year_1)
        ->where('Entry', '1')
        ->where('Action', '<', '2')
        ->groupBy('Login')
        ->groupBy('lotweek')
        ->groupBy('lotyear')
        ->orderBy('Profit', 'DESC')
        ->take(10)
        ->get();

      if ($datas->isEmpty()) {
        $this_week_1 = date('W') - 2;
        $datas = DB::table('raw_data_deal')
          ->where('OrderId', '!=', 0)
          ->select(DB::raw('SUM(Profit) AS Profit'), 'Login', 'lotweek', 'lotyear')
          ->where('lotweek', $this_week_1)
          ->where('lotyear', $this_year_1)
          ->where('Entry', '1')
          ->where('Action', '<', '2')
          ->groupBy('Login')
          ->groupBy('lotweek')
          ->groupBy('lotyear')
          ->orderBy('Profit', 'DESC')
          ->take(10)
          ->get();
      }
      // dd($this_week_1);
      if ($datas->isNotEmpty()) {

        foreach ($datas as $key => $value) {
          $accountData = DB::table('typeMT4AvailabeAccount')->where('mt4_id', $value->Login)->first();
          if ($accountData) {
            $perSymbol = DB::table('account_statistic')->select(DB::raw("`buy_numb` + `sell_numb` as total_numb"), 'sell_numb', 'buy_numb', 'nickname', 'symbol', 'buy_profit', 'sell_profit')->where('account', $value->Login)->orderBy(DB::raw("`buy_numb` + `sell_numb`"), 'desc')->get();
            // dd($perSymbol);
            $detail[$key] = (object) [
              'nickname' => '',
              'pair' => '',
              'qty' => 0,
              'behave' => null,
              'return' => 0
            ];
            foreach ($perSymbol as $ps) {
              $totalTrade = $ps->total_numb;
              $totalProfit = $ps->buy_profit + $ps->sell_profit;
              $percentage_profit = $totalProfit * 100 / $totalTrade;
              $ps->percentageProfit = $percentage_profit;
              $nickname = $ps->nickname;
              $detail[$key]->nickname = $nickname;
              if (($ps->buy_numb + $ps->sell_numb) > $detail[$key]->qty) {
                $detail[$key]->qty = $ps->buy_numb + $ps->sell_numb;
                $pair = explode("_", $ps->symbol);
                $detail[$key]->pair = $pair[0];
              }
            }

            $deposit = DB::table('raw_data_deal')->where('Volume', 0)->where('Login', $value->Login)->sum('Profit');
            $avg_pl = DB::table('raw_data_deal')->where('Entry', 1)->where('Login', $value->Login)->avg('Profit');

            if ($deposit == 0 or $avg_pl == 0) {
              $risk = 0;
            } else {
              $risk = abs($avg_pl / $deposit);
            }

            if ($risk < 0.05) {
              $behave = 'conservative';
            } elseif ($risk > 0.05 && $risk < 0.25) {
              $behave = 'moderate';
            } elseif ($risk > 0.25) {
              $behave = 'aggresive';
            }

            $detail[$key]->behave = $behave;
            $detail[$key]->mt4_id = $value->Login;
            $getReturnAfter = 0;
            $getReturnBefore = 0;

            $getReturnAfter = DB::table('raw_data_deal')->where('Login', $value->Login)->where('Entry', '1')->where('Action', '<', '2')->where('lotweek', $this_week_1)->where('lotyear', $this_year_1)->sum('Profit');
            $getReturnBefore = DB::table('raw_data_deal')->where('Login', $value->Login)->where('Entry', '1')->where('Action', '<', '2')->where('lotweek', $this_week_1 - 1)->where('lotyear', $this_year_1)->sum('Profit');

            if ($getReturnBefore == 0) {
              $getDeposit = DB::table('raw_data_deal')->where('Login', $value->Login)->where('Action', '2')->where('Profit', '>', 0)->sum('Profit');

              $return = ($getReturnAfter / $getDeposit * 100) + 100;

              // $return = 0;
            } else {
              // $return = (($getReturnAfter - $getReturnBefore)/$getReturnBefore)*100;
              $return = ($getReturnAfter / $getReturnBefore * 100) + 100;
            }

            $detail[$key]->return = $return;

          } else {

            $detail[$key] = (object) [
              'nickname' => '',
              'pair' => null,
              'qty' => 0,
              'behave' => null,
              'return' => 0
            ];
          }
        }
      } else {
        $detail = (object) [
          'nickname' => '',
          'pair' => null,
          'qty' => 0,
          'behave' => null,
          'return' => 0
        ];
      }

      // dd(collect($detail));
      return response()->json([
        'success' => true,
        'data' => [
          'data' => $detail,
        ]
      ]);

      // return response()->json([
      //   'success' => 'failed',
      //   'data' => 'Belum ada trader yang deal position'
      // ],400);
    }
  }

  public function VolumePrice()
  {
    // $date = date('Y-m-d');
    //DB::raw('SUM(volume_price) as total_volume')
    // $data = DB::table('symbol_technical_analysis')->select('symbol','volume_price as total_volume', 'created_at')
    //         ->where('timeframe','M5')
    //         ->whereBetween('created_at', [Carbon::now()->timezone('Asia/Dubai')->subDay(), Carbon::now('UTC')->timezone('Asia/Dubai')])
    //         ->groupBy('symbol')
    //         ->orderBy('total_volume','DESC')
    //         ->get();
    $data = DB::table('pair_volume')
      ->select('symbol', 'total_volume', 'price', 'percentage')
      ->orderBy('total_volume', 'DESC')
      ->get();



    return response()->json([
      'success' => true,
      'data' => $data,
    ]);
  }

  public function getAccountAmount()
  {
    $pendingReceivable = DB::table('partner_deposits')->select('amount')->where('uuid', auth()->user()->uuid)->where('status', 'pending')->sum();

    $pendingPayable = DB::table('partner_deposits')->select()->where('parent', auth()->user()->parent)->where('status', 'pending')->sum('amount');

    $data = (object) ['payable' => $pendingPayable, 'receivable' => $pendingReceivable];

    return $data;
  }

  public function returnProfitGrafik($login)
  {

    $getReturn = DB::table('raw_data_deal')
      ->select(DB::raw('SUM(Profit) as profits'), DB::raw('DATE(humanTime) as humanTimes'))
      ->where('Login', $login)
      ->where('Entry', '1')
      ->where('Action', '<', '2')
      ->groupBy('humanTimes')
      ->get();

    // dd($getReturn);

    foreach ($getReturn as $key => $value) {
      $getDeposit = DB::table('raw_data_deal')
        ->where('Login', $login)
        ->where('Action', '2')
        ->where('Profit', '>', 0)
        ->whereDate('humanTime', '<=', $value->humanTimes)
        ->sum('Profit');

      // $getProfit = DB::table('raw_data_deal')
      //             ->where('Login',$login)
      //             ->where('Entry','1')
      //             ->where('Action','<','2')
      //             ->whereDate('humanTime', '<=', $value->humanTimes)
      //             ->sum('Profit');

      // $getTotal = $getDeposit + $getProfit;

      // dd($getTotal);
      $getReturn[$key]->persentase = ($value->profits / $getDeposit * 100);
      // $getReturn[$key]->persentase = $getTotal;

    }

    $data = $getReturn;

    // dd($getReturn);


    return response()->json([
      'success' => true,
      'data' => $data
    ]);
  }

  public function TopAllTraderWeek(Request $request)
  {
    $this_week = date('W');
    $this_year = date('Y');

    $data = DB::table('raw_data_deal')
      ->where('OrderId', '!=', 0)
      ->select(DB::raw("SUM(`Profit`) AS `Profit`"), 'Login')
      ->where('lotweek', $this_week)
      ->where('lotyear', $this_year)
      ->where('Entry', '1')
      ->where('Action', '<', '2')
      ->groupBy('Login')
      ->groupBy('lotweek')
      ->groupBy('lotyear')
      ->orderBy('Profit', 'DESC')
      ->get();

    $row = [];
    if ($data->isNotEmpty()) {

      foreach ($data as $key => $value) {
        $accountData = DB::table('typeMT4AvailabeAccount')->where('mt4_id', $value->Login)->first();
        if ($accountData) {
          $perSymbol = DB::table('account_statistic')->select(DB::raw("`buy_numb` + `sell_numb` as total_numb"), 'sell_numb', 'buy_numb', 'nickname', 'symbol', 'buy_profit', 'sell_profit')->where('account', $value->Login)->orderBy(DB::raw("`buy_numb` + `sell_numb`"), 'desc')->get();
          $detail[$key] = (object) [
            'mt4_id' => '',
            'nickname' => '',
            'pair' => '',
            'qty' => 0,
            'behave' => null,
            'return' => 0
          ];
          foreach ($perSymbol as $ps) {
            $totalTrade = $ps->total_numb;
            $totalProfit = $ps->buy_profit + $ps->sell_profit;
            $percentage_profit = $totalProfit * 100 / $totalTrade;
            $ps->percentageProfit = $percentage_profit;
            $nickname = $ps->nickname;
            $detail[$key]->nickname = $nickname;
            if (($ps->buy_numb + $ps->sell_numb) > $detail[$key]->qty) {
              $detail[$key]->qty = $ps->buy_numb + $ps->sell_numb;
              $pair = explode("_", $ps->symbol);
              $detail[$key]->pair = $pair[0];
            }
          }

          $deposit = DB::table('raw_data_deal')->where('Volume', 0)->where('Login', $value->Login)->sum('Profit');
          $avg_pl = DB::table('raw_data_deal')->where('Entry', 1)->where('Login', $value->Login)->avg('Profit');

          if ($deposit == 0 or $avg_pl == 0) {
            $risk = 0;
          } else {
            $risk = abs($avg_pl / $deposit);
          }

          if ($risk < 0.05) {
            $behave = 'conservative';
          } elseif ($risk > 0.05 && $risk < 0.25) {
            $behave = 'moderate';
          } elseif ($risk > 0.25) {
            $behave = 'aggresive';
          }

          $detail[$key]->mt4_id = $value->Login;

          $detail[$key]->behave = $behave;

          $getReturnAfter = 0;
          $getReturnBefore = 0;

          $getReturnAfter = DB::table('raw_data_deal')->where('Login', $value->Login)->where('Entry', '1')->where('lotweek', $this_week)->where('lotyear', $this_year)->where('Action', '<', '2')->sum('Profit');
          $getReturnBefore = DB::table('raw_data_deal')->where('Login', $value->Login)->where('Entry', '1')->where('lotweek', $this_week - 1)->where('lotyear', $this_year)->where('Action', '<', '2')->sum('Profit');

          if ($getReturnAfter == 0 or $getReturnBefore == 0) {
            $return = 0;
          } else {
            $return = ($getReturnAfter / $getReturnBefore * 100) + 100;
          }

          $detail[$key]->return = $return;

        } else {

          $detail[$key] = (object) [
            'mt4_id' => '',
            'nickname' => '',
            'pair' => null,
            'qty' => 0,
            'behave' => null,
            'return' => 0
          ];
        }
      }

      // dd(collect($detail));

      return response()->json([
        'success' => true,
        'data' => [
          'data' => $detail,
        ]
      ]);

    } else {

      $this_week_1 = date('W') - 1;
      $this_year_1 = date('Y');

      // $datas = DB::table('raw_data_deal')->where('OrderId','!=',0)->where('lotweek',$this_week_1)->where('lotyear',$this_year_1)->orderBy('Profit','DESC')->take(10)->distinct()->get('Login');

      $datas = DB::table('raw_data_deal')
        ->where('OrderId', '!=', 0)
        ->select(DB::raw('SUM(Profit) AS Profit'), 'Login', 'lotweek', 'lotyear')
        ->where('lotweek', $this_week_1)
        ->where('lotyear', $this_year_1)
        ->where('Entry', '1')
        ->where('Action', '<', '2')
        ->groupBy('Login')
        ->groupBy('lotweek')
        ->groupBy('lotyear')
        ->orderBy('Profit', 'DESC')
        ->get();

      if ($datas->isEmpty()) {
        $this_week_1 = date('W') - 2;
        $datas = DB::table('raw_data_deal')
          ->where('OrderId', '!=', 0)
          ->select(DB::raw('SUM(Profit) AS Profit'), 'Login', 'lotweek', 'lotyear')
          ->where('lotweek', $this_week_1)
          ->where('lotyear', $this_year_1)
          ->where('Entry', '1')
          ->where('Action', '<', '2')
          ->groupBy('Login')
          ->groupBy('lotweek')
          ->groupBy('lotyear')
          ->orderBy('Profit', 'DESC')
          ->get();
      }
      // dd($this_week_1);
      if ($datas->isNotEmpty()) {

        foreach ($datas as $key => $value) {
          $accountData = DB::table('typeMT4AvailabeAccount')->where('mt4_id', $value->Login)->first();
          if ($accountData) {
            $perSymbol = DB::table('account_statistic')->select(DB::raw("`buy_numb` + `sell_numb` as total_numb"), 'sell_numb', 'buy_numb', 'nickname', 'symbol', 'buy_profit', 'sell_profit')->where('account', $value->Login)->orderBy(DB::raw("`buy_numb` + `sell_numb`"), 'desc')->get();
            // dd($perSymbol);
            $detail[$key] = (object) [
              'nickname' => '',
              'pair' => '',
              'qty' => 0,
              'behave' => null,
              'return' => 0
            ];
            foreach ($perSymbol as $ps) {
              $totalTrade = $ps->total_numb;
              $totalProfit = $ps->buy_profit + $ps->sell_profit;
              $percentage_profit = $totalProfit * 100 / $totalTrade;
              $ps->percentageProfit = $percentage_profit;
              $nickname = $ps->nickname;
              $detail[$key]->nickname = $nickname;
              if (($ps->buy_numb + $ps->sell_numb) > $detail[$key]->qty) {
                $detail[$key]->qty = $ps->buy_numb + $ps->sell_numb;
                $pair = explode("_", $ps->symbol);
                $detail[$key]->pair = $pair[0];
              }
            }

            $deposit = DB::table('raw_data_deal')->where('Volume', 0)->where('Login', $value->Login)->sum('Profit');
            $avg_pl = DB::table('raw_data_deal')->where('Entry', 1)->where('Login', $value->Login)->avg('Profit');

            if ($deposit == 0 or $avg_pl == 0) {
              $risk = 0;
            } else {
              $risk = abs($avg_pl / $deposit);
            }

            if ($risk < 0.05) {
              $behave = 'conservative';
            } elseif ($risk > 0.05 && $risk < 0.25) {
              $behave = 'moderate';
            } elseif ($risk > 0.25) {
              $behave = 'aggresive';
            }

            $detail[$key]->behave = $behave;
            $detail[$key]->mt4_id = $value->Login;

            $getReturnAfter = 0;
            $getReturnBefore = 0;

            $getReturnAfter = DB::table('raw_data_deal')->where('Login', $value->Login)->where('Entry', '1')->where('Action', '<', '2')->where('lotweek', $this_week_1)->where('lotyear', $this_year_1)->sum('Profit');
            $getReturnBefore = DB::table('raw_data_deal')->where('Login', $value->Login)->where('Entry', '1')->where('Action', '<', '2')->where('lotweek', $this_week_1 - 1)->where('lotyear', $this_year_1)->sum('Profit');

            if ($getReturnBefore == 0) {
              $getDeposit = DB::table('raw_data_deal')->where('Login', $value->Login)->where('Action', '2')->where('Profit', '>', 0)->sum('Profit');

              $return = ($getReturnAfter / $getDeposit * 100) + 100;

              // $return = 0;
            } else {
              // $return = (($getReturnAfter - $getReturnBefore)/$getReturnBefore)*100;
              $return = ($getReturnAfter / $getReturnBefore * 100) + 100;
            }

            $detail[$key]->return = $return;

          } else {

            $detail[$key] = (object) [
              'nickname' => '',
              'pair' => null,
              'qty' => 0,
              'behave' => null,
              'return' => 0
            ];
          }
        }
      } else {
        $detail = (object) [
          'nickname' => '',
          'pair' => null,
          'qty' => 0,
          'behave' => null,
          'return' => 0
        ];
      }

      // dd(collect($detail));
      return response()->json([
        'success' => true,
        'data' => [
          'data' => $detail,
        ]
      ]);

      // return response()->json([
      //   'success' => 'failed',
      //   'data' => 'Belum ada trader yang deal position'
      // ],400);
    }
  }


  // MINDAH GROUP ABC
  public function pindahgroupMT5()
  {
    $api = new LaravelMt5();

    // MINDAH GROUP di MT5 sesuai dgn IMS
    // FETCH USER PARENT == 54 (ABC)
    // FOREACH typeMT4AvailabeAccount WHERE UUID WHERE KodeGroup != demo\fib\FGT09\FIR51\IA18\

    $fetchUserAbc = DB::connection('live')
      ->table('typeMT4AvailabeAccount')
      ->where('parent', 54)
      ->where(function ($query) {
        $query->where('typeMT4AvailabeAccount.kodeGroup', 'not like', '%demo\\\fib\\\FGT09\\\FIR51\\\IA18\\\%');
      })
      ->get();
    
    // dd($fetchUserAbc);


    foreach ($fetchUserAbc as $v) {
      $account = $v->mt4_id;
      // dd($account);
      // JALANKAN FUNCTION PERPINDAHAN 
      $user = DB::connection('live')->table('users_cabinet')->where('uuid', $v->uuid)->first();
      $typeaccount = DB::connection('live')->table('typeMT4Accounts')->where('namaAccount', $v->typeAccount)->where('categoryAccount', $user->parent)->first();

      $ib = DB::connection('live')->table('users_ib')->where('id', $user->parent)->first();
      $mib = DB::connection('live')->table('users_ib')->where('id', $ib->masterib)->first();
      $sib = DB::connection('live')->table('users_ib')->where('id', $ib->seniorib)->first();
      $groupAccount = urlencode($typeaccount->accountGroup);
      $ex = explode('-', $groupAccount);
      $ngroup = $ex[0] . "-N-" . $ex[1] . "-" . $ex[2];
      $group = env('GROUP_MT5_SERVER') . $sib->groupMT5 . "\\" . $mib->groupMT5 . "\\" . $ib->groupMT5 . "\\" . $ngroup;

      $create = $api->httpPost('/api/user/update?login=' . $account . '&group=' . $group, []);
      $dataHasilUpdateGroup = json_decode($create);

      // UPDATE KODEGROUP

      $update = DB::connection('live')->table('typeMT4AvailabeAccount')->where('id', $v->id)->update([
        'kodeGroup' => $group,
        'deposit' => 1
      ]);
      echo "berhasil pindah " . $v->mt4_id . " ke grup " . $group."<br>";
    }
  }

    // MINDAH GROUP FINANCIA
    public function pindahgroupMT5ffx()
    {
      $api = new LaravelMt5();
  
      // MINDAH GROUP di MT5 sesuai dgn IMS
      // FETCH USER PARENT == 54 (ABC)
      // FOREACH typeMT4AvailabeAccount WHERE UUID WHERE KodeGroup != demo\fib\FGT09\FIR51\IA18\
  
      $fetchUserFfx = DB::connection('live')
        ->table('typeMT4AvailabeAccount')
        ->where('parent', 55)
        ->where(function ($query) {
          $query->where('typeMT4AvailabeAccount.kodeGroup', 'not like', '%demo\\\fib\\\FGT09\\\FIR51\\\FF43\\\%');
        })
        ->get();
      
      // dd($fetchUserFfx);
  
  
      foreach ($fetchUserFfx as $v) {
        $account = $v->mt4_id;
        // dd($account);
        // JALANKAN FUNCTION PERPINDAHAN 
        $user = DB::connection('live')->table('users_cabinet')->where('uuid', $v->uuid)->first();
        $typeaccount = DB::connection('live')->table('typeMT4Accounts')->where('namaAccount', $v->typeAccount)->where('categoryAccount', $user->parent)->first();
  
        $ib = DB::connection('live')->table('users_ib')->where('id', $user->parent)->first();
        $mib = DB::connection('live')->table('users_ib')->where('id', $ib->masterib)->first();
        $sib = DB::connection('live')->table('users_ib')->where('id', $ib->seniorib)->first();
        $groupAccount = urlencode($typeaccount->accountGroup);
        $ex = explode('-', $groupAccount);
        $ngroup = $ex[0] . "-N-" . $ex[1] . "-" . $ex[2];
        $group = env('GROUP_MT5_SERVER') . $sib->groupMT5 . "\\" . $mib->groupMT5 . "\\" . $ib->groupMT5 . "\\" . $ngroup;
  
        $create = $api->httpPost('/api/user/update?login=' . $account . '&group=' . $group, []);
        $dataHasilUpdateGroup = json_decode($create);
  
        // UPDATE KODEGROUP
  
        $update = DB::connection('live')->table('typeMT4AvailabeAccount')->where('id', $v->id)->update([
          'kodeGroup' => $group,
          'deposit' => 1
        ]);
        echo "berhasil pindah " . $v->mt4_id . " ke grup " . $group."<br>";
      }
    }
  public function checkAcc($id)
  {
    $api = new LaravelMt5();
    try {
      $user = $api->getTradingAccounts($id);
      $result = $api->getUser($id);
      $resultBatch = $api->httpGet('/api/user/get?login=' . $id, []);
      $dataBatch = json_decode($resultBatch);
      return response()->json([
        'success' => true,
        'data' => [
          'user' => $user,
          'result' => $result,
          'dataBatch' => $dataBatch,
        ],
      ]);
    } catch (\Exception $e) {
      dd($e);
    }
  }

}
