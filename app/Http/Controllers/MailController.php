<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mail\RegisterEmail;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{

  public function index(){

    Mail::to("yuda.yokesen@gmail.com")->send(new RegisterEmail());

    return "Email telah dikirim";

  }

}
