<?php

namespace App\Http\Controllers;
use JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use Storage;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Tarikhagustia\LaravelMt5\LaravelMt5;

class TickerController extends Controller
{
    public function listTicker(){
      $getSymbolPair = DB::table('pairs')->where('pair','LIKE','%.ORB')->select('base')->get();
      return $getSymbolPair;
    }

    public function getTicker(Request $request){

      $api = new LaravelMt5();

      $getmt5 = DB::table('typeMT4AvailabeAccount')->where('uuid',$request->uuid)->first();

      $getSymbolPair = DB::table('pairs')->where('pair','LIKE','%.FFX')->get();
      $var = '';
      foreach ($getSymbolPair as $key => $value) {
        if ($key == 0) {
          $var = $value->pair;
        }else{
          $var .= ",".$value->pair;
        }

      }

      $ticker = $api->httpGet('/api/tick/last_group?symbol=XAUUSD.FFX,GBPUSD.FFX,EURUSD.FFX,USDJPY.FFX,AUDUSD.FFX&group='.$getmt5->kodeGroup.'&trans_id=0',[]);

      $data = json_decode($ticker);

      return $data->answer;

    }

    public function price($symbol){
      $pair = explode(".",$symbol);
      $pair = $pair[0];
      $candle = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','M5')->select('time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->take(60)->get();
      $candlesticks = $candle->reverse();
      foreach ($candlesticks as $c) {
        $time = (int)$c->time_price*1000;
        $price[] = [
          'x' => $time,
          'y' => [$c->open_price,$c->high_price,$c->low_price,$c->close_price]
        ];
      }

      return response()->json($price);
    }

    public function volume($symbol){
      $pair = explode(".",$symbol);
      $pair = $pair[0];
      $candle = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','M5')->select('spread_price', 'time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->take(60)->get();
      $candlesticks = $candle->reverse();
      foreach ($candlesticks as $c) {
        $time = (int)$c->time_price*1000;
        $volume[] = [
          'x' => $time,
          'y' => $c->volume_price
        ];
      }

      return response()->json($volume);
    }

    public function pricetam5($symbol){
      $pair = explode(".",$symbol);
      $pair = $pair[0];
      $candle = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','M5')->select('spread_price','time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->take(60)->get();
      $candlesticks = $candle->reverse();

      $openPrice = 0;
      $closedPrice = 0;
      $lowPrice = 0;
      $highPrice = 0;
      $spreadPrice = 0;
      foreach ($candlesticks as $index=>$c) {
        $time = (int)$c->time_price*1000;
        $price[] = [
          'x' => $time,
          'y' => number_format((float)$c->close_price, 2, '.', '')
        ];

        // if($index == 59){
          $openPrice = $c->open_price;
          $closedPrice = $c->close_price;
          $lowPrice = $c->low_price;
          $highPrice = $c->high_price;
          $spreadPrice  =$c->spread_price;
        // }
      }

      $api = new LaravelMt5();
      $ticker = $api->httpGet('/api/tick/last_group?symbol='.$symbol.'&group=TY98\MT61\IT62\IT62-10K-TEST&trans_id=0',[]);

      $ticker1 = json_decode($ticker);
      $digit = $ticker1->answer[0]->Digits;

      return response()->json(['graph'=>$price, 'openPrice'=>number_format((float)$openPrice, $digit, '.', ''), 'closedPrice'=>number_format((float)$closedPrice, $digit, '.', ''), 'lowPrice'=>number_format((float)$lowPrice, $digit, '.', ''), 'highPrice'=>number_format((float)$highPrice, $digit, '.', ''), 'spreadPrice'=>number_format((float)$spreadPrice, 2, '.', '')]);
    }

    public function pricetam15($symbol){
      $pair = explode(".",$symbol);
      $pair = $pair[0];
      $candle = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','M15')->select('spread_price','time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->take(60)->get();
      $candlesticks = $candle->reverse();
      $openPrice = 0;
      $closedPrice = 0;
      $lowPrice = 0;
      $highPrice = 0;
      $spreadPrice = 0;
      foreach ($candlesticks as $index=>$c) {
        $time = (int)$c->time_price*1000;
        $price[] = [
          'x' => $time,
          'y' => number_format((float)$c->close_price, 2, '.', '')
        ];

        // if($index == 59){
          // $openPrice = $c->open_price;
          // $closedPrice = $c->close_price;
          // $lowPrice = $c->low_price;
          // $highPrice = $c->high_price;
          // $spreadPrice  =$c->spread_price;
        // }
      }

      $candle1 = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','M5')->select('spread_price','time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->first();

      $openPrice = $candle1->open_price;
      $closedPrice = $candle1->close_price;
      $lowPrice = $candle1->low_price;
      $highPrice = $candle1->high_price;
      $spreadPrice  =$candle1->spread_price;

      $api = new LaravelMt5();
      $ticker = $api->httpGet('/api/tick/last_group?symbol='.$symbol.'&group=TY98\MT61\IT62\IT62-10K-TEST&trans_id=0',[]);

      $ticker1 = json_decode($ticker);
      $digit = $ticker1->answer[0]->Digits;

      return response()->json(['graph'=>$price, 'openPrice'=>number_format((float)$openPrice, $digit, '.', ''), 'closedPrice'=>number_format((float)$closedPrice, $digit, '.', ''), 'lowPrice'=>number_format((float)$lowPrice, $digit, '.', ''), 'highPrice'=>number_format((float)$highPrice, $digit, '.', ''), 'spreadPrice'=>number_format((float)$spreadPrice, 2, '.', '')]);
    }

    public function pricetam30($symbol){
      $pair = explode(".",$symbol);
      $pair = $pair[0];
      $candle = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','M30')->select('spread_price','time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->take(60)->get();
      $candlesticks = $candle->reverse();

      $openPrice = 0;
      $closedPrice = 0;
      $lowPrice = 0;
      $highPrice = 0;
      $spreadPrice = 0;
      foreach ($candlesticks as $index=>$c) {
        $time = (int)$c->time_price*1000;
        $price[] = [
          'x' => $time,
          'y' => number_format((float)$c->close_price, 2, '.', '')
        ];

        // if($index == 59){
          // $openPrice = $c->open_price;
          // $closedPrice = $c->close_price;
          // $lowPrice = $c->low_price;
          // $highPrice = $c->high_price;
          // $spreadPrice  =$c->spread_price;
        // }
      }

      $candle1 = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','M5')->select('spread_price','time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->first();

      $openPrice = $candle1->open_price;
      $closedPrice = $candle1->close_price;
      $lowPrice = $candle1->low_price;
      $highPrice = $candle1->high_price;
      $spreadPrice  =$candle1->spread_price;

      $api = new LaravelMt5();
      $ticker = $api->httpGet('/api/tick/last_group?symbol='.$symbol.'&group=TY98\MT61\IT62\IT62-10K-TEST&trans_id=0',[]);

      $ticker1 = json_decode($ticker);
      $digit = $ticker1->answer[0]->Digits;

      return response()->json(['graph'=>$price, 'openPrice'=>number_format((float)$openPrice, $digit, '.', ''), 'closedPrice'=>number_format((float)$closedPrice, $digit, '.', ''), 'lowPrice'=>number_format((float)$lowPrice, $digit, '.', ''), 'highPrice'=>number_format((float)$highPrice, $digit, '.', ''), 'spreadPrice'=>number_format((float)$spreadPrice, 2, '.', '')]);
    }

    public function pricetah1($symbol){
      $pair = explode(".",$symbol);
      $pair = $pair[0];
      $candle = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','H1')->select('spread_price','time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->take(60)->get();
      $candlesticks = $candle->reverse();

      $openPrice = 0;
      $closedPrice = 0;
      $lowPrice = 0;
      $highPrice = 0;
      $spreadPrice = 0;
      foreach ($candlesticks as $index=>$c) {
        $time = (int)$c->time_price*1000;
        $price[] = [
          'x' => $time,
          'y' => number_format((float)$c->close_price, 2, '.', '')
        ];

        // if($index == 59){
          // $openPrice = $c->open_price;
          // $closedPrice = $c->close_price;
          // $lowPrice = $c->low_price;
          // $highPrice = $c->high_price;
          // $spreadPrice  =$c->spread_price;
        // }
      }

      $candle1 = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','M5')->select('spread_price','time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->first();

      $openPrice = $candle1->open_price;
      $closedPrice = $candle1->close_price;
      $lowPrice = $candle1->low_price;
      $highPrice = $candle1->high_price;
      $spreadPrice  =$candle1->spread_price;

      $api = new LaravelMt5();
      $ticker = $api->httpGet('/api/tick/last_group?symbol='.$symbol.'&group=TY98\MT61\IT62\IT62-10K-TEST&trans_id=0',[]);

      $ticker1 = json_decode($ticker);
      $digit = $ticker1->answer[0]->Digits;

      return response()->json(['graph'=>$price, 'openPrice'=>number_format((float)$openPrice, $digit, '.', ''), 'closedPrice'=>number_format((float)$closedPrice, $digit, '.', ''), 'lowPrice'=>number_format((float)$lowPrice, $digit, '.', ''), 'highPrice'=>number_format((float)$highPrice, $digit, '.', ''), 'spreadPrice'=>number_format((float)$spreadPrice, 2, '.', '')]);
    }

    public function pricetah4($symbol){
      $pair = explode(".",$symbol);
      $pair = $pair[0];
      $candle = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','H4')->select('spread_price','time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->take(60)->get();
      $candlesticks = $candle->reverse();

      $openPrice = 0;
      $closedPrice = 0;
      $lowPrice = 0;
      $highPrice = 0;
      $spreadPrice = 0;
      foreach ($candlesticks as $index=>$c) {
        $time = (int)$c->time_price*1000;
        $price[] = [
          'x' => $time,
          'y' => number_format((float)$c->close_price, 2, '.', '')
        ];

        // if($index == 59){
          // $openPrice = $c->open_price;
          // $closedPrice = $c->close_price;
          // $lowPrice = $c->low_price;
          // $highPrice = $c->high_price;
          // $spreadPrice  =$c->spread_price;
        // }
      }

      $candle1 = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','M5')->select('spread_price','time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->first();

      $openPrice = $candle1->open_price;
      $closedPrice = $candle1->close_price;
      $lowPrice = $candle1->low_price;
      $highPrice = $candle1->high_price;
      $spreadPrice  =$candle1->spread_price;

      $api = new LaravelMt5();
      $ticker = $api->httpGet('/api/tick/last_group?symbol='.$symbol.'&group=TY98\MT61\IT62\IT62-10K-TEST&trans_id=0',[]);

      $ticker1 = json_decode($ticker);
      $digit = $ticker1->answer[0]->Digits;

      return response()->json(['graph'=>$price, 'openPrice'=>number_format((float)$openPrice, $digit, '.', ''), 'closedPrice'=>number_format((float)$closedPrice, $digit, '.', ''), 'lowPrice'=>number_format((float)$lowPrice, $digit, '.', ''), 'highPrice'=>number_format((float)$highPrice, $digit, '.', ''), 'spreadPrice'=>number_format((float)$spreadPrice, 2, '.', '')]);
    }

    public function pricetad1($symbol){
      $pair = explode(".",$symbol);
      $pair = $pair[0];
      $candle = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','D1')->select('spread_price','time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->take(60)->get();
      $candlesticks = $candle->reverse();

      $openPrice = 0;
      $closedPrice = 0;
      $lowPrice = 0;
      $highPrice = 0;
      $spreadPrice = 0;
      foreach ($candlesticks as $index=>$c) {
        $time = (int)$c->time_price*1000;
        $price[] = [
          'x' => $time,
          'y' => number_format((float)$c->close_price, 2, '.', '')
        ];

        // if($index == 59){
          // $openPrice = $c->open_price;
          // $closedPrice = $c->close_price;
          // $lowPrice = $c->low_price;
          // $highPrice = $c->high_price;
          // $spreadPrice  =$c->spread_price;
        // }
      }

      $candle1 = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','M5')->select('spread_price','time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->first();

      $openPrice = $candle1->open_price;
      $closedPrice = $candle1->close_price;
      $lowPrice = $candle1->low_price;
      $highPrice = $candle1->high_price;
      $spreadPrice  =$candle1->spread_price;

      $api = new LaravelMt5();
      $ticker = $api->httpGet('/api/tick/last_group?symbol='.$symbol.'&group=TY98\MT61\IT62\IT62-10K-TEST&trans_id=0',[]);

      $ticker1 = json_decode($ticker);
      $digit = $ticker1->answer[0]->Digits;

      return response()->json(['graph'=>$price, 'openPrice'=>number_format((float)$openPrice, $digit, '.', ''), 'closedPrice'=>number_format((float)$closedPrice, $digit, '.', ''), 'lowPrice'=>number_format((float)$lowPrice, $digit, '.', ''), 'highPrice'=>number_format((float)$highPrice, $digit, '.', ''), 'spreadPrice'=>number_format((float)$spreadPrice, 2, '.', '')]);
    }

    public function pricetaw1($symbol){
      $pair = explode(".",$symbol);
      $pair = $pair[0];
      $candle = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','W1')->select('spread_price','time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->take(60)->get();
      $candlesticks = $candle->reverse();

      $openPrice = 0;
      $closedPrice = 0;
      $lowPrice = 0;
      $highPrice = 0;
      $spreadPrice = 0;
      foreach ($candlesticks as $index=>$c) {
        $time = (int)$c->time_price*1000;
        $price[] = [
          'x' => $time,
          'y' => number_format((float)$c->close_price, 2, '.', '')
        ];

        // if($index == 59){
          // $openPrice = $c->open_price;
          // $closedPrice = $c->close_price;
          // $lowPrice = $c->low_price;
          // $highPrice = $c->high_price;
          // $spreadPrice  =$c->spread_price;
        // }
      }

      $candle1 = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','M5')->select('spread_price','time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->first();

      $openPrice = $candle1->open_price;
      $closedPrice = $candle1->close_price;
      $lowPrice = $candle1->low_price;
      $highPrice = $candle1->high_price;
      $spreadPrice  =$candle1->spread_price;

      $api = new LaravelMt5();
      $ticker = $api->httpGet('/api/tick/last_group?symbol='.$symbol.'&group=TY98\MT61\IT62\IT62-10K-TEST&trans_id=0',[]);

      $ticker1 = json_decode($ticker);
      $digit = $ticker1->answer[0]->Digits;

      return response()->json(['graph'=>$price, 'openPrice'=>number_format((float)$openPrice, $digit, '.', ''), 'closedPrice'=>number_format((float)$closedPrice, $digit, '.', ''), 'lowPrice'=>number_format((float)$lowPrice, $digit, '.', ''), 'highPrice'=>number_format((float)$highPrice, $digit, '.', ''), 'spreadPrice'=>number_format((float)$spreadPrice, 2, '.', '')]);
    }

    public function pricetamn1($symbol){
      $pair = explode(".",$symbol);
      $pair = $pair[0];
      $candle = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','MN1')->select('spread_price','time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->take(60)->get();
      $candlesticks = $candle->reverse();

      $openPrice = 0;
      $closedPrice = 0;
      $lowPrice = 0;
      $highPrice = 0;
      $spreadPrice = 0;
      foreach ($candlesticks as $index=>$c) {
        $time = (int)$c->time_price*1000;
        $price[] = [
          'x' => $time,
          'y' => number_format((float)$c->close_price, 2, '.', '')
        ];

        // if($index == 59){
          // $openPrice = $c->open_price;
          // $closedPrice = $c->close_price;
          // $lowPrice = $c->low_price;
          // $highPrice = $c->high_price;
          // $spreadPrice  =$c->spread_price;
        // }
      }

      $candle1 = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','M5')->select('spread_price','time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->first();

      $openPrice = $candle1->open_price;
      $closedPrice = $candle1->close_price;
      $lowPrice = $candle1->low_price;
      $highPrice = $candle1->high_price;
      $spreadPrice  =$candle1->spread_price;

      $api = new LaravelMt5();
      $ticker = $api->httpGet('/api/tick/last_group?symbol='.$symbol.'&group=TY98\MT61\IT62\IT62-10K-TEST&trans_id=0',[]);

      $ticker1 = json_decode($ticker);
      $digit = $ticker1->answer[0]->Digits;

      return response()->json(['graph'=>$price, 'openPrice'=>number_format((float)$openPrice, $digit, '.', ''), 'closedPrice'=>number_format((float)$closedPrice, $digit, '.', ''), 'lowPrice'=>number_format((float)$lowPrice, $digit, '.', ''), 'highPrice'=>number_format((float)$highPrice, $digit, '.', ''), 'spreadPrice'=>number_format((float)$spreadPrice, 2, '.', '')]);
    }

    // public function volumeD1($symbol){
    //   $pair = explode(".",$symbol);
    //   $pair = $pair[0];
    //   $candle = DB::table('symbol_technical_analysis')->where('symbol','LIKE','%'.$pair.'%')->where('timeframe','D1')->select('time_price','open_price','high_price','low_price','close_price','volume_price')->orderby('time_price','desc')->take(60)->get();
    //   $candlesticks = $candle->reverse();
    //   foreach ($candlesticks as $c) {
    //     $time = (int)$c->time_price*1000;
    //     $volume[] = [
    //       'x' => $time,
    //       'y' => $c->volume_price
    //     ];
    //   }
    //
    //   return response()->json($volume);
    // }
}
