<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Auth;
use Tarikhagustia\LaravelMt5\LaravelMt5;
use Tarikhagustia\LaravelMt5\Entities\User;
use Tarikhagustia\LaravelMt5\Entities\Trade;
use Tarikh\PhpMeta\MetaTraderClient;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Arr;
use GuzzleHttp\Client;

class mt5createaccountController extends Controller
{
  public function createMt5(Request $request)
  {

    //return "masuk";

    $api = new LaravelMt5();
    $rules = [
      'login' => 'required|numeric',
      'password' => 'required|string',
      'group' => 'required|string',
      'name' => 'required|string',
      'email' => 'required|string',
      'phone' => 'required|numeric',
      'secret' => 'required',
      'address' => 'required',
      'city' => 'required',
      'leverage' => 'required'
    ];


    $valid = Validator::make($request->all(), $rules);

    if ($valid->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $valid->errors()
        ]
      ]);
    }


    //return "test";
    $group = $request->sibGroup . "\\" . $request->mibGroup . "\\" . $request->ibGroup . "\\" . $request->group;

    // $return = [
    //   'request' => $request->all(),
    //   'group' => $group
    // ];
    //
    // return $return;
    $address = '';
    $city = '';

    if ($request->secret == '$2y$10$K5skD5q/dtAZjS7ZfetKHOAhuNJ7AhvUzYV2uejBLXt6rrJ0KA1Fm') {

      $name = str_replace(' ', '%20', $request->name);
      if ($address == '') {
        $address = str_replace("\n", " ", $request->address);
        $address = preg_replace("/[^A-Za-z0-9 ]/", '', $address);
        $address = str_replace(' ', '%20', $address);
      } else {
        $address = 'unknown';
      }

      if ($city == '') {
        $city = str_replace(' ', '%20', $request->city);
      } else {
        $city = 'unknown';
      }

      $create = $api->httpPost('/api/user/add?login=' . $request->login . '&pass_main=' . $request->password . '&pass_investor=' . $request->password . '&leverage=' . $request->leverage . '&group=' . $group . '&name=' . $name . '&email=' . $request->email . '&phone=' . $request->phone . '&address=' . $address . '&city=' . $city, []);
      $data = json_decode($create);
      //dd($data);
      return response()->json([
        'status' => true,
        'data' => $data
      ]);
    } else {
      return response()->json([
        'status' => false,
        'data' => 'error auth'
      ]);
    }
  }

  public function updateGroup(Request $request)
  {

    $api = new LaravelMt5();
    $rules = [
      'login' => 'required|numeric',
      'group' => 'required|string'
    ];

    // dd($request->all());

    $valid = Validator::make($request->all(), $rules);

    if ($valid->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $valid->errors()
        ]
      ]);
    }

    if ($request->secret == '$2y$10$K5skD5q/dtAZjS7ZfetKHOAhuNJ7AhvUzYV2uejBLXt6rrJ0KA1Fm') {

      $create = $api->httpPost('/api/user/update?login=' . $request->login . '&group=' . $request->group, []);
      $data = json_decode($create);

      return response()->json([
        'status' => true,
        'data' => $data
      ]);
    } else {
      return response()->json([
        'status' => false,
        'data' => 'error auth'
      ]);
    }
  }
  public function checkAcc($id)
  {
    $api = new LaravelMt5();
    try {
      $user = $api->getTradingAccounts($id);
      $result = $api->getUser($id);

      dd($user, $result);
    } catch (\Exception $e) {
      dd($e);
    }
  }

  public function createAccountMt4(Request $request)
  {

    $api = new LaravelMt5();
    $rules = [
      'uuid' => 'required|string',
      'accountType' => 'required|string',
      // 'kodeGroup' => 'required|string'
    ];


    $valid = Validator::make($request->all(), $rules);

    if ($valid->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $valid->errors()
        ]
      ]);
    }



    if ($request->secret == '$2y$10$K5skD5q/dtAZjS7ZfetKHOAhuNJ7AhvUzYV2uejBLXt6rrJ0KA1Fm') {

      $group = DB::table('typeMT4GroupList')->where('typeAccount', $request->accountType)->first();
      $gn = $group->groupName;

      $max = DB::table('typeMT4AvailabeAccount')->where('mt4_id', 'LIKE', $gn . '%')->where('nocan', 0)->max('mt4_id');
      if ($max) {
        $accountId = $max + 1;
      } else {
        $accountId = $gn * 10000;
      }


      $user = DB::table('users_cabinet')->where('uuid', $request->uuid)->first();
      $name = $user->name;
      $time = time();

      $p_name = substr($name, 0, 3);
      $p_time = substr($time, -4);
      $password = $p_name . $p_time;

      $client = DB::table('users_cabinet')->where('uuid', $request->uuid)->first();
      $typeAccount = DB::table('typeMT4AvailabeAccount')->where('mt4_id', $request->mt4_id)->first();
      $accountCategory = DB::table('typeMT4Accounts')->where('namaAccount', $request->accountType)->where('categoryAccount', $client->parent)->first();
      $ib = DB::table('users_ib')->where('id', $client->parent)->first();
      $mib = DB::table('users_ib')->where('id', $ib->masterib)->first();
      $sib = DB::table('users_ib')->where('id', $ib->seniorib)->first();
      $login = $accountId;
      $groupAccount = urlencode($accountCategory->accountGroup);
      $leverage = $accountCategory->accountLeverage;
      $name = $client->name;
      $email = $client->email;
      $address = $client->address;
      $city = $client->city;
      $phone = preg_replace("/[^0-9]/", '', $client->whatsapp);

      $name = str_replace(' ', '%20', $name);
      $address = str_replace("\n", " ", $address);
      $address = preg_replace("/[^A-Za-z0-9 ]/", '', $address);
      $address = str_replace(' ', '%20', $address);
      $city = str_replace(' ', '%20', $city);

      $group = env('GROUP_MT5_SERVER') . $sib->groupMT5 . "\\" . $mib->groupMT5 . "\\" . $ib->groupMT5 . "\\" . $groupAccount;

      $create = $api->httpPost('/api/user/add?login=' . $login . '&pass_main=' . $password . '&pass_investor=' . $password . '&leverage=' . $leverage . '&group=' . $group . '&name=' . $name . '&email=' . $email . '&phone=' . $phone . '&address=' . $address . '&city=' . $city, []);
      $data = json_decode($create);

      if ($data->retcode == "0 Done") {
        $companySetPercent = 100 - $ib->senioribDefaultPercent;
        $SeniorIb = $ib->seniorib;
        $SeniorIbSetPercent = $ib->senioribDefaultPercent - $ib->masteribDefaultPercent;
        $MasterIb = $ib->masterib;
        $MasterIbSetPercent = $ib->masteribDefaultPercent - $ib->takingPositionPercentage;

        $ibSetPercent = $ib->takingPositionPercentage;

        $checkfloating = DB::table('typeMT4MasterTemplate')->where('masterName', $request->accountType)->first();

        if ($checkfloating->masterRate == 'FL') {
          $tipe_deposit = '11';
        } else {
          $tipe_deposit = '10';
        }

        $insert = DB::table('typeMT4AvailabeAccount')->insert([
          'typeAccount' => $request->accountType,
          'parent' => $user->parent,
          'uuid' => $request->uuid,
          'mt4_id' => $accountId,
          'tipe_deposit' => $tipe_deposit,
          'status' => 'approved',
          'password' => $password,
          'kodeGroup' => $group,
          'companySetPercent' => $companySetPercent,
          'SeniorIb' => $SeniorIb,
          'SeniorIbSetPercent' => $SeniorIbSetPercent,
          'MasterIb' => $MasterIb,
          'MasterIbSetPercent' => $MasterIbSetPercent,
          'ib' => $user->parent,
          'ibSetPercent' => $ibSetPercent,
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s')
        ]);
      }

      // send email
      try {
        $link = "https://fibpartners.com/market-insight";
        $client = new Client();
        $formBody = [
          "email" => $email,
          "password" => $password,
          "username" => $name,
          "metatrader_id" => $accountId,
          "link" => $request->link

        ];
        // kalau link sudah ada, ganti dengan punya fib
        $send = $client->post("https://mg.ibbisnis.id/api/email/metatrader-register", [
          'headers' => [
            'Authorization' => 'Bearer' . $request->bearerToken()
          ],
          'form_params' => $formBody
        ]);

        // return response()->json([
        //   'status' => 'success',
        //   'message' => 'berhasil mengirim email akun'
        // ]);

      } catch (\Exception $e) {
        dd($e);
        return response()->json([
          'status' => 'failed',
          'message' => 'gagal mengirim email akun'
        ], 422);

      }

      //dd($data);
      return response()->json([
        'status' => true,
        'data' => $data,
        'accountType' => $request->accountType
      ]);
    } else {
      return response()->json([
        'status' => false,
        'data' => 'error auth'
      ]);
    }
  }

  public function listAccountType($uuid)
  {
    $getClient = DB::table('users_cabinet')->select('parent')->where('uuid', $uuid)->first();
    $getTypes = DB::table('typeMT4Accounts')->select('typeMT4Accounts.namaAccount', 'typeMT4Accounts.accountGroup', 'id')->where('typeMT4Accounts.categoryAccount', $getClient->parent)->where('typeMT4Accounts.status', 'active')->get();
    // dd($getTypes);
    foreach ($getTypes as $index => $type) {
      // dd($type)
      $description = DB::table('typeMT4Description')->select('descOffer', 'descMark')->where('typeAccount', $type->id)->where('status', 'active')->get();
      // dd($description, $type->categoryAccount);
      $getTypes[$index]->deskripsi = $description;
    }
    return response()->json([
      'success' => true,
      'data' => $getTypes
    ]);
  }

  public function checkAvailableAccount(Request $request)
  {
    // to prevent IB to create user's mt5 account more than one for one type

    $client = DB::table('users_cabinet')->where('uuid', $request->uuid)->first();
    $ib = DB::table('users_ib')->select('groupMT5', 'masterib')->where('id', $client->parent)->first();
    $mib = DB::table('users_ib')->select('groupMT5', 'seniorib')->where('id', $ib->masterib)->first();
    $sib = DB::table('users_ib')->select('groupMT5')->where('id', $mib->seniorib)->first();
    $typeAccount = DB::table('typeMT4Accounts')->select('accountGroup')->where('categoryAccount', $client->parent)->where('namaAccount', $request->accountType)->first();

    $kode = env('GROUP_MT5_SERVER') . $sib->groupMT5 . '\\' . $mib->groupMT5 . '\\' . $ib->groupMT5 . '\\' . $typeAccount->accountGroup;

    $check = DB::table('typeMT4AvailabeAccount')->where('uuid', $request->uuid)->where('kodeGroup', $kode)->count();
    if ($check > 0) {
      return response()->json([
        'success' => false,
        'msg' => 'already have account'
      ], 200);
    } else {
      return response()->json([
        'success' => true,
        'msg' => 'success'
      ], 200);
    }
  }

  public function giveTradingCompetitionAccount(Request $request)
  {
    // return $request->all();
    $uuid = $request->uuid;

    $user = DB::table('users_cabinet')->where('uuid', $uuid)->first();
    $groupName = 'demo\Competition\Dec2023';

    $max = DB::table('typeMT4AvailabeAccount')->where('typeAccount', 'competition')->max('mt4_id');

    if ($max) {
      $accountId = $max + 1;
    } else {
      $kodenissa = DB::table('typeMT4MasterTemplate')->where('masterName', 'competition')->first();
      $accountId = ($kodenissa->masterNumber * 100000) + 1;
    }

    $name = $user->name;
    $time = time();

    $p_name = substr($name, 0, 3);
    $p_time = substr($time, -4);
    $password = env('MT5_PASS_PREFIX') . $p_name . '@' . $p_time;

    $ib = DB::table('users_ib')->where('id', $user->parent)->first();
    $mib = DB::table('users_ib')->where('id', $ib->masterib)->first();
    $sib = DB::table('users_ib')->where('id', $ib->seniorib)->first();

    $groupAccount = urlencode($groupName);
    $leverage = 500;
    $name = $user->name;
    $email = $user->email;
    $address = $user->address;
    $city = $user->city;
    $phone = preg_replace("/[^0-9]/", '', $user->whatsapp);

    $name = str_replace(' ', '%20', $name);
    $address = str_replace("\n", " ", $address);
    $address = preg_replace("/[^A-Za-z0-9 ]/", '', $address);
    $address = str_replace(' ', '%20', $address);
    $city = str_replace(' ', '%20', $city);

    $group = $groupAccount;

    $api = new LaravelMt5();
    $create = $api->httpPost('/api/user/add?login=' . $accountId . '&pass_main=' . $password . '&pass_investor=' . $password . '&leverage=' . $leverage . '&group=' . $group . '&name=' . $name . '&email=' . $email . '&phone=' . $phone . '&address=' . $address . '&city=' . $city, []);
    $data = json_decode($create);
    // dd($data);
    $sample[] = [
      'data' => $data,
      'account' => $accountId,
      'password' => $password
    ];

    if ($data->retcode == "0 Done") {
      $accountId = $data->answer->Login;
      $companySetPercent = 100 - $ib->senioribDefaultPercent;
      $SeniorIb = $ib->seniorib;
      $SeniorIbSetPercent = $ib->senioribDefaultPercent - $ib->masteribDefaultPercent;
      $MasterIb = $ib->masterib;
      $MasterIbSetPercent = $ib->masteribDefaultPercent - $ib->takingPositionPercentage;

      $ibSetPercent = $ib->takingPositionPercentage;

      $checkfloating = 10;


      $tipe_deposit = $checkfloating;


      if ($user->active_mt5_account == 0) {
        $update_active_mt5_account = DB::table('users_cabinet')->where('uuid', $uuid)->update([
          'active_mt5_account' => $accountId,
        ]);
      }

      $update_active_mt5_account = DB::table('users_cabinet')->where('uuid', $uuid)->update([
        'step_acc_competition' => 1,
      ]);

      $insert = DB::table('typeMT4AvailabeAccount')->insert([
        'typeAccount' => 'competition',
        'parent' => $user->parent,
        'nickname' => 'pending',
        'uuid' => $user->uuid,
        'mt4_id' => $accountId,
        'tipe_deposit' => $tipe_deposit,
        'status' => 'approved',
        'password' => $password,
        'kodeGroup' => $group,
        'companySetPercent' => $companySetPercent,
        'SeniorIb' => $SeniorIb,
        'SeniorIbSetPercent' => $SeniorIbSetPercent,
        'MasterIb' => $MasterIb,
        'MasterIbSetPercent' => $MasterIbSetPercent,
        'ib' => $user->parent,
        'ibSetPercent' => $ibSetPercent,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);

      $commet = "modalTc1000";

      $postBalance = $api->httpPost('/api/trade/balance?login=' . $accountId . '&type=2&balance=' . 10000 . '&comment=' . $commet, []);
      $decode_balance = json_decode($postBalance);
      $data_balance = $decode_balance->answer;
      $ticket = $data_balance->ticket;
      // dd($data_balance);

      return response()->json([
        'success' => true,
        'data' => $sample,
        'type' => 'competition',
      ]);

    } else {
      return response()->json([
        'success' => false,
        'message' => 'create account failed',
      ], 500);
    }

  }





  public function suntikManual($accountId)
  {
    // dd($accountId);
    $api = new LaravelMt5();

    $commet = "modalutkTesting50000";

    $postBalance = $api->httpPost('/api/trade/balance?login=' . $accountId . '&type=2&balance=' . 50000 . '&comment=' . $commet, []);
    $decode_balance = json_decode($postBalance);
    $data_balance = $decode_balance->answer;
    $ticket = $data_balance->ticket;

    return response()->json([
      'success' => true,
      'msg' => 'Berhasil suntik gan!',
    ]);
  }
}
