<?php

namespace App\Http\Controllers;
use JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use Storage;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class VideoController extends Controller
{
  public function video_list(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $videos = DB::table('academyVideo')->join('cms_users','cms_users.id','academyVideo.created_by')->orderby('academyVideo.id','desc')->get();
      if ( $videos) {
        return response()->json([
          'success' => true,
          'Data' => $videos
        ]);
      } else {
        return response()->json([
          'success' => false,
          'Data' => 'Data not Found'
        ]);
      }
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }
  public function video_detail(Request $request , $video_id)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      if (isset($video_id) && !empty($video_id)) {

        $video = DB::table('academyVideo')->join('cms_users','cms_users.id','academyVideo.created_by')->where('academyVideo.uavid', $video_id)->first();
        if ( $video) {
          return response()->json([
            'success' => true,
            'Data' => $video
          ]);
        } else {
          return response()->json([
            'success' => false,
            'Data' => 'Data not Found'
          ]);
        }
      } else {
        return response()->json([
          'success' => false,
          'Data' => 'Please Provide Video ID'
        ]);
      }

    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }
  public function question(Request $request , $video_id)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      if (isset($video_id) && !empty($video_id)) {
        $questions = DB::table('academyQuestion')->where('materiId', $video_id)->get();
        if ( $questions) {
          return response()->json([
            'success' => true,
            'Data' => $questions
          ]);
        } else {
          return response()->json([
            'success' => false,
            'Data' => 'Data not Found'
          ]);
        }
      } else {
        return response()->json([
          'success' => false,
          'Data' => 'Please Provide Video ID'
        ]);
      }

    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }
  public function submit_quiz(Request $request , $video_id)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      if (isset($video_id) && !empty($video_id)) {
        $id =   $user['id'];
        $uuid =   $user['uuid'];
        $questions = DB::table('academyQuestion')->where('materiId', $video_id)->get();
        $video = DB::table('academyVideo')->where('uavid', $video_id)->first();
        $score = $video->videoPoin/count($questions);

        $poin = 0;

        foreach ($questions as $key => $value) {
          $jawaban_quiz = $value->qJawaban;
          $jawaban_user = $request->qJawaban[$key];
          if ($jawaban_quiz == $jawaban_user) {
            $poin = $score;
          }else{
            $poin = 0;
          }

          $count = DB::table('academyRespond')->where('uuid',$uuid)->where('uaqid',$value->uaqid)->count();
          if($count < 1){
            $insert = DB::table('academyRespond')->insert([
              'uarid' => sha1($request->qJawaban[$key].$uuid).time(),
              'uuid' => $uuid,
              'uaqid' => $value->uaqid,
              'questionPoin' => $score,
              'answerUser' => $jawaban_user,
              'poinGain' =>$poin
            ]);
          }else{
            $update = DB::table('academyRespond')->where('uuid',$uuid)->where('uaqid',$value->uaqid)->update([
              'questionPoin' => $score,
              'answerUser' => $jawaban_user,
              'poinGain' =>$poin
            ]);
          }
        }

        return response()->json([
          'success' => true,
          'Data' => 'successfully submit answer and you get '.$poin,
          "poin" => $poin
        ]);

      } else {
        return response()->json([
          'success' => false,
          'Data' => 'Please Provide Video ID'
        ]);
      }


    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function academyScore(Request $request){

    $token = $request->header('Authorization');
    try {
      if ($user = JWTAuth::authenticate($token)) {
        $uuid =   $user['uuid'];
        $q = DB::table('academyRespond')->where('uuid',$uuid)->get();

        $score = 0;
        foreach ($q as $c => $s) {
          $score += $s->poinGain;
        }
        return response()->json([
          'success' => true,
          'score' => $score,
        ], 200);
      }
    } catch (JWTException $e) {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token.',
      ], 401);
    }

  }
}
