<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use DB;
use Illuminate\Support\Facades\Validator;

class TwilioController extends Controller
{
  public function sendSms(Request $request){
    $rules = [
        'phone_number' => 'required',
        'uuid'=>'required',
    ];

    $validator = Validator::make($request->all(), $rules);

    if($validator->fails()){
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ]);
    }
    $sid = env('TWILIO_ID');
    $token = env('TWILIO_TOKEN');
    $client = new Client($sid, $token);

    // generate kode otp
    $number = $this->getCode();

    $checkNumber = $this->checkNumber($request->phone_number);
    // dd($checkNumber);
    if($checkNumber == 'nomor tidak valid'){
      return response()->json([
        'success'=>true,
        'status'=>false,
        'msg'=>'nomor tidak valid'
      ],400);
    }
    $to = $checkNumber;

    // $send = $client->messages->create(
    //     // the number you'd like to send the message to
    //     $to,
    //     [
    //         // A Twilio phone number you purchased at twilio.com/console
    //         'from' => '+18504077584',
    //         // the body of the text message you'd like to send
    //         'body' => "Kode verifikasi anda adalah: $number",
    //         "messagingServiceSid" => env('TWILIO_SERVICE_ID'),
    //         'statusCallback'=>"https://api.8377-2842.xyz/api/v1/twilio-callback"
    //     ]
    // );
    // update no hp
    // dd($request->uuid);
    $no_hp = '0'.$request->phone_number;
    $update = DB::table('users_cabinet')->where('uuid', $request->uuid)->update([
      'phone'=>$no_hp,
      'whatsapp'=>$no_hp
    ]);

    // check apakah ada kode otp yang belum di pakai oleh orang ini
    $checkOtp = DB::table('kode_otp_sms')->where('user_uuid', $request->uuid)->where('status', 0)->get();
    if(count($checkOtp) > 1){
      $updateOtp = DB::table('kode_otp_sms')->where('user_uuid', $request->uuid)->where('status', 0)->update([
        'status'=>1
      ]);
    }

    $insert = DB::table('kode_otp_sms')->insert([
      'kode'=>$number,
      'no_hp'=>$no_hp,
      'created_at'=>date('Y-m-d H:i:s'),
      // 'status'=>0,
      // 'message_status'=>$send->status,
      'message_status'=>'sent',
      // 'twilio_sms_id'=>$send->sid,
      'user_uuid'=>$request->uuid
    ]);

    return response()->json([
      'success'=>true,
      'status'=>true,
      'msg'=>'Kode sudah dikirim'
    ]);

  }
  public function getCode(){
    $number = rand(100000,999999);

    // cek ke DB apakah sudah ada codenya
    $cek = DB::table('kode_otp_sms')->where('kode', $number)->where('status', 0)->count();
    if($cek == 0){
      return $number;
    }else{
      $this->getCode();
    }
  }

  public function checkNumber($number){
    $whatsapp = $number;
        $whatsapp = str_replace('-','',$whatsapp);
        $whatsapp = str_replace(' ','',$whatsapp);
        $whatsapp = str_replace(' ','',$whatsapp);
        $whatsapp = str_replace('.','',$whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
        $check_number = str_split($whatsapp);
        $new_number = "+62";

        if($check_number[0]=='0'){
          foreach($check_number as $n => $number){
            if($n > 0){
              if($check_number[1]=='8'){
                $new_number .= $number;
              }else{
                //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
                //Return redirect()->back();
                //echo "ga bisa dibenerin nomor $isi->id <br>";
                $new_number .= $number;
                return $new_number;
              }
            }
          }
        }else{
          if($check_number[0]=='8'){
            $new_number = "+62".$whatsapp;
          }elseif($check_number[0]=='6'){
            $new_number = '+'.$whatsapp;
          }else{
            //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
            //Return redirect()->back();
            $new_number = "+62".$whatsapp;
            //echo "ga bisa dibenerin  $isi->id <br>";
            return $new_number;
          }
        }

      return $new_number;
  }
  public function twilioCallback(Request $request){
    $insertCallback = DB::table('webhook_callback')->insert([
      'data'=>$request->SmsSid.'-'. $request->MessageStatus,
      'created_at'=>date('Y-m-d H:i:s')
    ]);

    // $update = DB::table('kode_otp_sms')->where('twilio_sms_id', $request->SmsSid)->update([
    //   'message_status'=>$request->MessageStatus
    // ]);
  }

  public function verifyOtpCode(Request $request){

    $no_hp = '0'.$request->no_hp;
    // $cekCode = DB::table('kode_otp_sms')->where('no_hp', $no_hp)->where('kode', $request->kode)->where('status', 0)->count();
    // if($cekCode > 0){

      $updateStatus = DB::table('kode_otp_sms')->where('no_hp', $no_hp)->where('kode', $request->kode)->where('status', 0)->update([
        'status'=>1
      ]);
      // update no hp dan create mt5 demo account
      $method = new LabController;
      $mt5 = $method->createMt5($request->user_uuid);
      $update = DB::table('users_cabinet')->where('phone', $no_hp)->where('uuid', $request->user_uuid)->update([
        'phone_verification'=>1
      ]);

      return response()->json([
        'status'=>true,
        'success'=>true,
        'mt5'=>$mt5,
        'msg'=>'verifikasi berhasil'
      ]);
    // }else{
    //   return response()->json([
    //     'status'=>true,
    //     'success'=>false,
    //     'mt5'=>null,
    //     'msg'=>'verifikasi gagal'
    //   ]);
    // }
  }
}
