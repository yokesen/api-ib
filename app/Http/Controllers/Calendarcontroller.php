<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DB;

class Calendarcontroller extends Controller
{
  public function calendar(){

    $response = Http::get('https://nfs.faireconomy.media/ff_calendar_thisweek.json');
    $results = $response->json();
    // return $results;

    foreach ($results as $key => $value) {
      $rawDate = $value['date'];
      $date = explode('T',$rawDate);
      $time = explode('-',$date[1]);
      $newCalendar[] = [
        "title" => $value['title'],
        "country" => $value['country'],
        "z" => date("z",strtotime($date[0]." ".$time[0]." + 11 hours ")),
        "date" => date("Y-m-d",strtotime($date[0]." ".$time[0]." + 11 hours ")),
        "time" => date("H:i",strtotime($date[0]." ".$time[0]." + 11 hours ")),
        "impact" => $value['impact'],
        "forecast" => $value['forecast'],
        "previous" => $value['previous'],
      ];
    }

    $group = [];
    $count = count($newCalendar);
    $start = $newCalendar[0]['z'];
    $end = $newCalendar[$count-1]['z'];

    for ($i=$start; $i < $end+1; $i++) {

      foreach ($newCalendar as $key => $value) {
        if ($i == $value['z']) {
          $isi[] = $value;
        }
      }

      $group[] = [
        "date" => $value['date'],
        "value" => $isi
      ];

    }

    $final = [
      "success" => 200,
      "data" => $group
    ];

    return $final;
  }

  public function calendarPerSymbol($symbol){
    $response = Http::get('https://nfs.faireconomy.media/ff_calendar_thisweek.json');
    $results = $response->json();

    $results = array_filter($results, function($item) use($symbol){
      return $item['country'] == $symbol;
    });
    // dd(array_values($results));
    foreach ($results as $key => $value) {
      $rawDate = $value['date'];
      $date = explode('T',$rawDate);
      $time = explode('-',$date[1]);
      $newCalendar[] = [
        "title" => $value['title'],
        "country" => $value['country'],
        "z" => date("z",strtotime($date[0]." ".$time[0]." + 11 hours ")),
        "date" => date("Y-m-d",strtotime($date[0]." ".$time[0]." + 11 hours ")),
        "time" => date("H:i",strtotime($date[0]." ".$time[0]." + 11 hours ")),
        "impact" => $value['impact'],
        "forecast" => $value['forecast'],
        "previous" => $value['previous'],
      ];
    }
    return array_reverse($newCalendar);
    // dd(count($newCalendar), array_reverse($newCalendar));

  }

  public function bearishBullish($symbol){
    $tam15 = DB::table('symbol_technical_analysis')->select('bullishpct', 'bearishpct', 'neutralpct')->where('symbol','LIKE','%'.$symbol.'%')->where('timeframe','M15')->orderby('id','desc')->first();
      if($tam15){
        if((int) $tam15->bullishpct > (int) $tam15->bearishpct){
          $status = (int) $tam15->bullishpct > 50 ? 'strong bullish' : 'bullish';
        }else{
          $status = (int) $tam15->bearishpct > 50 ? 'strong bearish' : 'bearish';
        }
        return response()->json(['status'=>$status, 'bullishpct'=>$tam15->bullishpct, 'bearishpct'=>$tam15->bearishpct, 'neutralpct'=>$tam15->neutralpct]);
      }else{
        return response()->json('error', 400);
      }

    // dd((int) $tam15->bullishpct);

  }
}
