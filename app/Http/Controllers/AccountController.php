<?php

namespace App\Http\Controllers;

use JWTAuth;
use Auth;
use Illuminate\Http\Request;
use Tarikhagustia\LaravelMt5\LaravelMt5;
use Tarikhagustia\LaravelMt5\Entities\User;
use Tarikhagustia\LaravelMt5\Entities\Trade;
use Tarikh\PhpMeta\MetaTraderClient;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use DB;

class AccountController extends Controller
{
    public function data_account(Request $request){
      $rules = [
        'login' => 'required|numeric',

      ];

      $valid = Validator::make($request->all(), $rules);

      if($valid->fails()){
        return response()->json([
          'status' => false,
          'errors' => [
            $valid->errors()
          ]
        ]);
      }

      $token = $request->header('Authorization');

      try {
        if ($user = JWTAuth::authenticate($token)) {
          $uuid =   $user['uuid'];
          $myAccounts = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->login)->first();

          if ($myAccounts) {
            if ( $myAccounts->uuid == $uuid) {
              $api = new LaravelMt5();
              $user = $api->getTradingAccounts($request->login);
              $internals = DB::table('withdrawals')->where('uuid',$uuid)->where('metatrader',$request->login)->where('internal','1')->orderby('id','desc')->get();
              $withdrawals = DB::table('withdrawals')->where('uuid',$uuid)->where('metatrader',$request->login)->whereNULL('internal')->orderby('id','desc')->get();
              $deposits = DB::table('deposits')->where('uuid',$uuid)->where('metatrader',$request->login)->orderby('id','desc')->get();

              return response()->json([
                'success' => true,
                'data' => [
                  'mt5' => $user,
                  'deposits' => $deposits,
                  'withdrawals'=> $withdrawals,
                  'internals' => $internals
                ]
              ]);
            } else {
              return response()->json([
                'success' => false,
                'Data' => 'forbiden : wrong user'
              ]);
            }
          }else{
            return response()->json([
              'success' => false,
              'Data' => 'Data not Found'
            ]);
          }

        }else{
          return response()->json([
            'success' => false,
            'message' => 'Something wrong with Token.',
          ], 401);
        }
      } catch (JWTException $e) {
        return response()->json([
          'success' => false,
          'message' => 'Invalid Token.',
        ], 401);
      }
    }

    public function accountType(){
      $accountType = DB::table('typeMT4MasterTemplate')->where('masterStatus','active')->get();
      return response()->json([
        'success' => true,
        'data' => $accountType
      ], Response::HTTP_OK);
    }

    public function myAccountType(Request $request){
      $user = auth()->user();
      

      
      try {

        $typeAccount = DB::table('typeMT4Accounts')
        ->join('typeMT4MasterTemplate', 'typeMT4Accounts.namaAccount', '=', 'typeMT4MasterTemplate.masterName')
        ->where('typeMT4MasterTemplate.masterStatus', 'active')
        ->where('categoryAccount', auth()->user()->id)
        ->get();
    
        
        return response()->json([
          'success' => true,
          'typeAccount' => $typeAccount
        ], 200);

        
      } catch (JWTException $e) {
        return response()->json([
          'success' => false,
          'message' => 'Invalid Token.',
        ], 401);
      }
    }

    public function postCommissionIb(Request $request){

      $comm = $request->comm;

      try {
        foreach ($comm as $key => $com) {
          $typeAccount = DB::table('typeMT4Accounts')->where('categoryAccount',auth()->user()->id)->where('namaAccount',$key)->update([
            'accountCom' => $com,
            'deskripsiAccount' => 'new'
          ]);
        }
        

        return response()->json([
          'success' => true,
          'typeAccount' => $typeAccount
        ], 200);

        
      } catch (JWTException $e) {
        return response()->json([
          'success' => false,
          'message' => 'Invalid Token.',
        ], 401);
      }

    }
}
