<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class BalanceStatementController extends Controller
{
  public function weekly(){
    $statements = DB::table('statement_balance_sheet')->where('parent', auth()->user()->id)->orderby('id','desc')->paginate(20);
    $checkBankSegre = DB::table('bank_segre')->where('parent',auth()->user()->parent)->where('statusRekening','active')->count(); //harus auth()->user()->parent karena check atasannya bisa ditransfer atau ga
    $capital = DB::table('statement_balance_sheet')->where('parent', auth()->user()->id)->where('tipe_transaksi','LIKE','%Warrant Capital%')->sum('amount_partner');
    $outstanding = DB::table('statement_balance_sheet')->where('parent', auth()->user()->id)->where('tipe_mutasi','LIKE','%Company%')->where('status_payment','0')->sum('amount_partner');
    $warrant = $capital + $outstanding;
    return response()->json([
      'success' => true,
      'data' => $statements,
      'checkBankSegre' => $checkBankSegre,
      'warrant' => $warrant
    ]);
  }

  public function weeklySettlement(){
    $statements = DB::table('statement_balance_sheet')->where('parent', auth()->user()->id)->where('status_payment','0')->where('tipe_transaksi','weekly settlement')->orderby('id','asc')->paginate(20);
    return response()->json([
      'success' => true,
      'data' => $statements
    ]);
  }

  public function commission(){
    $lots = DB::table('lot_week_by_parent')->where('parent', auth()->user()->id)->orderby('id','desc')->paginate(10);

    return response()->json([
      'success' => true,
      'data' => $lots,
      'user' => auth()->user()
    ]);
  }

  public function tradingStatement(){
    $weeklyClosedStatements = DB::table('statement_partner_weekly')->where('parent', auth()->user()->id)->whereNotNull('weekoty')->orderby('id','desc')->paginate(10);
    /*-------------------*/
    return response()->json([
      'success' => true,
      'data' => $weeklyClosedStatements
    ]);
  }

  public function showTradingStatementClient(Request $request){
    $weeklyClosedStatements = DB::table('statement_client_weekly')->join('users_cabinet','users_cabinet.uuid','statement_client_weekly.uuid')->where('statement_client_weekly.parent',auth()->user()->id)->where('statement_client_weekly.weekoty',$request->week)->where('statement_client_weekly.year',$request->year)->orderby('statement_client_weekly.id','desc')->paginate(10);
    return response()->json([
      'success' => true,
      'data' => $weeklyClosedStatements
    ]);
  }

  public function showTradingStatementAccount(Request $request){
    $weeklyClosedStatements = DB::table('raw_data_deal')->where('uuid',$request->uuid)->where('lotweek',$request->week)->where('lotyear',$request->year)->where('Entry','1')->where('Action','<','2')->orderby('Time','desc')->get()->toArray();
    $weeklyBalanceStatements = DB::table('raw_data_deal')->where('uuid',$request->uuid)->where('lotweek',$request->week)->where('lotyear',$request->year)->where('Action','2')->orderby('Time','desc')->get()->toArray();
    $distinct = DB::table('raw_data_position')->where('uuid',$request->uuid)->where('lotweek',$request->week)->where('lotyear',$request->year)->distinct()->select('Position')->get();
    $weeklyFloatingStatements = [];
    foreach ($distinct as $v) {
      $arr = DB::table('raw_data_position')->where('Position',$v->Position)->orderby('humanTime','desc')->first();
      $weeklyFloatingStatements[] = $arr;
    }
    $data = array_merge($weeklyClosedStatements,$weeklyBalanceStatements);
    $data = array_merge($data, $weeklyFloatingStatements);
    $data = collect($data)->sortByDesc('created_at')->values();

    return response()->json([
      'success' => true,
      'data' => $data
    ]);
  }

  public function commissionDetail($id){
    $weeklyClosedStatements = array();
    $weeklyFloatingStatements = array();
    $weeklyBalanceStatements = array();
    $commission = array();
    $lastweekFloatingPL = array();
    $adjustPL = array();
    $adjustRollover = array();

    $lot = DB::table('lot_week_by_parent')->where('parent', auth()->user()->id)->where('id', $id)->orderby('id','desc')->first();
    // return $lot;
    if($lot){
      $parent = auth()->user()->id;
      $week = $lot->week;
      $year = $lot->year;
      $weeklyClosedStatements = DB::table('raw_data_deal')->where('parent',$parent)->where('lotweek',$week)->where('lotyear',$year)->where('Entry','1')->where('Action','<','2')->orderby('Time','desc')->get();
      $weeklyBalanceStatements = DB::table('raw_data_deal')->where('parent',$parent)->where('lotweek',$week)->where('lotyear',$year)->where('Action','2')->orderby('Time','desc')->get();

      $commission = DB::table('lot_week_by_parent')->where('parent',$parent)->where('week',$week)->where('year',$year)->first();
      $getlotdate = DB::table('raw_data_position')->where('parent',$parent)->where('lotweek',$week)->where('lotyear',$year)->distinct()->select('lotdate')->get();
      // return $getlotdate;
      foreach ($getlotdate as $key => $value) {
        $date = date('w',strtotime('1 Jan 2022 + '.$value->lotdate.' days'));
        if ($date == "6") {
          $lotdate = $value->lotdate;
          $weeklyFloatingStatements = DB::table('raw_data_position')->where('parent',$parent)->where('lotweek',$week)->where('lotyear',$year)->where('lotdate',$lotdate)->get();
          foreach ($weeklyFloatingStatements as $k => $v) {
            // code...
            $datelot = $v->lotdate - 1;
            $weeklyFloatingStatements[$k]->countswap = DB::table('raw_data_position')->where('Position',$v->Position)->where('lotdate',$datelot)->count();
            $weeklyFloatingStatements[$k]->stor = DB::table('raw_data_position')->where('Position',$v->Position)->where('lotdate',$datelot)->first();
          }
        }
      }
      //dd($lotdate);

      $lastweekFloatingPL = DB::table('statement_partner_weekly')->where('parent',$parent)->where('weekoty','<',$week)->where('year',$year)->orderby('id','desc')->first();
      $adjustPL = DB::table('raw_data_deal')->where('parent',$parent)->where('lotweek',$week)->where('lotyear',$year)->where('Action','2')->where('flag_statement','1')->get();
      $adjustRollover = DB::table('raw_data_deal')->where('parent',$parent)->where('lotweek',$week)->where('lotyear',$year)->where('Action','2')->where('flag_swap','1')->get();
    }
     //dd($adjustPL,$adjustRollover);
     return response()->json(compact('weeklyClosedStatements','weeklyFloatingStatements','weeklyBalanceStatements','commission','lastweekFloatingPL','adjustPL','adjustRollover'));
  }

  public function weeklySettlementDetail(Request $request){
    $statements = DB::table('statement_balance_sheet')->where('sumber', $request->sumber)->where('parent',auth()->user()->id)->first();

    if (auth()->user()->id_cms_privileges == 50) {
      $reconsile = DB::table('calculate_mib_weekly')->where('hashCalc',$request->sumber)->first();
      if (!$reconsile) {
        $reconsile = DB::table('calculate_sib_weekly')->where('hashCalc',$request->sumber)->first();
        $downline = 0;
      }else{
        $downline = $reconsile->mib;
      }
      $banksatu = DB::table('bank_segre')->where('parent',auth()->user()->id)->where('statusRekening','active')->first();
      $bankdua= DB::table('bank_segre')->where('parent',$downline)->where('statusRekening','active')->first();
      $status = "sib";
    }elseif(auth()->user()->id_cms_privileges == 60){
      $reconsile = DB::table('calculate_ib_weekly')->where('hashCalc',$request->sumber)->first();
      if (!$reconsile) {
        $reconsile = DB::table('calculate_mib_weekly')->where('hashCalc',$request->sumber)->first();
        $downline = $reconsile->sib;
      }else{
        $downline = $reconsile->ib;
      }
      $banksatu = DB::table('bank_segre')->where('parent',auth()->user()->id)->where('statusRekening','active')->first();
      $bankdua= DB::table('bank_segre')->where('parent',$downline)->where('statusRekening','active')->first();
      $status = "mib";
    }elseif (auth()->user()->id_cms_privileges == 70) {
      $reconsile = DB::table('calculate_client_weekly')->where('hashCalc',$request->sumber)->first();
      if (!$reconsile) {
        $reconsile = DB::table('calculate_ib_weekly')->where('hashCalc',$request->sumber)->first();
        $downline = $reconsile->mib;
      }else{
        $downline = $reconsile->ib;
      }
      $banksatu = DB::table('bank_segre')->where('parent',auth()->user()->id)->where('statusRekening','active')->first();
      $bankdua= DB::table('bank_segre')->where('parent',$downline)->where('statusRekening','active')->first();
      $status = "ib";
    }

    if ($statements->mutasi > 0) {
        $bank_to = $banksatu;
        $bank_from = $bankdua;
      }elseif($statements->mutasi < 0){
        $bank_to = $bankdua;
        $bank_from = $banksatu;
      }


    $bank = DB::table('bank_segre')->where('parent',auth()->user()->parent)->where('statusRekening','active')->count();
    return response()->json([
      'success' => true,
      'data' => $statements,
      'bank_from' => $bank_from,
      'bank_to' => $bank_to
    ]);
  }

  public function weeklySettlementPaymentSlip(Request $request){

    //return $request->all();

    /** if request has photo then do upload photo*/
    $path = NULL;
    if($request->file('photo')){

      return response()->json([
        "success" => false,
        "message" => "image bukan base64"
      ], 422);

    }
    else if($request->photo){
      $imageName = "settlement-".\Str::random(2).time();
      $this->uploadGambar($request->photo, $imageName);
      $path = $imageName.".png";
    }
    else{
      $path = '/images/illustration/default-img.jpg';
    }
    /** end upload photo*/
    $check = DB::table('statement_balance_sheet')->where('id',$request->idbc)->first();
    if ($check->sumber == $request->uid) {
      $update = DB::table('statement_balance_sheet')->where('sumber',$request->uid)->update([
        'bukti_payment' => $path
      ]);
      return response()->json([
        'success' => true,
        'message' => 'Successfully Upload Payment Slip'
      ]);
    }else{
      return response()->json([
        'success' => false,
        'message' => 'Data Integrity Fail on UID'
      ]);
    }


  }

  public function weeklySettlementPaymentVerification(Request $request){
    if ($request->status=='Approve') {
      $update = DB::table('statement_balance_sheet')->where('sumber',$request->uid)->update([
        'status_payment' => '1'
      ]);
      return response()->json([
        'success' => true,
        'message' => 'Successfully Verify the payment'
      ]);
    }else{
      return response()->json([
        'success' => false,
        'message' => 'Data Integrity Fail on Payment'
      ]);
    }
  }

  public function uploadGambar($image, $image_name){
     $client = new Client();
     $res = $client->post("https://static.8377-2842.xyz/api/v1/upload/image/ib",[
       "form_params" => [
         "secret" => "anantonthemovedoesmorethanadozingox",
         "gambar" => $image,
         "image_name" => $image_name
       ]
     ]);
   }
}
