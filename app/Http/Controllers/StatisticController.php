<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Tarikhagustia\LaravelMt5\LaravelMt5;
use App\Models\InMarketClosedDataModel;


class StatisticController extends Controller
{
    public function selfInsightTotal($login){

    }

    public function selfInsightperSymbol($login,$symbol){
      $baseSymbol = explode(".",$symbol);
      $countSelfInsight = DB::table('account_statistic')->where('account',$login)->where('symbol','LIKE','%'.$baseSymbol[0].'%')->count();
      if ($countSelfInsight > 0) {
        $selfInsight = DB::table('account_statistic')->where('account',$login)->where('symbol','LIKE','%'.$baseSymbol[0].'%')->first();
        $package = [$selfInsight->pct_buy_num,$selfInsight->pct_sell_profit,$selfInsight->pct_sell_loss,$selfInsight->pct_sell_num,$selfInsight->pct_buy_loss,$selfInsight->pct_buy_profit];
      }else{
        $package = [0,0,0,0,0,0];
      }
      return response()->json($package);
    }

    public function traderTradeTotalperSymbol($symbol){
      $baseSymbol = explode(".",$symbol);

      $this_week = date('W');
      $today = date('z');
      $last30day = $today - 30;
      $this_year = date('Y');

      $fBuy = 0;
      $fSell = 0;
      $cBuy = 0;
      $cSell = 0;

      $positions = DB::table('raw_data_position')->where('Symbol','LIKE','%'.$baseSymbol[0].'%')->where('lotweek',$this_week)->where('lotyear',$this_year)->distinct()->select('Position')->get();

      foreach ($positions as $pos) {
        $floating = DB::table('raw_data_position')->where('Position',$pos->Position)->first();
        if ($floating->Action == "0") {
          $fBuy++;
        }elseif($floating->Action == "1"){
          $fSell++;
        }
      }

      $deals = DB::table('raw_data_deal')->where('Symbol','LIKE','%'.$baseSymbol[0].'%')->where('lotdate','>',$last30day)->where('lotyear',$this_year)->get();

      foreach ($deals as $deal) {
        if ($deal->Entry == "1") {
          if ($deal->Action == "0") {
            $cBuy++;
          }elseif($deal->Action == "1"){
            $cSell++;
          }
        }
      }

      $tBuy = $fBuy + $cBuy;
      $tSell = $fSell + $cSell;

      if ($fBuy+$fSell > 0) {
        $pctFBuy = 100* $fBuy / ($fBuy+$fSell);
      }else{
        $pctFBuy = 0;
      }

      if ($cBuy+$cSell > 0) {
        $pctCBuy = 100* $cBuy / ($cBuy+$cSell);
      }else{
        $pctCBuy = 0;
      }

      if ($tBuy+$tSell > 0) {
        $pctTBuy = 100* $tBuy / ($tBuy+$tSell);
      }else{
        $pctTBuy = 0;
      }

      if ($fBuy+$fSell > 0) {
        $pctFSell = 100* $fSell / ($fBuy+$fSell);
      }else{
        $pctFSell = 0;
      }

      if ($cBuy+$cSell > 0) {
        $pctCSell = 100* $cSell / ($cBuy+$cSell);
      }else{
        $pctCSell = 0;
      }

      if ($tBuy+$tSell > 0) {
        $pctTSell = 100* $tSell / ($tBuy+$tSell);
      }else{
        $pctTSell =0;
      }

      $package = [$pctTBuy,$pctTSell];
      return response()->json($package);
    }

    public function traderTradeClosedperSymbol($symbol){
      $baseSymbol = explode(".",$symbol);

      $this_week = date('W');
      $today = date('z');
      $last30day = $today - 30;
      $this_year = date('Y');

      $cBuy = 0;
      $cSell = 0;

      $deals = DB::table('raw_data_deal')->where('Symbol','LIKE','%'.$baseSymbol[0].'%')->where('lotdate','>',$last30day)->where('lotyear',$this_year)->get();

      foreach ($deals as $deal) {
        if ($deal->Entry == "1") {
          if ($deal->Action == "0") {
            $cBuy++;
          }elseif($deal->Action == "1"){
            $cSell++;
          }
        }
      }

      if ($cBuy+$cSell > 0) {
        $pctCBuy = 100* $cBuy / ($cBuy+$cSell);
      }else{
        $pctCBuy = 0;
      }

      if ($cBuy+$cSell > 0) {
        $pctCSell = 100* $cSell / ($cBuy+$cSell);
      }else{
        $pctCSell = 0;
      }

      $package = [$pctCBuy,$pctCSell];
      return response()->json($package);
    }

    public function traderTradeFloatingperSymbol($symbol){
      $baseSymbol = explode(".",$symbol);

      $this_week = date('W');
      $today = date('z');
      $last30day = $today - 30;
      $this_year = date('Y');

      $fBuy = 0;
      $fSell = 0;

      $positions = DB::table('raw_data_position')->where('Symbol','LIKE','%'.$baseSymbol[0].'%')->where('lotweek',$this_week)->where('lotyear',$this_year)->distinct()->select('Position')->get();

      foreach ($positions as $pos) {
        $floating = DB::table('raw_data_position')->where('Position',$pos->Position)->first();
        if ($floating->Action == "0") {
          $fBuy++;
        }elseif($floating->Action == "1"){
          $fSell++;
        }
      }

      if ($fBuy+$fSell > 0) {
        $pctFBuy = 100* $fBuy / ($fBuy+$fSell);
      }else{
        $pctFBuy = 0;
      }

      if ($fBuy+$fSell > 0) {
        $pctFSell = 100* $fSell / ($fBuy+$fSell);
      }else{
        $pctFSell = 0;
      }

      $package = [$pctFBuy,$pctFSell];
      return response()->json($package);
    }

    public function inMarketClosedData(Request $request){
      $api = new LaravelMt5();
      $resultBatch = $api->httpGet('/api/user/get?login='.$request->login,[]);
      $dataBatch = json_decode($resultBatch);
      if($dataBatch->retcode == "0 Done"){
        $traderBuy = InMarketClosedDataModel::where('symbol','LIKE','%'.$request->symbol.'%')->where('buy_numb','>',2)->orderby('pct_buy_profit','desc')->limit(5)->get();
        $traderSell = InMarketClosedDataModel::where('symbol','LIKE','%'.$request->symbol.'%')->where('sell_numb','>',2)->orderby('pct_sell_profit','desc')->limit(5)->get();
      }
      return response()->json([
        'success'=>true,
        'traderBuy' => $traderBuy,
        'traderSell' => $traderSell,
      ]);
    }

    public function inmarketFloatingData(Request $request){
      $this_week = date('W');
      $this_year = date('Y');
      $api = new LaravelMt5();
      $resultBatch = $api->httpGet('/api/user/get?login='.$request->login,[]);
      $dataBatch = json_decode($resultBatch);
      if($dataBatch->retcode == "0 Done"){
        $accBuyFloating = [];
        $accSellFloating = [];

        $positions = DB::table('raw_data_position')->where('Symbol','LIKE','%'.$request->symbol.'%')->where('lotweek',$this_week)->where('lotyear',$this_year)->distinct()->select('Position')->get();

        $fBuy = 0;
        $fSell = 0;

        foreach ($positions as $pos) {
          $floating = DB::table('raw_data_position')->where('Position',$pos->Position)->first();
          if ($floating->Action == "0") {
            $accBuyFloating[] = $floating->Login;
            $fBuy++;
          }elseif($floating->Action == "1"){
            $accSellFloating[] = $floating->Login;
            $fSell++;
          }
        }

        if ($fBuy > 0) {
          $flbuy = array_values(array_flip(array_flip( $accBuyFloating)));
          foreach ($flbuy as $abf) {
            $floatingBuy = InMarketClosedDataModel::where('symbol','LIKE','%'.$request->symbol.'%')->where('account',$abf)->first();
            if ($floatingBuy) {
              $accFloatBuy[] = $floatingBuy;
            }
          }
          $sortflbuy = collect($accFloatBuy)->sortBy('pct_buy_profit')->reverse()->toArray();
        }else{
          $sortflbuy = [];
        }

        if ($fSell > 0) {
          $flsell = array_values(array_flip(array_flip( $accSellFloating)));
          foreach ($flsell as $asf) {
            $floatingSell = InMarketClosedDataModel::where('symbol','LIKE','%'.$request->symbol.'%')->where('account',$asf)->first();
            if ($floatingSell) {
              $accFloatSell[] = $floatingSell;
            }
          }
          $sortflsell = collect($accFloatSell)->sortBy('pct_sell_profit')->reverse()->toArray();
        }else{
          $sortflsell = [];
        }
      }
      // dd($sortflbuy);
      return response()->json([
        'success'=>true,
        'accFloatBuy' => array_values($sortflbuy),
        'accFloatSell' => array_values($sortflsell)
      ]);
    }
}
