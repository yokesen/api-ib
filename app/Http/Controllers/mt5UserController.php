<?php

namespace App\Http\Controllers;
use JWTAuth;
use Auth;
use Illuminate\Http\Request;
use Tarikhagustia\LaravelMt5\LaravelMt5;
use Tarikhagustia\LaravelMt5\Entities\User;
use Tarikhagustia\LaravelMt5\Entities\Trade;
use Tarikh\PhpMeta\MetaTraderClient;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Arr;
use App\Models\symbolTechnicalAnalysisModel;

class mt5UserController extends Controller
{
    public function login(Request $request){

      $api = new LaravelMt5();

      $result = $api->getUser($request->login);

      return response()->json([
        'success' => true,
        'response' => $result,
      ]);
    }

    public function checkBalance(Request $request){

      $api = new LaravelMt5();

      try {
        $user = $api->getTradingAccounts($request->login);
        $balance = $user->Balance;

        return response()->json([
          'success' => true,
          'balance' => $balance,
        ]);
      } catch (\Exception $e) {
        return response()->json([
          'success' => false,
          'balance' => 'not found',
        ]);
      }
    }

    public function preliminary_trade($id,$asset){
      $token = $request->header('Authorization');
      try {
        if ($user = JWTAuth::authenticate($token)) {
          $uuid =   $user['uuid'];
          $myAccounts = DB::table('typeMT4AvailabeAccount')->where('uuid',$uuid)->whereNotNull('mt4_id')->get();

          if (count($myAccounts)>0) {

            foreach ($myAccounts as $key => $value) {
              $exampleLogin=$value->mt4_id;
              $timestampfrom=strtotime('2021-08-23 00:00:00');
              $timestampto=time();
              $api = new LaravelMt5();
              // Get Closed Order Total and pagination
              $total = $api->getDealTotal($exampleLogin, $timestampfrom, $timestampto);
              $trades = $api->getDealPaginate($exampleLogin, $timestampfrom, $timestampto, 0, $total);
              $trading[] = $trades;
            }



            return response()->json([
              'success' => true,
              'data' => $trading
            ]);
          }else{
            return response()->json([
              'success' => false,
              'Data' => 'Data not Found'
            ]);
          }

        }
      } catch (JWTException $e) {
        return response()->json([
          'success' => false,
          'message' => 'Invalid Token.',
        ], 401);
      }
    }

    public function dataTrading(Request $request){

      $token = $request->header('Authorization');
      try {
        if ($user = JWTAuth::authenticate($token)) {
          $uuid =   $user['uuid'];
          $myAccounts = DB::table('typeMT4AvailabeAccount')->where('uuid',$uuid)->whereNotNull('mt4_id')->get();

          if (count($myAccounts)>0) {

            foreach ($myAccounts as $key => $value) {
              $exampleLogin=$value->mt4_id;
              $timestampfrom=strtotime('2021-08-23 00:00:00');
              $timestampto=time();
              $api = new LaravelMt5();
              // Get Closed Order Total and pagination
              $total = $api->getDealTotal($exampleLogin, $timestampfrom, $timestampto);
              $trades = $api->getDealPaginate($exampleLogin, $timestampfrom, $timestampto, 0, $total);
              $trading[] = $trades;
            }



            return response()->json([
              'success' => true,
              'data' => $trading
            ]);
          }else{
            return response()->json([
              'success' => false,
              'Data' => 'Data not Found'
            ]);
          }

        }
      } catch (JWTException $e) {
        return response()->json([
          'success' => false,
          'message' => 'Invalid Token.',
        ], 401);
      }
    }

    public function bypassAccountData(Request $request){
      $api = new LaravelMt5();

      $resultBatch = $api->httpGet('/api/user/get?login='.$request->login,[]);
      $dataBatch = json_decode($resultBatch);

      if($dataBatch->retcode == "0 Done"){
        $grup = $dataBatch->answer->Group;

        $user = $api->getTradingAccounts($request->login);

        $balance = $user->Balance;
        $equity = $user->Equity;
        $freeMargin = $user->MarginFree;

        $getSymbolPair = DB::table('pairs')->where('pair',$request->symbol)->first();
        $base = $getSymbolPair->base;
        $path = DB::table('pairs')->join('groupMetatrader','groupMetatrader.path','pairs.path')->where('groupMetatrader.grupMT',$grup)->where('pairs.base',$base)->first();
        $pair = $path->pair;

        $symbolGroup = $api->httpGet('/api/symbol/get_group?symbol='.$pair.'&group='.$grup,[]);
        $sGroup = json_decode($symbolGroup);

        $leverage = $dataBatch->answer->Leverage;
        $MarginInitial = $sGroup->answer->MarginInitial;
        $VolumeMin = $sGroup->answer->VolumeMin/10000;
        $VolumeMax = $sGroup->answer->VolumeMax/10000;
        $VolumeStep = $sGroup->answer->VolumeStep/10000;

        $margin_requirement = $MarginInitial/$leverage;
        $maxVolumeFreeMargin = floor(($freeMargin/$margin_requirement) * 100) / 100;

        switch ($sGroup->answer->VolumeStep) {
          case '100': $fraction = 2;break;
          case '1000': $fraction = 1;break;
          case '10000': $fraction = 0;break;
          default : $fraction = 0;break;
        }

        if($maxVolumeFreeMargin>$VolumeMax){
          $maxVolumeFreeMargin = $VolumeMax;
        }

        return response()->json([
          'success' => true,
          'data' => [
            'pair' => $pair,
            'login' => $user->Login,
            'equity' => number_format($equity,2,'.',''),
            'freeMargin' => number_format($freeMargin,2,'.',''),
            'leverage' => $leverage,
            'marginRequirement' => number_format($margin_requirement,0,'.',''),
            'VolumeMin' => number_format($VolumeMin,2,'.',''),
            'VolumeMax' => number_format($VolumeMax,2,'.',''),
            'VolumeStep' => number_format($VolumeStep,2,'.',''),
            'MaxVolumeFromFreeMargin' => number_format($maxVolumeFreeMargin,$fraction,'.',''),
          ]
        ]);
      }else{
        return response()->json([
          'success' => false,
          'Data' => 'no data in server'
        ]);
      }
    }

    public function dataAccount(Request $request){
      $token = $request->header('Authorization');
      try {
        if ($user = JWTAuth::authenticate($token)) {
          $uuid =   $user['uuid'];
          $myAccounts = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->login)->first();

          if ($myAccounts) {
            if ( $myAccounts->uuid == $uuid) {
              $api = new LaravelMt5();

              $resultBatch = $api->httpGet('/api/user/get?login='.$request->login,[]);
              $dataBatch = json_decode($resultBatch);

              if($dataBatch->retcode == "0 Done"){
                $grup = $dataBatch->answer->Group;

                $user = $api->getTradingAccounts($request->login);

                $balance = $user->Balance;
                $equity = $user->Equity;
                $freeMargin = $user->MarginFree;

                $getSymbolPair = DB::table('pairs')->where('pair',$request->symbol)->first();
                $base = $getSymbolPair->base;
                $path = DB::table('pairs')->join('groupMetatrader','groupMetatrader.path','pairs.path')->where('groupMetatrader.grupMT',$grup)->where('pairs.base',$base)->first();
                $pair = $path->pair;

                $symbolGroup = $api->httpGet('/api/symbol/get_group?symbol='.$pair.'&group='.$grup,[]);
                $sGroup = json_decode($symbolGroup);

                $leverage = $dataBatch->answer->Leverage;
                $MarginInitial = $sGroup->answer->MarginInitial;
                $VolumeMin = $sGroup->answer->VolumeMin/10000;
                $VolumeMax = $sGroup->answer->VolumeMax/10000;
                $VolumeStep = $sGroup->answer->VolumeStep/10000;

                $margin_requirement = $MarginInitial/$leverage;
                $maxVolumeFreeMargin = floor(($freeMargin/$margin_requirement) * 100) / 100;

                switch ($sGroup->answer->VolumeStep) {
                  case '100': $fraction = 2;break;
                  case '1000': $fraction = 1;break;
                  case '10000': $fraction = 0;break;
                  default : $fraction = 0;break;
                }

                if($maxVolumeFreeMargin>$VolumeMax){
                  $maxVolumeFreeMargin = $VolumeMax;
                }

                return response()->json([
                  'success' => true,
                  'data' => [
                    'pair' => $pair,
                    'login' => $user->Login,
                    'equity' => number_format($equity,2,'.',''),
                    'freeMargin' => number_format($freeMargin,2,'.',''),
                    'leverage' => $leverage,
                    'marginRequirement' => number_format($margin_requirement,0,'.',''),
                    'VolumeMin' => number_format($VolumeMin,2,'.',''),
                    'VolumeMax' => number_format($VolumeMax,2,'.',''),
                    'VolumeStep' => number_format($VolumeStep,2,'.',''),
                    'MaxVolumeFromFreeMargin' => number_format($maxVolumeFreeMargin,$fraction,'.',''),
                  ]
                ]);
              }else{

                return response()->json([
                  'success' => false,
                  'Data' => ['result' => 'no data in server']
                ]);
              }


            } else {
              return response()->json([
                'success' => false,
                'Data' => 'forbiden : wrong user'
              ]);
            }
          }else{
            return response()->json([
              'success' => false,
              'Data' => 'Data not Found'
            ]);
          }

        }
      } catch (JWTException $e) {
        return response()->json([
          'success' => false,
          'message' => 'Invalid Token.',
        ], 401);
      }
    }

    public function openPendingOrderNo(Request $request){
      $rules = [
        'login' => 'required|numeric',
        'symbol' => 'required|string',
        'volume' => 'required|numeric',
        'type' => 'required|numeric',
        'priceOrder' => 'required|numeric',
        'priceTP' => 'required|numeric',
        'priceSL' => 'required|numeric',
        'comment' => 'required',
      ];

      $valid = Validator::make($request->all(), $rules);

      if($valid->fails()){
        return response()->json([
          'status' => false,
          'errors' => [
            $valid->errors()
          ]
        ]);
      }

      $api = new LaravelMt5();

      $data = [
            'Login'  => $request->login,
            'Symbol' => $request->symbol,
            'Volume' => $request->volume,
            'Type'   => $request->type,
            'PriceOrder' => $request->priceOrder,
            'PriceTP' => $request->priceTP,
            'PriceSL' => $request->priceSL,
            'Comment' => $request->comment,
            'Action' => 201,
            'TypeTime' => 2,
            'TimeExpiration' => strtotime('2021-08-30 00:00:00'),
            'TypeFill' => 2
        ];

    }

    public function openPendingOrder(Request $request){

      $rules = [
        'login' => 'required|numeric',
        'symbol' => 'required|string',
        'volume' => 'required|numeric',
        'type' => 'required|numeric',
        'priceOrder' => 'required|numeric',
        'priceTP' => 'required|numeric',
        'priceSL' => 'required|numeric',
        'comment' => 'required',
      ];

      $valid = Validator::make($request->all(), $rules);

      if($valid->fails()){
        return response()->json([
          'status' => false,
          'errors' => [
            $valid->errors()
          ]
        ]);
      }

      $token = $request->header('Authorization');
      try {
        if ($user = JWTAuth::authenticate($token)) {
          $uuid =   $user['uuid'];
          $myAccounts = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->login)->first();
          if ($myAccounts) {
            if ( $myAccounts->uuid == $uuid) {

              $api = new LaravelMt5();

              $symbol = $api->httpGet('/api/symbol/get?symbol='.$request->symbol,[]);
              $rsymbol = json_decode($symbol);
              $digit = $rsymbol->answer->Digits;

                $data = [
                      'Login'  => $request->login,
                      'Symbol' => $request->symbol,
                      'Volume' => $request->volume,
                      'Type'   => $request->type,
                      'Digits' => $digit,
                      'PriceOrder' => $request->priceOrder,
                      'PriceTP' => $request->priceTP,
                      'PriceSL' => $request->priceSL,
                      'Comment' => $request->comment,
                      'Action' => 201,
                      'TypeTime' => 0,
                      'TypeFill' => 2
                  ];

                  $result = $api->dealerSend($data);

                return response()->json([
                  'success' => true,
                  'result' => $result,
                  'data' => $data,
                  'digit' => $digit
                ]);


            } else {
              return response()->json([
                'success' => false,
                'Data' => 'forbiden : wrong user'
              ]);
            }
          }else{
            return response()->json([
              'success' => false,
              'Data' => 'Data not Found'
            ]);
          }

        }
      } catch (JWTException $e) {
        return response()->json([
          'success' => false,
          'message' => 'Invalid Token.',
        ], 401);
      }
    }

    public function tradeLoginAccount(Request $request){
      $rules = [
        'login' => 'required|numeric',
        'password' => 'required'
      ];

      $valid = Validator::make($request->all(), $rules);

      if($valid->fails()){
        return response()->json([
          'status' => false,
          'errors' => [
            $valid->errors()
          ]
        ]);
      }

      $token = $request->header('Authorization');
      try {
        if ($user = JWTAuth::authenticate($token)) {
          $uuid =   $user['uuid'];
          $myAccounts = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->login)->first();
          if ($myAccounts) {
            if ( $myAccounts->uuid == $uuid) {

              $api = new LaravelMt5();

              $getPassword = $api->httpGet('/api/user/check_password?login='.$request->login.'&type=main&password='.$request->password,[]);
              $dataPassword = json_decode($getPassword);

              if ($dataPassword->retcode == "0 Done") {
                return response()->json([
                  'success' => true,
                  'result' => ['logged in']
                ]);
              }else{
                return response()->json([
                  'success' => false,
                  'result' => ['login failed']
                ]);
              }


            } else {
              return response()->json([
                'success' => false,
                'Data' => 'forbiden : wrong user'
              ]);
            }
          }else{
            return response()->json([
              'success' => false,
              'Data' => 'Data not Found'
            ]);
          }

        }
      } catch (JWTException $e) {
        return response()->json([
          'success' => false,
          'message' => 'Invalid Token.',
        ], 401);
      }

    }

    public function updateOrder(Request $request){
      $rules = [
        'login' => 'required|numeric',
        'symbol' => 'required|string',
        'type' => 'required|numeric',
        'priceOrder' => 'required|numeric',
        'priceTP' => 'required|numeric',
        'priceSL' => 'required|numeric',
        'comment' => 'required',
        'order' => 'required'
      ];

      $valid = Validator::make($request->all(), $rules);

      if($valid->fails()){
        return response()->json([
          'status' => false,
          'errors' => [
            $valid->errors()
          ]
        ]);
      }

      $token = $request->header('Authorization');
      try {
        if ($user = JWTAuth::authenticate($token)) {
          $uuid =   $user['uuid'];
          $myAccounts = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->login)->first();
          if ($myAccounts) {
            if ( $myAccounts->uuid == $uuid) {

              $api = new LaravelMt5();

              $data = [
                    'Login'  => $request->login,
                    'Symbol' => $request->symbol,
                    'Type'   => $request->type,
                    'Order'   => $request->order,
                    'PriceOrder' => $request->priceOrder,
                    'PriceTP' => $request->priceTP,
                    'PriceSL' => $request->priceSL,
                    'Comment' => $request->comment,
                    'Action' => 203,
                ];

                $result = $api->dealerSend($data);

              return response()->json([
                'success' => true,
                'data' => $result
              ]);
            } else {
              return response()->json([
                'success' => false,
                'Data' => 'forbiden : wrong user'
              ]);
            }
          }else{
            return response()->json([
              'success' => false,
              'Data' => 'Data not Found'
            ]);
          }

        }
      } catch (JWTException $e) {
        return response()->json([
          'success' => false,
          'message' => 'Invalid Token.',
        ], 401);
      }
    }

    public function cancelOrder(Request $request){
      $rules = [
        'login' => 'required|numeric',
        'symbol' => 'required|string',
        'type' => 'required|numeric',
        'comment' => 'required',
        'order' => 'required',
      ];

      $valid = Validator::make($request->all(), $rules);

      if($valid->fails()){
        return response()->json([
          'status' => false,
          'errors' => [
            $valid->errors()
          ]
        ]);
      }

      $token = $request->header('Authorization');
      try {
        if ($user = JWTAuth::authenticate($token)) {
          $uuid =   $user['uuid'];
          $myAccounts = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->login)->first();
          if ($myAccounts) {
            if ( $myAccounts->uuid == $uuid) {

              $api = new LaravelMt5();

              $data = [
                    'Login'  => $request->login,
                    'Symbol' => $request->symbol,
                    'Type'   => $request->type,
                    'Order'   => $request->order,
                    'Comment' => $request->comment,
                    'Action' => 204,
                ];

                $result = $api->dealerSend($data);

              return response()->json([
                'success' => true,
                'data' => $result
              ]);
            } else {
              return response()->json([
                'success' => false,
                'Data' => 'forbiden : wrong user'
              ]);
            }
          }else{
            return response()->json([
              'success' => false,
              'Data' => 'Data not Found'
            ]);
          }

        }
      } catch (JWTException $e) {
        return response()->json([
          'success' => false,
          'message' => 'Invalid Token.',
        ], 401);
      }
    }

    public function trade($id){

      $api = new LaravelMt5();
      $result = $api->dealerSend([
            'Login'  => 8113,
            'Symbol' => 'EURUSD',
            'Volume' => 5000,
            'Type'   => 0,
            'Action' => 200,
            'Comment' => 'by STk',
            'PriceSL' => '1.80022',
            'PriceTP' => '1.63444'
        ]);
      dd($result);
    }

    public function getTrades(Request $request){

      $rules = [
        'login' => 'required|numeric'
      ];

      $valid = Validator::make($request->all(), $rules);

      if($valid->fails()){
        return response()->json([
          'status' => false,
          'errors' => [
            $valid->errors()
          ]
        ]);
      }

      $token = $request->header('Authorization');
      try {
        if ($user = JWTAuth::authenticate($token)) {
          $uuid =   $user['uuid'];
          $myAccounts = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->login)->first();
          if ($myAccounts) {
            if ( $myAccounts->uuid == $uuid) {

              $api = new LaravelMt5();
              $exampleLogin=$request->login;
              $today = date('Y-m-d');
              $timestampfrom=strtotime($today.' 00:00:00 - 1 hour');
              $timestampto=time();

              $getTotalOrder = $api->httpGet('/api/order/get_total?login='.$exampleLogin,[]);
              $dataTotal = json_decode($getTotalOrder);

              if ($dataTotal->retcode == '0 Done') {
                $getPageOrder = $api->httpGet('/api/order/get_page?login='.$exampleLogin.'&offset=0&total='.$dataTotal->answer->total,[]);
                $dataOrder = json_decode($getPageOrder);
                //dd($dataOrder->answer);
                $comments = Arr::pluck($dataOrder->answer, 'Comment');
                if ($request->comment != '') {
                  $checkSearch = array_search($request->comment,$dataOrder->answer);
                  foreach ($dataOrder->answer as $xTrade) {
                    if ($xTrade->Comment == $request->comment) {
                      $saveTrade = DB::table('signals_trade')->insert([
                        'uuid' => $uuid,
                        'signal_id' => $request->ticket,
                        'accountId' => $request->login,
                        'ticketId' => $xTrade->Order,
                        'priceOrder' => $xTrade->PriceOrder,
                        'symbol' => $xTrade->Symbol,
                        'digit' => $xTrade->Digits,
                        'type' => $xTrade->Type
                      ]);
                    }
                  }
                }


                return response()->json([
                  'success' => true,
                  'data' => $comments
                ]);
              }else{
                return response()->json([
                  'success' => false,
                  'data' => []
                ]);
              }


            } else {
              return response()->json([
                'success' => false,
                'Data' => 'forbiden : wrong user'
              ]);
            }
          }else{
            return response()->json([
              'success' => false,
              'Data' => 'Data not Found'
            ]);
          }

        }
      } catch (JWTException $e) {
        return response()->json([
          'success' => false,
          'message' => 'Invalid Token.',
        ], 401);
      }
    }

    public function dataAccountMT5(Request $request){
      $api = new LaravelMt5();
      $this_week = date('W');
      $today = date('z');
      $last30day = $today - 30;
      $this_year = date('Y');
      $resultBatch = $api->httpGet('/api/user/get?login='.$request->login,[]);
      $dataBatch = json_decode($resultBatch);
      if($dataBatch->retcode == "0 Done"){
        $grup = $dataBatch->answer->Group;

        $user = $api->getTradingAccounts($request->login);

        $balance = $user->Balance;
        $equity = $user->Equity;
        $freeMargin = $user->MarginFree;

        $mt5Account = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->login)->first();
        $typemt5 = DB::table('typeMT4MasterTemplate')->where('masterName',$mt5Account->typeAccount)->first();

        $pair = $request->symbol;
        $baseSymbol = explode(".",$pair);

        $pair = $baseSymbol[0].$typemt5->sufix;

        $symbolGroup = $api->httpGet('/api/symbol/get_group?symbol='.$pair.'&group='.$grup,[]);
        $sGroup = json_decode($symbolGroup);

        $leverage = $dataBatch->answer->Leverage;
        $MarginInitial = $sGroup->answer->MarginInitial;
        $VolumeMin = $sGroup->answer->VolumeMin/10000;
        $VolumeMax = $sGroup->answer->VolumeMax/10000;
        $VolumeStep = $sGroup->answer->VolumeStep/10000;
        $Digit = $sGroup->answer->Digits;
        $margin_requirement = $MarginInitial/$leverage;
        $maxVolumeFreeMargin = $freeMargin/$margin_requirement;

        switch ($sGroup->answer->VolumeStep) {
          case '100': $fraction = 2;break;
          case '1000': $fraction = 1;break;
          case '10000': $fraction = 0;break;
          default : $fraction = 0;break;
        }

        if($maxVolumeFreeMargin>$VolumeMax){
          $maxVolumeFreeMargin = $VolumeMax;
        }

        $fBuy = 0;
        $fSell = 0;
        $cBuy = 0;
        $cSell = 0;

        // bisa jadi api sendiri untuk in market floating
        $accBuyFloating = [];
        $accSellFloating = [];

        $positions = DB::table('raw_data_position')->where('Symbol','LIKE','%'.$baseSymbol[0].'%')->where('lotweek',$this_week)->where('lotyear',$this_year)->distinct()->select('Position')->get();

        foreach ($positions as $pos) {
          $floating = DB::table('raw_data_position')->where('Position',$pos->Position)->first();
          if ($floating->Action == "0") {
            $accBuyFloating[] = $floating->Login;
            $fBuy++;
          }elseif($floating->Action == "1"){
            $accSellFloating[] = $floating->Login;
            $fSell++;
          }
        }
        // //

        // dibutuhkan? //
        $deals = DB::table('raw_data_deal')->where('Symbol','LIKE','%'.$baseSymbol[0].'%')->where('lotdate','>',$last30day)->where('lotyear',$this_year)->get();

        foreach ($deals as $deal) {
          if ($deal->Entry == "1") {
            if ($deal->Action == "0") {
              $cBuy++;
            }elseif($deal->Action == "1"){
              $cSell++;
            }
          }
        }

        $tBuy = $fBuy + $cBuy;
        $tSell = $fSell + $cSell;

        if ($fBuy+$fSell > 0) {
          $pctFBuy = 100* $fBuy / ($fBuy+$fSell);
        }else{
          $pctFBuy = 0;
        }

        if ($cBuy+$cSell > 0) {
          $pctCBuy = 100* $cBuy / ($cBuy+$cSell);
        }else{
          $pctCBuy = 0;
        }

        if ($tBuy+$tSell > 0) {
          $pctTBuy = 100* $tBuy / ($tBuy+$tSell);
        }else{
          $pctTBuy = 0;
        }

        if ($fBuy+$fSell > 0) {
          $pctFSell = 100* $fSell / ($fBuy+$fSell);
        }else{
          $pctFSell = 0;
        }

        if ($cBuy+$cSell > 0) {
          $pctCSell = 100* $cSell / ($cBuy+$cSell);
        }else{
          $pctCSell = 0;
        }

        if ($tBuy+$tSell) {
          $pctTSell = 100* $tSell / ($tBuy+$tSell);
        }else{
          $pctTSell =0;
        }
        // //

        $tam5 = DB::table('symbol_technical_analysis')->select('datadetail')->where('symbol','LIKE','%'.$baseSymbol[0].'%')->where('timeframe','M5')->orderby('id','desc')->first();
        $tam15 = DB::table('symbol_technical_analysis')->select('datadetail')->where('symbol','LIKE','%'.$baseSymbol[0].'%')->where('timeframe','M15')->orderby('id','desc')->first();
        $tam30 = DB::table('symbol_technical_analysis')->select('datadetail')->where('symbol','LIKE','%'.$baseSymbol[0].'%')->where('timeframe','M30')->orderby('id','desc')->first();
        $tah1 = DB::table('symbol_technical_analysis')->select('datadetail')->where('symbol','LIKE','%'.$baseSymbol[0].'%')->where('timeframe','H1')->orderby('id','desc')->first();
        $tah4 = DB::table('symbol_technical_analysis')->select('datadetail')->where('symbol','LIKE','%'.$baseSymbol[0].'%')->where('timeframe','H4')->orderby('id','desc')->first();
        $tad1 = DB::table('symbol_technical_analysis')->select('datadetail')->where('symbol','LIKE','%'.$baseSymbol[0].'%')->where('timeframe','D1')->orderby('id','desc')->first();
        $taw1 = DB::table('symbol_technical_analysis')->select('datadetail')->where('symbol','LIKE','%'.$baseSymbol[0].'%')->where('timeframe','W1')->orderby('id','desc')->first();
        $tamn1 = DB::table('symbol_technical_analysis')->select('datadetail')->where('symbol','LIKE','%'.$baseSymbol[0].'%')->where('timeframe','MN1')->orderby('id','desc')->first();

        // bisa api sendiri //
        $traderBuy = DB::table('account_statistic')->where('symbol','LIKE','%'.$baseSymbol[0].'%')->where('buy_numb','>',2)->orderby('pct_buy_profit','desc')->limit(5)->get();
        $traderSell = DB::table('account_statistic')->where('symbol','LIKE','%'.$baseSymbol[0].'%')->where('sell_numb','>',2)->orderby('pct_sell_profit','desc')->limit(5)->get();
        // //

        // api sendiri untuk in market floating
        if ($fBuy > 0) {
          $flbuy = array_values(array_flip(array_flip( $accBuyFloating)));
          foreach ($flbuy as $abf) {
            $floatingBuy = DB::table('account_statistic')->where('symbol','LIKE','%'.$baseSymbol[0].'%')->where('account',$abf)->first();
            if ($floatingBuy) {
              $accFloatBuy[] = $floatingBuy;
            }
          }
          $sortflbuy = collect($accFloatBuy)->sortBy('pct_buy_profit')->reverse()->toArray();
        }else{
          $sortflbuy = [];
        }

        if ($fSell > 0) {
          $flsell = array_values(array_flip(array_flip( $accSellFloating)));
          foreach ($flsell as $asf) {
            $floatingSell = DB::table('account_statistic')->where('symbol','LIKE','%'.$baseSymbol[0].'%')->where('account',$asf)->first();
            if ($floatingSell) {
              $accFloatSell[] = $floatingSell;
            }
          }
          $sortflsell = collect($accFloatSell)->sortBy('pct_sell_profit')->reverse()->toArray();
        }else{
          $sortflsell = [];
        }

        //

        return response()->json([
          'success' => true,
          'data' => [
            'digit' => $Digit,
            'today' => $today,
            'last30day' => $last30day,
            'pair' => $pair,
            'grup' => $grup,
            'login' => $user->Login,
            'equity' => number_format($equity,2,'.',''),
            'freeMargin' => number_format($freeMargin,2,'.',''),
            'leverage' => $leverage,
            'marginRequirement' => number_format($margin_requirement,0,'.',''),
            'VolumeMin' => number_format($VolumeMin,2,'.',''),
            'VolumeMax' => number_format($VolumeMax,2,'.',''),
            'VolumeStep' => number_format($VolumeStep,2,'.',''),
            'MaxVolumeFromFreeMargin' => number_format($maxVolumeFreeMargin,2,'.',''),
            'fBuy' => $fBuy,
            'fSell' => $fSell,
            'cBuy' => $cBuy,
            'cSell' => $cSell,
            'tBuy' => $tBuy,
            'tSell' => $tSell,
            'deal' => $deals,
            'pctFBuy' => $pctFBuy,
            'pctFSell' => $pctFSell,
            'pctCBuy' => $pctCBuy,
            'pctCSell' => $pctCSell,
            'pctTBuy' => $pctTBuy,
            'pctTSell' => $pctTSell,
            'tam5' => $tam5,
            'tam15' => $tam15,
            'tam30' => $tam30,
            'tah1' => $tah1,
            'tah4' => $tah4,
            'tad1' => $tad1,
            'taw1' => $taw1,
            'tamn1' => $tamn1,
            'traderBuy' => $traderBuy,
            'traderSell' => $traderSell,
            'accFloatBuy' => $sortflbuy,
            'accFloatSell' => $sortflsell
          ]
        ]);
      }
    }

    public function selfinsight(Request $request){

      $countSelfInsight = DB::table('account_statistic')->where('account',$request->login)->count();
      if ($countSelfInsight > 0) {
        $perSymbol = DB::table('account_statistic')->where('account',$request->login)->get();
        $buy_pl = 0;
        $sell_pl = 0;
        $total_buy_profit = 0;
        $total_buy_loss = 0;
        $total_sell_profit = 0;
        $total_sell_loss = 0;
        $buy_numb = 0;
        $sell_numb = 0;
        $buy_profit = 0;
        $buy_loss = 0;
        $sell_profit = 0;
        $sell_loss = 0;
        $pct_buy_num = 0;
        $pct_sell_num = 0;
        $pct_buy_profit = 0;
        $pct_buy_loss = 0;
        $pct_sell_profit = 0;
        $pct_sell_loss = 0;
        $avg_profit = 0;
        $avg_loss = 0;

        foreach ($perSymbol as $ps) {
          $nickname = $ps->nickname;
          $account = $ps->account;
          $buy_pl += $ps->buy_pl;
          $sell_pl += $ps->sell_pl;
          $total_buy_profit += $ps->total_buy_profit;
          $total_buy_loss += $ps->total_buy_loss;
          $total_sell_profit += $ps->total_sell_profit;
          $total_sell_loss += $ps->total_sell_loss;
          $buy_numb += $ps->buy_numb;
          $sell_numb += $ps->sell_numb;
          $buy_profit += $ps->buy_profit;
          $buy_loss += $ps->buy_loss;
          $sell_profit += $ps->sell_profit;
          $sell_loss += $ps->sell_loss;
          $deals[$ps->symbol] = DB::table('raw_data_deal')->where('Login',$account)->where('Symbol','LIKE','%'.$ps->symbol.'%')->where('VolumeClosed','>',0)->orderby('TimeMsc','desc')->limit(10)->get();
        }

        $pct_buy_num = 100 * $buy_pl / ($buy_pl + $sell_pl);
        $pct_sell_num = 100 * $sell_pl / ($buy_pl + $sell_pl);
        $pct_buy_profit = 100 * $buy_profit / $buy_numb;
        $pct_buy_loss = 100 * $buy_loss / $buy_numb;
        $pct_sell_profit = 100 * $sell_profit / $sell_numb;
        $pct_sell_loss = 100 * $sell_loss / $sell_numb;
        $avg_profit = ($total_buy_profit + $total_sell_profit) / ($buy_numb + $sell_numb);
        $avg_loss = ($total_buy_loss + $total_sell_loss) / ($buy_numb + $sell_numb);

        $totalSymbol = (object)[
                              'nickname' => $nickname,
                              'account' => $account,
                              'symbol' => "all",
                              'buy_pl' => $buy_pl,
                              'sell_pl' => $sell_pl,
                              'total_buy_profit' => $total_buy_profit,
                              'total_buy_loss' => $total_buy_loss,
                              'total_sell_profit' => $total_sell_profit,
                              'total_sell_loss' => $total_sell_loss,
                              'buy_numb' => $buy_numb,
                              'sell_numb' => $sell_numb,
                              'buy_profit' => $buy_profit,
                              'buy_loss' => $buy_loss,
                              'sell_profit' => $sell_profit,
                              'sell_loss' => $sell_loss,
                              'pct_buy_num' => $pct_buy_num,
                              'pct_sell_num' => $pct_sell_num,
                              'pct_buy_profit' => $pct_buy_profit,
                              'pct_buy_loss' => $pct_buy_loss,
                              'pct_sell_profit' => $pct_sell_profit,
                              'pct_sell_loss' => $pct_sell_loss,
                              'avg_profit' => $avg_profit,
                              'avg_loss' => $avg_loss,
                            ];
      }else{
        $perSymbol = (object)[
                              'nickname' => "",
                              'account' => "",
                              'symbol' => "",
                              'buy_pl' => 0,
                              'sell_pl' => 0,
                              'total_buy_profit' => 0,
                              'total_buy_loss' => 0,
                              'total_sell_profit' => 0,
                              'total_sell_loss' => 0,
                              'buy_numb' => 0,
                              'sell_numb' => 0,
                              'buy_profit' => 0,
                              'buy_loss' => 0,
                              'sell_profit' => 0,
                              'sell_loss' => 0,
                              'pct_buy_num' => 0,
                              'pct_sell_num' => 0,
                              'pct_buy_profit' => 0,
                              'pct_buy_loss' => 0,
                              'pct_sell_profit' => 0,
                              'pct_sell_loss' => 0,
                              'avg_profit' => 0,
                              'avg_loss' => 0,
                            ];

      }



      return response()->json([
        'success' => true,
        'data' => [
          'mt5' => $request->login,
          'perSymbol' => $perSymbol,
          'totalSymbol' => $totalSymbol,
          'deals' => $deals,
          'nickname' => $nickname,
        ]
      ]);
    }

    public function inTradeInstant(Request $request){
      // dd($request->all());
      $mt5Account = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->login)->first();
      $typemt5 = DB::table('typeMT4MasterTemplate')->where('masterName',$mt5Account->typeAccount)->first();

      $pair = $request->pair;
      $baseSymbol = explode(".",$pair);

      $pair = $baseSymbol[0].$typemt5->sufix;

      $package = [
            'Login'  => $request->login,
            'Symbol' => $pair,
            'Digits' => $request->digit,
            'Volume' => $request->volume,
            'Type'   => $request->trade,
            'PriceTP' => $request->takeprofit,
            'PriceSL' => $request->stoploss,
            'Action' => 200,
            'Comment' => 'INSTANT',
        ];

      // return response()->json([
      //   'success' => true,
      //   'data' => $package
      // ]);

      $api = new LaravelMt5();
      $result = $api->dealerSend($package);

      $data = json_decode($result);
      return response()->json([
        'success' => true,
        'data' => $data
      ]);
    }

    public function inTradePending(Request $request){
      //return $request->all();
      $api = new LaravelMt5();
      $result = $api->dealerSend([
            'Login'  => $request->login,
            'Symbol' => $request->pair,
            'Volume' => $request->volume,
            'Type'   => $request->type,
            'Digits' => $request->digit,
            'PriceOrder' => $request->price,
            'PriceTP' => $request->takeprofit,
            'PriceSL' => $request->stoploss,
            'Comment' => 'PENDING',
            'Action' => 201,
            'TypeTime' => 0,
            'TypeFill' => 2
        ]);

      $data = json_decode($result);
      return response()->json([
        'success' => true,
        'data' => $data

      ]);
    }

    public function tam5($symbol){
      $tam5 = symbolTechnicalAnalysisModel::select('bullishpct', 'bearishpct', 'neutralpct', 'datadetail')->where('symbol','LIKE','%'.$symbol.'%')->where('timeframe','M5')->orderby('id','desc')->first();

      return response()->json($tam5);
    }

    public function tam15($symbol){
      $tam15 = symbolTechnicalAnalysisModel::select('bullishpct', 'bearishpct', 'neutralpct', 'datadetail')->where('symbol','LIKE','%'.$symbol.'%')->where('timeframe','M15')->orderby('id','desc')->first();

      return response()->json($tam15);
    }

    public function tam30($symbol){
      $tam30 = symbolTechnicalAnalysisModel::select('bullishpct', 'bearishpct', 'neutralpct', 'datadetail')->where('symbol','LIKE','%'.$symbol.'%')->where('timeframe','M30')->orderby('id','desc')->first();

      return response()->json($tam30);
    }

    public function tah1($symbol){
      $tah1 = symbolTechnicalAnalysisModel::select('bullishpct', 'bearishpct', 'neutralpct', 'datadetail')->where('symbol','LIKE','%'.$symbol.'%')->where('timeframe','H1')->orderby('id','desc')->first();

      return response()->json($tah1);
    }

    public function tah4($symbol){
      $tah4 = symbolTechnicalAnalysisModel::select('bullishpct', 'bearishpct', 'neutralpct', 'datadetail')->where('symbol','LIKE','%'.$symbol.'%')->where('timeframe','H4')->orderby('id','desc')->first();

      return response()->json($tah4);
    }

    public function tad1($symbol){
      $tad1 = symbolTechnicalAnalysisModel::select('bullishpct', 'bearishpct', 'neutralpct', 'datadetail')->where('symbol','LIKE','%'.$symbol.'%')->where('timeframe','D1')->orderby('id','desc')->first();

      return response()->json($tad1);
    }

    public function taw1($symbol){
      $taw1 = symbolTechnicalAnalysisModel::select('bullishpct', 'bearishpct', 'neutralpct', 'datadetail')->where('symbol','LIKE','%'.$symbol.'%')->where('timeframe','W1')->orderby('id','desc')->first();

      return response()->json($taw1);
    }

    public function tamn1($symbol){
      $tamn1 = symbolTechnicalAnalysisModel::select('bullishpct', 'bearishpct', 'neutralpct', 'datadetail')->where('symbol','LIKE','%'.$symbol.'%')->where('timeframe','MN1')->orderby('id','desc')->first();

      return response()->json($tamn1);
    }

    public function favPair(Request $request){
      $countSelfInsight = DB::table('account_statistic')->where('account',$request->login)->count();

      if ($countSelfInsight > 0) {
        $perSymbol = DB::table('account_statistic')->select(DB::raw("`buy_numb` + `sell_numb` as total_numb"),'sell_numb', 'buy_numb', 'nickname', 'symbol', 'buy_profit', 'sell_profit')->where('account',$request->login)->orderBy(DB::raw("`buy_numb` + `sell_numb`"), 'desc')->get();

        foreach ($perSymbol as $ps) {
          // return pair
          $symbol_profit = DB::table('raw_data_deal')->where('symbol', 'LIKE', '%'.$ps->symbol.'%')->where('login', $request->login)->where('profit', '>', 0)->count();
          $symboll_loss = DB::table('raw_data_deal')->where('symbol', 'LIKE', '%'.$ps->symbol.'%')->where('login', $request->login)->where('profit', '<', 0)->count();
          $total_symbol = $symbol_profit + $symboll_loss;
          $symbol_return_percent = $symbol_profit/$total_symbol * 100;

          $totalTrade = $ps->total_numb;
          $totalProfit = $ps->buy_profit + $ps->sell_profit;
          $percentage_profit = $totalProfit * 100 / $totalTrade;
          $ps->percentageProfit = $percentage_profit;
          $ps->symbol_return_percent = $symbol_return_percent;
          $nickname = $ps->nickname;

        }
      }else{
        $nickname = '';
        $perSymbol = [];
      }

      return response()->json([
        'success' => true,
        'listFavPair'=>$perSymbol,
      ]);
    }

    public function dataEquityDepositReturn($uuid){

      $api = new LaravelMt5();

      $getAccount = DB::table('typeMT4AvailabeAccount')->where('uuid',$uuid)->get();
      // dd($getAccount);
      $totalDeposit =0;
      $totalProfit =0;
      $totalEquity =0;
      $totalfreeMargin =0;

      foreach ($getAccount as $account) {
        $getReturn = DB::table('raw_data_deal')->where('Login',$account->mt4_id)->where('Entry','1')->where('Action','<','2')->sum('Profit');
        $getDeposit = DB::table('raw_data_deal')->where('Login',$account->mt4_id)->where('Action','2')->where('Profit','>',0)->sum('Profit');
        $user = $api->getTradingAccounts($account->mt4_id);
        $equity = $user->Equity;
        $freeMargin = $user->MarginFree;

        $totalDeposit += $getDeposit;
        $totalProfit += $getReturn;
        $totalEquity += $equity;
        $totalfreeMargin += $freeMargin;
        // echo '==================<br>account : '.$account->mt4_id.'<br>';
        // echo 'total profit : '.$getReturn."<br>";
        // echo 'total deposit : '.$getDeposit."<br>";
        // echo 'last equity : '.$equity."<br>";
        // echo 'last freeMargin : '.$totalfreeMargin."<br>";
      }

      // echo 'FreeMargin : '.$totalfreeMargin."<br>";

      // echo 'total profit : '.$totalProfit."<br>";
      // echo 'total Deposit : '.$totalDeposit."<br>";
      // echo 'Equity : '.$totalEquity."<br>";
      // echo 'FreeMargin : '.$totalfreeMargin."<br>";

      $return = ($totalProfit/$totalDeposit*100) + 100;

      // echo 'RETURN : '.$return;

      return response()->json([
        'success'=>true,
        'totalProfit'=>$totalProfit,
        'totalDeposit'=>$totalDeposit,
        'totalEquity'=>$totalEquity,
        'totalfreeMargin'=>$totalfreeMargin,
        'return'=>$return
      ]);
    }

    public function dataEquityDepositReturnMtAccount($login){

      $api = new LaravelMt5();

      $getAccount = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$login)->first();
      // dd($getAccount);
      $totalDeposit =0;
      $totalProfit =0;
      $totalEquity =0;
      $totalfreeMargin =0;

      if($getAccount){
          $getReturn = DB::table('raw_data_deal')->where('Login',$getAccount->mt4_id)->where('Entry','1')->where('Action','<','2')->sum('Profit');
          $getDeposit = DB::table('raw_data_deal')->where('Login',$getAccount->mt4_id)->where('Action','2')->where('Profit','>',0)->sum('Profit');
          $user = $api->getTradingAccounts($getAccount->mt4_id);
          $equity = $user->Equity;
          $freeMargin = $user->MarginFree;

          $totalDeposit += $getDeposit;
          $totalProfit += $getReturn;
          $totalEquity += $equity;
          $totalfreeMargin += $freeMargin;

        $return = ($totalProfit/$totalDeposit*100) + 100;

        // echo 'RETURN : '.$return;

        return response()->json([
          'success'=>true,
          'totalProfit'=>number_format((float)$totalProfit, 2, '.', ''),
          'totalDeposit'=>$totalDeposit,
          'totalEquity'=>number_format((float)$totalEquity, 2, '.', ''),
          'totalfreeMargin'=>number_format((float)$totalfreeMargin, 2, '.', ''),
          'return'=>number_format((float)$return, 2, '.', '')
        ]);
      }else{
        return response()->json([
          'success'=>false,
          'message' => 'Account Tidak ada'
        ],401);
      }
    }

    public function returnProfitGrafik($login){

      $getReturn = DB::table('raw_data_deal')
                    ->select(DB::raw('SUM(Profit) as profits') , DB::raw('DATE(humanTime) as humanTimes'))
                    ->where('Login',$login)
                    ->where('Entry','1')
                    ->where('Action','<','2')
                    ->groupBy('humanTimes')
                    ->get();
  
      // dd($getReturn);
  
      foreach ($getReturn as $key => $value) {
        $getDeposit = DB::table('raw_data_deal')
                      ->where('Login',$login)
                      ->where('Action','2')
                      ->where('Profit','>',0)
                      ->whereDate('humanTime', '<=', $value->humanTimes)
                      ->sum('Profit');

        // dd($getTotal);
          $getReturn[$key]->persentase = number_format(($value->profits/$getDeposit*100), 2, '.', '');
          // $getReturn[$key]->persentase = $getTotal;
        
      }
  
      $data = $getReturn;
  
      // dd($getReturn);
  
  
      return response()->json([
        'success'=>true,
        'data'=>$data
      ]);
    }
}
