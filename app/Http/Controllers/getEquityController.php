<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tarikhagustia\LaravelMt5\LaravelMt5;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Arr;
use JWTAuth;
use Auth;

class getEquityController extends Controller
{
    public function checkEquity(Request $request){
      if ($request->secret == '$2y$10$K5skD5q/dtAZjS7ZfetKHOAhuNJ7AhvUzYV2uejBLXt6rrJ0KA1Fm') {
        $api = new LaravelMt5();

        $ib = $request->ib;
        $group = DB::table('typeMT4Accounts')->where('categoryAccount',$ib)->get();

        foreach ($group as $k => $g) {
          /*get list account from this groups*/
          if ($k == 0) {
            $list = 'real%5c'.$g->accountGroup;
          }else{
            $list .= ',real%5c'.$g->accountGroup;
          }
        }

        $group = $api->httpGet('/api/user/logins?group='.$list,[]);
        $sGroup = json_decode($group);
        $accounts = $sGroup->answer;
        $equity = 0;
        $depo = 0;
        $wd = 0;

        date_default_timezone_set('Europe/Tallinn');

        $start=strtotime('monday this week');
        $stop=strtotime('sunday this week');

        foreach ($accounts as $key => $acc) {
          $getTradingAccounts = $api->getTradingAccounts($acc);
          $equity += $getTradingAccounts->Equity;

          $getTotalOrder = $api->httpGet('/api/deal/get_total?login=110000&from='.$start.'&to='.$stop,[]);
          $dataTotal = json_decode($getTotalOrder);

          if ($dataTotal->retcode == '0 Done') {
            $getPageOrder = $api->httpGet('/api/deal/get_page?login=110000&offset=0&total='.$dataTotal->answer->total,[]);
            $dataOrder = json_decode($getPageOrder);
            $deal = $dataOrder->answer;
            foreach ($deal as $f) {
              if($f->Action == '2' && $f->Profit > 0){
                $depo += $f->Profit;
              }elseif($f->Action == '2' && $f->Profit < 0){
                $wd -= $f->Profit;
              }
            }

          }

        }

        $pl = $wd + $equity - $depo;
        $limit = $depo + $pl;

        $user = DB::table('users_ib')->where('id',$ib)->first();

        $creditGiven = $user->creditGiven;
        $percent = $limit/$creditGiven;

        $update = DB::table('users_ib')->where('id',$ib)->update([
          'lastKnownEquity' => $equity,
          'lastKnownDeposit' => $depo,
          'lastKnownWithdrawal' => $wd,
          'lastKnownPL' => $pl,
          'lastKnownMargin' => $limit,
          'LastKnownPercentLimit' => $percent
        ]);

        dd('equity '.$equity,'depo '.$depo,'wd '.$wd,'pl '.$pl,'limit '.$limit);

        return response()->json([
          'status' => true,
          'equity' => $equity
        ]);
      }else{
        return response()->json([
          'status' => false,
          'data' => 'error auth'
        ]);
      }
    }
}
