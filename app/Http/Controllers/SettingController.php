<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Symfony\Component\HttpFoundation\Response;
use Auth;

class SettingController extends Controller
{
    public function limitMasterIb(){
      $setting_mdtp = DB::table('setting_all')->where('type','master_default_taking_position')->first();
      $master_default_taking_position = $setting_mdtp->value;

      $setting_mntp = DB::table('setting_all')->where('type','master_minimum_taking_position')->first();
      $master_minimum_taking_position = $setting_mntp->value;

      $setting_mmtp = DB::table('setting_all')->where('type','master_maximum_taking_position')->first();
      $master_maximum_taking_position = $setting_mmtp->value;

      $setting_mmcredit = DB::table('setting_all')->where('type','master_minimum_credit')->first();
      $master_minimum_credit = $setting_mmcredit->value;

      $setting_senior_available_credit = DB::table('wallets')->where('uuid',auth()->user()->uuid)->first();
      $master_maximum_credit = $setting_senior_available_credit->amount;


      // $accountType = DB::table('typeMT4_accounts_choose_level')->select('typeMT4_accounts_choose_level.namaAccount', 'typeMT4_accounts_choose_level.accountGroup','typeMT4MasterTemplate.id')->where('typeMT4_accounts_choose_level.categoryAccount', auth()->user()->id)->where('typeMT4_accounts_choose_level.status', 'active')->join('typeMT4MasterTemplate', 'typeMT4MasterTemplate.masterName', 'typeMT4_accounts_choose_level.namaAccount')->get();
      $accountType = DB::table('typeMT4Accounts')->select('typeMT4Accounts.namaAccount', 'typeMT4Accounts.accountGroup','typeMT4MasterTemplate.id')->where('typeMT4Accounts.categoryAccount', auth()->user()->id)->where('typeMT4Accounts.status', 'active')->join('typeMT4MasterTemplate', 'typeMT4MasterTemplate.masterName', 'typeMT4Accounts.namaAccount')->get();
      // $accountType = DB::table('typeMT4MasterTemplate')->where('masterStatus','active')->get();

      $data = [
        'master_default_taking_position' => $master_default_taking_position,
        'master_minimum_taking_position' => $master_minimum_taking_position,
        'master_maximum_taking_position' => $master_maximum_taking_position,
        'master_minimum_credit' => $master_minimum_credit,
        'master_maximum_credit' => $master_maximum_credit,
        'accountType' => $accountType
      ];

      return response()->json([
        'success' => true,
        'data' => $data
      ], Response::HTTP_OK);
    }

    public function limitIb(){
      $setting_idtp = DB::table('setting_all')->where('type','ib_default_taking_position')->first();
      $ib_default_taking_position = $setting_idtp->value;

      $setting_intp = DB::table('setting_all')->where('type','ib_minimum_taking_position')->first();
      $ib_minimum_taking_position = $setting_intp->value;

      $setting_imtp = DB::table('setting_all')->where('type','ib_maximum_taking_position')->first();
      $ib_maximum_taking_position = $setting_imtp->value;

      // $setting_imcredit = DB::table('setting_all')->where('type','master_minimum_credit')->first();
      // $ib_minimum_credit = $setting_imcredit->value;

      $ib_minimum_credit = 5000000;

      $setting_master_available_credit = DB::table('wallets')->where('uuid',auth()->user()->uuid)->first();
      $ib_maximum_credit = $setting_master_available_credit->amount;

      $accountType = DB::table('typeMT4_accounts_choose_level')->select('typeMT4_accounts_choose_level.namaAccount', 'typeMT4_accounts_choose_level.accountGroup','typeMT4MasterTemplate.id')->where('typeMT4_accounts_choose_level.categoryAccount', auth()->user()->id)->where('typeMT4_accounts_choose_level.status', 'active')->join('typeMT4MasterTemplate', 'typeMT4MasterTemplate.masterName', 'typeMT4_accounts_choose_level.namaAccount')->get();
      // $accountType = DB::table('typeMT4Accounts')->select('typeMT4Accounts.namaAccount', 'typeMT4Accounts.accountGroup','typeMT4MasterTemplate.id')->where('typeMT4Accounts.categoryAccount', auth()->user()->id)->where('typeMT4Accounts.status', 'active')->join('typeMT4MasterTemplate', 'typeMT4MasterTemplate.masterName', 'typeMT4Accounts.namaAccount')->get();
      // $accountType = DB::table('typeMT4MasterTemplate')->where('masterStatus','active')->get();

      $data = [
        'ib_default_taking_position' => $ib_default_taking_position,
        'ib_minimum_taking_position' => $ib_minimum_taking_position,
        'ib_maximum_taking_position' => $ib_maximum_taking_position,
        'ib_minimum_credit' => $ib_minimum_credit,
        'ib_maximum_credit' => $ib_maximum_credit,
        'accountType' => $accountType
      ];

      return response()->json([
        'success' => true,
        'data' => $data
      ], Response::HTTP_OK);
    }
}
