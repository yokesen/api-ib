<?php

namespace App\Http\Controllers;
use JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use Storage;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class SignalController extends Controller
{
  public function signal_category(Request $request) // GET
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid =   $user['uuid'];
      $category = DB::table('signal_category')->where('categoryStatus','active')->orderby('id','desc')->get();
      if ( $category) {
        return response()->json([
          'success' => true,
          'Data' => $category
        ]);
      } else {
        return response()->json([
          'success' => false,
          'Data' => 'Data not Found'
        ]);
      }
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function signal_release(Request $request , $signalname_id) // GET
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid =   $user['uuid'];
      $signals = DB::table('signals')
      ->join('signal_name','signal_name.id','signals.signal_name')
      ->join('pairs','pairs.id','signals.item')
      ->where('signals.signal_name' , $signalname_id)
      ->orderby('signals.id','desc')->get();
      if ( $signals) {
        return response()->json([
          'success' => true,
          'Data' => $signals
        ]);
      } else {
        return response()->json([
          'success' => false,
          'Data' => 'Data not Found'
        ]);
      }
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function signal_list(Request $request , $category_id) // GET
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid =   $user['uuid'];
      $signals = DB::table('signal_name')->where('signalCategory',$category_id)->where('status', 'active')->orderby('id', 'desc')->get();
      if ( $signals) {
        return response()->json([
          'success' => true,
          'Data' => $signals
        ]);
      } else {
        return response()->json([
          'success' => false,
          'Data' => 'Data not Found'
        ]);
      }
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function petaHarga(Request $request){

    $token = $request->header('Authorization');
    try {
      if ($user = JWTAuth::authenticate($token)) {
        $petaharga = DB::table('signal_roadmap')->orderby('id','desc')->where('status','active')->get();
        return response()->json([
          'success' => true,
          'images' => $petaharga,
        ], 200);
      }
    } catch (JWTException $e) {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token.',
      ], 401);
    }

  }

}
