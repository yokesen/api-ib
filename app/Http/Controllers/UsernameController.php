<?php

namespace App\Http\Controllers;

use JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use Storage;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;


class UsernameController extends Controller
{

  public function check_username_changed(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid =   $user['uuid'];
      $check = DB::table('check_username')->where('uuid', $uuid)->count();
      return response()->json([
        'success' => true,
        'response' => [
          'check'=>$check,
        ],
      ]);

    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

  }

  public function search_username(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $username =   $user['username'];
      $email =   $user['email'];
      $check = DB::table('users_cabinet')->where('username',$request->username)->count();
      if($request->username ==  $username){
        $check = 0;
      }else{}
        if($check > 0){
          $suggests = [];
          $username1 = $request->username.date('y');
          $check1 = DB::table('users_cabinet')->where('username',$username1)->count();
          if($check1 == 0){
            array_push($suggests,$username1);
          }
          $username2 = $request->username.date('m');
          $check2 = DB::table('users_cabinet')->where('username',$username2)->count();
          if($check2 == 0){
            array_push($suggests,$username2);
          }
          $username3 = explode('@',$email)[0];
          echo  $check3 = DB::table('users_cabinet')->where('username',$username3)->count();
          if($check3 == 0){
            array_push($suggests,$username3);
          }
          return response()->json([
            'success' => true,
            'response' => [
              'check'=> $check,
              'suggested'=>$suggests
            ]
          ]);
        }else{
          return response()->json([
            'success' => true,
            'response' => [
              'check'=>$check
            ]
          ]);
        }
      } else {
        return response()->json([
          'success' => false,
          'message' => 'Invalid Token Or Error'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
      }
    }

    public function change_username(Request $request)
    {
      $token = $request->header('Authorization');

      $user = JWTAuth::authenticate($token);
      if ($user) {
        $uuid =   $user['uuid'];
        $update_username =  DB::table('users_cabinet')->where('uuid', $uuid)->update(['username' => $request->username]);
        if ($update_username) {
          $insert_username =   DB::table('check_username')->insert([
            'uuid' => $uuid,
            'old_username' => $user['username'],
            'new_username' => $request->username
          ]);
          return response()->json([
            'success' => true,
            'response' => 'Username Updated Successfully'
          ]);
        } else {
          return response()->json([
            'success' => false,
            'response' => 'Username Updated Not Successfully'
          ]);
        }

      } else {
        return response()->json([
          'success' => false,
          'message' => 'Invalid Token Or Error'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
      }
    }
  }
