<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use Hash;
use Storage;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Carbon\Carbon;
use GuzzleHttp\Client;

class UserController extends User implements JWTSubject
{
  public function register(Request $request)
  {
    $rules = [
      'name' => 'required|min:2|max:50',
      'email' => 'required|email|unique:users_cabinet',
      'phone' => 'required|min:10',
      'password' => 'required|string|min:5|max:20'
    ];
    // $messages = [
    //     'name.required' => 'Kesalahan : Nama wajib diisi!',
    //     'name.min' => 'Kesalahan : Nama depan minimal 2 huruf!',
    //     'email.required' => 'Kesalahan : Email wajib diisi!',
    //     'email.email' => 'Kesalahan : Email tidak valid!',
    //     'phone.required' => 'Kesalahan : Nomor Whatsapp wajib diisi!',
    //     'phone.min' => 'Kesalahan : Nomor Whatsapp-mu sepertinya salah!',
    //     'password.required' => 'Kesalahan : Password-mu masih kosong nih!',
    //     'password.min' => 'Kesalahan : Password-mu minimal harus 8 huruf/angka!',
    //     'password.max' => 'Kesalahan : Password-mu terlalu panjang!',
    // ];

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ]);
    } else {
      //Request is valid, create new user
      $user = User::create([
        'username' => explode('@', $request->email)[0],
        'uuid' => sha1($request->email) . time(),
        'name' => $request->name,
        'email' => $request->email,
        'password' => bcrypt($request->password),
        'phone' => $request->phone,
        'whatsapp' => preg_replace("/[^0-9]/", "", $request->phone),
        'disclaimer' => $request->disclaimer,
        'providerId' => $request->providerId,
        'providerOrigin' => $request->providerOrigin,
        'parent' => $request->parent,
        'first_landing' => $request->first_landing,
        'utm_campaign' => $request->utm_campaign,
        'utm_source' => $request->utm_source,
        'utm_medium' => $request->utm_medium,
        'utm_content' => $request->utm_content,
        'utm_term' => $request->utm_term,
        'ipaddress' => $request->ipaddress,
        'device' => $request->device,
        'screen' => $request->screen,
        'platform' => $request->platform,
        'platformVersion' => $request->platformVersion,
        'browser' => $request->browser,
        'browserVersion' => $request->browserVersion,
        'language' => $request->language,
        'robot' => $request->robot,
        'id_cms_privileges' => 99
      ]);


      //User created, return success response
      return response()->json([
        'success' => true,
        'status' => true,
        'message' => 'User created successfully',
        'uuid' => $user->uuid
      ], Response::HTTP_OK);
    }
  }

  public function createib(Request $request)
  {

    //return $request->all();
    $rules = [
      'name' => 'required|min:2|max:50',
      'email' => 'required|email|unique:users_cabinet',
      'phone' => 'required|min:10',
      'password' => 'required|string|min:5|max:20'
    ];
    // $messages = [
    //     'name.required' => 'Kesalahan : Nama wajib diisi!',
    //     'name.min' => 'Kesalahan : Nama depan minimal 2 huruf!',
    //     'email.required' => 'Kesalahan : Email wajib diisi!',
    //     'email.email' => 'Kesalahan : Email tidak valid!',
    //     'phone.required' => 'Kesalahan : Nomor Whatsapp wajib diisi!',
    //     'phone.min' => 'Kesalahan : Nomor Whatsapp-mu sepertinya salah!',
    //     'password.required' => 'Kesalahan : Password-mu masih kosong nih!',
    //     'password.min' => 'Kesalahan : Password-mu minimal harus 8 huruf/angka!',
    //     'password.max' => 'Kesalahan : Password-mu terlalu panjang!',
    // ];

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ]);
    } else {
      //Request is valid, create new user
      $user = User::create([
        'username' => explode('@', $request->email)[0],
        'uuid' => sha1($request->email) . time(),
        'name' => $request->name,
        'email' => $request->email,
        'password' => bcrypt($request->password),
        'phone' => $request->phone,
        'whatsapp' => preg_replace("/[^0-9]/", "", $request->phone),
        'disclaimer' => $request->disclaimer,
        'providerId' => $request->providerId,
        'providerOrigin' => $request->providerOrigin,
        'parent' => $request->parent,
        'first_landing' => $request->first_landing,
        'utm_campaign' => $request->utm_campaign,
        'utm_source' => $request->utm_source,
        'utm_medium' => $request->utm_medium,
        'utm_content' => $request->utm_content,
        'utm_term' => $request->utm_term,
        'ipaddress' => $request->ipaddress,
        'device' => $request->device,
        'screen' => $request->screen,
        'platform' => $request->platform,
        'platformVersion' => $request->platformVersion,
        'browser' => $request->browser,
        'browserVersion' => $request->browserVersion,
        'language' => $request->language,
        'robot' => $request->robot,
        'id_cms_privileges' => $request->id_cms_privileges,
        'takingPositionPercentage' => $request->takingPositionPercentage,
        'seniorib' => $request->seniorib,
        'senioribDefaultPercent' => $request->senioribDefaultPercent,
        'masterib' => $request->masterib,
        'masteribDefaultPercent' => $request->masteribDefaultPercent,
        'ib' => $request->ib,
        'ibDefaultPercent' => $request->ibDefaultPercent
      ]);

      /**Try Login */

      /** */

      //User created, return success response
      return response()->json([
        'success' => true,
        'message' => 'User created successfully',
        'uuid' => $user->uuid
      ], Response::HTTP_OK);
    }
  }


  public function authenticate(Request $request)
  {
    $rules = [
      'email' => 'required|email',
      'password' => 'required|string|min:5|max:20'
    ];

    $credentials = Validator::make($request->all(), $rules);

    if ($credentials->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $credentials->errors()
        ]
      ]);
    }
    $credentials = $request->only('email', 'password');
    //Create token
    try {
      if (!$token = JWTAuth::attempt($credentials)) {

        /** Check Email tidak ada*/
        $emailCheck = DB::table('users_ib')->where('email', $request->email)->count();
        if ($emailCheck == 0) {
          return response()->json([
            'success' => false,
            'message' => 'Email tidak ditemukan',
          ], 400);
        }

        return response()->json([
          'success' => false,
          'message' => 'Kombinasi email dan password salah',
        ], 400);
      }
    } catch (JWTException $e) {
      return $credentials;
      return response()->json([
        'success' => false,
        'message' => 'Could not create token.',
      ], 500);
    }
    $user = DB::table('users_ib')->where('email', $request->email)->first();
    DB::table('users_ib')->where('email', $request->email)->update([
      'last_login' => now()
    ]);
    return response()->json([
      'success' => true,
      'user' => $user,
      'token' => $token,
    ]);
  }

  public function authenticateWithGoogle(Request $request)
  {
    $rules = [
      'displayName' => 'required',
      'email' => 'required|email',
      'id' => 'required',
      'photoUrl' => 'required',
      '_idToken' => 'required',
    ];

    $valid = Validator::make($request->all(), $rules);

    if ($valid->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $valid->errors()
        ]
      ]);
    }

    $uuid = sha1($request->email) . time();

    $exist = User::where('email', $request->email)->count();

    //dd($ip,$users,$users->id,$users->email,$uuid,$exist,Cookie::get(),Cookie::get('utm_source'),$platform,$version_platform,$device,$browser,$version_browser,$languages,$robot);
    if ($exist < 1) {
      $insert = DB::table('users_cabinet')->insertGetId([
        'name' => $request->displayName,
        'email' => $request->email,
        'email_validation' => 'true',
        'email_verification' => 'verified',
        'photo' => $request->photoUrl,
        'providerId' => $request->id,
        'providerOrigin' => 'google-app',
        'id_cms_privileges' => '7',
        'parent' => '0',
        'utm_source' => '',
        'utm_medium' => '',
        'utm_campaign' => '',
        'utm_term' => '',
        'utm_content' => '',
        'uuid' => $uuid,
        'ipaddress' => '',
        'screen' => '',
        'platform' => '',
        'platformVersion' => '',
        'device' => '',
        'browser' => '',
        'browserVersion' => '',
        'language' => '',
        'robot' => ''
      ]);


    } else {
      $update = DB::table('users_cabinet')->where('email', $request->email)->update([
        'providerId' => $request->id,
        'providerOrigin' => 'google-app',
        'email_validation' => 'true',
        'email_verification' => 'verified',
      ]);
    }


    $credentials = User::where('email', $request->email)->first();
    // return $credentials;
    try {
      if (!$token = JWTAuth::fromUser($credentials)) {
        return response()->json([
          'success' => false,
          'message' => 'Login credentials are invalid.',
        ], 400);
      }
    } catch (JWTException $e) {
      return $credentials;
      return response()->json([
        'success' => false,
        'message' => 'Could not create token.',
      ], 500);
    }
    return response()->json([
      'success' => true,
      'token' => $token,
    ]);
  }

  public function authenticateWithUUID(Request $request)
  {
    $rules = [
      'email' => 'required|email',
      'uuid' => 'required',
    ];

    $valid = Validator::make($request->all(), $rules);

    if ($valid->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $valid->errors()
        ]
      ]);
    }

    $credentials = User::where('email', $request->email)->where('uuid', $request->uuid)->first();

    try {
      if (!$token = JWTAuth::fromUser($credentials)) {
        return response()->json([
          'success' => false,
          'message' => 'Login credentials are invalid.',
        ], 400);
      }
    } catch (JWTException $e) {
      return $credentials;
      return response()->json([
        'success' => false,
        'message' => 'Could not create token.',
      ], 500);
    }
    return response()->json([
      'success' => true,
      'token' => $token,
    ]);
  }

  public function logout(Request $request)
  {
    $token = $request->header('Authorization');
    //Request is validated, do logout
    try {
      JWTAuth::invalidate($token);

      return response()->json([
        'success' => true,
        'message' => 'User has been logged out'
      ]);
    } catch (JWTException $exception) {
      return response()->json([
        'success' => false,
        'message' => 'Sorry, user cannot be logged out'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function get_user(Request $request)
  {
    $token = $request->header('Authorization');
    try {
      if ($user = JWTAuth::authenticate($token)) {
        return response()->json(['user' => $user]);
      }
    } catch (JWTException $e) {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token.',
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

  }
  public function account_type_list(Request $request)
  {

    $token = $request->header('Authorization');
    //Request is validated, do logout
    try {
      JWTAuth::invalidate($token);
      $listAccounts = DB::table('typeMT4Accounts')
        ->leftjoin('typeMT4Category', 'typeMT4Category.id', 'typeMT4Accounts.categoryAccount')
        ->where('typeMT4Accounts.status', 'active')
        ->where('typeMT4Category.status', 'active')
        ->orderby('typeMT4Accounts.minimumDepo', 'asc')
        ->select('typeMT4Category.namaCategory', 'typeMT4Accounts.id', 'typeMT4Accounts.namaAccount', 'typeMT4Accounts.deskripsiAccount', 'typeMT4Accounts.minimumDepo', 'typeMT4Accounts.bestChoice')->get();

      if ($listAccounts) {
        return response()->json([
          'success' => true,
          'response' => $listAccounts
        ]);
      }

    } catch (JWTException $exception) {
      return response()->json([
        'success' => false,
        'message' => 'Sorry, Invalid Token'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }


  }
  public function my_account(Request $request)
  {

    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid = $user['uuid'];
      $myAccounts = DB::table('typeMT4AvailabeAccount')->where('uuid', $uuid)->orderby('process_at', 'desc')->get();
      if ($myAccounts) {
        return response()->json([
          'success' => true,
          'response' => $myAccounts
        ]);
      }

    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

  }

  public function create_account(Request $request)
  {
    $token = $request->header('Authorization');
    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid = $user['uuid'];
      $rules = [
        'accountType' => 'required|max:20'
      ];

      // $messages = [
      //   'accountType.required' => 'loh seharusnya ada pilihan akunnya, tapi kok kosong ya?',
      //   'accountType.max' => 'salah isian nih?',
      // ];

      $validator = Validator::make($request->all(), $rules);
      if ($validator->fails()) {
        return response()->json([
          'status' => false,
          'errors' => $validator->errors()
        ]);
      }

      $insert = DB::table('typeMT4AvailabeAccount')->insert([
        'typeAccount' => $request->accountType,
        'uuid' => $uuid,
        'status' => 'waiting',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);
      if ($insert) {
        return response()->json([
          'success' => true
        ]);
      }
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }


  public function update_profile(Request $request)
  {
    $token = $request->header('Authorization');
    //return $request->all();
    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid = $user['uuid'];
      // dd($user);
      $dob = $request->has('dob') ? $request->dob : $user->dob;
      $whatsapp = $request->has('whatsapp') ? $request->whatsapp : $user->whatsapp;
      $update = DB::table('users_ib')->where('uuid', $uuid)->update([
        'whatsapp' => $whatsapp,
        'dob' => $dob,
        'city' => $request->city,
        'address' => $request->address,
      ]);

      if ($update) {
        return response()->json([
          'success' => true
        ]);
      }
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function UploadImage(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid = $user['uuid'];
      if ($request->file('gambar')) {
        $image = $request->file('gambar');
        $time = time();
        /*image name*/
        $extention = $image->extension();
        $image_name = $uuid . $time . "." . $extention;
        $thumb_name = "thumb-" . $uuid . $time . "." . $extention;
        /*image name*/
        /*THUMB harus diatas supaya ga ke rotate*/
        $image_thumb = Image::make($image)->fit(200, 200);
        $image_thumb = $image_thumb->stream();
        Storage::disk('upcloud')->put('images//user/' . $thumb_name, $image_thumb->__toString());
        /*THUMB harus diatas supaya ga ke rotate*/
        $dimension = getimagesize($image);
        $width = $dimension[0];
        $height = $dimension[1];
        if ($height > $width) {
          // create Image from file
          $image_normal = Image::make($image)->heighten(600, function ($constraint) {
            $constraint->upsize();
          });
        } else {
          $image_normal = Image::make($image)->widen(600, function ($constraint) {
            $constraint->upsize();
          });
        }
        $image_normal = $image_normal->stream();
        Storage::disk('upcloud')->put('images//user/' . $image_name, $image_normal->__toString());
      }
      return response()->json([
        'success' => true,
        'response' => $image_name,
      ]);
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function update_password(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {

      $rules = [
        'current_password' => 'required',
        'password' => 'required|string|min:5|max:20'
      ];
      $credentials = Validator::make($request->all(), $rules);
      if ($credentials->fails()) {
        return response()->json([
          'status' => false,
          'errors' => $credentials->errors()
        ]);
      }

      $uuid = $user['uuid'];
      
      // $old_password = bcrypt($request->current_password);
      // $hash = Hash::check($old_password, $user['password']);
      // return response()->json([
      //   'user' => $user,
      //   'old' => $old_password,
      //   'current' => $user['password'],
      //   'hash' => $hash
      // ]);

      // if(Hash::check($old_password, $user['password'])){
      $password = Hash::make($request->password);
        $update_password = DB::table('users_ib')->where('uuid', $uuid)->update(['password' => $password]);
        if ($update_password) {
          return response()->json([
            'success' => true,
            'response' => 'Password Updated Successfully'
          ]);
        } else {
          return response()->json([
            'success' => false,
            'response' => 'Password Updated Not Successfully'
          ]);
        }
      // }else{
      //   return response()->json([
      //     'success' => false,
      //     'response' => 'Current Password Wrong'
      //   ]);
      // }
      
      
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  

  // public function FunctionName(Request $request)
  // {
  //     $token = $request->header('Authorization');

  //     $user = JWTAuth::authenticate($token);
  //     if ($user) {
  //     $uuid =   $user['uuid'];

  //     } else {
  //         return response()->json([
  //         'success' => false,
  //         'message' => 'Invalid Token Or Error'
  //     ], Response::HTTP_INTERNAL_SERVER_ERROR);
  // }
  // }
  public function editLevel(Request $request, $id)
  {
    $rules = [
      'status' => 'required|in:lead,contact,losing,winning,potential'
    ];
    $credentials = Validator::make($request->all(), $rules);
    if ($credentials->fails()) {
      return response()->json([
        'status' => false,
        'errors' => $credentials->errors()
      ], 422);
    }

    $uuid = $id;
    $update = DB::table('users_cabinet')->where('uuid', $uuid)->update([
      'status' => $request->status,
    ]);
    return response()->json([
      'success' => true,
      'message' => 'berhasil merubah status'
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   */

  public function updateUser(Request $request, $id)
  {
    $uuid = $id;
    $user = DB::table('users_cabinet')->where('uuid', $uuid)->first();

    if (empty($user->username)) {
      $rules = [
        'photo' => 'max:10000',
        'username' => 'required|min:4|max:30|unique:users_cabinet',
        'name' => 'required|min:2|max:70',
        'dob' => 'required|date',
        'username' => 'required',
        'email' => 'required',
        'whatsapp' => 'required',
        'address' => 'required',
        'city' => 'required'
      ];

      $messages = [
        'photo.max' => 'photo yg diupload barusan terlalu besar, ada yang kecilan sedikit ga?',
        'username.required' => 'usernamenya kosong',
        'username.min' => 'username terlalu pendek, minimal 4 huruf ya',
        'username.max' => 'username terlalu panjang, maximal 30 huruf ya',
        'username.unique' => 'username sudah dipakai, ayo pilih username yang lain',
        'name.required' => 'namenya kosong',
        'name.min' => 'nama terlalu pendek, minimal 2 huruf ya',
        'name.max' => 'nama terlalu panjang, maximal 70 huruf ya',
        'dob.required' => 'tanggal lahirmu brp?',
        'dob.date' => 'bukan tanggal nih',
        'email.required' => 'wajib mengirimkan email',
        'address.required' => 'alamat wajib diisi ya',
        'city.required' => 'kota wajib diisi ya'
      ];
    } else {
      $rules = [
        'photo' => 'max:10000',
        'name' => 'required|min:2|max:70',
        'dob' => 'required|date',
        'username' => 'required',
        'email' => 'required',
        'whatsapp' => 'required',
        'address' => 'required',
        'city' => 'required'
      ];

      $messages = [
        'photo.max' => 'photo yg diupload barusan terlalu besar, ada yang kecilan sedikit ga?',
        'name.required' => 'namenya kosong',
        'name.min' => 'nama terlalu pendek, minimal 2 huruf ya',
        'name.max' => 'nama terlalu panjang, maximal 70 huruf ya',
        'dob.required' => 'tanggal lahirmu brp?',
        'dob.date' => 'bukan tanggal nih',
        'email.required' => 'wajib mengirimkan email',
        'address.required' => 'alamat wajib diisi ya',
        'city.required' => 'kota wajib diisi ya'
      ];
    }


    $validator = Validator::make($request->all(), $rules, $messages);

    if ($validator->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ]);
    }

    if ($request->file('photo') || $request->photo) {
      if ($request->file('photo')) {
        $image = $request->file('photo');
        $time = time();
        /*image name*/
        $extention = $image->extension();
        $image_name = $uuid . $time . "." . $extention;
        $thumb_name = "thumb-" . $uuid . $time . "." . $extention;
        /*image name*/
        /*THUMB harus diatas supaya ga ke rotate*/
        $image_thumb = Image::make($image)->fit(200, 200);
        $image_thumb = $image_thumb->stream();
        Storage::disk('upcloud')->put('images/orbitrade/user/' . $thumb_name, $image_thumb->__toString());
        /*THUMB harus diatas supaya ga ke rotate*/
      }

      $update = DB::table('users_cabinet')->where('uuid', $uuid)->update([
        'username' => $request->username,
        'name' => $request->name,
        'photo' => env('IMG_USER') . $thumb_name,
        'email' => $request->email,
        'whatsapp' => $request->whatsapp,
        'address' => $request->address,
        'city' => $request->city,
        'dob' => $request->dob,
        'consent_wa' => $request->consent_wa,
        'consent_email' => $request->consent_email,
        'consent_marketing' => $request->consent_marketing
      ]);
    } else {
      $update = DB::table('users_cabinet')->where('uuid', $uuid)->update([
        'username' => $request->username,
        'name' => $request->name,
        'dob' => $request->dob,
        'email' => $request->email,
        'whatsapp' => $request->whatsapp,
        'address' => $request->address,
        'city' => $request->city,
        'consent_wa' => $request->consent_wa,
        'consent_email' => $request->consent_email,
        'consent_marketing' => $request->consent_marketing
      ]);

    }

    return response()->json([
      'success' => true,
      'message' => 'berhasil mengubah data'
    ]);
  }


  public function editBank(Request $request)
  {
    $rules = [
      'uuid' => 'required',
      'bank_name' => 'required',
      'account_name' => 'required',
      'account_number' => 'required'
    ];

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ], 422);
    }

    $uuid = $request->uuid;
    $user = DB::table('users_cabinet')->where('uuid', $uuid)->first();

    if ($user) {
      $update = DB::table('banks')->where('uuid', $uuid)->update([
        'bank_name' => $request->bank_name,
        'account_name' => $request->account_name,
        'account_number' => $request->account_number
      ]);

      $user = DB::table('banks')->where('uuid', $uuid)->first();
      $data = array(
        'bank_name' => $user->bank_name,
        "account_number" => $user->account_number,
        "account_name" => $user->account_name,
      );
      return response()->json([
        'success' => true,
        'message' => 'berhasil merubah data',
        'data' => $data
      ]);
    } else {
      return response()->json([
        'success' => false,
        'message' => 'user tidak ditemukan'
      ]);
    }

  }

  public function uploadKtp(Request $request)
  {
    $rules = [
      'photo' => 'required',
      'uuid' => 'required'
    ];

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ], 422);
    }

    /** if request has photo then do upload photo*/
    $path = NULL;
    if ($request->file('photo')) {
      return response()->json([
        'success' => false,
        'message' => 'format image bukan base64'
      ], 422);

    } else if ($request->photo) {
      $imageName = "KTP-" . \Str::random(2) . time();
      $this->uploadGambar($request->photo, $imageName);
      $path = $imageName;
    } else {
      $user = DB::table('users_cabinet')->where('uuid', $request->uuid)->first();
      $path = $user->photoKTP;
    }
    /** end upload photo*/

    DB::table('users_cabinet')->where('uuid', $request->uuid)->update([
      'photoKTP' => $path,
      'ktp_verification' => 'approved',
      'updated_at' => now()
    ]);

    return response()->json([
      'success' => true,
      'message' => 'berhasil upload ktp',
      'path' => $path
    ]);

  }

  public function uploadBukuTabungan(Request $request)
  {
    /** Start Validation*/
    $rules = [
      'photo' => 'required',
      'uuid' => 'required'
    ];

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ], 422);
    }
    /** End validation*/

    /** if request has photo then do upload photo*/
    $path = NULL;
    $user = DB::table('users_cabinet')->where('uuid', $request->uuid)->first();
    if ($request->file('photo')) {
      return response()->json([
        "success" => false,
        "message" => "image bukan base64"
      ], 422);
    } else if ($request->photo) {
      $imageName = "TBGN-" . \Str::random(2) . time();
      $this->uploadGambar($request->photo, $imageName);
      $path = $imageName . ".png";
    } else {
      $path = $user->photoTabungan;
    }
    /** end upload photo*/

    DB::table('users_cabinet')->where('uuid', $request->uuid)->update([
      'photoTabungan' => $path,
      'tabungan_verification' => 'approved',
      'updated_at' => now()
    ]);
    $user = DB::table('users_cabinet')->where('uuid', $request->uuid)->first();
    return response()->json([
      'success' => true,
      'message' => 'berhasil upload buku tabungan',
      'user' => $user
    ]);

  }

  public function homeData()
  {
    $leads = DB::table('users_cabinet')->where('users_cabinet.parent', auth()->user()->id)->where('users_cabinet.status', 'lead')->take(5)->get();

    $user = DB::table('users_ib')->where('id', auth()->user()->id)->first();

    if ($user->seniorib == 0) {
      $ibs = DB::table('users_ib')->where('users_ib.seniorib', auth()->user()->id)->where('users_ib.masterib', '!=', 0)->where('users_ib.id_cms_privileges', '70')->take(5)->get();
    } else {
      $ibs = DB::table('users_ib')->where('users_ib.masterib', auth()->user()->id)->where('users_ib.id_cms_privileges', '70')->take(5)->get();
    }

    if ($user->masterib == 0) {
      $cekIb = DB::table('users_ib')->where('masterib', auth()->user()->id)->where('users_ib.id_cms_privileges', '70')->pluck('id');
      $clients = DB::table('users_cabinet')->whereIn('users_cabinet.parent', $cekIb)->where('users_cabinet.status', 'winning')->take(5)->get();
    } else {
      $clients = DB::table('users_cabinet')->where('users_cabinet.parent', auth()->user()->id)->where('users_cabinet.status', 'winning')->take(5)->get();
    }
    // $ibs = DB::table('users_ib')->where('users_ib.masterib', auth()->user()->id)->where('users_ib.id_cms_privileges', '70')->take(5)->get();
    $masters = DB::table('users_ib')->where('users_ib.seniorib', auth()->user()->id)->where('users_ib.id_cms_privileges', '60')->take(5)->get();
    $reconsile = DB::table('statement_reconsile')->where('parent', auth()->user()->id)->orderby('id', 'desc')->first();
    $weeklyClosedStatements = DB::table('statement_partner_weekly')->where('parent', auth()->user()->id)->whereNotNull('weekoty')->orderby('id', 'desc')->first();
    $tradingStatements = DB::table('raw_data_deal')
      ->join('users_cabinet', 'users_cabinet.uuid', 'raw_data_deal.uuid')
      ->where('users_cabinet.parent', auth()->user()->id)
      ->orderby('Time', 'desc')
      ->take(5)
      ->where('price', '!=', 0)
      ->get();
    // return auth()->user();

    if ($reconsile) {
      $reconsile->periode = $this->setPeriode($reconsile->weekoty, $reconsile->year);
    }
    if ($weeklyClosedStatements) {
      $weeklyClosedStatements->periode = $this->setPeriode($weeklyClosedStatements->weekoty, $weeklyClosedStatements->year);
      $weeklyClosedStatements->total = $weeklyClosedStatements->profit - $weeklyClosedStatements->floating;
    }

    foreach ($leads as $key => $value) {
      $todayProfit = DB::table('raw_data_deal')
        ->whereDate('raw_data_deal.humanTime', Carbon::today())
        ->join('typeMT4AvailabeAccount', 'typeMT4AvailabeAccount.mt4_id', 'raw_data_deal.Login')
        ->where('typeMT4AvailabeAccount.uuid', $value->uuid)
        ->sum('profit');
      $leads[$key]->today_profit = $todayProfit;
    }

    foreach ($clients as $key => $value) {
      $todayProfit = DB::table('raw_data_deal')
        ->whereDate('raw_data_deal.humanTime', Carbon::today())
        ->join('typeMT4AvailabeAccount', 'typeMT4AvailabeAccount.mt4_id', 'raw_data_deal.Login')
        ->where('typeMT4AvailabeAccount.uuid', $value->uuid)
        ->sum('profit');
      $clients[$key]->today_profit = $todayProfit;
    }

    // client AKTIF
    $total_client_sdh_deposit = DB::table('wallet_deposit_request')->where('parent',$user->id)->where('status','approved')->distinct()->select('uuid')->get();
    $total_client = count($total_client_sdh_deposit);

    $date = date('Y-m-d H:i:s');
    $last_2_month = date('Y-m-d H:i:s',strtotime(' - 2 months'));
    
    $client_trade_2_months = DB::table('raw_data_deal')->where('parent',$user->id)->where('humanTime','>',$last_2_month)->distinct()->select('uuid')->get();
    $client_active = count($client_trade_2_months);

    $client_not_active = $total_client - $client_active;

    /** Response data*/
    $response = array(
      'user' => $user,
      'leads' => $leads,
      'clients' => $clients,
      'masters' => $masters,
      'ibs' => $ibs,
      'weekly_reconsile' => $reconsile,
      'recent_trading_statement' => $weeklyClosedStatements,
      'recent_transaction' => $tradingStatements,
      'total_client' => $total_client,
      'client_aktif' => $client_active,
      'client_tdk_aktif' => $client_not_active,
    );

    return response()->json([
      'success' => true,
      'data' => $response
    ]);
  }

  public function createTokenForgotPassword(Request $request)
  {
    /** Start Validation*/
    $rules = [
      'email' => 'required|exists:users_cabinet,email',
    ];

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ], 422);
    }
    /** End validation*/

    $randomString = \Str::random(500);

    /** Update forgot password token*/
    DB::table('users_cabinet')->where('email', $request->email)->update([
      'forgot_password_token' => $randomString
    ]);

    return response()->json([
      'success' => true,
      'email' => $request->email,
      'token_forgot_password' => $randomString,
      'url' => env("FRONT_URL") . "new-password/$request->email/verify/$randomString"
    ]);
    // return $randomString;

  }

  public function resetPassword(Request $request)
  {

    /** Start Validation*/
    $rules = [
      'token' => 'required|exists:users_cabinet,forgot_password_token',
      'password' => 'required|confirmed'
    ];

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ], 422);
    }
    /** End validation*/

    $password = bcrypt($request->password);

    DB::table('users_cabinet')->where('forgot_password_token', $request->token)->update([
      'password' => $password,
      'updated_at' => now()
    ]);

    $user = DB::table('users_cabinet')->where('forgot_password_token', $request->token)->first();

    return response()->json([
      'success' => true,
      'data' => $user,
      'message' => "berhasil merubah password akun"
    ]);

  }

  public function getReferralLink($uuid)
  {
    $user = DB::table('users_cabinet')->where('uuid', $uuid)->first();
    if ($user) {
      $link = env("FRONT_URL") . "?ref=$user->id&utm_source=copylink&utm_campaign=Affiliate&utm_medium=super&utm_content=master";

      return response()->json([
        'success' => true,
        'link' => $link
      ]);
    } else {
      return response()->json([
        'success' => false,
        'message' => 'user tidak ditemukan'
      ]);
    }
  }

  public function setPeriode($week, $year)
  {
    $timestamp = mktime(0, 0, 0, 1, 1, $year) + ($week * 7 * 24 * 60 * 60);
    $timestamp_for_monday = $timestamp - 86400 * (date('N', $timestamp) - 1);
    $date_for_monday = date('d-m-Y', $timestamp_for_monday);
    $date_for_friday = date('d-m-Y', strtotime('friday ', $timestamp_for_monday));
    return "$date_for_monday to $date_for_friday";
  }

  public function uploadGambar($image, $image_name)
  {
    $client = new Client();
    $res = $client->post("https://static.8377-2842.xyz/api/v1/upload/image/ib", [
      "form_params" => [
        "secret" => "anantonthemovedoesmorethanadozingox",
        "gambar" => $image,
        "image_name" => $image_name
      ]
    ]);
  }

  //-----------------------------------------------------

  public function generateTokenResetPassword(Request $request)
  {
    $rules = [
      'email' => 'required|exists:users_cabinet,email',
    ];

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ], 422);
    }
    /** End validation*/
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $user_id = $user['id'];
      $check = DB::table('users_cabinet')->where('parent', $user_id)->first();
      if ($check) {
        $randomString = \Str::random(500);

        /** Update reset password token*/
        DB::table('users_cabinet')->where('email', $request->email)->update([
          'forgot_password_token' => $randomString
        ]);

        return response()->json([
          'success' => true,
          'email' => $request->email,
          'token_reset_password' => $randomString
        ]);
      } else {
        return response()->json([
          'success' => false,
          'message' => 'Error Access Not Allowed'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
      }
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

  }

  public function getTokenResetPassword(Request $request)
  {
    $rules = [
      'email' => 'required|exists:users_cabinet,email',
    ];

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ], 422);
    }
    /** End validation*/
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {

      /** get reset password token*/
      $token_reset_password = DB::table('users_cabinet')->where('email', $request->email)->first()->forgot_password_token;
      return response()->json([
        'success' => true,
        'token_reset_password' => $token_reset_password
      ]);
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function updateProfile(Request $request, $id)
  {
    $uuid = $id;
    $user = DB::table('users_ib')->where('uuid', $uuid)->first();

    if (empty($user->username)) {
      $rules = [
        'username' => 'required|min:4|max:30|unique:users_ib',
        'name' => 'required|min:2|max:70',
        'dob' => 'required|date',
        'username' => 'required',
        'whatsapp' => 'required',
        'address' => 'required',
        'city' => 'required'
      ];

      $messages = [
        'username.required' => 'usernamenya kosong',
        'username.min' => 'username terlalu pendek, minimal 4 huruf ya',
        'username.max' => 'username terlalu panjang, maximal 30 huruf ya',
        'username.unique' => 'username sudah dipakai, ayo pilih username yang lain',
        'name.required' => 'namenya kosong',
        'name.min' => 'nama terlalu pendek, minimal 2 huruf ya',
        'name.max' => 'nama terlalu panjang, maximal 70 huruf ya',
        'dob.required' => 'tanggal lahirmu brp?',
        'dob.date' => 'bukan tanggal nih',
        'address.required' => 'alamat wajib diisi ya',
        'city.required' => 'kota wajib diisi ya'
      ];
    } else {
      $rules = [
        'name' => 'required|min:2|max:70',
        'dob' => 'required|date',
        'username' => 'required',
        'whatsapp' => 'required',
        'address' => 'required',
        'city' => 'required'
      ];

      $messages = [
        'name.required' => 'namenya kosong',
        'name.min' => 'nama terlalu pendek, minimal 2 huruf ya',
        'name.max' => 'nama terlalu panjang, maximal 70 huruf ya',
        'dob.required' => 'tanggal lahirmu brp?',
        'dob.date' => 'bukan tanggal nih',
        'address.required' => 'alamat wajib diisi ya',
        'city.required' => 'kota wajib diisi ya'
      ];
    }


    $validator = Validator::make($request->all(), $rules, $messages);

    if ($validator->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ]);
    }


    DB::table('users_ib')->where('uuid', $uuid)->update([
      'username' => $request->username,
      'name' => $request->name,
      'dob' => $request->dob,
      'whatsapp' => $request->whatsapp,
      'address' => $request->address,
      'city' => $request->city,
    ]);

    $res = DB::table('users_ib')->select('username', 'name', 'dob', 'email', 'whatsapp', 'address', 'city')->where('uuid', $uuid)->first();

    return response()->json([
      'success' => true,
      'message' => 'berhasil mengubah data',
      'data' => $res
    ]);
  }

  public function changePasswordIB(Request $request)
  {
    $data = $request->all();

    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $validator = Validator::make($data, [
        'current_password' => 'required|string|min:8',
        'password' => 'required|string|min:8|confirmed'
      ]);

      if ($validator->fails()) {
        return response()->json([
          'status' => 'failed',
          'message' => $validator->errors()
        ], 422);
      }

      $old_password = $user['password'];
      $user_id = $user['id'];


      $data = \Hash::check($data["current_password"], $old_password);

      /** Current Password is same*/

      if ($data == true) {

        $newPassword = \Hash::make($request->password);

        DB::table('users_ib')->where('id', $user_id)->update([
          'password' => $newPassword,
          'updated_at' => now()
        ]);

        return response()->json([
          'status' => 'success',
          'message' => 'berhasil merubah password'
        ]);

      }
      /** Current Password is different*/else {
        $pass["password"][0] = 'Current password does not match';
        return response()->json([
          'status' => 'failed',
          'message' => $pass
        ], 400);

      }

    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

  }

  public function create_master_ib(Request $request)
  {
    // $rexponse = [
    //   'request' => $request->all(),
    //   'user' => auth()->user()
    // ];
    // return $rexponse;

    $rules = [
      'name' => 'required|string|min:3|max:50',
      'email' => 'required|string|min:6|max:100',
      'phone' => 'required|string|min:6|max:100',
      'takingPositionPercentage' => 'required|numeric|min:20|max:60',
      'password' => 'required|string|min:5|max:20',
      'creditGiven' => 'required',
      'group' => 'required'
    ];

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ]);
    }

    /* COPY FROM ADMIN CREATE SIB*/
    $timesufix = substr(strval(time()), -2); //okay

    $dbUsername = DB::table('cities')->where('used', '0')->first(); //okay
    $sufix = preg_replace("/[^A-Za-z0-9 ]/", '', $dbUsername->name); //okay
    $sufix = strtoupper($sufix); //okay
    $sufix = substr($sufix, -2); //okay
    $sufix .= $timesufix; //okay

    $name = $request->name; //okay

    $prefixs = explode(' ', $name); //okay
    $prefix = ''; //okay
    foreach ($prefixs as $pre) { //okay
      $pref = substr($pre, 0, 1); //okay
      $prefix .= strtoupper($pref); //okay
    }
    $username = $prefix . $sufix; //okay
    $group = $prefix . $timesufix; //okay
    // /dd($username,$group);
    $updateDbUsername = DB::table('cities')->where('id', $dbUsername->id)->update([
      'used' => '1'
    ]); //okay

    //dd($request->all());
    $uuid = sha1($request->email) . time(); //okay

    $depositGuarantee = ($request->takingPositionPercentage) * $request->creditGiven /100;

    $saveUserIb = DB::table('users_ib')->insertGetId([
      'uuid' => $uuid,
      'name' => $request->name,
      'username' => $username,
      'groupMT5' => $group,
      'email' => $request->email,
      'email_validation' => 'new',
      'email_verification' => 'new',
      'password' => Hash::make($request->password),
      'phone' => $request->phone,
      'whatsapp' => $request->phone,
      'providerId' => auth()->user()->id,
      'providerOrigin' => $request->origin,
      'id_cms_privileges' => '60',
      'seniorib' => auth()->user()->id,
      'senioribDefaultPercent' => auth()->user()->takingPositionPercentage,
      'parent' => auth()->user()->id,
      'freeMarginStatus' => $request->freeMarginStatus,
      'depositGuarantee' => $depositGuarantee,
      'creditGiven' => $request->creditGiven,
      'takingPositionPercentage' => $request->takingPositionPercentage,
      'dailySettlementLimit' => $request->dailySettlementLimit,
      'status' => 'pending'
    ]);

    // $saveTypeCategory = DB::table('typeMT4Category')->insert([
    //   'id' => $saveUserIb,
    //   'namaCategory' => $username,
    //   'status' => 'active'
    // ]);
    //
    foreach ($request->group as $v) {
      $grup = DB::table('typeMT4MasterTemplate')->where('id', $v)->first();

      $group = $group . "-" . $grup->masterRate . "-" . $grup->masterGroup;

      $savetypeMT4Accounts = DB::table('typeMT4_accounts_choose_level')->insert([
        'categoryAccount' => $saveUserIb,
        'namaAccount' => $grup->masterName,
        'accountGroup' => $group,
        'bestChoice' => "yes",
        'status' => "active",
      ]);
      //
      //   $savetypeMT4Accounts = DB::table('typeMT4GroupList')->insert([
      //     'typeAccount' => $grup->masterName,
      //     'groupName' => $grup->masterNumber,
      //     'status' => "active",
      //   ]);
    }

    return response()->json([
      'success' => true,
      'message' => 'User created successfully'
    ], Response::HTTP_OK);

  }

  public function create_ib(Request $request)
  {
    // $rexponse = [
    //   'request' => $request->all(),
    //   'user' => auth()->user()
    // ];
    // return $rexponse;

    $rules = [
      'name' => 'required|string|min:3|max:50',
      'email' => 'required|string|min:6|max:100',
      'phone' => 'required|string|min:6|max:100',
      'takingPositionPercentage' => 'required|numeric|min:10|max:30',
      'password' => 'required|string|min:5|max:20',
      'creditGiven' => 'required',
      'group' => 'required'
    ];

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return response()->json([
        'status' => false,
        'errors' => 'Undefined',
        // [
        //   $validator->errors()
        // ]
      ]);
    }

    /* COPY FROM ADMIN CREATE SIB*/
    $timesufix = substr(strval(time()), -2); //okay

    $dbUsername = DB::table('cities')->where('used', '0')->first(); //okay
    $sufix = preg_replace("/[^A-Za-z0-9 ]/", '', $dbUsername->name); //okay
    $sufix = strtoupper($sufix); //okay
    $sufix = substr($sufix, -2); //okay
    $sufix .= $timesufix; //okay

    $name = $request->name; //okay

    $prefixs = explode(' ', $name); //okay
    $prefix = ''; //okay
    foreach ($prefixs as $pre) { //okay
      $pref = substr($pre, 0, 1); //okay
      $prefix .= strtoupper($pref); //okay
    }
    $username = $prefix . $sufix; //okay
    $group = $prefix . $timesufix; //okay
    // /dd($username,$group);
    $updateDbUsername = DB::table('cities')->where('id', $dbUsername->id)->update([
      'used' => '1'
    ]); //okay

    //dd($request->all());
    $uuid = sha1($request->email) . time(); //okay
    $depositGuarantee = ($request->takingPositionPercentage) * $request->creditGiven /100;
    //harus ada condition to check this ib is whitelabel or not, harus nyangkut ke form

    $saveUserIb = DB::table('users_ib')->insertGetId([
      'uuid' => $uuid,
      'name' => $request->name,
      'username' => $username,
      'groupMT5' => $group,
      'email' => $request->email,
      'email_validation' => 'new',
      'email_verification' => 'new',
      'password' => Hash::make($request->password),
      'phone' => $request->phone,
      'whatsapp' => $request->phone,
      'providerId' => auth()->user()->id,
      'providerOrigin' => $request->origin,
      'id_cms_privileges' => '70',
      'seniorib' => auth()->user()->seniorib,
      'senioribDefaultPercent' => auth()->user()->senioribDefaultPercent,
      'masterib' => auth()->user()->id,
      'masteribDefaultPercent' => auth()->user()->takingPositionPercentage,
      'parent' => auth()->user()->id,
      'freeMarginStatus' => $request->freeMarginStatus,
      'depositGuarantee' => $depositGuarantee,
      'creditGiven' => $request->creditGiven,
      'takingPositionPercentage' => $request->takingPositionPercentage,
      'dailySettlementLimit' => $request->dailySettlementLimit,
      'status' => 'pending'
    ]);

    $saveTypeCategory = DB::table('typeMT4Category')->insert([
      'id' => $saveUserIb,
      'namaCategory' => $username,
      'status' => 'active'
    ]);

    foreach ($request->group as $v) {
      $grup = DB::table('typeMT4MasterTemplate')->where('id', $v)->first();

      $creategroup = $group . "-" . $grup->masterRate . "-" . $grup->masterGroup;

      $savetypeMT4Accounts = DB::table('typeMT4Accounts')->insert([
        'categoryAccount' => $saveUserIb,
        'namaAccount' => $grup->masterName,
        'accountGroup' => $creategroup,
        'bestChoice' => "yes",
        'status' => "pending",
      ]);

      $savetypeMT4Accounts = DB::table('typeMT4GroupList')->insert([
        'typeAccount' => $grup->masterName,
        'groupName' => $grup->masterNumber,
        'status' => "active",
      ]);
    }

    return response()->json([
      'success' => true,
      'message' => 'User created successfully'
    ], Response::HTTP_OK);

  }

  public function userDetail(Request $request)
  {
    $user = DB::table('users_ib')->where('uuid', $request->uuid)->first();
    $custody_banks = DB::table('bank_segre')->get();
    $data['user'] = $user;
    $data['listAllow'] = $custody_banks;
    if ($user) {
      return response()->json([
        'success' => true,
        'data' => $data
      ], Response::HTTP_OK);
    } else {
      return response()->json([
        'success' => false,
        'data' => "no user"
      ], Response::HTTP_OK);
    }
  }

  public function sendMailForgotPassword(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'email' => 'required|email|exists:users_cabinet,email',
    ], [
      'email.exists' => 'Maaf, email Anda tidak terdaftar. Silakan coba menggunakan email yang sudah terdaftar'
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors(), 422);
    }

    $user = User::where('email', $request->email)->first();

    if ($user) {
      //Create Password Reset Token
      $token = md5(date('Y-m-d H:i:s') . $user->name . $user->email) . time();

      $insert = DB::table('password_resets')->insert([
        'token' => $token,
        'email' => $request->email,
        'created_at' => now(),
      ]);

      // Send Email
      $client = new GuzzleClient();

      // $send = $client->post('https://mg.gudangin.id/api/v1/forgot-password', [
      //     'form_params' => [
      //         'email' => $request->email,
      //         'name' => $user->name,
      //         'token' => $token
      //     ]
      // ]);
      return response()->json([
        'status' => true,
        'success' => true,
        'msg' => 'Token created'
      ]);
    }

    return response()->json([
      'status' => true,
      'success' => false,
      'msg' => 'User not found'
    ]);
  }

  public function forgotPassword(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'token' => 'required',
      'password' => 'required|min:8'
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors(), 400);
    }

    // check user
    // currentDate
    $date = date('Y-m-d H:i:s');
    $user = DB::table('password_resets')->where('token', $request->token)->where('expired_at', '>', $date)->first();
    if ($user) {
      User::where('email', $user->email)->update([
        'password' => bcrypt($request->password)
      ]);

      return response()->json([
        'status' => true,
        'success' => true,
        'Message' => 'Password telah diganti'
      ]);
    }
    return response()->json([
      'status' => true,
      'success' => false,
      'Message' => 'Token Not Found'
    ]);

  }

  public function checkOnboarding($uuid)
  {
    $client = DB::table('users_ib')->where('uuid', $uuid)->first();
    $dataAddress = DB::table('users_ib')->where('uuid', $uuid)->first();
    $address = true;
    // dd($dataAddress, empty($address->address));
    if (empty($dataAddress->address) || empty($dataAddress->city)) {
      $address = false;
    }

    $dataBank = DB::table('bank_segre')->where('parent', $client->id)->count();
    $bank = true;
    if ($dataBank < 1) {
      $bank = false;
    }

    $dataTopUp = DB::table('partner_deposits')->where('uuid', $uuid)->count();
    $topUp = true;
    if ($dataTopUp < 1) {
      $topUp = false;
    }

    //tambah untuk agreement atur dl mau di kolom apa, and harusnya itu pdf/uploaded lsg masuk ke object storage (nah ini jg harus di discuss nerimanya pdf apa jpg)

    $agreement = true;
    if (empty($dataAddress->agreement) || $dataAddress->agreement === 'not yet') {
      $agreement = false;
    }
    return response()->json([
      'success' => true,
      'address' => $address,
      'agreement' => $agreement,
      'bank' => $bank,
      'topUp' => $topUp,
    ]);
  }



  // public function uupdate_profile_onboarding()
}