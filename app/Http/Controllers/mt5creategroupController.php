<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tarikhagustia\LaravelMt5\LaravelMt5;
use Tarikhagustia\LaravelMt5\Entities\User;
use Tarikhagustia\LaravelMt5\Entities\Trade;
use Tarikh\PhpMeta\MetaTraderClient;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Validator;

class mt5creategroupController extends Controller
{
  public function createGroup(Request $request){

    $rules = [
      'folder' => 'required|string',
      'group' => 'required|string',
      'email' => 'required|string'
    ];

    $valid = Validator::make($request->all(), $rules);

    if($valid->fails()){
      return response()->json([
        'status' => false,
        'errors' => [
          $valid->errors()
        ]
      ]);
    }

    $api = new LaravelMt5();
    $reqgroup = env('GROUP_MT5_SERVER').$request->sib."\\".$request->mib."\\".$request->folder."\\".$request->group; 
    //dd($reqgroup);
    try {

      $group = $api->httpPost('/api/group/add',[
        "Group" => env('GROUP_MT5_SERVER').$request->sib."\\".$request->mib."\\".$request->folder."\\".$request->group,
        "Server" => "1",
        "PermissionsFlags" => "2",
        "AuthMode" => "0",
        "AuthPasswordMin" => "5",
        "AuthOTPMode" => "0",
        "Company" => "PT. Mahadana Asta Berjangka",
        "CompanyPage" => "MT5-PTSEN-09-STANDARD",
        "CompanyEmail" => "",
        "CompanySupportPage" => "https://www.mql5.com/[lang:en|ru|es|pt|zh]",
        "CompanySupportEmail" => "",
        "CompanyCatalog" => "",
        "CompanyDepositURL" => "",
        "CompanyWithdrawalURL" => "",
        "Currency" => "USD",
        "CurrencyDigits" => "2",
        "ReportsMode" => "0",
        "ReportsFlags" => "0",
        "ReportsEmail" => $request->email,
        "NewsMode" => "2",
        "NewsCategory" => "",
        "NewsLangs" => [],
        "MailMode" => "1",
        "TradeFlags" => "31",
        "TradeTransferMode" => "0",
        "TradeInterestrate" => "0.00",
        "TradeVirtualCredit" => "0.00",
        "MarginMode" => "0",
        "MarginFlags" => "0",
        "MarginSOMode" => "0",
        "MarginFreeMode" => "1",
        "MarginCall" => "50.00",
        "MarginStopOut" => "30.00",
        "MarginFreeProfitMode" => "0",
        "DemoLeverage" => "0",
        "DemoDeposit" => "0.00",
        "DemoTradesClean" => "0",
        "LimitHistory" => "0",
        "LimitOrders" => "0",
        "LimitSymbols" => "0",
        "LimitPositions" => "0",
        "Commissions" => [],
        "Symbols" => "*"
      ]);

      return $group;
    } catch (\Exception $e) {
      return $e;
    }

  }

  // public function createGroupCompetition($periode){

  //   $api = new LaravelMt5();
  //   $reqgroup = 'demo\\Competition\\'.$periode; 
  //   //dd($reqgroup);
  //   try {

  //     $group = $api->httpPost('/api/group/add',[
  //       "Group" => $reqgroup,
  //       "Server" => "1",
  //       "PermissionsFlags" => "2",
  //       "AuthMode" => "0",
  //       "AuthPasswordMin" => "5",
  //       "AuthOTPMode" => "0",
  //       "Company" => "PT. Mahadana Asta Berjangka",
  //       "CompanyPage" => "MT5-PTSEN-09-STANDARD",
  //       "CompanyEmail" => "",
  //       "CompanySupportPage" => "https://www.mql5.com/[lang:en|ru|es|pt|zh]",
  //       "CompanySupportEmail" => "",
  //       "CompanyCatalog" => "",
  //       "CompanyDepositURL" => "",
  //       "CompanyWithdrawalURL" => "",
  //       "Currency" => "USD",
  //       "CurrencyDigits" => "2",
  //       "ReportsMode" => "0",
  //       "ReportsFlags" => "0",
  //       "ReportsEmail" => 'competition@fgtpro.pro',
  //       "NewsMode" => "2",
  //       "NewsCategory" => "",
  //       "NewsLangs" => [],
  //       "MailMode" => "1",
  //       "TradeFlags" => "31",
  //       "TradeTransferMode" => "0",
  //       "TradeInterestrate" => "0.00",
  //       "TradeVirtualCredit" => "0.00",
  //       "MarginMode" => "0",
  //       "MarginFlags" => "0",
  //       "MarginSOMode" => "0",
  //       "MarginFreeMode" => "1",
  //       "MarginCall" => "50.00",
  //       "MarginStopOut" => "30.00",
  //       "MarginFreeProfitMode" => "0",
  //       "DemoLeverage" => "0",
  //       "DemoDeposit" => "0.00",
  //       "DemoTradesClean" => "0",
  //       "LimitHistory" => "0",
  //       "LimitOrders" => "0",
  //       "LimitSymbols" => "0",
  //       "LimitPositions" => "0",
  //       "Commissions" => [],
  //       "Symbols" => "*"
  //     ]);

  //     return $group;
  //   } catch (\Exception $e) {
  //     return $e;
  //   }

  // }
}
