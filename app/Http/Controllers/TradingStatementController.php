<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TradingStatementController extends Controller
{
    public function currentTrade(Request $request){
      $user = DB::table('users_ib')->where('uuid',$request->uuid)->first();
      $year = date('Y');
      $week = date('W',strtotime('last monday'));
      $yesterday = date('z',strtotime('yesterday'));
      $deal = [];
      $adjust = [];
      $floating = [];
      $commission = [];

      $getDeal = [];
      $getAdjust = [];
      $getFloating = [];
      $getCommission = [];

      if ($user->id_cms_privileges == 50) {
        $getIb = DB::table('users_ib')->where('seniorib',$user->id)->get();
      }elseif ($user->id_cms_privileges == 60) {
        $getIb = DB::table('users_ib')->where('masterib',$user->id)->get();
      }elseif($user->id_cms_privileges == 70){
          $getIb = DB::table('users_ib')->where('id',$user->id)->get();
      }

      foreach($getIb as $ib){
        $getDeal[] = DB::table('raw_data_deal')->where('parent',$ib->id)->where('Action','<','2')->where('VolumeClosed','>','0')->where('lotyear',$year)->where('lotweek',$week)->orderby('Time','asc')->get();
        $getAdjust[] = DB::table('raw_data_deal')->where('parent',$ib->id)->where('Action','=','2')->where('Comment','LIKE','%adj%')->where('lotyear',$year)->where('lotweek',$week)->orderby('Time','asc')->get();
        $getFloating[] = DB::table('raw_data_position')->where('parent',$ib->id)->where('lotyear',$year)->where('lotdate',$yesterday)->orderby('TimeCreate','asc')->get();
        $getCommission[] = DB::table('raw_data_deal')->where('parent',$ib->id)->where('Commission','<','0')->where('lotyear',$year)->where('lotweek',$week)->orderby('Time','asc')->get();

      }

      return response()->json([
        'success' => true,
        'deal' => $getDeal,
        'adjust' => $getAdjust,
        'floating' => $getFloating,
        'commission' => $getCommission
      ]);

    }
}
