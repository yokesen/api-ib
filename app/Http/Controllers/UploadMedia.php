<?php

namespace App\Http\Controllers;

use JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use Storage;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class UploadMedia extends Controller
{
  public function UploadKTP(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid = $user['uuid'];
      if ($request->file('gambar')) {
        $image = $request->file('gambar');
        $time = time();
        /*image name*/
        $extention = $image->extension();
        $image_name = $uuid . $time . "." . $extention;
        $thumb_name = "thumb-" . $uuid . $time . "." . $extention;
        /*image name*/
        /*THUMB harus diatas supaya ga ke rotate*/
        $image_thumb = Image::make($image)->fit(200, 200);
        $image_thumb = $image_thumb->stream();
        Storage::disk('upcloud')->put('images/orbitrade/user/' . $thumb_name, $image_thumb->__toString());
        /*THUMB harus diatas supaya ga ke rotate*/
        $dimension = getimagesize($image);
        $width = $dimension[0];
        $height = $dimension[1];
        if ($height > $width) {
          // create Image from file
          $image_normal = Image::make($image)->heighten(600, function ($constraint) {
            $constraint->upsize();
          });
        } else {
          $image_normal = Image::make($image)->widen(600, function ($constraint) {
            $constraint->upsize();
          });
        }
        $image_normal = $image_normal->stream();

        Storage::disk('upcloud')->put('images/orbitrade/user/' . $image_name, $image_normal->__toString());
        $update = DB::table('users_cabinet')->where('uuid', $uuid)->update([
          'photoKTP' => $image_name
        ]);

        return response()->json([
          'success' => true,
          'response' => $image_name,
        ]);
      } else {

        return response()->json([
          'success' => false,
          'response' => 'File Not Found Or Use Key as gambar',
        ]);
      }


    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function uploadBukuTabungan(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid = $user['uuid'];
      if ($request->file('gambar')) {
        $image = $request->file('gambar');
        $time = time();
        /*image name*/
        $extention = $image->extension();
        $image_name = $uuid . $time . "." . $extention;
        $thumb_name = "thumb-" . $uuid . $time . "." . $extention;
        /*image name*/
        /*THUMB harus diatas supaya ga ke rotate*/
        $image_thumb = Image::make($image)->fit(200, 200);
        $image_thumb = $image_thumb->stream();
        Storage::disk('upcloud')->put('images/orbitrade/user/' . $thumb_name, $image_thumb->__toString());
        /*THUMB harus diatas supaya ga ke rotate*/
        $dimension = getimagesize($image);
        $width = $dimension[0];
        $height = $dimension[1];
        if ($height > $width) {
          // create Image from file
          $image_normal = Image::make($image)->heighten(600, function ($constraint) {
            $constraint->upsize();
          });
        } else {
          $image_normal = Image::make($image)->widen(600, function ($constraint) {
            $constraint->upsize();
          });
        }
        $image_normal = $image_normal->stream();

        Storage::disk('upcloud')->put('images/orbitrade/user/' . $image_name, $image_normal->__toString());
        $update = DB::table('users_cabinet')->where('uuid', $uuid)->update([
          'photoTabungan' => $image_name
        ]);
        return response()->json([
          'success' => true,
          'response' => $image_name,
        ]);
      } else {

        return response()->json([
          'success' => false,
          'response' => 'File Not Found Or Use Key as gambar',
        ]);
      }


    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }
  public function uploadAgreement(Request $request)
  {
      $token = $request->header('Authorization');
  
      $user = JWTAuth::authenticate($token);
      if ($user) {
          $uuid = $user['uuid'];
          if ($request->file('agreement_pdf')) {
              $pdf = $request->file('agreement_pdf');
              $time = time();
              /* PDF name */
              $extension = $pdf->extension();
              $pdf_name = $uuid . $time . "." . $extension;
              /* PDF name */
  
              Storage::disk('upcloud')->put('/images/user/agreement/' . $pdf_name, file_get_contents($pdf));

  
              $update = DB::table('users_ib')->where('uuid', $uuid)->update([
                  'agreement' => $pdf_name
              ]);
  
              return response()->json([
                  'success' => true,
                  'response' => $pdf_name,
              ]);
          } else {
              return response()->json([
                  'success' => false,
                  'response' => 'File Not Found Or Use Key as agreement_pdf',
              ]);
          }
      } else {
          return response()->json([
              'success' => false,
              'message' => 'Invalid Token Or Error'
          ], Response::HTTP_INTERNAL_SERVER_ERROR);
      }
  }
  

}
