<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tarikh\PhpMeta\Entities\Trade;
use Tarikhagustia\LaravelMt5\LaravelMt5;
use DB;

class DepoWdController extends Controller
{
  public function deposit(Request $request){
    if ($request->secret == '$2y$10$K5skD5q/dtAZjS7ZfetKHOAhuNJ7AhvUzYV2uejBLXt6rrJ0KA1Fm') {
      try {
        $api = new LaravelMt5();
        $trade = new Trade();
        $trade->setLogin($request->login);
        $trade->setAmount($request->amount);
        $trade->setComment("Deposit by $request->parentName");
        $trade->setType(Trade::DEAL_BALANCE);
        $result = $api->trade($trade);

        return response()->json([
          'status' => true,
          'ticket' => $result->getTicket()
        ]);

      } catch (\Exception $e) {
        return response()->json([
          'status' => false,
          'error' => $e
        ]);
      }
    }else{
      return response()->json([
        'status' => false,
        'data' => 'error auth'
      ]);
    }
  }

  public function withdrawal(Request $request){
    if ($request->secret == '$2y$10$K5skD5q/dtAZjS7ZfetKHOAhuNJ7AhvUzYV2uejBLXt6rrJ0KA1Fm') {
      try {
        $api = new LaravelMt5();
        $trade = new Trade();
        $trade->setLogin($request->login);
        $trade->setAmount($request->amount);
        $trade->setComment("Withdrawal by $request->parentName");
        $trade->setType(Trade::DEAL_BALANCE);
        $result = $api->trade($trade);

        return response()->json([
          'status' => true,
          'ticket' => $result->getTicket()
        ]);
      } catch (\Exception $e) {
        return response()->json([
          'status' => false,
          'error' => $e
        ]);
      }
    }else{
      return response()->json([
        'status' => false,
        'data' => 'error auth'
      ]);
    }
  }

  public function directDeposit(Request $request){
    // return $request;

    /** Calculate maximal deposit*/
    // 10000*0.7*(profile()->creditGiven - profile()->lastKnownPL + profile()->lastKnownWithdrawal - profile()->lastKnownDeposit)}}
    $kredit = auth()->user()->creditGiven - auth()->user()->lastKnownPL + auth()->user()->lastKnownWithdrawal - auth()->user()->lastKnownDeposit;
    $maksDepo = 1000 * 0.7 * $kredit;

    $rules = [
        'bank_name' => 'required',
        'account_number' => 'required',
        'account_name' => 'required',
        'accountmt5' => 'required|exists:typeMT4AvailabeAccount,mt4_id',
        'amount' => "required|numeric|min:10000|max:$maksDepo",
        'transferTo' => 'required',
        'uuid' => 'required|exists:users_cabinet,uuid|min:5'
    ];

    $validator = \Validator::make($request->all(), $rules);

    if($validator->fails()){
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ], 422);
    }

    /**Check if uuid is belongs to parent */
    $clientAcc = DB::table('users_cabinet')->where('parent', auth()->user()->id)->where('uuid', $request->uuid)->first();
    if(!$clientAcc){
      return response()->json([
        'success' => false,
        'message' => 'akun client tidak terhubung dengan akun anda'
      ], 400);
    }
    /** */

    /** Check if mt5account is belongs to uuid*/
    $mt4AccCheck = DB::table('typeMT4AvailabeAccount')->where('uuid', $request->uuid)->where('mt4_id',$request->accountmt5)->first();
    if(!$mt4AccCheck){
      return response()->json([
        'success' => false,
        'message' => 'akun metatrader tidak terhubung dengan akun client'
      ], 400);
    }
    /** */

    $amount = preg_replace("/[^0-9]/", "", $request->amount);
    $approved_amount = $amount/10000;

    $response = \Http::post(env('BRIDGE_END_POINT').'/go/deposit',[
      'secret' => env('BRIDGE_KEY'),
      'login' => $request->accountmt5,
      'amount' => $approved_amount,
      'parentName' => auth()->user()->username
    ]);
    $results = $response->json();
    // return $results;
    if ($results['status']) {
      // $bankDetail = detilBank($request->transferTo);
      $bankDetail = DB::table('bank_segre')->where('namaBank', $request->transferTo)->first();
      if ($request->photo) {
        $photo = $request->photo;
      }else{
        $photo = '/images/upload-default.jpg';
      }

      $insert = DB::table('deposits')->insert([
        'uuid' => $request->uuid,
        'amount' => $amount,
        'tipe_deposit' => $request->tipeDeposit,
        'currency' => 'IDR',
        'approved_amount' => $approved_amount,
        'currency_rate' => 10000,
        'photo' => $photo,
        'status' => 'approved',
        'process_by' => $results['ticket'],
        'reason' => "Deposit by ".auth()->user()->name,
        'from_bank' => $request->bank_name,
        'from_name' => $request->account_name,
        'from_rekening' => $request->account_number,
        'to_bank' => $bankDetail->namaBank,
        'to_name' => $bankDetail->namaRekening,
        'to_rekening' => $bankDetail->nomorRekening,
        'metatrader' => $request->accountmt5,
        'parent' => auth()->user()->id
      ]);

      /** Change deposit status on mt4 table*/
      DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->accountmt5)->update([
        'deposit' => 1,
        'updated_at' => now()
      ]);

      $checkTipe = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->accountmt5)->first();
      // return $checkTipe;
      $type = $checkTipe->tipe_deposit;
      if ($type == 0) {
        $update = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->accountmt5)->update([
          'tipe_deposit' => $request->tipeDeposit
        ]);
      }
    }
    DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->accountmt5)->update([
      'deposit' => 1,
      'updated_at' => now()
    ]);
    return response()->json([
      'success' => true,
      'message' => 'berhasil melakukan deposit sejumlah '. $request->amount
    ]);

  }

  public function directWithdrawal(Request $request){

    $rules = [
      'accountmt5' => 'required|exists:typeMT4AvailabeAccount,mt4_id',
      'uuid' => 'required|exists:users_cabinet,uuid|min:5',
      'amount' => 'required|numeric|min:1'
    ];
    $credentials = \Validator::make($request->all(), $rules);
    if($credentials->fails()){
      return response()->json([
        'status' => false,
        'errors' => $credentials->errors()
      ], 422);
    }

    /**Check if uuid is belongs to parent */
    $clientAcc = DB::table('users_cabinet')->where('parent', auth()->user()->id)->where('uuid', $request->uuid)->first();
    if(!$clientAcc){
      return response()->json([
        'success' => false,
        'message' => 'akun client tidak terhubung dengan akun anda'
      ], 400);
    }
    /** */

    /** Check if mt5account is belongs to uuid*/
    $mt4AccCheck = DB::table('typeMT4AvailabeAccount')->where('uuid', $request->uuid)->where('mt4_id',$request->accountmt5)->first();
    if(!$mt4AccCheck){
      return response()->json([
        'success' => false,
        'message' => 'akun metatrader tidak terhubung dengan akun client'
      ], 400);
    }
    /** */


    $response = \Http::post(env('BRIDGE_END_POINT').'/go/withdrawal',[
      'secret' => env('BRIDGE_KEY'),
      'login' => $request->accountmt5,
      'amount' => $request->amount * -1,
      'parentName' => auth()->user()->username
    ]);
    $results = $response->json();

    if ($results['status']) {
      $metatrader = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->accountmt5)->first();
      $bank = DB::table('banks')->where('uuid',$request->uuid)->first();

      $insert = DB::table('withdrawals')->insert([

        'uuid' => $request->uuid,
        'metatrader' => $request->accountmt5,
        'amount' => $request->amount,
        'tipe_deposit' => $metatrader->tipe_deposit,
        'currency' => '',
        'currency_rate' => 1,
        'approved_amount' => 0,
        'status' => 'pending',
        'bank_name' => $bank->bank_name,
        'account_name' => $bank->account_name,
        'account_number' => $bank->account_number

      ]);

      $res = array(
        "amount" => 'US$ '.$request->amount,
        "account_metatrader" => $request->accountmt5,
        'bank_name' => $bank->bank_name,
        'account_name' => $bank->account_name,
        'account_number' => $bank->account_number
      );

      return response()->json([
        'success' => true,
        'message' => 'berhasil melakukan withdraw',
        'data' => $res
      ]);
        // Alert::success( 'Success','Withdrawal! - US$ '.$request->amount.' dari account '.$request->accountmt5)->showConfirmButton('OK', '#DB1430');
    }else{
      return response()->json([
        'success' => false,
        'message' => 'possibly not enough money'
      ], 422);
        // Alert::success( 'GAGAL','possibly not enough money')->showConfirmButton('OK', '#DB1430');
    }
  }

  public function withdrawConfirmation(Request $request){
    /** Validation*/
    $rules = [
      'status' => 'required|in:approved,rejected',
      'withdraw_id' => 'required|exists:withdrawals,id'
    ];
    $credentials = \Validator::make($request->all(), $rules);
    if($credentials->fails()){
      return response()->json([
        'status' => false,
        'errors' => $credentials->errors()
      ], 422);
    }
    /** End validation*/

    /** Check if withdraw_id is belongs to user_id*/
    $withdraw = DB::table('withdrawals')->where('id', $request->withdraw_id)->where('parent', auth()->user()->id)->first();
    /** Withdraws does not belong to user that login*/
    if(!$withdraw){
      return response()->json([
        'success' => false,
        'message' => 'withdraws data does not belong to users'
      ], 400);
    }

    /** Check if status already approved*/
    if($withdraw->status == 'approved'){
      return response()->json([
        'success' => false,
        'message' => 'gagal merubah data'
      ]);
    }

    /**Change status */
    if($request->status == 'approved'){
      /** Potong data amount di metatrader account*/
      // $response = \Http::post(env('BRIDGE_END_POINT').'/go/withdrawal',[
      //   'secret' => env('BRIDGE_KEY'),
      //   'login' => $withdraw->metatrader,
      //   'amount' => $withdraw->amount * -1,
      //   'parentName' => auth()->user()->username
      // ]);
      //
      // $results = $response->json();

      // if ($results['status']) {

        /** Update status data*/
        DB::table('withdrawals')->where('id', $request->withdraw_id)->update([
          'status' => 'approved',
          'approved_amount' => $withdraw->amount * -1,
          'updated_at' => now()
        ]);

        return response()->json([
          'success' => true,
          'message' => 'WITHDRAW SELESAI',
          'data' => $withdraw
        ]);
          // Alert::success( 'Success','Withdrawal! - US$ '.$request->amount.' dari account '.$request->accountmt5)->showConfirmButton('OK', '#DB1430');
      // }else{
      //   return response()->json([
      //     'success' => false,
      //     'message' => 'possibly not enough money'
      //   ]);
      //     // Alert::success( 'GAGAL','possibly not enough money')->showConfirmButton('OK', '#DB1430');
      // }
    }
    elseif($request->status == 'rejected'){

      DB::table('withdrawals')->where('id', $request->withdraw_id)->update([
        'status' => 'rejected',
        'updated_at' => now()
      ]);

      return response()->json([
        'success' => true,
        'message' => 'berhasil menolak withdraw'
      ]);

    }

  }

  public function depositConfirmation(Request $request){
    /** Validation*/
    $rules = [
      'status' => 'required|in:approved,rejected',
      'deposit_id' => 'required|exists:deposits,id'
    ];
    $credentials = \Validator::make($request->all(), $rules);
    if($credentials->fails()){
      return response()->json([
        'status' => false,
        'errors' => $credentials->errors()
      ], 422);
    }
    /** End validation*/

    /** Check if deposit does exist*/
    $deposit = DB::table('deposits')->where('id', $request->deposit_id)->first();
    if(!$deposit){
      return response()->json([
        'success' => false,
        'errors' => array(
          'message' => ['deposit tidak ditemukan']
        )
      ], 422);
    }

    /** Check if deposit does not belong to parent that login*/
    if($deposit->parent != auth()->user()->id){
      return response()->json([
        'success' => false,
        'errors' => array(
          'message' => ['deposit tidak ditemukan']
        )
      ], 422);
    }
    /** */

    /** Check if deposit status is not pending*/
    if($deposit->status != 'pending'){
      return response()->json([
        'success' => false,
        'errors' => array(
          'message' => ['tidak dapat merubah data']
        )
      ], 422);
    }
    /***/

    $kredit = auth()->user()->creditGiven - auth()->user()->lastKnownPL + auth()->user()->lastKnownWithdrawal - auth()->user()->lastKnownDeposit;
    $maksDepo = 1000 * 0.7 * $kredit;
    $maksDepo = 2000000000;
    /** Check if amount depo gt maksDepo*/
    if($deposit->amount > $maksDepo){
      return response()->json([
        'success' => false,
        'errors' => array(
          'message' => ['Amount deposit melebihi batas maksimal deposit']
        )
      ], 422);
    }
    /** End check*/

    /** Status rejected*/
    if($request->status == 'rejected'){

        DB::table('deposits')->where('id', $request->deposit_id)->update([
          'status' => 'rejected',
          'updated_at' => now()
        ]);

        return response()->json([
          'status' => true,
          'message' => 'berhasil menolak deposit'
        ]);

    }
    /** */

    /** If status == approved*/
    $response = \Http::post(env('BRIDGE_END_POINT').'/go/deposit',[
      'secret' => env('BRIDGE_KEY'),
      'login' => $deposit->metatrader,
      'amount' => $deposit->approved_amount,
      'parentName' => auth()->user()->username
    ]);
    $results = $response->json();

    if ($results['status']) {

      DB::table('deposits')->where('id', $deposit->id)->update([
        'status' => 'approved',
        'updated_at' => now()
      ]);

      $checkTipe = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$deposit->metatrader)->first();
      $type = $checkTipe->tipe_deposit;
      if ($type == 0) {
        $update = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$deposit->metatrader)->update([
          'tipe_deposit' => $request->tipeDeposit
        ]);
      }
      DB::table('typeMT4AvailabeAccount')->where('mt4_id',$deposit->metatrader)->update([
        'deposit' => 1,
        'updated_at' => now()
      ]);
    }
    DB::table('typeMT4AvailabeAccount')->where('mt4_id',$deposit->metatrader)->update([
      'deposit' => 1,
      'updated_at' => now()
    ]);
    return response()->json([
      'success' => true,
      'message' => 'berhasil melakukan deposit sejumlah '. $deposit->amount
    ]);
  }

  public function getDeposit(Request $request){ 
  $webid = $request->input('webid'); // Extract the webid from the incoming request

    if($request->has('status')){
      $data = DB::table('deposits')
      ->where('parent', auth()->user()->id)
      ->where('status', $request->status)
      ->where('webid', $webid)
      ->orderByDesc('deposits.id')
      ->paginate(10);
    }
    else{
      $data = DB::table('deposits')
      ->where('parent', auth()->user()->id)
      ->where('webid', $webid)
      ->orderByDesc('deposits.id')
      ->paginate(10);  
    }
    return response()->json([
      'success' => true,
      'data' => $data,
    ]);

  }

  public function getDepositDetail($id){

    $data = DB::table('deposits')
            ->where('deposits.parent', auth()->user()->id)
            ->where('deposits.id', $id)
      ->orderByDesc('deposits.id')
            ->join('users_cabinet', 'users_cabinet.uuid', 'deposits.uuid')
            ->select('deposits.*', 'users_cabinet.name as deposit_for')
            ->first();
    $bank = DB::table('banks')->where('uuid', auth()->user()->uuid)->first();
    $data->deposit_by = auth()->user()->name;
    $data->bank_partner = $bank;
    return response()->json([
      'success' => true,
      'data' => $data
    
    ]);

  }

  public function getWithdraws(Request $request){
  $webid = $request->input('webid'); // Extract the webid from the incoming request
  // return auth()->user();
    if($request->has('status')){
      $data = DB::table('withdrawals')
      ->where('parent', auth()->user()->id)
      ->where('webid', $webid)
      ->where('status', $request->status)
      ->orderByDesc('withdrawals.id')
      ->paginate(10);
  
    }
    else{
      $data = DB::table('withdrawals')
      ->where('parent', auth()->user()->id)
      ->where('webid', $webid)
      ->orderByDesc('withdrawals.id')
      ->paginate(10);
  
    }
    return response()->json([
      'success' => true,
      'data' => $data
    ]);

  }

  public function getWithdrawDetail($id){

    $data = DB::table('withdrawals')
            ->where('withdrawals.parent', auth()->user()->id)
            ->where('withdrawals.id', $id)
            ->where('webid', env('WEBID'))
            ->orderByDesc('withdrawals.id')
            ->join('users_cabinet', 'users_cabinet.uuid', 'withdrawals.uuid')
            ->select('withdrawals.*', 'users_cabinet.name as deposit_for')
            ->first();
    $bank = DB::table('banks')->where('uuid', auth()->user()->uuid)->first();
    $data->deposit_by = auth()->user()->name;
    $data->bank_partner = $bank;
    return response()->json([
      'success' => true,
      'data' => $data
    ]);

  }

  public function warrantList($uuid){
    $usersIb = DB::table('users_ib')->where('uuid', $uuid)->first();
    $capital = DB::table('statement_balance_sheet')->where('parent', $usersIb->id)->where('tipe_transaksi','LIKE','%Warrant Capital%')->sum('amount_partner');
    $outstanding = DB::table('statement_balance_sheet')->where('parent', $usersIb->id)->where('tipe_mutasi','LIKE','%Company%')->where('status_payment','0')->sum('amount_partner');
    $warrant = $capital + $outstanding;
    $warrantList = DB::table('partner_deposits')->where('uuid', $uuid)->orderby('created_at','desc')->paginate(6);
    //JOHAN TEST WARRANT TOTAL AND WALLET DEPO
    $warrantTotal = DB::table('partner_deposits')->where('uuid', $uuid)->where('status','approved')->sum('approved_amount');
    $depositTotal = DB::table('wallet_deposit_request')
    ->selectRaw('SUM((approved_amount) / 10000) as total')
    ->where('parent', $usersIb->id)
    ->where('status', 'approved')
    ->value('total');

    $creditAvail = DB::table('wallets')->where('uuid',$uuid)->first();
    $credit = $creditAvail->amount;

    $warrantAvailable = $warrantTotal - $depositTotal;
    $walletDeposit = DB::table('wallet_deposit_request')->where('parent', $usersIb->id)->where('status','approved')->orderByDesc('created_at')->paginate(6);
    $creditGiven = $usersIb->creditGiven; //dear coach, ni baru initial credit doang, harus tambah kalo dia ada top up warrant lagi

    $walletBooks = DB::table('wallet_book')->where('wallet',$uuid)->orderby('id','desc')->paginate(20);
    
    return response()->json([
      'success'=>true,
      'creditGiven'=>$creditGiven, //untuk ambil total credit misal 70:30 dari 1000 ini 1000nya
      'warrantList'=>$warrantList,
      'totalWarrant'=>$warrant,
      'capital'=>$capital,
      'outstanding'=>$outstanding,
      'total_warrant'=>$warrantTotal, //johan coba buat untuk total warrant yg sudah di approve
      'depositTotal'=>$depositTotal,
      'walletDeposit'=>$walletDeposit,
      'warrant_available'=>$warrantAvailable,
      'credit_available' => $credit,
      'walletBooks' => $walletBooks,
     //ini total capital sama outstandingnya 0, sepertinya karena statement balance sheet kmrn di ubah dari warrant capital jadi initial balance, jadi ga ada yang ketarik
    ]);
  }

  public function getAccountAmount($uuid){
      $usersIb = DB::table('users_ib')->where('uuid', $uuid)->first();
      $capital = DB::table('statement_balance_sheet')->where('parent', $usersIb->id)->where('tipe_transaksi','LIKE','%Warrant Capital%')->sum('amount_partner');
      $outstanding = DB::table('statement_balance_sheet')->where('parent', $usersIb->id)->where('tipe_mutasi','LIKE','%Company%')->where('status_payment','0')->sum('amount_partner');
      $warrant = $capital + $outstanding;
       $pendingReceivable = DB::table('partner_deposits')->select('amount')->where('uuid', $uuid)->where('status', 'pending')->sum('amount');
       $pendingPayable = DB::table('partner_deposits')->select('amount')->where('parent', auth()->user()->parent)->where('uuid', $uuid)->where('status', 'pending')->sum('amount');

       $data = (object) ['payable' => $pendingPayable, 'receivable' => $pendingReceivable, 'totalWarrant'=>$warrant];

       return response()->json([
           'success' => true,
           'data' => $data
       ]);
  }

  public function detailPendingReceivable($uuid){
    $pendingReceivable = DB::table('partner_deposits')->where('uuid', $uuid)->where('status', 'pending')->paginate(10);

    return response()->json([
      'pendingReceivable'=>$pendingReceivable
    ]);
  }

  public function detailPendingPayable($uuid){
    $pendingPayable = DB::table('partner_deposits')->where('parent', auth()->user()->parent)->where('uuid', $uuid)->where('status', 'pending')->paginate(10);

    return response()->json([
      'pendingPayable'=>$pendingPayable
    ]);
  }


}
