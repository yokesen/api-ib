<?php

namespace App\Http\Controllers;
use JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use Storage;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class WithdrawalController extends Controller
{

  public function detail_deposit(Request $request , $id=null)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid =   $user['uuid'];
      if (isset($id) && !empty($id)) {
        $deposit = DB::table('deposits')->where('id',$id)->first();
        return response()->json([
          'success' => true,
          'Data' => $deposit
        ]);
      } else {
        return response()->json([
          'success' => false,
          'Data' => 'Please Provide ID'
        ]);
      }

    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }


  public function detail_internal_transfer(Request $request , $id=null)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid =   $user['uuid'];
      if (isset($id) && !empty($id)) {
        $internal = DB::table('withdrawals')->where('id',$id)->first();
        return response()->json([
          'success' => true,
          'Data' => $internal
        ]);
      } else {
        return response()->json([
          'success' => false,
          'Data' => 'Please Provide ID'
        ]);
      }

    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }


  public function detail_withdrawal(Request $request , $id=null)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid =   $user['uuid'];
      if (isset($id) && !empty($id)) {
        $withdrawal = DB::table('withdrawals')->where('id',$id)->first();
        return response()->json([
          'success' => true,
          'Data' => $withdrawal
        ]);
      } else {
        return response()->json([
          'success' => false,
          'Data' => 'Please Provide ID'
        ]);
      }

    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function my_internal_transfer(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid =   $user['uuid'];
      $internals = DB::table('withdrawals')->where('uuid',$uuid)->where('internal','1')->orderby('id','desc')->get();
      return response()->json([
        'success' => true,
        'Data' => $internals
      ]);

    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function my_withdrawal(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid =   $user['uuid'];
      $withdrawals = DB::table('withdrawals')->where('uuid',$uuid)->whereNull('internal')->orderby('id','desc')->get();
      return response()->json([
        'success' => true,
        'Data' => $withdrawals
      ]);
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function request_internal_transfer(Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid =   $user['uuid'];

         $rules = [
            'fromAccount' => 'required',
            'amount' => 'required|min:1',
            'toAccount' => 'required'
      ];

      $credentials = Validator::make($request->all(), $rules);

      if($credentials->fails()){
        return response()->json([
          'status' => false,
          'errors' => [
            $credentials->errors()
          ]
        ]);
      }

      $insert = DB::table('withdrawals')->insert([
        'uuid' => $uuid,
        'metatrader' => $request->fromAccount,
        'amount' => $request->amount,
        'tipe_deposit' => 10,
        'currency' => 'US$',
        'currency_rate' => 1,
        'approved_amount' => $request->amount,
        'status' => 'pending',
        'bank_name' => 'internal transfer',
        'account_name' => 'internal transfer',
        'account_number' => 'internal transfer',
        'internalToAccountMeta' => $request->toAccount,
        'internal' => '1'
      ]);
      if ($insert) {
        return response()->json([
          'success' => true,
          'response' => 'Internal Transfer requested please wait and check the status regularly'
        ]);
      } else {
        return response()->json([
          'success' => false,
          'response' => 'Error In Storing Database'
        ]);
      }
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function request_withdrawal (Request $request)
  {
    $token = $request->header('Authorization');

    $user = JWTAuth::authenticate($token);
    if ($user) {
      $uuid =   $user['uuid'];


      $rules = [
        'metatrader' => 'required',
        'amount' => 'required',
        'bank_name' => 'required',
        'account_name' => 'required',
        'account_number' => 'required',
      ];

      $credentials = Validator::make($request->all(), $rules);

      if($credentials->fails()){
        return response()->json([
          'status' => false,
          'errors' => [
            $credentials->errors()
          ]
        ]);
      }

      $metatrader = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$request->accountmt4)->first();
      $bank = DB::table('banks')->where('uuid',$uuid)->first();
      $insert = DB::table('withdrawals')->insert([

        'uuid' => $uuid,
        'metatrader' => $request->metatrader,
        'amount' => $request->amount,
        'tipe_deposit' => $metatrader->tipe_deposit,
        'currency' => '',
        'currency_rate' => 1,
        'approved_amount' => 0,
        'status' => 'pending',
        'bank_name' => $bank ->bank_name,
        'account_name' => $bank ->account_name,
        'account_number' => $bank->account_number

      ]);
      if ($insert) {
        return response()->json([
          'success' => true,
          'response' => 'Withdrawal requested please wait and check the status regularly'
        ]);
      } else {
        return response()->json([
          'success' => false,
          'response' => 'Error In Storing Database'
        ]);
      }
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Token Or Error'
      ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

}
