<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Jenssegers\Agent\Agent;
use Cookie;
use Session;
use Tarikhagustia\LaravelMt5\LaravelMt5;
use Tarikhagustia\LaravelMt5\Entities\User;
use Tarikhagustia\LaravelMt5\Entities\Trade;
use Tarikh\PhpMeta\MetaTraderClient;
use Illuminate\Support\Facades\Hash;

class LeadController extends Controller
{
  public function viewLead(Request $request){
    $menu = 'viewLead';
    $leads = DB::table('users_cabinet')->where('parent', auth()->user()->id)->where('status', 'lead')->count();

    /*-------------------*/
    if($request->has('status')){
      
      $users = DB::table('users_cabinet')
                // ->leftJoin('deposits', 'deposits.uuid', 'users_cabinet.uuid')
                ->where('users_cabinet.status', $request->status)
                ->orderby('id','desc')
                ->where('users_cabinet.parent',auth()->user()->id)
                // ->where('deposits.status', 'approved')
                ->select('users_cabinet.*')
                // ->get();
                ->paginate(20);
      foreach ($users->items() as $key => $value) {
        // code...
        $mt5check = DB::table('typeMT4AvailabeAccount')->where('status', 'approved')->where('uuid', $value->uuid)->count();
        $users->items()[$key]->mt5 = $mt5check != 0 ? true : false;

        $depositCheck = DB::table('deposits')->where('uuid', $value->uuid)->where('status', 'approved')->count();
        $users->items()[$key]->deposit = $depositCheck != 0 ? true : false;

      }
    }
    else{
      $users = DB::table('users_cabinet')
          // ->leftJoin('deposits', 'deposits.uuid', 'users_cabinet.uuid')
          ->orderby('users_cabinet.id','desc')
          ->where('users_cabinet.parent',auth()->user()->id)
          ->select('users_cabinet.*')
          // ->leftJoin('typeMT4AvailabeAccount', 'typeMT4AvailabeAccount.uuid', 'users_cabinet.uuid')
          ->paginate(20);
      foreach ($users->items() as $key => $value) {
        // code...
        $mt5check = DB::table('typeMT4AvailabeAccount')->where('status', 'approved')->where('uuid', $value->uuid)->count();
        $users->items()[$key]->mt5 = $mt5check != 0 ? true : false;

        $depositCheck = DB::table('deposits')->where('uuid', $value->uuid)->where('status', 'approved')->count();
        $users->items()[$key]->deposit = $depositCheck != 0 ? true : false;

      }
    }
 
    return response()->json([
      'success' => true,
      'menu' => $menu,
      'users' => $users,
      'lead'=> $leads
    ]);
  }

  public function processAddLead(Request $request){
    $rules = [
        'name' => 'required|min:2|max:50',
        'email' => 'required|email|unique:users_cabinet',
        'whatsapp' => 'required|min:10',
        'password' => 'required|min:5'
    ];

    $validator = Validator::make($request->all(), $rules);

    if($validator->fails()){
      return response()->json([
        'status' => false,
        'errors' => [
          $validator->errors()
        ]
      ]);
    }

    $agent = new Agent();
    if($agent->isPhone()){
      $screen = "Phone";
    }elseif($agent->isTablet()){
      $screen = "Tablet";
    }elseif($agent->isDesktop()){
      $screen = "Desktop";
    }else{
      $screen = null;
    }
    $platform = $agent->platform();
    $version_platform = $agent->version($platform);
    $device = $agent->device();
    $browser = $agent->browser();
    $version_browser = $agent->version($browser);
    $languages = serialize($agent->languages());
    $robot = $agent->robot();
    $ip = request()->ip();
    $uuid = sha1($request->email).time();

    $whatsapp = preg_replace("/[^0-9]/", "", $request->phone );
    /*CREATE USERNAME*/
    $username3 = explode('@',$request->email)[0];
    $check3 = DB::table('users_cabinet')->where('username',$username3)->count();
    if($check3 == 0){
      $username = $username3;
    }else{
      $username2 = $username3.date('md');
      $check2 = DB::table('users_cabinet')->where('username',$username2)->count();
      if($check2 == 0){
        $username = $username2;
      }else{
        $username = '';
      }
    }
    /*CREATE USERNAME*/

    /** if request has photo then do upload photo*/
  $path = NULL;
  if($request->file('photo')){

    $file = $request->file('photo');
    $uniqueFileName = str_replace(" ","",time()."-".auth()->user()->id."-".$file->getClientOriginalName());
    Storage::disk('upcloudinvestorich')->put('images/user/photo-profile/'.$uniqueFileName.".png", $file);
    // $path = $file->storeAs('public/photos', $uniqueFileName);
    $path = "images/user/photo-profile/".$uniqueFileName;

    }
    else if($request->photo){

        $base64_image = $request->image;
        $base64_image = "data:image/png;base64,".$base64_image;

        $data = substr($base64_image, strpos($base64_image, ',') + 1);

        $file = base64_decode($data);
        $uniqueFileName = "photos-ava".auth()->user()->id.time();

        \Storage::disk('upcloudinvestorich')->put('images/user/photo-profile/'.$uniqueFileName.".png", $file);

        $path = "images/user/photo-profile/".$uniqueFileName.".png";
    }
    else{
      $path = auth()->user()->photo;
    }
    /** end upload photo*/

    $insert = DB::table('users_cabinet')->insertGetId([
      'name' => $request->name,
      'photo' => $path,
      'username' => $username,
      'email' => $request->email,
      'email_validation' => 'new',
      'email_verification' => 'new',
      'password' => Hash::make($request->password),
      'phone' => $request->whatsapp,
      'whatsapp' => $request->whatsapp,
      'providerId' => $uuid,
      'providerOrigin' => 'add manual lead',
      'id_cms_privileges' => '7',
      'parent' => auth()->user()->id,
      'uuid' => $uuid,
      'ipaddress' => $ip,
      'screen' => $screen,
      'platform' => $platform,
      'platformVersion' => $version_platform,
      'device' => $device,
      'browser' => $browser,
      'browserVersion' => $version_browser,
      'language' => $languages,
      'robot' => $robot,
      'utm_campaign' => $request->utm_campaign,
      'utm_source' => $request->utm_source,
      'utm_medium' => $request->utm_medium,
      'utm_term' => $request->utm_term,
      'first_landing' => $request->first_landing,
      'status' => 'lead'
    ]);

    $users = DB::table('users_cabinet')->where('id', $insert)->first();
    return response()->json([
      'success' => true,
      'message' => 'berhasil membuat lead',
      'data' => $users
    ]);
  }

  public function getLeadDetail($id){
    $user = DB::table('users_cabinet')->where('uuid',$id)->first();

    if ($user->forgot_password_token == null) {
      $randomString = \Str::random(500);
      /** Generate reset password token*/
      DB::table('users_cabinet')->where('uuid',$id)->update([
        'forgot_password_token' => $randomString
      ]);
    }
    $user = DB::table('users_cabinet')->where('uuid',$id)->first();


    $role = DB::table('cms_privileges')->where('id',$user->id_cms_privileges)->first();
    $roles = DB::table('cms_privileges')->where('id','>','6')->get();
    $parent = DB::table('users_ib')->where('id',$user->parent)->select('username','uuid')->first();
    $deposits = DB::table('deposits')->where('uuid',$id)->orderby('id','desc')->paginate(7);
    $bank = DB::table('banks')->where('uuid',$id)->orderby('id','desc')->first();
    $withdrawals = DB::table('withdrawals')->where('uuid',$id)->orderby('id','desc')->paginate(7);
    $metatrader4 = DB::table('typeMT4AvailabeAccount')->where('uuid',$id)->orderby('id','desc')->paginate(7);
    $mt4 = DB::table('typeMT4AvailabeAccount')->where('uuid',$id)->where('status','approved')->orderby('id','desc')->get();
    $referral = DB::table('users_cabinet')->where('parent',$user->id)->orderby('id','desc')->paginate(7);
    $listAllow = DB::table('bank_lists')->get();
    $listCategories = DB::table('typeMT4Category')->where('status','active')->orderby('id','asc')->get();
    $listAccounts = DB::table('typeMT4Accounts')
      ->leftjoin('typeMT4Category','typeMT4Category.id','typeMT4Accounts.categoryAccount')
      ->where('typeMT4Accounts.categoryAccount',$user->parent)
      ->where('typeMT4Accounts.status','active')
      ->where('typeMT4Category.status','active')
      ->orderby('typeMT4Accounts.minimumDepo','asc')
      ->select('typeMT4Category.namaCategory','typeMT4Accounts.id','typeMT4Accounts.accountGroup','typeMT4Accounts.namaAccount','typeMT4Accounts.deskripsiAccount','typeMT4Accounts.minimumDepo','typeMT4Accounts.bestChoice')
      ->get();
    if(empty($bank)){
      $insert = DB::table('banks')->insert([
        'uuid' => $id
      ]);
      $bank = DB::table('banks')->where('uuid',$id)->orderby('id','desc')->first();
    }

    /*EQUITY*/


    //return view('ib.admin.my-client.edit-client',compact(
    return response()->json(compact(
      'user',
      'role',
      'parent',
      'deposits',
      'bank',
      'withdrawals',
      'metatrader4',
      'referral',
      'listAllow',
      'roles',
      'mt4',
      'listCategories',
      'listAccounts'
    ));

  }

  public function mt5detail($mt5,$uuid){
    $month = date('m');
    $user = DB::table('users_cabinet')->where('uuid',$uuid)->first();
    $mt5_acc = DB::table('typeMT4AvailabeAccount')->where('mt4_id',$mt5)->where('uuid',$uuid)->first();
    $api = new LaravelMt5();
    $mt5_trading = $api->getTradingAccounts($mt5);
    $mt5_history = DB::table('calculate_account_weekly')->where('accountId',$mt5)->orderby('weekoty','desc')->get();
    $mt5_deal = DB::table('raw_data_deal')->where('login',$mt5)->where('lotmonth',$month)->where('VolumeClosed','>',0)->orderby('lotweek','desc')->get();

    return response()->json([
      'success' => true,
      'message' => 'data okay',
      'lead' => $user,
      'mt5' => $mt5_acc,
      'trading' => $mt5_trading,
      'history' => $mt5_history,
      'deal' => $mt5_deal
    ]);
  }

  public function searchLeads(Request $request)
  {
      $search = $request->search;
      $leads = DB::table('users_cabinet')->where('name','LIKE','%'.$search.'%')->orWhere('email','LIKE','%'.$search.'%')->get();

      return response()->json([
        'success' => true,
        'message' => 'data ono',
        'lead' => $leads,
      ]);
  }


}
