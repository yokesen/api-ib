<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class MarginController extends Controller
{
  public function viewNetMargin(){
    $margin = DB::table('raw_data_deal')->where('parent',auth()->user()->id)->where('Action','2')->where('Comment','!=','Adjust')->orderby('Time','desc')->paginate(10);
    return response()->json([
      'success' => true,
      'data' => $margin
    ]);
  }

  public function viewMarginIn(){
    $menu = 'viewMarginIn';
    /*-------------------*/
    $margin = DB::table('raw_data_deal')->where('parent',auth()->user()->id)->where('Action','2')->where('Comment','!=','Adjust')->where('Profit','>',0)->orderby('Time','desc')->paginate(10);
    return response()->json([
      'success' => true,
      'data' => $margin
    ]);
  }

  public function viewMarginOut(){
    $menu = 'viewMarginOut';
    /*-------------------*/
    $margin = DB::table('raw_data_deal')->where('parent',auth()->user()->id)->where('Action','2')->where('Comment','!=','Adjust')->where('Profit','<',0)->orderby('Time','desc')->paginate(10);
    return response()->json([
      'success' => true,
      'data' => $margin
    ]);
  }

  public function viewInternalTransfer(){
    /*-------------------*/
    $margin = DB::table('withdrawals')
                  ->join('users_cabinet','users_cabinet.uuid','withdrawals.uuid')
                  ->where('withdrawals.parent',auth()->user()->id)
                  ->where('withdrawals.internal',1)
                  ->orderby('withdrawals.created_at','desc')
                  ->select(
                    'users_cabinet.username',
                    'withdrawals.created_at',
                    'withdrawals.metatrader',
                    'withdrawals.internalToAccountMeta',
                    'withdrawals.approved_amount',
                    'withdrawals.status',
                    'withdrawals.reason',
                    )
                  ->paginate(10);
    return response()->json([
      'success' => true,
      'data' => $margin
    ]);
  }

  public function weeklyReconsile(){
    $reconsile = DB::table('statement_reconsile')->where('parent',auth()->user()->id)->orderby('id','desc')->paginate(20);
    return response()->json([
      'success' => true,
      'data' => $reconsile
    ]);
  }

  public function weeklyReconsileDetail($id){
    $reconsile = DB::table('statement_reconsile')->orderby('id','desc')->where('id', $id)->first();
    // return $lists;
    /** Reconsile tidak ditemukan*/
    if(!$reconsile){
      return response()->json([
        'success' => false,
        'message' => 'data tidak ditemukan'
      ]);
    }
    else{
      /* Reconsile bukan a.n user login*/
      if($reconsile->parent != auth()->user()->id){
        return response()->json([
          'success' => false,
          'message' => 'tidak memiliki ijin untuk mengakses data'
        ]);
      }

      $users = DB::table('users_ib')->where('id', auth()->user()->id)->first();
      $lists = DB::table('statement_balance_sheet')
                ->where('parent', auth()->user()->id)
                ->where('dayoty', $reconsile->dayoty)
                ->where('weekoty', $reconsile->weekoty)
                ->where('year', $reconsile->year)
                ->orderByDesc('id')
                // ->where('price', '!=', 0)
                ->get();
      return response()->json([
        'success' => true,
        'data' => array(
          "reconsile" => $reconsile,
          "users" => $users,
          "reconsile_detail" => $lists
        )
      ]);
    }

  }

}
