<?php

namespace App\Http\Controllers;

use JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use Storage;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;


class ReferalController extends Controller
{
    public function my_client_list(Request $request)
   {
    $token = $request->header('Authorization');
 
        $user = JWTAuth::authenticate($token);
        if ($user) {
        $id =   $user['id'];
        $clients = DB::table('users_cabinet')->where('ref',$id)->get();
        if ($clients) {
            return response()->json([
                'success' => true,
                'Data' => $clients
            ]);
        } else {
            return response()->json([
                'success' => false,
                'Data' => 'Data not Found'
            ]);
        }
        } else {
            return response()->json([
            'success' => false,
            'message' => 'Invalid Token Or Error'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    } 
   }
   public function register_referral_program(Request $request)
   {
    $token = $request->header('Authorization');
 
        $user = JWTAuth::authenticate($token);
        if ($user) {
        $uuid =   $user['uuid'];
        $register = DB::table('users_cabinet')->where('uuid',$uuid)->update([
            'id_cms_privileges' => 12
        ]);
        
        if ($register) {
            return response()->json([
                'success' => true,
                'Data' => 'Referral register requested, please wait for approval'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'Data' => 'Check Info Token Etc'
            ]);
        }
        } else {
            return response()->json([
            'success' => false,
            'message' => 'Invalid Token Or Error'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    } 
   }

   public function this_month_volume(Request $request)
   {
    $token = $request->header('Authorization');
 
        $user = JWTAuth::authenticate($token);
        if ($user) {
        $uuid =   $user['uuid'];
        $id =   $user['id'];
        $clients = DB::table('lot_counter')->where('parent',$id)->get();
        foreach($clients as $client ){
            $volume = DB::table('lot_counter')->where('uuid',$uuid)->where('lotmonth',date('m'))->sum('lotamount');
        }

        return response()->json([
            'success' => true,
            'Data' => $volume
        ]);
      
        } else {
            return response()->json([
            'success' => false,
            'message' => 'Invalid Token Or Error'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    } 
   }

}
