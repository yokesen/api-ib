<?php

use DB;

$server = DB::table('mt5_connection')->where('nickname','fgt-pro-1')->first();

return [
    'server'    => $server->mt5_server_ip,
    'port'      => $server->mt5_server_port,
    'login'     => $server->mt5_server_web_login,
    'password'  => $server->mt5_server_web_password
];