<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKodeOtpSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kode_otp_sms', function (Blueprint $table) {
            $table->id();
            $table->string('no_hp', 20);
            $table->string('kode', 6);
            $table->integer('status')->default(0);
            $table->string('message_status', 20)->nullable();
            $table->dateTime('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kode_otp_sms');
    }
}
