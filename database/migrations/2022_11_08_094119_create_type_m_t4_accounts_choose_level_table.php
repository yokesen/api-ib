<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypeMT4AccountsChooseLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('typeMT4_accounts_choose_level', function (Blueprint $table) {
            $table->id();
            $table->integer('categoryAccount')->default(0);
            $table->string('namaAccount',100)->nullable();
            $table->text('deskripsiAccount')->nullable();
            $table->float('minimumDepo')->default(0);
            $table->integer('startAccNumber')->nullable();
            $table->integer('endAccNumber')->nullable();
            $table->string('accountGroup',255)->nullable();
            $table->integer('accountLeverage')->nullable();
            $table->float('accountMinLot')->nullable();
            $table->float('accountMinStep')->nullable();
            $table->float('accountSpread')->nullable();
            $table->float('accountCom')->nullable();
            $table->string('bestChoice',10)->default('no');
            $table->string('status',10)->default('inactive');
            $table->dateTime('approved_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('typeMT4_accounts_choose_level');
    }
}
